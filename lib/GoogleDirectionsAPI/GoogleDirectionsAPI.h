//
//  GoogleDirectionsAPI.h
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 24.03.13.
//  Copyright (c) 2013 Tomáš Kohout. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

#define GoogleDirectionsModeDriving 0
#define GoogleDirectionsModeWalking 1
#define GoogleDirectionsModeTransit 2

@interface GoogleDirectionsAPI : NSObject
+ (GoogleDirectionsAPI *)sharedService;
- (void) polylineFromStart: (NSString *) start end: (NSString *) end mode: (NSInteger) mode success: (void(^)(MKPolyline * polyline, MKCoordinateRegion region, CLLocationCoordinate2D start, CLLocationCoordinate2D end)) onSuccess failure: (void(^)(NSError *error)) onFailure;

@end
