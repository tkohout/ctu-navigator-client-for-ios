//
//  GoogleDirectionsRouteAnnotation.h
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 25.03.13.
//  Copyright (c) 2013 Tomáš Kohout. All rights reserved.
//

#import <MapKit/MapKit.h>
#define GoogleDirectionsRouteAnnotationStart 0
#define GoogleDirectionsRouteAnnotationEnd 1

@interface GoogleDirectionsRouteAnnotation : MKPointAnnotation
@property (nonatomic, assign) NSInteger type;
@end
