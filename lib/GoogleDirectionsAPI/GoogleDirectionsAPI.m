//
//  GoogleDirectionsAPI.m
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 24.03.13.
//  Copyright (c) 2013 Tomáš Kohout. All rights reserved.
//

#import "GoogleDirectionsAPI.h"
#import <RestKit/RestKit.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
#import "DataService.h"

@implementation GoogleDirectionsAPI

+ (GoogleDirectionsAPI *)sharedService{
    DEFINE_SHARED_INSTANCE_USING_BLOCK(^(){
        return [[self alloc] init];
    });
}

- (void) polylineFromStart: (NSString *) start end: (NSString *) end mode: (NSInteger) mode success: (void(^)(MKPolyline * polyline, MKCoordinateRegion region, CLLocationCoordinate2D start, CLLocationCoordinate2D end)) onSuccess failure: (void(^)(NSError *error)) onFailure{
    
    
    AFHTTPClient *_httpClient = [AFHTTPClient clientWithBaseURL:[NSURL URLWithString:@"http://maps.googleapis.com/"]];
    [_httpClient registerHTTPOperationClass: [AFJSONRequestOperation class]];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:start forKey:@"origin"];
    [parameters setObject:end forKey:@"destination"];
    [parameters setObject:@"true" forKey:@"sensor"];
    
    NSString * modeString;
    
    if (mode == GoogleDirectionsModeDriving){
        modeString = @"driving";
    }else if (mode == GoogleDirectionsModeWalking){
        modeString = @"walking";
    }else if (mode == GoogleDirectionsModeTransit){
        modeString = @"walking";
        [parameters setObject:[NSNumber numberWithDouble:[[NSDate date] timeIntervalSince1970]] forKey:@"departure_time"];
    }
    
    [parameters setObject:modeString forKey:@"mode"];
    
    NSMutableURLRequest *request = [_httpClient requestWithMethod:@"GET" path: @"maps/api/directions/json" parameters:parameters];
    request.cachePolicy = NSURLRequestReloadIgnoringLocalCacheData;
    
    
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        
        NSInteger statusCode = response.statusCode;
        if (statusCode == 200) {
            
            int count = 0;
            
            CLLocationCoordinate2D * coordinates = [self parseResponse:JSON count: &count];
            
            if (count > 0){
                MKPolyline *polyLine = [MKPolyline polylineWithCoordinates:coordinates count: count];
                MKCoordinateRegion region = [self findRegionForCoordinates:coordinates count:count];
                onSuccess(polyLine, region, coordinates[0], coordinates[count-1]);
            }else{
                onFailure([[DataService sharedService] errorWithCode:APIErrorNotFound message:NSLocalizedString(@"Not found", nil)]);
            }
            
            free(coordinates);
            
        } else {
            onFailure([[DataService sharedService] errorWithCode:APIErrorNotFound message:NSLocalizedString(@"Not found", nil)]);
        }
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        onFailure(error);
    }];
    
    [operation start];
}

- (CLLocationCoordinate2D *)parseResponse:(NSDictionary *)response count: (int *) count {
    NSArray *routes = [response objectForKey:@"routes"];
    NSDictionary *route = [routes lastObject];
    if (route) {
        NSString *overviewPolyline = [[route objectForKey: @"overview_polyline"] objectForKey:@"points"];
        NSArray * path = [self decodePolyLine:overviewPolyline];
        
        NSInteger numberOfSteps = path.count;
        
                
        CLLocationCoordinate2D *coordinates = malloc(numberOfSteps * sizeof(CLLocationCoordinate2D));

        
        for (NSInteger index = 0; index < numberOfSteps; index++) {
            CLLocation *location = [path objectAtIndex:index];
            CLLocationCoordinate2D coordinate = location.coordinate;
            coordinates[index] = coordinate;
        }
        
        *count = numberOfSteps;
        
        return coordinates;
    }
    
    return nil;
}

- (NSMutableArray *)decodePolyLine:(NSString *)encodedStr {
    NSMutableString *encoded = [[NSMutableString alloc] initWithCapacity:[encodedStr length]];
    [encoded appendString:encodedStr];
    [encoded replaceOccurrencesOfString:@"\\\\" withString:@"\\"
                                options:NSLiteralSearch
                                  range:NSMakeRange(0, [encoded length])];
    NSInteger len = [encoded length];
    NSInteger index = 0;
    NSMutableArray *array = [[NSMutableArray alloc] init];
    NSInteger lat=0;
    NSInteger lng=0;
    while (index < len) {
        NSInteger b;
        NSInteger shift = 0;
        NSInteger result = 0;
        do {
            b = [encoded characterAtIndex:index++] - 63;
            result |= (b & 0x1f) << shift;
            shift += 5;
        } while (b >= 0x20);
        NSInteger dlat = ((result & 1) ? ~(result >> 1) : (result >> 1));
        lat += dlat;
        shift = 0;
        result = 0;
        do {
            b = [encoded characterAtIndex:index++] - 63;
            result |= (b & 0x1f) << shift;
            shift += 5;
        } while (b >= 0x20);
        NSInteger dlng = ((result & 1) ? ~(result >> 1) : (result >> 1));
        lng += dlng;
        NSNumber *latitude = [[NSNumber alloc] initWithFloat:lat * 1e-5];
        NSNumber *longitude = [[NSNumber alloc] initWithFloat:lng * 1e-5];
        
        CLLocation *location = [[CLLocation alloc] initWithLatitude:[latitude floatValue] longitude:[longitude floatValue]];
        [array addObject:location];
    }
    
    return array;
}



- (MKCoordinateRegion)findRegionForCoordinates: (CLLocationCoordinate2D *) coordinates count: (int) count{

    CLLocationDegrees maxLat = -90.0f;
    CLLocationDegrees maxLon = -180.0f;
    CLLocationDegrees minLat = 90.0f;
    CLLocationDegrees minLon = 180.0f;
    
    for (int i = 0; i < count; i++) {
        CLLocationCoordinate2D coordinate = coordinates[i];
        if(coordinate.latitude > maxLat) {
            maxLat = coordinate.latitude;
        }
        if(coordinate.latitude < minLat) {
            minLat = coordinate.latitude;
        }
        if(coordinate.longitude > maxLon) {
            maxLon = coordinate.longitude;
        }
        if(coordinate.longitude < minLon) {
            minLon = coordinate.longitude;
        }
    }
    
    MKCoordinateRegion region;
    region.center.latitude     = (maxLat + minLat) / 2;
    region.center.longitude    = (maxLon + minLon) / 2;
    region.span.latitudeDelta  = maxLat - minLat;
    region.span.longitudeDelta = maxLon - minLon;
    
    return region;
}

@end
