//
//  AppDelegate.m
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 23.09.12.
//  Copyright (c) 2012 Tomáš Kohout. All rights reserved.
//

#import "AppDelegate.h"
#import <CoreData/CoreData.h>
#import <RestKit/Search.h>
#import <RestKit/RestKit.h>
#import "LocationService.h"
#import "DataService.h"
#import "StaticVars.h"

#import "LoginViewController.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions{
    
    
    [AFNetworkActivityIndicatorManager sharedManager].enabled = YES;
    
    //Initialize restkit mapping
    [DataService sharedService].delegate = self;
    [[DataService sharedService] initializeMapping];
    
    //User can logout in settings
    BOOL logged_in = [[NSUserDefaults standardUserDefaults] boolForKey:@"logged_in"];
    
    if (![DataService sharedService].user || !logged_in){
        [self showLoginViewControllerWithWarning:NO];
    }
    
    self.window.tintColor = [StaticVars sharedService].tintColor;
    
    return YES;
    
}

- (void) showLoginViewControllerWithWarning: (BOOL) showWarning{
    UITabBarController *mvc = (UITabBarController *)self.window.rootViewController;
    self.window.rootViewController = [mvc.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
    if (showWarning){
        [((LoginViewController *)self.window.rootViewController) showWarning];
    }
    if ([DataService sharedService].user){
        [[DataService sharedService] logout];
    }
}

#pragma mark DataServiceUserDelegate

-(void)userUnauthorized{
    [self showLoginViewControllerWithWarning:YES];
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}



@end
