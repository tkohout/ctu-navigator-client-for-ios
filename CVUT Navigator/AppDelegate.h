//
//  AppDelegate.h
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 23.09.12.
//  Copyright (c) 2012 Tomáš Kohout. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataService.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate,DataServiceUserDelegate>

@property (strong, nonatomic) UIWindow *window;



@end
