//
//  Course.m
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 06.12.12.
//  Copyright (c) 2012 Tomáš Kohout. All rights reserved.
//

#import "Course.h"


@implementation Course

@dynamic longName;
@dynamic web;
@dynamic timetableEntries;

- (NSString *)detail{
    return [self longName];
}

@end
