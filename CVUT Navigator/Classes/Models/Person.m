//
//  Person.m
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 06.12.12.
//  Copyright (c) 2012 Tomáš Kohout. All rights reserved.
//

#import "Person.h"



@implementation Person

@dynamic username;

@dynamic type;
@dynamic email;
@dynamic year;
@dynamic room;
@dynamic roomId;
@dynamic roomName;
@dynamic hasRoom;
@dynamic firstName;
@dynamic lastName;



- (void) awakeFromInsert
{
    [super awakeFromInsert];
    self.hasRoom = [NSNumber numberWithBool:NO];
}

- (void)setRoomId:(NSNumber *)roomId
{   [self willChangeValueForKey:@"roomId"];
    [self setHasRoom:[NSNumber numberWithBool:YES]];
    [self setPrimitiveValue:roomId forKey:@"roomId"];
    [self didChangeValueForKey:@"roomId"];
}

- (void) setTypeWithEnum:(PersonType) type{
    [self setType:[NSNumber numberWithInteger:type]];
}

- (void) setTypeWithString:(NSString *) aType{
    PersonType type;
    
    if ([aType isEqualToString:@"student"]){
        type = PersonTypeStudent;
    }else if ([aType isEqualToString:@"teacher"]){
        type = PersonTypeTeacher;
    }else{
        type = PersonTypeOther;
    }
    
    [self setTypeWithEnum:type];
}


- (NSString *)name{
    return [NSString stringWithFormat:@"%@ %@", self.firstName, self.lastName];
}




- (NSString *)detail{
   return @"";
}


@end
