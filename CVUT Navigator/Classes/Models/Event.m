//
//  Event.m
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 11.03.13.
//  Copyright (c) 2013 Tomáš Kohout. All rights reserved.
//

#import "Event.h"


@implementation Event

@dynamic eventName;
@dynamic eventDetail;



- (void) setDate:(NSDate *)date{
    [self willChangeValueForKey:@"date"];
    [self setPrimitiveValue:date forKey:@"date"];
    
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar] ;
    [gregorian setFirstWeekday:2];
    NSUInteger weekday = [gregorian ordinalityOfUnit:NSWeekdayCalendarUnit inUnit:NSWeekCalendarUnit forDate:[self date]] ;
    
    [self setDay:[NSNumber numberWithInt: weekday]];
    [self didChangeValueForKey:@"date"];
}


@end
