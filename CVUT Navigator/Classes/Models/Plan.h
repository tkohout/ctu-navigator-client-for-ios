//
//  Plan.h
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 06.02.13.
//  Copyright (c) 2013 Tomáš Kohout. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Entity.h"

@class Building, Node, Floor;

@interface Plan : Entity

@property (nonatomic, retain) NSNumber * buildingId;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) Building *building;
@property (nonatomic, retain, readonly) NSArray * entrances;
@property (nonatomic, retain) NSSet *floors;

@end

@interface Plan (CoreDataGeneratedAccessors)

- (void)addFloorsObject:(NSManagedObject *)value;
- (void)removeFloorsObject:(NSManagedObject *)value;
- (void)addFloors:(NSSet *)values;
- (void)removeFloors:(NSSet *)values;

- (Floor *)groundFloor;
- (Floor *)topFloor;
- (Floor *)bottomFloor;

@end
