//
//  Floor.h
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 19.02.13.
//  Copyright (c) 2013 Tomáš Kohout. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import <CoreLocation/CoreLocation.h>
#import "Entity.h"

@class Node, Plan;

@interface Floor : Entity

@property (nonatomic, retain) NSNumber * northEastLatitude;
@property (nonatomic, retain) NSNumber * northEastLongitude;
@property (nonatomic, retain) NSNumber * planId;
@property (nonatomic, retain) NSNumber * southWestLatitude;
@property (nonatomic, retain) NSNumber * southWestLongitude;
@property (nonatomic, retain) Plan *plan;
@property (nonatomic, retain) NSSet *nodes;

@property (nonatomic, assign, readonly) CLLocationCoordinate2D southWestConstrain;
@property (nonatomic, assign, readonly) CLLocationCoordinate2D northEastConstrain;

@property (nonatomic, retain) NSNumber * maxZoom;
@property (nonatomic, retain) NSNumber * minZoom;
@property (nonatomic, retain) NSString * tiles;
@property (nonatomic, retain) NSString * floorName;
@property (nonatomic, retain) NSNumber * floorNumber;

@end

@interface Floor (CoreDataGeneratedAccessors)

- (void)addNodesObject:(Node *)value;
- (void)removeNodesObject:(Node *)value;
- (void)addNodes:(NSSet *)values;
- (void)removeNodes:(NSSet *)values;
- (BOOL) canBeShowedOnPlan;


@end
