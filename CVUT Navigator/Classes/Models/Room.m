//
//  Room.m
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 06.12.12.
//  Copyright (c) 2012 Tomáš Kohout. All rights reserved.
//
#import "Node.h"
#import "Room.h"
#import "Building.h"
#import "TimetableEntry.h"
#import "Floor.h"
#import "Plan.h"


@implementation Room

@dynamic timetableEntries;


- (NSString *)detail{
    Building * building = [self building];
    if (self.building){
        return [NSString stringWithFormat:@"%@, %@", building.name, [self floorName]];
    }
    
    return @"";
}



@end
