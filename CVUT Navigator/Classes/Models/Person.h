//
//  Person.h
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 06.12.12.
//  Copyright (c) 2012 Tomáš Kohout. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Searchable.h"

typedef NS_ENUM(NSInteger, PersonType) {
    PersonTypeStudent,
    PersonTypeTeacher,
    PersonTypeOther
};

@class Room;

@interface Person : Searchable

@property (nonatomic, retain) NSString * username;
@property (nonatomic, retain) NSNumber * type;
@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSString * firstName;
@property (nonatomic, retain) NSString * lastName;
@property (nonatomic, retain) NSNumber * year;
@property (nonatomic, retain) NSNumber * roomId;
@property (nonatomic, retain) Room * room;

@property (nonatomic, retain) NSString * roomName;
@property (nonatomic, retain) NSNumber * hasRoom;


- (void) setTypeWithEnum:(PersonType) type;
- (void) setTypeWithString:(NSString *) aType;
@end
