//
//  Searchable.h
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 11.12.12.
//  Copyright (c) 2012 Tomáš Kohout. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Entity.h"

@interface Searchable : Entity

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * detail;
@end
