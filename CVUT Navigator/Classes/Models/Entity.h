//
//  Entity.h
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 26.04.13.
//  Copyright (c) 2013 Tomáš Kohout. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Entity : NSManagedObject

@property (nonatomic, retain) NSDate * updatedDate;
@property (nonatomic, retain) NSNumber * id;
- (BOOL)isEqualToEntity:(id)object;

@end
