//
//  WeekInfo.m
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 25.04.13.
//  Copyright (c) 2013 Tomáš Kohout. All rights reserved.
//

#import "WeekInfo.h"


@implementation WeekInfo

@dynamic week;
@dynamic parity;


- (NSNumber *)currentWeek{
    if (!self.week){
        return nil;
    }
    
    NSDate * now = [NSDate date];
    
    return [self getWeekForDate:now];
}

- (NSNumber *) getWeekForDate: (NSDate *) date{
    NSCalendar * calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar] ;
    [calendar setMinimumDaysInFirstWeek:4];
    [calendar setFirstWeekday:2];
    
    NSInteger weekNow = [[calendar components: NSWeekOfYearCalendarUnit fromDate:date] weekOfYear];
    NSInteger weekLastUpdated = [[calendar components: NSWeekOfYearCalendarUnit fromDate:self.updatedDate] weekOfYear];
    
    
    if (weekNow - weekLastUpdated != 0 ){
        return [NSNumber numberWithInteger:[self.week integerValue] + (weekNow - weekLastUpdated)];
    }else{
        return self.week;
    }
}

- (NSNumber *) getParityForDate: (NSDate *) date{
    NSInteger diff = [[self getWeekForDate:date] integerValue] - [self.week integerValue];
    
    if (diff != 0){
        return [NSNumber numberWithInteger:([self.parity integerValue] + diff)%2];
    }else{
        return self.parity;
    }
    
}

- (NSNumber *) currentParity{
    if (!self.week){
        return nil;
    }
    return [self getParityForDate:[NSDate date]];
}

@end
