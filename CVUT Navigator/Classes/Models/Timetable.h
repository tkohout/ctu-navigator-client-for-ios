//
//  TimetableEntriesSet.h
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 10.03.13.
//  Copyright (c) 2013 Tomáš Kohout. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "TimetableEntry.h"


@interface Timetable : NSObject
-(id)initWithEntries:(NSArray *)entries collectionView: (UICollectionView *) collectionView minuteToPixelRatio: (double) minuteToPixelRatio;

-(NSArray *)getEntriesForDay:(NSInteger)day fromTime:(NSInteger)time toTime: (NSInteger) time;

-(TimetableEntry *)getEntryForIndexPath: (NSIndexPath *) indexPath;
-(TimetableEntry *)getEntryForIndexPath: (NSIndexPath *) indexPath subRow: (NSInteger *) aSubRow;
- (NSArray *) getEntriesInRect: (CGRect) rect;

- (CGPoint) getLeftMostEntryPosition;
- (BOOL) shouldMoveTimetable;

- (NSInteger) subRowsForDay: (NSInteger) day;
- (NSInteger) subRowsBeforeDay: (NSInteger) day;
- (NSInteger) dayForSubRow: (NSInteger) subRow;
- (BOOL) isSubRowFirst: (NSInteger) subRow;
- (NSInteger) subRowsCount;

- (double)getPositionForDay: (NSInteger) day;

- (NSInteger)convertPxToTime: (NSInteger) px;
- (NSInteger)convertTimeToPx: (NSInteger) time;
+ (double) viewWidthIfTime: (NSInteger) time isPx: (NSInteger) px;


@end
