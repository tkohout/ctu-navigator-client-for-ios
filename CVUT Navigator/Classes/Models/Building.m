//
//  Building.m
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 06.12.12.
//  Copyright (c) 2012 Tomáš Kohout. All rights reserved.
//

#import "Building.h"
#import "Room.h"


@implementation Building

@dynamic address;
@dynamic code;
@dynamic latitude;
@dynamic longitude;
@dynamic planId;
@dynamic plan;
@dynamic hasPlan;

- (NSString *)detail{
    return [self address];
}

- (void) awakeFromInsert
{
    [super awakeFromInsert];
    self.hasPlan = [NSNumber numberWithBool:NO];
}

- (void)setPlanId:(NSNumber *)planId
{   [self willChangeValueForKey:@"planId"];
    [self setHasPlan:[NSNumber numberWithBool:YES]];
    [self setPrimitiveValue:planId forKey:@"planId"];
    [self didChangeValueForKey:@"planId"];
}

-(CLLocationCoordinate2D)coordinate{
    return CLLocationCoordinate2DMake([self.latitude doubleValue], [self.longitude doubleValue]);
}

@end
