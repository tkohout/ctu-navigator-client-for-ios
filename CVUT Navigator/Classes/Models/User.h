//
//  UserInfo.h
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 25.04.13.
//  Copyright (c) 2013 Tomáš Kohout. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface User : NSManagedObject

@property (nonatomic, retain) NSString * username;
@property (nonatomic, retain) NSString * accessToken;
@property (nonatomic, retain) NSString * refreshToken;
@property (nonatomic, retain) NSString * tokenType;
@property (nonatomic, retain) NSDate * expiresIn;

@end
