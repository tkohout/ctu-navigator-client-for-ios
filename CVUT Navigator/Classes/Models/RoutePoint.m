//
//  RoutePoint.m
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 25.02.13.
//  Copyright (c) 2013 Tomáš Kohout. All rights reserved.
//

#import "RoutePoint.h"
#import "Searchable.h"
#import "Building.h"
#import "Room.h"
#import "LocationService.h"

@implementation RoutePoint

@synthesize item;
//id item;

-(id)initWithRouteItem:(id) anItem{
    if (!self){
        self = [self init];
    }
    
    item = anItem ;
    return self;
}

- (id)initAsCurrentLocation{
    return [self initWithRouteItem:CURRENT_LOCATION];
}

- (BOOL) isCurrentLocation{
    return ([item respondsToSelector:@selector(isEqualToString:)] &&  [item isEqualToString: CURRENT_LOCATION]);
}

- (BOOL) isNode{
    return [self.item isKindOfClass:[Node class]];
}

- (BOOL) isBuilding{
    return [self.item isKindOfClass:[Building class]];
}

- (BOOL) isPlacemark{
    return [item isKindOfClass:[CLPlacemark class]];
}


- (BOOL) isAddress{
    return (![self isCurrentLocation] && [item isKindOfClass:[NSString class]]);
}

- (NSString *) name{
    
    if ([self isCurrentLocation]){
        return @"Current location";
    }else if ([item isKindOfClass:[CLPlacemark class]]){
        item = (CLPlacemark *) item;
        NSString * name = [NSString stringWithFormat:@"%@%@%@", [item thoroughfare] ? [[item thoroughfare] stringByAppendingString:@" "] : @"", [item subThoroughfare] ? [[item subThoroughfare] stringByAppendingString:@", "] : @"", [item subLocality] ? [item subLocality] : @""];
        
        if ([name isEqualToString:@""]){
            return self.detail;
        }
        
        return name;
    }else if ([item isKindOfClass:[NSString class]]){
        return item;
    }else{
        item = (Searchable *) item;
        return [item name];
    }
}

- (NSString *) detail{
    
    if ([self isCurrentLocation]){
        return nil;
    }else if ([item isKindOfClass:[CLPlacemark class]]){
        item = (CLPlacemark *) item;
        NSString * detail = [NSString stringWithFormat:@"%@%@%@", [item subAdministrativeArea] ? [[item subAdministrativeArea] stringByAppendingString:@", "] : @"", [item postalCode] ? [[item postalCode] stringByAppendingString:@", "] : @"", [item country] ? [item country] : @""];
        return detail;
    }else{
        item = (Searchable *) item;
        return [item detail];
    }
}

- (UIImage *) image{
    
    if ([self isCurrentLocation]){
        return [UIImage imageNamed:@"location_current_black.png"];
    }else if ([item isKindOfClass:[CLPlacemark class]]){
        
        return [UIImage imageNamed:@"location_circle.png"];
    }else{
        if ([item isKindOfClass:[Building class]]){
            return [UIImage imageNamed:@"bank.png"];
        }else{
            return [UIImage imageNamed:@"house.png"];
        }
    }
    
}


- (CLLocationCoordinate2D) coordinate{
    CLLocationCoordinate2D coordinate;
    
    if ([self isBuilding]){
        item = (Building *) item;
        coordinate.latitude = [[item latitude] doubleValue];
        coordinate.longitude = [[item longitude] doubleValue];
    }else if ([self isNode]){
        item = (Node *) item;
        
        if ([item building]){
        
        coordinate.latitude = [[[item building] latitude] doubleValue];
        coordinate.longitude = [[[item building] longitude] doubleValue];
            
        }else{
            coordinate.latitude = [[item  latitude] doubleValue];
            coordinate.longitude = [[item longitude] doubleValue];
        }
    }else if ([self isPlacemark]){
        item = (CLPlacemark *) item;
        
        coordinate = [[(CLPlacemark *)item location] coordinate];
    }else if ([self isCurrentLocation]){
        
        if ([LocationService sharedService].currentLocation){
            coordinate = [LocationService sharedService].currentLocation.coordinate;
        }
    }
    
    return coordinate;
}

- (NSString *)type{
    if ([self isCurrentLocation]){
        return CURRENT_LOCATION;
    }
    
    return [[item class] description];
}

- (BOOL)isEqualToRoutePoint:(RoutePoint *)routePoint{
    if ([self.item class] != [routePoint.item class]){
        return NO;
    }
    
    if ([self isCurrentLocation] || [item isKindOfClass:[CLPlacemark class]]){
        return (self.coordinate.latitude == routePoint.coordinate.latitude && self.coordinate.longitude == routePoint.coordinate.longitude);
    }else if ([self isAddress]){
        return [item isEqualToString:routePoint.item];
    }else if ([self isBuilding]){
        item = (Building *) item;
        return [[item id] isEqualToNumber:((Building *)routePoint.item).id];
    }else if ([self isNode]){
        item = (Node *) item;
        return [[item id] isEqualToNumber:((Node *)routePoint.item).id];
    }
    
    return NO;
}

- (NSNumber *)buildingId{
    if ([self isBuilding] ){
        return ((Building *)self.item).id;
    }else if ([self isNode] ){
        return ((Node *)self.item).buildingId;
    }else if ([self isCurrentLocation]){
        if ([LocationService sharedService].currentLocationType == LocationServiceCurrentLocationTypeIndoor){
            return [LocationService sharedService].currentIndoorLocationNode.buildingId;
        }
    }
    return nil;
}


- (Building *)building{
    if ([self isBuilding]){
        return ((Building *)self.item);
    }else if ([self isNode]){
        return ((Node *)self.item).building;
    }else if ([self isCurrentLocation]){
        if ([LocationService sharedService].currentLocationType == LocationServiceCurrentLocationTypeIndoor){
            return [LocationService sharedService].currentIndoorLocationNode.building;
        }
    }
    return nil;
}

- (BOOL) isInSameBuildingAsRoutePoint: (RoutePoint *) routePoint{
    return (self.buildingId != nil && routePoint.buildingId != nil  && [self.buildingId isEqualToNumber:routePoint.buildingId]);
}


-(id) copyWithZone: (NSZone *) zone{
    RoutePoint *routePointCopy = [[RoutePoint allocWithZone: zone] initWithRouteItem:self.item];
    return routePointCopy;
}



@end
