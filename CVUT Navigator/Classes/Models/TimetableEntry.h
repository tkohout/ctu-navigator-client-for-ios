//
//  TimetableEntry.h
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 06.12.12.
//  Copyright (c) 2012 Tomáš Kohout. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Entity.h"

@class Room, Course, Person;


typedef NS_ENUM(NSInteger, TimetableEntryType){
    TimetableEntryTypeLecture,
    TimetableEntryTypePractice,
    TimetableEntryTypeLaboratory,
    TimetableEntryTypeEvent
};


@interface TimetableEntry : Entity

@property (nonatomic, retain) NSNumber * day;
@property (nonatomic, retain) NSString * dayString;
@property (nonatomic, retain) NSNumber * parity;
@property (nonatomic, retain) NSString * parityString;

@property (nonatomic, retain) NSNumber * time;
@property (nonatomic, retain) NSString * timeString;
@property (nonatomic, retain) NSNumber * length;
@property (nonatomic, retain) NSNumber * type;
@property (nonatomic, retain) NSString * typeString;
@property (nonatomic, retain) UIColor * color;

@property (nonatomic, retain) NSDate * date;

@property (nonatomic, retain, readonly) NSNumber * endTime;

@property (nonatomic, retain) Course *course;
@property (nonatomic, retain) Room *room;
@property (nonatomic, retain) Person *teacher;

@property (nonatomic, retain) NSString * roomName;


-(NSString *) timeFormatted;
-(NSString *) startTimeFormatted;
-(NSString *) endTimeFormatted;

-(NSString *) dayString;
-(NSString *) typeString;


@end
