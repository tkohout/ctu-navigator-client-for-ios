//
//  RoutePoint.h
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 25.02.13.
//  Copyright (c) 2013 Tomáš Kohout. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
@class Building;
@interface RoutePoint : NSObject <NSCopying>

- initWithRouteItem: (id) item;

@property (nonatomic, readonly) BOOL isCurrentLocation;
@property (nonatomic, readonly) BOOL isAddress;
@property (nonatomic, readonly) BOOL isBuilding;
@property (nonatomic, readonly) BOOL isNode;
@property (nonatomic, readonly) BOOL isPlacemark;

- (BOOL) isEqualToRoutePoint: (RoutePoint *) routePoint;
- (BOOL) isInSameBuildingAsRoutePoint: (RoutePoint *) routePoint;
-(id) copyWithZone: (NSZone *) zone;


- (id)initAsCurrentLocation;


@property (nonatomic, strong) id item;


@property (nonatomic, strong, readonly) NSString * name;
@property (nonatomic, strong, readonly) NSString * detail;
@property (nonatomic, strong, readonly) NSString * type;
@property (nonatomic, strong, readonly) UIImage * image;
@property (nonatomic, strong, readonly) Building * building;
@property (nonatomic, strong, readonly) NSNumber * buildingId;
@property (nonatomic) CLLocationCoordinate2D coordinate;



@end
