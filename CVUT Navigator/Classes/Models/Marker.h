//
//  Marker.h
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 27.03.13.
//  Copyright (c) 2013 Tomáš Kohout. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Entity.h"

@class Node;

@interface Marker : Entity

@property (nonatomic, retain) NSNumber * angle;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) Node *node;

@end
