//
//  Entity.m
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 26.04.13.
//  Copyright (c) 2013 Tomáš Kohout. All rights reserved.
//

#import "Entity.h"


@implementation Entity

@dynamic updatedDate;
@dynamic id;

+ (void) load {
    
    [[NSNotificationCenter defaultCenter] addObserver: (id)[self class]
                                                 selector: @selector(objectContextWillSave:)
                                                     name: NSManagedObjectContextWillSaveNotification
                                                   object: nil];
    
}

+ (void) objectContextWillSave: (NSNotification*) notification {
    NSManagedObjectContext* context = [notification object];
    
    NSSet* allModified = [context.insertedObjects setByAddingObjectsFromSet: context.updatedObjects];
    
    NSPredicate* predicate = [NSPredicate predicateWithFormat: @"self isKindOfClass: %@", [self class]];
    NSSet* modifiable = [allModified filteredSetUsingPredicate: predicate];
    
    [modifiable makeObjectsPerformSelector: @selector(setUpdatedDate:) withObject: [NSDate date]];
}

- (BOOL)isEqualToEntity:(id)object{
    if ([object isKindOfClass:[Entity class]]){
        return [self.id isEqualToNumber:((Entity *) object).id];
    }
    
    return NO;
}



@end
