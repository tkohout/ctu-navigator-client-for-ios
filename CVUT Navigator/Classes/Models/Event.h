//
//  Event.h
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 11.03.13.
//  Copyright (c) 2013 Tomáš Kohout. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "TimetableEntry.h"


@interface Event : TimetableEntry

@property (nonatomic, retain) NSString * eventName;
@property (nonatomic, retain) NSString * eventDetail;


@end
