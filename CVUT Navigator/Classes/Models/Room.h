//
//  Room.h
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 06.02.13.
//  Copyright (c) 2013 Tomáš Kohout. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Node.h"
#import "Building.h"

@class Floor, TimetableEntry;

@interface Room : Node

@property (nonatomic, retain) NSSet *timetableEntries;
@end

@interface Room (CoreDataGeneratedAccessors)

- (void)addTimetableEntriesObject:(TimetableEntry *)value;
- (void)removeTimetableEntriesObject:(TimetableEntry *)value;
- (void)addTimetableEntries:(NSSet *)values;
- (void)removeTimetableEntries:(NSSet *)values;

@end
