//
//  UserInfo.m
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 25.04.13.
//  Copyright (c) 2013 Tomáš Kohout. All rights reserved.
//

#import "User.h"


@implementation User

@dynamic username;
@dynamic accessToken;
@dynamic refreshToken;
@dynamic tokenType;
@dynamic expiresIn;

@end
