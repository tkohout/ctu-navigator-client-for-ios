//
//  Node.m
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 19.02.13.
//  Copyright (c) 2013 Tomáš Kohout. All rights reserved.
//

#import "Node.h"
#import "Floor.h"
#import "Marker.h"
#import "Plan.h"
#import "Building.h"


@implementation Node

@dynamic buildingId;

@dynamic building;
@dynamic latitude;
@dynamic longitude;
@dynamic markerId;
@dynamic marker;
@dynamic floorName;
@dynamic type;
@dynamic hasMarker;
@dynamic floorId;


@dynamic floor;

- (void) awakeFromInsert
{
    [super awakeFromInsert];
    self.hasMarker = [NSNumber numberWithBool:NO];
}

- (void)setMarkerId:(NSNumber *)markerId
{   [self willChangeValueForKey:@"markerId"];
    [self setHasMarker:[NSNumber numberWithBool:YES]];
    [self setPrimitiveValue:markerId forKey:@"markerId"];
    [self didChangeValueForKey:@"markerId"];
}

-(CLLocationCoordinate2D)coordinate{
    return CLLocationCoordinate2DMake([self.latitude doubleValue], [self.longitude doubleValue]);
}

/*- (Floor *) floor{
    if (self.building && self.building.plan){
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"floorNumber = %@", self.floorNumber];
        
        NSSet * filteredSet = [self.building.plan.floors filteredSetUsingPredicate:predicate];
        
        if ([filteredSet count] > 0){
            return [filteredSet anyObject];
        }
    }

    return nil;
}*/

- (BOOL) isFromFloor: (Floor *) floor{
    return ([self.floorId isEqualToNumber:floor.id]);
}

- (BOOL) isFromSameFloorAsNode: (Node *) node{
    return ([self.floorId isEqualToNumber:node.floorId]);
}

@end
