//
//  Course.h
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 06.12.12.
//  Copyright (c) 2012 Tomáš Kohout. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Searchable.h"

@interface Course : Searchable

@property (nonatomic, retain) NSString * longName;
@property (nonatomic, retain) NSString * web;
@property (nonatomic, retain) NSSet *timetableEntries;
@end

@interface Course (CoreDataGeneratedAccessors)

- (void)addTimetableEntriesObject:(NSManagedObject *)value;
- (void)removeTimetableEntriesObject:(NSManagedObject *)value;
- (void)addTimetableEntries:(NSSet *)values;
- (void)removeTimetableEntries:(NSSet *)values;

@end
