//
//  TimetableEntriesSet.m
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 10.03.13.
//  Copyright (c) 2013 Tomáš Kohout. All rights reserved.
//

#import "Timetable.h"
#import "TimetableEntry.h"
#import "PSTCollectionView.h"

//#import "TimetableEntry+IndexPath.h"
#define DAYS 7


#define STARTING_TIME_IN_MINUTES (7*60+30)
#define LESSON_LENGTH_IN_MINUTES 90
#define BREAK_LENGTH_IN_MINUTES 15

#define ENDING_TIME_IN_MINUTES (STARTING_TIME_IN_MINUTES + 8*(LESSON_LENGTH_IN_MINUTES +BREAK_LENGTH_IN_MINUTES))



@interface Timetable()
@property (nonatomic, retain) NSMutableArray * entries;
@property (nonatomic, retain) NSMutableArray * entriesSubrowsCount;
@property (nonatomic, retain) UICollectionView * collectionView;
@property (nonatomic, assign,readonly) CGSize timetableSize;
@property (nonatomic, assign) double minuteToPixelRatio;
@end


@implementation Timetable



- (NSMutableArray *) addEntry: (TimetableEntry *) entry toDayArray: (NSMutableArray *) dayArray subRow: (NSInteger) subRow{
    
    if (subRow < [dayArray count]){
        NSMutableArray * subRowArray =  [dayArray objectAtIndex:subRow];
        
        for (TimetableEntry * searchedEntry in subRowArray){
            if (entry != searchedEntry){
                if (([entry.time integerValue] >= [searchedEntry.time integerValue] && [entry.time integerValue] < [searchedEntry.endTime integerValue] ) || ([searchedEntry.time integerValue] >= [entry.time integerValue] && [searchedEntry.time integerValue] < [entry.endTime integerValue])){
                    return [self addEntry:entry toDayArray:dayArray subRow:subRow+1];
                }
            }
        }
        
        [subRowArray addObject:entry];
        return dayArray;
        
    }else{
        NSMutableArray * newSubRowArray = [[NSMutableArray alloc] init];
        [newSubRowArray addObject: entry];
        [dayArray addObject: newSubRowArray];
        
        return dayArray;
    }
}


-(id)initWithEntries:(NSArray *)entries collectionView: (UICollectionView *) collectionView minuteToPixelRatio: (double) minuteToPixelRatio{
    self = [self init];
    if (self){
        self.entries = [[NSMutableArray alloc] init];
        self.entriesSubrowsCount = [[NSMutableArray alloc] init];
        self.collectionView = collectionView;
        self.minuteToPixelRatio = minuteToPixelRatio;
        
        for (NSInteger i=0; i < DAYS; i++){
            [self.entries addObject: [[NSMutableArray alloc] init]];
        }
        
        for (TimetableEntry * entry in entries){
            NSMutableArray * dayArray = [self.entries objectAtIndex:[entry.day intValue]-1];
            [self addEntry:entry toDayArray:dayArray subRow:0];
        }
        
        for (NSInteger i=0; i < DAYS; i++){
            [self.entriesSubrowsCount addObject: [NSNumber numberWithInt:[[self.entries objectAtIndex:i] count]]];
        }
        
    }
    
    return self;
}

-(NSArray *)getEntriesForDay:(NSInteger)day fromTime:(NSInteger)fromTime toTime: (NSInteger) toTime{
    NSMutableArray * entries = [[NSMutableArray alloc] init];
    
    if (day <= DAYS && day > 0){
        NSMutableArray * dayArray = [self.entries objectAtIndex: day-1];
        
        int i = 0;
        
        for (NSArray * subRowArray in dayArray){
            
            NSMutableArray * subEntries = [[NSMutableArray alloc] init];
            
            for (TimetableEntry * entry in subRowArray){
                if ([entry.time intValue] <= toTime && [entry.time intValue] + [entry.length intValue] >= fromTime){
                    
                    
                    [subEntries addObject:@{@"entry": entry, @"indexPath": [NSIndexPath indexPathForItem:i inSection:day-1]}];
                }
                i++;
            }
            
            [entries addObject:subEntries];
        }
    }
    
    return [[NSArray alloc] initWithArray:entries];
}

- (CGSize)timetableSize{
    return self.collectionView.collectionViewLayout.collectionViewContentSize;
}
- (NSArray *) getEntriesInRect: (CGRect) rect{
    
    NSInteger fromTime = [self convertPxToTime:rect.origin.x];
    NSInteger toTime = [self convertPxToTime:rect.origin.x+rect.size.width];
    
    
    NSInteger startDay= [self dayForSubRow: (rect.origin.y/self.timetableSize.height) * [self subRowsCount]];
    NSInteger endDay= [self dayForSubRow: (ceil((double)(rect.origin.y + rect.size.height)/self.timetableSize.height)) * [self subRowsCount]];
    
    
    NSMutableArray * entries = [[NSMutableArray alloc] init];
    
    
    for(NSInteger day=startDay ; day < endDay; day++) {
        [entries addObject: [self getEntriesForDay:day+1 fromTime:fromTime toTime:toTime]];
    }
    
    return entries;
}

- (BOOL) shouldMoveTimetable{
    NSArray * entries = [self getEntriesInRect:self.collectionView.bounds];
    
    BOOL shouldMove = YES;
    
    for (NSArray * dayArray in entries){
        for (NSArray * subRows in dayArray){
            if ([subRows count] > 0) {
                shouldMove = NO;
            }
        }
    }
    
    return shouldMove;
}

- (CGPoint) getLeftMostEntryPosition{
    
    TimetableEntry * leftMostEntry;
    NSInteger minTime = 0;
    NSInteger subRowMin = 0;
    
    for ( NSArray * dayArray in self.entries ){
        NSInteger subRow = 0;
        for (NSArray * subRowArray in dayArray){
            
            for (TimetableEntry * entry in subRowArray){
                if (minTime == 0 || [entry.time integerValue] < minTime){
                    leftMostEntry = entry;
                    minTime = [entry.time integerValue];
                    subRowMin = subRow;
                }
            }
            subRow++;
        }
        
    }
    
    CGPoint leftMostEntryPosition = CGPointZero;
    
    
    if (leftMostEntry){
        leftMostEntryPosition = CGPointMake([self convertTimeToPx:[leftMostEntry.time integerValue]], ((double)subRowMin/[self subRowsCount])*self.timetableSize.height);
    }
    
    
    return leftMostEntryPosition;
}

- (double)getPositionForDay: (NSInteger) day{
    NSInteger subRows = [self subRowsBeforeDay:day];
    return ((double)subRows/[self subRowsCount])*self.timetableSize.height;
}

-(TimetableEntry *)getEntryForIndexPath: (NSIndexPath *) indexPath subRow: (NSInteger *) aSubRow{
    NSMutableArray * dayArray = [self.entries objectAtIndex: indexPath.section];
    int i = 0;
    
    int subRow = 0;
    
    for (NSArray * subRowArray in dayArray){
        
        for (TimetableEntry * entry in subRowArray){
            if (i == indexPath.row){
                if (aSubRow) *aSubRow = subRow;
                return entry;
            }
            i++;
        }
        subRow ++;
    }
    
    return nil;
    
    
}

-(TimetableEntry *)getEntryForIndexPath: (NSIndexPath *) indexPath{
    return [self getEntryForIndexPath:indexPath subRow:nil];
}

- (NSInteger) subRowsForDay: (NSInteger) day{
    NSInteger subRows = [[self.entriesSubrowsCount objectAtIndex:day] integerValue];
    
    return ((subRows > 0)? subRows : 1);
}

- (NSInteger) dayForSubRow: (NSInteger) subRow{
    NSInteger subRowsBefore = 0;
    int day;
    
    for (day = 0; day < DAYS; day++){
        if (subRowsBefore>=subRow){
            return day;
        }
        
        subRowsBefore += [self subRowsForDay:day];
    }
    
    return day;
}

- (NSInteger) subRowsBeforeDay: (NSInteger) day{
    NSInteger subRowsBefore = 0;
    
    for (int i = 0; i<day; i++){
        subRowsBefore += [self subRowsForDay:i];
    }
    
    return subRowsBefore;
}

- (BOOL)isSubRowFirst:(NSInteger)subRow{
    NSInteger subRowsBefore = 0;
    
    for (int i = 0; i<DAYS; i++){
        if (subRowsBefore == subRow){
            return YES;
        }
        
        if (subRowsBefore > subRow){
            return NO;
        }
        
        subRowsBefore += [self subRowsForDay:i];
    }
    
    return NO;
}

- (NSInteger) subRowsCount{
    NSInteger subRowsBefore = 0;
    
    for (int i = 0; i<DAYS; i++){
        subRowsBefore += [self subRowsForDay:i];
    }
    
    return subRowsBefore;
}

- (NSInteger) convertPxToTime:(NSInteger)px{
    NSInteger time = ((double)px / self.timetableSize.width) * (ENDING_TIME_IN_MINUTES - STARTING_TIME_IN_MINUTES) + STARTING_TIME_IN_MINUTES;
    
    return time;
}

- (NSInteger) convertTimeToPx:(NSInteger)time{
    NSInteger px = ((double)(time - STARTING_TIME_IN_MINUTES)/ (ENDING_TIME_IN_MINUTES - STARTING_TIME_IN_MINUTES)) *  self.timetableSize.width ;
    return px;
}

+ (double) viewWidthIfTime: (NSInteger) time isPx: (NSInteger) px{
    double width = ((ENDING_TIME_IN_MINUTES - STARTING_TIME_IN_MINUTES) / (double)time) * px;
    return width;
}
@end
