//
//  Floor.m
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 19.02.13.
//  Copyright (c) 2013 Tomáš Kohout. All rights reserved.
//

#import "Floor.h"
#import "Node.h"
#import "Plan.h"


@implementation Floor


@dynamic northEastLatitude;
@dynamic northEastLongitude;
@dynamic planId;
@dynamic southWestLatitude;
@dynamic southWestLongitude;
@dynamic plan;
@dynamic nodes;

@dynamic floorName;
@dynamic floorNumber;
@dynamic maxZoom;
@dynamic minZoom;
@dynamic tiles;



- (CLLocationCoordinate2D)southWestConstrain{
    return CLLocationCoordinate2DMake([self.southWestLatitude doubleValue], [self.southWestLongitude doubleValue]);
};

- (CLLocationCoordinate2D)northEastConstrain{
    return CLLocationCoordinate2DMake([self.northEastLatitude doubleValue], [self.northEastLongitude doubleValue]);
};



- (BOOL) canBeShowedOnPlan{
    if (self.tiles && self.northEastLatitude && self.northEastLongitude && self.southWestLatitude && self.southWestLongitude && self.maxZoom && self.minZoom){
        return YES;
    }
    
    return NO;
}



@end
