//
//  Node.h
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 19.02.13.
//  Copyright (c) 2013 Tomáš Kohout. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Searchable.h"
#import <CoreLocation/CoreLocation.h>

@class Floor, Marker, Building;

@interface Node : Searchable



/*Building*/
@property (nonatomic, retain) NSNumber * buildingId;
@property (nonatomic, retain) Building *building;

/*Floor*/
@property (nonatomic, retain) NSString * floorName;
@property (nonatomic, retain) Floor * floor;
@property (nonatomic, retain) NSNumber * floorId;

@property (nonatomic, retain) NSNumber * latitude;
@property (nonatomic, retain) NSNumber * longitude;
@property (nonatomic, retain) NSNumber * markerId;
@property (nonatomic, retain) NSString * type;

@property (nonatomic, retain) Marker *marker;
@property (nonatomic, retain) NSNumber * hasMarker;


@property (nonatomic, assign, readonly) CLLocationCoordinate2D coordinate;


- (BOOL) isFromFloor: (Floor *) floor;
- (BOOL) isFromSameFloorAsNode: (Node *) node;
@end
