//
//  Plan.m
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 06.02.13.
//  Copyright (c) 2013 Tomáš Kohout. All rights reserved.
//

#import "Plan.h"
#import "Building.h"
#import "Floor.h"
#import "Node.h"

@implementation Plan

@dynamic name;
@dynamic building;
@dynamic buildingId;
@dynamic floors;


- (NSArray *)entrances{
    
    
    NSMutableArray * entrances = [[NSMutableArray alloc] init];
    
    Floor * groundFloor;
    
    groundFloor = self.groundFloor;
    
    if (!groundFloor){
        return nil;
    }
    
    for (Node * node in groundFloor.nodes){
        if ([node.type isEqualToString: @"entrance"]){
            [entrances addObject:node];
        }
    }
    
    return entrances;
}

- (Floor *)groundFloor{
    
    for (Floor * floor in self.floors){
        if ([floor.floorNumber integerValue] == 0){
            return floor;
        }
    }
    return nil;
}
- (Floor *)topFloor{
    
    if ([self.floors count] > 0){
        
        Floor * topFloor = [self.floors anyObject];
        
        for (Floor * floor in self.floors){
            if ([floor.floorNumber integerValue] > [topFloor.floorNumber integerValue]){
                topFloor = floor;
            }
        }
        
        return topFloor;
    }
    
    return nil;
}
- (Floor *)bottomFloor{
    
    if ([self.floors count] > 0){
        
        Floor * bottomFloor = [self.floors anyObject];
        
        for (Floor * floor in self.floors){
            if ([floor.floorNumber integerValue] < [bottomFloor.floorNumber integerValue]){
                bottomFloor = floor;
            }
        }
        
        return bottomFloor;
    }
    
    return nil;
}

@end
