//
//  TimetableEntry.m
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 06.12.12.
//  Copyright (c) 2012 Tomáš Kohout. All rights reserved.
//

#import "TimetableEntry.h"
#import "Room.h"
#import "Course.h"
#import "Person.h"


@implementation TimetableEntry

@dynamic day;
@dynamic parity;
@dynamic length;
@dynamic type;
@dynamic course;
@dynamic room;
@dynamic teacher;
@dynamic timeString;
@dynamic time;
@dynamic dayString;
@dynamic typeString;
@dynamic parityString;
@dynamic date;
@dynamic roomName;

-(NSString *) timeFormatted{
    NSString *format = [NSString stringWithFormat:@"%%0%dd", 2];
    
    NSInteger hour = ([self.time intValue]) / 60;
    NSInteger minute = ([self.time intValue]) % 60;
    
    NSInteger endHour = ([self.time intValue] + [self.length intValue]) / 60;
    NSInteger endMinute = ([self.time intValue] + [self.length intValue]) % 60;
    
    return [NSString stringWithFormat:@"%d:%@ - %d:%@", hour,  [NSString stringWithFormat:format,minute ], endHour, [NSString stringWithFormat:format,endMinute]];
}

-(NSString *) startTimeFormatted{
    return [self standardTimeFormat: self.time];
}

-(NSString *) endTimeFormatted{
    return [self standardTimeFormat:self.endTime];
}

-(NSString *) standardTimeFormat: (NSNumber *) time{
    NSString *format = [NSString stringWithFormat:@"%%0%dd", 2];
    
    NSInteger hour = ([time intValue]) / 60;
    NSInteger minute = ([time intValue]) % 60;
    
    return [NSString stringWithFormat:@"%d:%@", hour,  [NSString stringWithFormat:format,minute ]];
}



- (NSNumber *) endTime{
    return [NSNumber numberWithInt:[self.time integerValue] + [self.length integerValue]];
}


-(void)setTimeString:(NSString *)aTimeString{
    NSArray *components = [aTimeString componentsSeparatedByString: @":"];
    if ([components count] == 2){
        self.time = [NSNumber numberWithInt: [[components objectAtIndex:0] intValue] * 60 + [[components objectAtIndex:1] intValue]];
    }
    
    //timeString = aTimeString;
}

-(void)setParityString:(NSString *)aParityString{
    if ([aParityString caseInsensitiveCompare: @"even"] == NSOrderedSame){
        self.parity = [NSNumber numberWithInteger:WeekParityEven];
    }else if ([aParityString caseInsensitiveCompare: @"odd"] == NSOrderedSame){
        self.parity = [NSNumber numberWithInteger:WeekParityOdd];
    }else{
        self.parity = [NSNumber numberWithInteger:WeekParityBoth];
    }
}


-(void)setDayString:(NSString *)aDayString{
    if ([aDayString isEqualToString: @"monday"]){
        self.day = [NSNumber numberWithInt: 1];
    }else if([aDayString isEqualToString: @"tuesday"]){
        self.day = [NSNumber numberWithInt: 2];
    }else if([aDayString isEqualToString: @"wednesday"]){
        self.day = [NSNumber numberWithInt: 3];
    }else if([aDayString isEqualToString: @"thursday"]){
        self.day = [NSNumber numberWithInt: 4];
    }else if([aDayString isEqualToString: @"friday"]){
        self.day = [NSNumber numberWithInt: 5];
    }
}

- (NSString *) dayString{
    switch([self.day intValue]){
        case 1:
            return NSLocalizedString(@"Pondělí", nil);
            break;
        case 2:
            return NSLocalizedString(@"Úterý", nil);
            break;
        case 3:
            return NSLocalizedString(@"Středa", nil);
            break;
        case 4:
            return NSLocalizedString(@"Čtvrtek", nil);
            break;
        case 5:
            return NSLocalizedString(@"Pátek", nil);
            break;
            
    }
    
    return @"";
}

- (NSString *) typeString{
    switch([self.type intValue]){
        case TimetableEntryTypeLecture:
            return NSLocalizedString(@"Přednáška", nil);
            break;
        case TimetableEntryTypePractice:
            return NSLocalizedString(@"Cvičení", nil);
            break;
        case TimetableEntryTypeLaboratory:
            return NSLocalizedString(@"Laboratoř", nil);
            break;
    }
    
    return NSLocalizedString(@"Událost", nil);
}






- (void) setTypeString:(NSString *) aType{
    TimetableEntryType type;
    
    if ([aType caseInsensitiveCompare:@"lecture"] == NSOrderedSame){
        type = TimetableEntryTypeLecture;
    }else if ([aType caseInsensitiveCompare:@"practice"] == NSOrderedSame || [aType caseInsensitiveCompare:@"tutorial"] == NSOrderedSame){
        type = TimetableEntryTypePractice;
    }else if ([aType caseInsensitiveCompare:@"laboratory"] == NSOrderedSame){
        type = TimetableEntryTypeLaboratory;
    }else{
        type = TimetableEntryTypeEvent;
    }
    
    [self setTypeWithEnum:type];
}

- (void) setTypeWithEnum:(PersonType) type{
    [self setType:[NSNumber numberWithInteger:type]];
}

- (UIColor *) color{
    if ([self.type intValue] == TimetableEntryTypeEvent){
        return [UIColor redColor];
    }else if ([self.type intValue] == TimetableEntryTypeLecture){
        return [UIColor colorWithRed:0 green:74.0/255.0 blue:117.0/255.0 alpha:1.0];
    }else if ([self.type intValue] == TimetableEntryTypePractice){
        return [UIColor colorWithRed:194.0/255.0 green:136.0/255.0 blue:0.0/255.0 alpha:1.0];
    }else{
        return [UIColor purpleColor];
    }
}


@end
