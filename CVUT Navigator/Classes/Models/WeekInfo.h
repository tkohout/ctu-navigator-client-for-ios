//
//  WeekInfo.h
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 25.04.13.
//  Copyright (c) 2013 Tomáš Kohout. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Entity.h"

@interface WeekInfo : Entity

@property (nonatomic, retain) NSNumber * week;
@property (nonatomic, retain) NSNumber * parity;

@property (nonatomic, retain, readonly) NSNumber * currentWeek;
@property (nonatomic, retain, readonly) NSNumber * currentParity;

- (NSNumber *) getWeekForDate: (NSDate *) date;
- (NSNumber *) getParityForDate: (NSDate *) date;

@end
