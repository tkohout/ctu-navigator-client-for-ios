//
//  Building.h
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 06.12.12.
//  Copyright (c) 2012 Tomáš Kohout. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Searchable.h"
#import "Plan.h"
#import <CoreLocation/CoreLocation.h>

@class Classroom;

@interface Building : Searchable

@property (nonatomic, retain) NSString * address;
@property (nonatomic, retain) NSString * code;
@property (nonatomic, retain) NSNumber * latitude;
@property (nonatomic, retain) NSNumber * longitude;
@property (nonatomic, retain) NSNumber * planId;
@property (nonatomic, retain) NSNumber * hasPlan;
@property (nonatomic, retain) Plan *plan;

@property (nonatomic, assign, readonly) CLLocationCoordinate2D coordinate;

@end

@interface Building (CoreDataGeneratedAccessors)

- (void)addRoomsObject:(Classroom *)value;
- (void)removeRoomsObject:(Classroom *)value;
- (void)addRooms:(NSSet *)values;
- (void)removeRooms:(NSSet *)values;

@end
