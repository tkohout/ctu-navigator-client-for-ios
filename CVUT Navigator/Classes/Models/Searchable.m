//
//  Searchable.m
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 11.12.12.
//  Copyright (c) 2012 Tomáš Kohout. All rights reserved.
//

#import "Searchable.h"


@implementation Searchable

@dynamic name;
@dynamic detail;
@end
