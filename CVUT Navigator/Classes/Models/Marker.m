//
//  Marker.m
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 27.03.13.
//  Copyright (c) 2013 Tomáš Kohout. All rights reserved.
//

#import "Marker.h"
#import "Node.h"


@implementation Marker

@dynamic angle;
@dynamic type;
@dynamic node;

@end
