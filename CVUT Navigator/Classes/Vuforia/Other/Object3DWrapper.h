//
//  Object3D+CVUTNavigator.h
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 28.03.13.
//  Copyright (c) 2013 Tomáš Kohout. All rights reserved.
//
#ifndef VUFORIA_DISABLE

#import "AR_EAGLView.h"

typedef NS_ENUM(NSInteger, Object3DType) {
    Object3DTypeStraightArrow,
    Object3DTypeLeftArrow,
    Object3DTypeRightArrow,
    Object3DTypeReverseArrow,
    Object3DTypePlaceholder,
    Object3DTypeLoading,
    Object3DTypeFinnish,
    Object3DTypeUpArrow,
    Object3DTypeDownArrow,
    Object3DTypeWarning
};

typedef NS_ENUM(NSInteger, Object3DRotationDirection){
    Object3DRotationDirectionClockWise,
    Object3DRotationDirectionCounterClockWise
};

typedef NS_ENUM(NSInteger, Object3DAxis){
    Object3DAxisX,
    Object3DAxisZ,
    Object3DAxisY
};

typedef NS_ENUM(NSInteger, Object3DMovingDirection){
    Object3DMovingDirectionDown,
    Object3DMovingDirectionUp
};

typedef struct  {
    double x, y, z;
} Object3DRotation;

typedef struct  {
    double x, y, z;
} Object3DTranslate;

typedef struct  {
    double x, y, z;
} Object3DScale;



@interface Object3DWrapper: NSObject

@property (nonatomic, retain) Object3D * originalObject;

@property (nonatomic, assign) Object3DTranslate translate;
@property (nonatomic, assign) Object3DRotation rotation;
@property (nonatomic, assign) Object3DScale scale;

@property (nonatomic, assign) BOOL isRotating;
@property (nonatomic, assign) double rotationSpeed;
@property (nonatomic, assign) Object3DRotationDirection rotationDirection;
@property (nonatomic, assign) Object3DAxis rotationAxis;

@property (nonatomic, assign) BOOL isMoving;
@property (nonatomic, assign) double movingSpeed;
@property (nonatomic, assign) Object3DAxis movingAxis;
@property (nonatomic, assign) double movingRangeFrom;
@property (nonatomic, assign) double movingRangeTo;
@property (nonatomic, assign) double movingDirection;

+ (Object3D *) initObject3DWithNumVertices:(int)aNumVertices vertices:(const float *)aVertices normals:(const float *)aNormals texcoords:(const float *)aTexCoords texture: (Texture*) aTexture;

- initWithObject3D: (Object3D *) object3D translate:(Object3DTranslate) aTranslate rotation: (Object3DRotation) aRotation scale: (Object3DScale) aScale;

- (void) animateObject;
- (void) startRotationWithSpeed: (double) aSpeed direction: (Object3DRotationDirection) aDirection ofAxis: (Object3DAxis) anAxis;
- (void) startMovementWithSpeed: (double) aSpeed rangeFrom: (double) rangeFrom rangeTo: (double) rangeTo ofAxis: (Object3DAxis) anAxis;

- (void) stopRotation;
@end

#endif