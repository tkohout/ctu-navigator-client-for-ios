//
//  Object3D+CVUTNavigator.m
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 28.03.13.
//  Copyright (c) 2013 Tomáš Kohout. All rights reserved.
//
#ifndef VUFORIA_DISABLE

#import "Object3DWrapper.h"

#import <sys/time.h>
@interface Object3DWrapper()
    @property (nonatomic, assign) double lastFrameTime;
@end


@implementation Object3DWrapper

- (void)setRotation:(Object3DRotation)rotation{
    _rotation.x= rotation.x;
    _rotation.y= rotation.y;
    _rotation.z= rotation.z;
}

- (void)setTranslate:(Object3DTranslate)translate{
    _translate.x = translate.x;
    _translate.y = translate.y;
    _translate.z = translate.z;
}

- (void)setScale:(Object3DScale)scale{
    _scale.x = scale.x;
    _scale.y = scale.y;
    _scale.z = scale.z;
}


+ (Object3D *)initObject3DWithNumVertices:(int)aNumVertices vertices:(const float *)aVertices normals:(const float *)aNormals texcoords:(const float *)aTexCoords texture:(Texture *)aTexture{
    Object3D * obj3D = [[Object3D alloc] init];
    obj3D.numVertices = aNumVertices;
    obj3D.vertices = aVertices;
    obj3D.normals = aNormals;
    obj3D.texCoords = aTexCoords;
    obj3D.texture = aTexture;
    
    return obj3D;
}

- (id)initWithObject3D:(Object3D *)object3D translate:(Object3DTranslate)aTranslate rotation:(Object3DRotation)aRotation scale:(Object3DScale)aScale{
    self = [super init];
    
    if (self){
        self.originalObject = object3D;
        self.translate = aTranslate;
        self.rotation = aRotation;
        self.scale = aScale;
        self.isRotating = NO;
    }
    return self;
}




- (void) startRotationWithSpeed: (double) aSpeed direction: (Object3DRotationDirection) aDirection ofAxis: (Object3DAxis) anAxis{
    self.isRotating = YES;
    
    self.rotationDirection = aDirection;
    self.rotationSpeed = aSpeed;
    self.rotationAxis = anAxis;
}

- (void) startMovementWithSpeed:(double)aSpeed rangeFrom:(double)rangeFrom rangeTo:(double)rangeTo ofAxis:(Object3DAxis)anAxis{
    self.isMoving = YES;
    self.movingSpeed = aSpeed;
    self.movingAxis = anAxis;
    self.movingRangeFrom = rangeFrom;
    self.movingRangeTo = rangeTo;
    self.movingDirection = Object3DMovingDirectionUp;
}

- (void) stopRotation{
    self.isRotating = NO;
}

- (void) stopMovement{
    self.isMoving = NO;
}

- (double) getCurrentTime
{
    struct timeval tv;
    gettimeofday(&tv, NULL);
    double t = tv.tv_sec + tv.tv_usec/1000000.0;
    return t;
}


- (double)lastFrameTime{
    if (!_lastFrameTime){
        _lastFrameTime = [self getCurrentTime];
    }
    
    return _lastFrameTime;
}


- (void) animateObject{
    double time;
    
    if (!self.isMoving && !self.isRotating){
        return;
    }
    
    time = [self getCurrentTime];
    
    
    if (self.isRotating){
        
        double dt = (double)(time-self.lastFrameTime);
        double rotateDiff = dt * (360.0f*self.rotationSpeed)/3.1415f;
        
        int multiplier = 1;
        
        if (self.rotationDirection == Object3DRotationDirectionCounterClockWise){
            multiplier = -1;
        }
        
        Object3DRotation currentRotation;
        currentRotation.x = self.rotation.x;
        currentRotation.y = self.rotation.y;
        currentRotation.z = self.rotation.z;
        
        
        if (self.rotationAxis == Object3DAxisX){
            currentRotation.x = self.rotation.x + rotateDiff * multiplier;
        }else if (self.rotationAxis == Object3DAxisY){
            currentRotation.y = self.rotation.y + rotateDiff * multiplier;
        }else{
            
            if (self.rotationDirection == Object3DRotationDirectionCounterClockWise){
               currentRotation.z = self.rotation.z - rotateDiff;
            }else{
                currentRotation.z = self.rotation.z + rotateDiff;
            }
        }
        
        self.rotation = currentRotation;
        
        
    }
    
    if (self.isMoving){
        double dt = (double)(time-self.lastFrameTime);
        double diff = self.movingRangeTo - self.movingRangeFrom;
        
        double move = diff * dt * self.movingSpeed;
        double newPosition;
        int multiplier = -1;
        
        if (self.movingDirection == Object3DMovingDirectionUp){
            multiplier = 1;
        }
        
        if (self.movingAxis == Object3DAxisX){
            newPosition = self.translate.x + move * multiplier;
        }else if (self.movingAxis == Object3DAxisY){
            newPosition = self.translate.y + move * multiplier;
        }else{
            newPosition = self.translate.z + move * multiplier;
        }
        
        if (newPosition > self.movingRangeTo ){
            newPosition = self.movingRangeTo;
            self.movingDirection = Object3DMovingDirectionDown;
        }else if (newPosition < self.movingRangeFrom){
            newPosition = self.movingRangeFrom;
            self.movingDirection = Object3DMovingDirectionUp;
        }
        
        Object3DTranslate translate;
        
        if (self.movingAxis == Object3DAxisX){
            translate.x = newPosition;
        }else if (self.movingAxis == Object3DAxisY){
            translate.y = newPosition;
        }else{
            translate.z = newPosition;
        }
        
        self.translate = translate;
        
    }
    
    self.lastFrameTime = time;
}

@end
#endif
