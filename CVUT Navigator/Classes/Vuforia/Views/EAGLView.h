/*==============================================================================
 Copyright (c) 2010-2013 QUALCOMM Austria Research Center GmbH.
 All Rights Reserved.
 Qualcomm Confidential and Proprietary
 ==============================================================================*/
#ifndef VUFORIA_DISABLE
#import "AR_EAGLView.h"
// This class wraps the CAEAGLLayer from CoreAnimation into a convenient UIView
// subclass.  The view content is basically an EAGL surface you render your
// OpenGL scene into.  Note that setting the view non-opaque will only work if
// the EAGL surface has an alpha channel.

#import "Object3DWrapper.h"

typedef NS_ENUM(NSInteger, PlacementType){
    PlacementTypeFloor,
    PlacementTypeWallStraight,
    PlacementTypeWallSide
};

@protocol EAGLViewDelegate <NSObject>

- (void) objectTypeForMarkerId: (NSInteger) markerId success: (void(^)(Object3DType, PlacementType placementType, int)) onSuccess;

@optional
- (void) mainMarkerChanged: (NSInteger) markerId;
- (void) markerDisplayed: (NSInteger) markerId;
- (void) allMarkersHidden;

@end


@interface EAGLView : AR_EAGLView
{
}

@property (nonatomic, retain) id<EAGLViewDelegate> delegate;



@end
#endif