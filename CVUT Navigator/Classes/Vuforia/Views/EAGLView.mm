/*==============================================================================
 Copyright (c) 2010-2013 QUALCOMM Austria Research Center GmbH .
 All Rights Reserved.
 Qualcomm Confidential and Proprietary
 ==============================================================================*/
#ifndef VUFORIA_DISABLE

#import "EAGLView.h"
#import "Texture.h"
#import "QCARutils.h"


#import "straight.h"
#import "left.h"
#import "right.h"
#import "gear.h"
#import "finnish.h"
#import "up.h"
#import "down.h"
#import "compass.h"
#import "reverse.h"
#import "warning.h"


#import <QCAR/Renderer.h>
#import <QCAR/VideoBackgroundConfig.h>
#import <QCAR/Marker.h>
#import <QCAR/MarkerResult.h>
#import "Object3DWrapper.h"


// OpenGL 2
#import "ShaderUtils.h"
#define MAKESTRING(x) #x

#import "RoutingService.h"
#include <sys/time.h>

namespace {
    // Letter object scale factor and translation
    const float kLetterScale = 100.0f;
    const float kLetterTranslate = 0.0f;
    
    // Texture filenames
    const char* textureFilenames[] = {
        /*"letter_Q.png",
         "letter_C.png",*/
        "texture_straight.png",
        "texture_left.png",
        "texture_gear.png",
        "texture_right.png",
        "texture_finnish.png",
        "texture_up.png",
        "texture_compass_2048.png",
        "texture_reverse.png",
        "texture_warning.png"
    };
}




@interface EAGLView ()

@property (nonatomic,retain) NSMutableDictionary * markerObjects;
@property (nonatomic, assign) NSInteger closestMarker;
@property (nonatomic,retain) NSMutableArray * markerArray;


/*Objects*/
@property (nonatomic, retain) Object3D * leftArrow;
@property (nonatomic, retain) Object3D * straightArrow;
@property (nonatomic, retain) Object3D * loadingGear;
@property (nonatomic, retain) Object3D * finnishFlag;
@property (nonatomic, retain) Object3D * upArrow;
@property (nonatomic, retain) Object3D * downArrow;
@property (nonatomic, retain) Object3D * placeholder;
@property (nonatomic, retain) Object3D * reverseArrow;
@property (nonatomic, retain) Object3D * warning;
@end


@implementation EAGLView


/*
 Objects declarations
 */

#pragma mark 3D objects

- (Object3D *)leftArrow{
    if (!_leftArrow){
        _leftArrow = [Object3DWrapper initObject3DWithNumVertices:leftNumVerts vertices:leftVerts normals:leftNormals texcoords:leftTexCoords texture:[textures objectAtIndex:1]];
    }
    
    return _leftArrow;
}

- (Object3D *)straightArrow{
    if (!_straightArrow){
        _straightArrow = [Object3DWrapper initObject3DWithNumVertices:straightNumVerts vertices:straightVerts normals:straightNormals texcoords:straightTexCoords texture:[textures objectAtIndex:0]];
    }
    return _straightArrow;
}

- (Object3D *)loadingGear{
    if (!_loadingGear){
        _loadingGear = [Object3DWrapper initObject3DWithNumVertices:gearNumVerts vertices:gearVerts normals:gearNormals texcoords:gearTexCoords texture:[textures objectAtIndex:2]];
    }
    return _loadingGear;
}

- (Object3D *)finnishFlag{
    if (!_finnishFlag){
        _finnishFlag = [Object3DWrapper initObject3DWithNumVertices:finnishNumVerts vertices:finnishVerts normals:finnishNormals texcoords:finnishTexCoords texture:[textures objectAtIndex:4]];
    }
    return _finnishFlag;
}

- (Object3D *)upArrow{
    if (!_upArrow){
        _upArrow = [Object3DWrapper initObject3DWithNumVertices:upNumVerts vertices:upVerts normals:upNormals texcoords:upTexCoords texture:[textures objectAtIndex:5]];
    }
    return _upArrow;
}

- (Object3D *)downArrow{
    if (!_downArrow){
        _downArrow = [Object3DWrapper initObject3DWithNumVertices:downNumVerts vertices:downVerts normals:downNormals texcoords:downTexCoords texture:[textures objectAtIndex:5]];
    }
    return _downArrow;
}

- (Object3D *)placeholder{
    if (!_placeholder){
        _placeholder = [Object3DWrapper initObject3DWithNumVertices:compassNumVerts vertices:compassVerts normals:compassNormals texcoords:compassTexCoords texture:[textures objectAtIndex:6]];
    }
    return _placeholder;
}

- (Object3D *)reverseArrow{
    if (!_reverseArrow){
        _reverseArrow = [Object3DWrapper initObject3DWithNumVertices:reverseNumVerts vertices:reverseVerts normals:reverseNormals texcoords:reverseTexCoords texture:[textures objectAtIndex:7]];
    }
    return _reverseArrow;
}

- (Object3D *)warning{
    if (!_warning){
        _warning = [Object3DWrapper initObject3DWithNumVertices:warningNumVerts vertices:warningVerts normals:warningNormals texcoords:warningTexCoords texture:[textures objectAtIndex:8]];
    }
    
    return _warning;
}


- (NSMutableDictionary *)markerObjects{
    if (!_markerObjects){
        _markerObjects = [[NSMutableDictionary alloc] init];
    }
    
    return _markerObjects;
}

- (NSMutableArray *)markerArray{
    if (!_markerArray){
        _markerArray = [[NSMutableArray alloc] init];
    }
    
    return _markerArray;
}




- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
	if (self)
    {
        // create list of textures we want loading - ARViewController will do this for us
        int nTextures = sizeof(textureFilenames) / sizeof(textureFilenames[0]);
        for (int i = 0; i < nTextures; ++i)
            [textureList addObject: [NSString stringWithUTF8String:textureFilenames[i]]];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(routingStateChanged)
                                                     name:@"VuforiaRoutingStateChanged"
                                                   object:nil];
        self.closestMarker = -1;
    }
    return self;
}

- (void) routingStateChanged{
    [self clearCache];
}



- (void) renderObjectForMarkerId: (NSInteger) markerId onTrackable: (const QCAR::TrackableResult*) trackable{
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(objectTypeForMarkerId:success:)]){
        
        Object3DType type = Object3DTypeLoading;
        
        //Cached types
        NSArray * renderedObjects = [self objectsFromCacheForMarker:markerId];
        
        if (!renderedObjects){
            
            //set loading state so next time it doesn't send new request;
            renderedObjects = [self getObjectsForType:Object3DTypeLoading];
            
            [self addObjectsToCache:renderedObjects forMarker:markerId];
            
            [self.delegate objectTypeForMarkerId:markerId success:^(Object3DType aType, PlacementType placementType, int placementAngle) {
                
                
                NSArray * renderedObjects = [self getObjectsForType:aType];
                
                for (Object3DWrapper * object in renderedObjects){
                    [self applyPlacementAngle:placementAngle forObject:object withType:aType placementType:placementType];
                }
                
                [self addObjectsToCache:renderedObjects  forMarker:markerId];
            }];
        }
        
        for (Object3DWrapper * object in renderedObjects){
            [self renderObject:object onTrackable:trackable];
        }
        
    }else{
        [NSException raise:NSInvalidArgumentException
                    format:@"No delegate or objectTypeForMarkerId: method"];
        
    }
    
}

- (void) applyPlacementAngle: (int) placementAngle forObject: (Object3DWrapper *) object withType: (Object3DType) aType placementType: (PlacementType) placementType{
    Object3DRotation markerRotation;
    markerRotation.x = object.rotation.x;
    markerRotation.y = object.rotation.y;
    markerRotation.z = object.rotation.z;
    
    //Loading and finish flag is still the same, not rotating with placement
    if (aType == Object3DTypeLoading || aType == Object3DTypeFinnish){
        return;
    }
    
    //Placeholder is rotated in different axis when on the wall
    if (aType == Object3DTypePlaceholder){
        if (placementType == PlacementTypeWallSide || placementType == PlacementTypeWallStraight){
            markerRotation.x = object.rotation.x - 90;
            object.rotation = markerRotation;
            
            Object3DTranslate markerTranslate;
            markerTranslate.x = object.translate.x;
            markerTranslate.y = object.translate.y + 20;
            markerTranslate.z = object.translate.z-50;
            
            object.translate = markerTranslate;
        }
        return;
    }
    
    //Rotate object in axis Z when on the floor
    if (placementType == PlacementTypeFloor){
        
        markerRotation.y = object.rotation.y;
        
        //Right arrow is flipped in 180 - we have to rotate other direction then
        if (aType == Object3DTypeRightArrow){
            markerRotation.z = object.rotation.z + (double)placementAngle;
        
        //Rotating rest of the objects on the floor (left, up, down, finnish, straight)
        }else{
            markerRotation.z = object.rotation.z - (double)placementAngle;
        }
        
    //Marker is on the wall
    }else{
        
        if (aType == Object3DTypeLeftArrow || aType == Object3DTypeRightArrow){
            //If left or right arrow is on side, we have to rotate in X by 90 and Z by placement
            if (placementType == PlacementTypeWallSide){
                markerRotation.x = object.rotation.x - 90;
                //Right arrow is flipped in 180 - we have to rotate other direction then
                markerRotation.z = object.rotation.z + ((aType == Object3DTypeRightArrow) ? (double) placementAngle : -((double) placementAngle) );
               
            }else{
                //It is on straight wall in same direction we are going, it will be same as on the floor, but rotated in Y not Z
                markerRotation.y = object.rotation.y +  ((aType == Object3DTypeRightArrow) ? (double) placementAngle : -((double) placementAngle) );
                
            }
        }else if (aType == Object3DTypeStraightArrow){
            if (placementType == PlacementTypeWallSide){
                //Straight arrow on side, have to be rotate by Z and then it's directions is rotated with Y axis
                markerRotation.z = object.rotation.z + 90;
                markerRotation.y = object.rotation.y - 90 - (double) placementAngle;
            }else{
                //Same as on the door, but rotated in Y
                markerRotation.y = object.rotation.y - (double)placementAngle;
            }
        }else if (aType == Object3DTypeUpArrow || aType == Object3DTypeDownArrow){
            //Elevator or stairs on the wall, it's not rotated by placement, only axis are changed
            markerRotation.z = object.rotation.z;
            markerRotation.y = object.rotation.y;
            markerRotation.x = object.rotation.x - 90;
            
            [object setMovingAxis:Object3DAxisY];
            [object setMovingRangeFrom:-15];
            [object setMovingRangeTo:15];
            
        }else if (aType == Object3DTypeFinnish){
            //Not rotated by placement, axis are changed
            markerRotation.z = object.rotation.z;
            markerRotation.y = object.rotation.y - 90;
            markerRotation.x = object.rotation.x;
        }
    }
    
    object.rotation = markerRotation;
}

#pragma mark Cache
- (NSArray *) objectsFromCacheForMarker: (NSInteger) markerId{
    NSArray * objects = [self.markerObjects objectForKey:[NSNumber numberWithInt:markerId]];
    return objects;
}

- (void)addObjectsToCache: (NSArray *) objects forMarker: (NSInteger) markerId{
    [self.markerObjects setObject:objects forKey:[NSNumber numberWithInt:markerId]];
}

- (void) clearCache{
    self.closestMarker = -1;
    [self.markerObjects removeAllObjects];
}

- (void)clearCacheMarkersBesides: (NSArray *) markersArray{
    NSMutableDictionary * newMarkerObjects = [[NSMutableDictionary alloc] init];
    
    for (NSNumber * markerId in markersArray){
        [newMarkerObjects setObject:[self objectsFromCacheForMarker: [markerId integerValue]] forKey:[NSNumber numberWithInt:[markerId integerValue]]];
    }
    
    [self.markerObjects removeAllObjects];
    self.markerObjects = nil;
    
    self.markerObjects = newMarkerObjects;
}

- (NSArray *)getObjectsForType: (Object3DType) type{
    NSMutableArray * objects = [[NSMutableArray alloc] initWithCapacity:2];
    /*Default objects settings*/
    Object3DRotation rotation;
    rotation.x = 0;
    rotation.y = 0;
    rotation.z = 0;
    
    Object3DScale scale;
    scale.x =100.0f;
    scale.y =100.0f;
    scale.z =100.0f;
    
    Object3DTranslate translate;
    translate.x = 0;
    translate.y = 0;
    translate.z = 0;
    
    
    if (type == Object3DTypeLeftArrow){
        Object3DWrapper * object = [[Object3DWrapper alloc] initWithObject3D: self.leftArrow translate:translate rotation: rotation scale: scale ];
        [objects addObject:object];
        
    }else if (type == Object3DTypeRightArrow){
        rotation.y = 180;
        Object3DWrapper * object = [[Object3DWrapper alloc] initWithObject3D: self.leftArrow translate:translate rotation: rotation scale: scale ];
        [objects addObject:object];
        
    }else if (type == Object3DTypeStraightArrow){
        Object3DWrapper * object = [[Object3DWrapper alloc] initWithObject3D: self.straightArrow translate:translate rotation: rotation scale: scale ];
        [objects addObject:object];
        
    }else if (type == Object3DTypeLoading){
        /*Two rotating gears*/
        scale.x =50.0f;
        scale.y =50.0f;
        scale.z =50.0f;
        
        Object3DWrapper * bigOne = [[Object3DWrapper alloc] initWithObject3D: self.loadingGear translate:translate rotation: rotation scale: scale ];
        [bigOne startRotationWithSpeed:0.5 direction:Object3DRotationDirectionClockWise ofAxis:Object3DAxisZ];
        [objects addObject:bigOne];
        
        Object3DRotation rotationSmall;
        Object3DScale scaleSmall;
        Object3DTranslate translateSmall;
        
        scaleSmall.x =25.0f;
        scaleSmall.y =25.0f;
        scaleSmall.z =50.0f;
        
        translateSmall.y = -26.0f;
        translateSmall.z = 0.0f;
        translateSmall.x = 26.0f;
        
        rotationSmall.x = 0;
        rotationSmall.y = 0;
        rotationSmall.z = 0;
        
        Object3DWrapper * smallOne = [[Object3DWrapper alloc] initWithObject3D: self.loadingGear translate:translateSmall rotation: rotationSmall scale: scaleSmall ];
        [smallOne startRotationWithSpeed:4 direction:Object3DRotationDirectionClockWise ofAxis:Object3DAxisZ];
        
        [objects addObject:smallOne];
        
    }else if (type == Object3DTypeFinnish){
        scale.x =75.0f;
        scale.y =75.0f;
        scale.z =75.0f;
        rotation.x = 90.0f;
        translate.z = 55.0f;
        Object3DWrapper * object = [[Object3DWrapper alloc] initWithObject3D: self.finnishFlag translate:translate rotation: rotation scale: scale ];
        [objects addObject:object];
        
    }else if (type == Object3DTypeUpArrow){
        scale.x =100.0f;
        scale.y =50.0f;
        scale.z =100.0f;
        translate.z = 30.0f;
        Object3DWrapper * object = [[Object3DWrapper alloc] initWithObject3D: self.upArrow translate:translate rotation: rotation scale: scale ];
        [object startMovementWithSpeed:1 rangeFrom:30.0f rangeTo:60.0f ofAxis:Object3DAxisZ];
        [objects addObject:object];
        
    }else if (type == Object3DTypeDownArrow){
        scale.x =100.0f;
        scale.y =50.0f;
        scale.z =100.0f;
        translate.z = 30.0f;
        Object3DWrapper * object = [[Object3DWrapper alloc] initWithObject3D: self.downArrow translate:translate rotation: rotation scale: scale ];
        [object startMovementWithSpeed:1 rangeFrom:30.0f rangeTo:60.0f ofAxis:Object3DAxisZ];
        [objects addObject:object];
        
    }else if (type == Object3DTypePlaceholder){
        scale.x =100.0f;
        scale.y =100.0f;
        scale.z =100.0f;
        translate.z = 50.0f;
        
        Object3DWrapper * object = [[Object3DWrapper alloc] initWithObject3D: self.placeholder translate:translate rotation: rotation scale: scale ];
        [object startRotationWithSpeed:0.5 direction:Object3DRotationDirectionClockWise ofAxis:Object3DAxisZ];
        [objects addObject:object];
    }else if (type == Object3DTypeReverseArrow){
        rotation.x = 90;
        rotation.z = 180;
        Object3DWrapper * object = [[Object3DWrapper alloc] initWithObject3D: self.reverseArrow translate:translate rotation: rotation scale: scale ];
        [objects addObject:object];
    }else if (type == Object3DTypeWarning){
        rotation.x = 90.0f;
        scale.x = 70.0f;
        scale.y = 100.0f;
        scale.z = 70.0f;
        
        translate.y = -25.0f;
        translate.z = 5.0f;
        
        Object3DWrapper * object = [[Object3DWrapper alloc] initWithObject3D: self.warning translate:translate rotation: rotation scale: scale ];
        [objects addObject:object];
    }
    
    return objects;
}



#pragma mark Rendering

- (void) renderObject: (Object3DWrapper *) obj3D onTrackable: (const QCAR::TrackableResult*) trackable {
    QCAR::Matrix34F pose = trackable->getPose();
    QCAR::Matrix44F modelViewMatrix = QCAR::Tool::convertPose2GLMatrix(pose);
    
    ShaderUtils::translatePoseMatrix(obj3D.translate.x, obj3D.translate.y, obj3D.translate.z, &modelViewMatrix.data[0]);
    
    ShaderUtils::rotatePoseMatrix(obj3D.rotation.x, 1.0f, 0, 0, &modelViewMatrix.data[0]);
    ShaderUtils::rotatePoseMatrix(obj3D.rotation.y , 0, 1.0f, 0, &modelViewMatrix.data[0]);
    ShaderUtils::rotatePoseMatrix(obj3D.rotation.z, 0, 0, 1.0f, &modelViewMatrix.data[0]);
    
    ShaderUtils::scalePoseMatrix(obj3D.scale.x, obj3D.scale.y, obj3D.scale.z, &modelViewMatrix.data[0]);
    
    //Animation
    [obj3D animateObject];
    
    [self renderObject: obj3D withMatrix: modelViewMatrix];
    
}


- (void) renderObject: (Object3DWrapper *) obj3D withMatrix: (QCAR::Matrix44F) modelViewMatrix {
    // Render with OpenGL 2
    QCAR::Matrix44F modelViewProjection;
    
    ShaderUtils::multiplyMatrix(&qUtils.projectionMatrix.data[0], &modelViewMatrix.data[0], &modelViewProjection.data[0]);
    
    
    glUseProgram(shaderProgramID);
    
    glVertexAttribPointer(vertexHandle, 3, GL_FLOAT, GL_FALSE, 0, obj3D.originalObject.vertices);
    glVertexAttribPointer(normalHandle, 3, GL_FLOAT, GL_FALSE, 0, obj3D.originalObject.normals);
    glVertexAttribPointer(textureCoordHandle, 2, GL_FLOAT, GL_FALSE, 0, obj3D.originalObject.texCoords);
    
    glEnableVertexAttribArray(vertexHandle);
    glEnableVertexAttribArray(normalHandle);
    glEnableVertexAttribArray(textureCoordHandle);
    
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, [obj3D.originalObject.texture textureID]);
    glUniformMatrix4fv(mvpMatrixHandle, 1, GL_FALSE, (GLfloat*)&modelViewProjection.data[0]);
    glUniform1i(texSampler2DHandle, 0 );
    
    glDrawArrays(GL_TRIANGLES, 0, obj3D.originalObject.numVertices);
    
    ShaderUtils::checkGlError("FrameMarkers renderFrameQCAR");
}


////////////////////////////////////////////////////////////////////////////////
// Draw the current frame using OpenGL
//
// This method is called by QCAR when it wishes to render the current frame to
// the screen.
//
// *** QCAR will call this method on a single background thread ***
- (void)renderFrameQCAR
{
    if (APPSTATUS_CAMERA_RUNNING == qUtils.appStatus) {
        [self setFramebuffer];
        
        //glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        
        // Clear colour and depth buffers
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        
        // Retrieve tracking state and render video background and
        QCAR::State state = QCAR::Renderer::getInstance().begin();
        QCAR::Renderer::getInstance().drawVideoBackground();
        
        glEnable(GL_DEPTH_TEST);
        // We must detect if background reflection is active and adjust the culling direction.
        // If the reflection is active, this means the pose matrix has been reflected as well,
        // therefore standard counter clockwise face culling will result in "inside out" models.
        glDisable(GL_CULL_FACE);
        //glCullFace(GL_BACK);
        if(QCAR::Renderer::getInstance().getVideoBackgroundConfig().mReflection == QCAR::VIDEO_BACKGROUND_REFLECTION_ON)
            glFrontFace(GL_CW);  //Front camera
        else
            glFrontFace(GL_CCW);   //Back camera
        
        static int lastMarkerResultsCount = 0;
        
        int markerResultsCount = 0;
        
        double closestDistance = -1;
        NSInteger closestMarker = -1;
        
        [self.markerArray removeAllObjects];
        
        // Did we find any trackables this frame?
        for(int i = 0; i < state.getNumTrackableResults(); ++i) {
            // Get the trackable
            const QCAR::TrackableResult* trackableResult = state.getTrackableResult(i);
            
            // Check the type of the trackable:
            if (trackableResult->getType() == QCAR::TrackableResult::MARKER_RESULT){
                
                const QCAR::MarkerResult* markerResult = static_cast<const QCAR::MarkerResult*>(trackableResult);
                const QCAR::Marker& marker = markerResult->getTrackable();
                
                // Choose the object and texture based on the marker ID
                int markerId = marker.getMarkerId();
                
                [self.markerArray addObject:[NSNumber numberWithInteger:markerId]];
                
                QCAR::Matrix34F pose = trackableResult->getPose();
                
                QCAR::Vec3F position(pose.data[3], pose.data[7], pose.data[11]);
                float distance = sqrt(position.data[0] * position.data[0] +
                                      position.data[1] * position.data[1] +
                                      position.data[2] * position.data[2]);
                
                if (distance < closestDistance || closestDistance == -1){
                    closestDistance = distance;
                    closestMarker = markerId;
                }
                
                [self renderObjectForMarkerId:markerId onTrackable:trackableResult];
                
                markerResultsCount++;
                
            }
        }
        
        if (closestMarker != -1 && closestMarker != self.closestMarker){
            self.closestMarker = closestMarker;
            if (self.delegate && [self.delegate respondsToSelector:@selector(mainMarkerChanged:)]){
                [self.delegate mainMarkerChanged: closestMarker];
            }
        }
        
        
        //[self clearCacheMarkersBesides: self.markerArray];
        
        
        
        /*if (markerResultsCount == 0 && lastMarkerResultsCount > 0){
         if (self.delegate && [self.delegate respondsToSelector:@selector(allMarkersHidden)]){
         [self.delegate allMarkersHidden];
         [self.showedObjects removeAllObjects];
         }
         }*/
        
        lastMarkerResultsCount = markerResultsCount;
        
        
        glDisable(GL_DEPTH_TEST);
        glDisable(GL_CULL_FACE);
        glDisableVertexAttribArray(vertexHandle);
        glDisableVertexAttribArray(normalHandle);
        glDisableVertexAttribArray(textureCoordHandle);
        
        QCAR::Renderer::getInstance().end();
        [self presentFramebuffer];
    }
}

@end

#endif
