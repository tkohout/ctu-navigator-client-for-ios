/*==============================================================================
Copyright (c) 2010-2013 QUALCOMM Austria Research Center GmbH .
All Rights Reserved.
Qualcomm Confidential and Proprietary
==============================================================================*/
#ifndef VUFORIA_DISABLE

#import <UIKit/UIKit.h>
#import "EAGLView.h"

typedef NS_ENUM(NSInteger, ControlsOverlayMode) {
    ControlsOverlayModeNoPosition,
    ControlsOverlayModePositionFound,
    ControlsOverlayModeRouting,
    ControlsOverlayModeLoading,
    ControlsOverlayModeError
};
@class Plan, Node;

@protocol ControlsOverlayDelegate <NSObject>
- (Node *) nodeForPlanView;
@optional
    - (void) navigateButtonTapped;
    - (void) mapButtonTapped;
    - (void) routingCancelled;
@end

@interface ControlsOverlayViewController : UIViewController 

@property (nonatomic, retain) id<ControlsOverlayDelegate> delegate;
@property (nonatomic, retain) UIView * topBar;
@property (nonatomic, assign) ControlsOverlayMode mode;
- (void)setErrorModeWithMessage: (NSString *)message;

- (void)showMinimizedTopbarAnimated: (BOOL) animated;
- (void)showFullTopbarAnimated: (BOOL) animated;

- (void)showDirections: (NSString *) directions;




@end
#endif