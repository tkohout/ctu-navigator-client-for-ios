/*============================================================================
 Copyright (c) 2010-2013 QUALCOMM Austria Research Center GmbH .
 All Rights Reserved.
 Qualcomm Confidential and Proprietary
 ============================================================================*/
#ifndef VUFORIA_DISABLE

#import "VuforiaParentViewController.h"
#import "ControlsOverlayViewController.h"
#import "OverlayViewController.h"
#import "ARViewController.h"
#import "EAGLView.h"
#import "QCARutils.h"
#import <QuartzCore/QuartzCore.h>

#import "RoutingService.h"
#import "DataService.h"
#import "RouteViewController.h"
#import "Object3DWrapper.h"
#import "LocationService.h"


#import "Node.h"
#import "Building.h"
#import "Floor.h"
#import "Marker.h"
#import "PlanViewController.h"

#import "UtilitiesGeo.h"
#import "Reachability.h"


extern bool isInterfaceOrientationPortrait;

@interface VuforiaParentViewController() <PlanViewControllerDelegate>

@property (nonatomic, retain) Building * currentLocationBuilding;
@property (nonatomic, assign) BOOL isLoading;
@end



@implementation VuforiaParentViewController // subclass of ARParentViewController


- (void)setCurrentLocationBuilding:(Building *)currentLocationBuilding{
    if (currentLocationBuilding && currentLocationBuilding != _currentLocationBuilding){
        _currentLocationBuilding = currentLocationBuilding;
        //[controlsOverlayVC performSelectorOnMainThread:@selector(showMapViewWithPlan:) withObject:currentLocationBuilding.plan waitUntilDone:NO];
    }
}


-(void)setAppWindow:(UIWindow *)window{
    appWindow = window;
}
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
    [self createParentViewAndSplashContinuation];
    
    // Add the EAGLView and the overlay view to the window
    arViewController = [[ARViewController alloc] init];
    
    // need to set size here to setup camera image size for AR
    //arViewController.arViewSize = arViewRect.size;
    arViewController.arViewSize = self.arViewSize;
    arViewController.arParentViewController = self;
    
    [parentView addSubview:arViewController.view];
    
    // Create an auto-rotating overlay view and its view controller (used for
    // displaying buttons)
    controlsOverlayVC = [[ControlsOverlayViewController alloc] init];
    controlsOverlayVC.delegate = self;
    //dominoesSetButtonOverlay(buttonOverlayVC);
    //[controlsOverlayVC setMenuCallBack:@selector(showMenu) forTarget:self];
    
    [parentView addSubview: controlsOverlayVC.view];
    
    // Hide the AR and button overlay views so the parent view can be seen
    // during start-up (the parent view contains the splash continuation image
    // on iPad and is empty on iPhone and iPod)
    [arViewController.view setHidden:YES];
    [controlsOverlayVC.view setHidden:NO];
    //buttonOverlayVC.view.backgroundColor = [UIColor redColor];
    // Create an auto-rotating overlay view and its view controller (used for
    // displaying UI objects, such as the camera control menu)
    overlayViewController = [[OverlayViewController alloc] init];
    [parentView addSubview: overlayViewController.view];
    
    self.view = parentView;
    
}

- (void)createParentViewAndSplashContinuation
{
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    
    // iPhone and iPod - create the parent view, but don't populate it with
    // an image
    parentView = [[UIImageView alloc] initWithFrame:arViewRect];
    
    // Create a splash view
    splashView = [[UIView alloc] initWithFrame:screenBounds];
    splashView.backgroundColor = [UIColor blackColor];
    
    if (self.tabBarController){
        splashView.frame = CGRectMake(screenBounds.origin.x, screenBounds.origin.y, screenBounds.size.width, screenBounds.size.height - self.tabBarController.tabBar.frame.size.height);
    }else{
        splashView.frame = screenBounds;
    }
    NSInteger width = screenBounds.size.width-40;
    NSInteger height = 60;
    
    UILabel * loadingText = [[UILabel alloc] initWithFrame:CGRectMake(splashView.center.x - width/2, splashView.center.y-height/2, width, height)];
    loadingText.textAlignment = NSTextAlignmentCenter;
    loadingText.textColor = [UIColor whiteColor];
    loadingText.backgroundColor = [UIColor clearColor];
    loadingText.text = NSLocalizedString(@"Načítám rozhraní rozšířené reality ...", nil);
    loadingText.numberOfLines = 0;
    loadingText.font = [UIFont boldSystemFontOfSize:18.0f];
    UIActivityIndicatorView * spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    spinner.center = CGPointMake(splashView.center.x, splashView.center.y - 70);
    [spinner startAnimating];
    
    [splashView addSubview:loadingText];
    [splashView addSubview:spinner];
    
    // Add the splash view directly to the window (this prevents the splash
    // view from rotating, so it is always portrait)
    [appWindow addSubview:splashView];
    
    
    // userInteractionEnabled defaults to NO for UIImageViews
    [parentView setUserInteractionEnabled:YES];
    
    // Poll to see if the camera video stream has started and if so remove the
    // splash screen
    [NSTimer scheduledTimerWithTimeInterval:0.2 target:self selector:@selector(endSplash:) userInfo:nil repeats:YES];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self sendRoutingStateChangedNotification];
    if ([RoutingService sharedService].isRouting){
        if (self.isLoading){
            [controlsOverlayVC setMode:ControlsOverlayModeLoading];
        }else{
            [controlsOverlayVC setMode:ControlsOverlayModeRouting];
            //[controlsOverlayVC showDirections:@""];
        }
    }else if ([LocationService sharedService].currentLocationType == LocationServiceCurrentLocationTypeIndoor){
        [controlsOverlayVC setMode:ControlsOverlayModePositionFound];
    }else{
        [controlsOverlayVC setMode:ControlsOverlayModeNoPosition];
    }
}

- (void)setIsLoading:(BOOL)isLoading{
    _isLoading = isLoading;
}

#pragma mark EAGLViewDelegate

- (void) setControlsOverlayMode: (NSNumber *) mode{
    [controlsOverlayVC setMode:((ControlsOverlayMode)[mode integerValue])];
}

- (void) reloadControls{
    ControlsOverlayMode mode;
    if (self.isLoading){
        mode = ControlsOverlayModeLoading;
    }else if ([RoutingService sharedService].isRouting){
        mode = ControlsOverlayModeRouting;
    }else if ([LocationService sharedService].currentIndoorLocationNode){
        mode = ControlsOverlayModePositionFound;
    }else{
        mode = ControlsOverlayModeNoPosition;
    }
    [self performSelectorOnMainThread: @selector(setControlsOverlayMode:) withObject:[NSNumber numberWithInt: mode] waitUntilDone:NO];
}

- (void)mainMarkerChanged:(NSInteger)markerId{
    //self.isLoading = YES;
    [self reloadControls];
    
    Node * node = [[DataService sharedService] getNodeForMarkerIdFromDatabase:markerId];
    if (node){
        [LocationService sharedService].currentIndoorLocationNode = node;
        
        self.currentLocationBuilding = node.building;
        
        if ([RoutingService sharedService].isRouting){
            NSString * directionsText;
            if ([[RoutingService sharedService] isNodeInRoutePath:node]){
                
                NSInteger step = [[RoutingService sharedService] getStepForNode:node addToStepsWhenMissing: YES];
                if (step != NSNotFound){
                    [RoutingService sharedService].currentStep = step;
                }
            
            directionsText = [[RoutingService sharedService] getDirectionTextForIndoorNode:node];
        }else{
            directionsText = NSLocalizedString(@"Cesta nebyla nalezena, zkuste jiný marker", nil);
        }
        
        [self performSelectorOnMainThread:@selector(showDirections:) withObject:directionsText waitUntilDone:NO];
    }
    }else{
        //[self showErrorInTopControl:NSLocalizedString(@"Marker se nepodařilo stáhnout", nil)];
    }
}

- (void) showErrorInTopControl: (NSString *) errorMessage{
    if ([RoutingService sharedService].isRouting){
        [self performSelectorOnMainThread:@selector(setControlsOverlayMode:) withObject:[NSNumber numberWithInteger:ControlsOverlayModeRouting] waitUntilDone:NO];
        [controlsOverlayVC performSelectorOnMainThread:@selector(showDirections:) withObject:errorMessage waitUntilDone:NO];
    }else{
        [controlsOverlayVC performSelectorOnMainThread:@selector(setErrorModeWithMessage:) withObject:errorMessage waitUntilDone:NO];
    }
}

- (void) showDirections: (NSString *) directions{
    [controlsOverlayVC showDirections:directions];
}

- (void) backgroundMarkerLoad: (NSDictionary *) args{
    NSInteger markerId = [[args objectForKey:@"markerId"] integerValue];
    void (^onSuccess)(Object3DType, PlacementType, int) = [args objectForKey:@"onSuccess"];
}

- (void) objectTypeForMarkerId:(NSInteger)markerId success:(void (^)(Object3DType, PlacementType, int))onSuccess{
    self.isLoading = YES;
    [self reloadControls];
    
    [[DataService sharedService] getNodeForMarkerId:markerId success:^(Node * node) {
        [LocationService sharedService].currentIndoorLocationNode = node;
        
        //If there is route path loaded
        if ([[RoutingService sharedService] isLoadedRoutePathForBuilding:node.building]){
            
            //If our node is in that route path
            if ([[RoutingService sharedService] isNodeInRoutePath:node]){
                
                //Angle in degrees in which we should turn on current marker
                int turnHeading = [[RoutingService sharedService] getTurnHeadingInRoutePathForNode:node];
                
                //Order position type of node in the route path
                RoutePathNodeType type = [[RoutingService sharedService] getRoutePathTypeForNode:node];
                
                //Heading that is remaining from whole turn heading (if turnAngle is -50 we go left, but still has to turn remaining 5)
                int remainingHeading = 0;
                
                //Type of the object that will be displayed on the marker
                Object3DType objectType;
                
                
                //It is the elevator or stairs
                if ([[RoutingService sharedService] isNodeStairsOrElevatorInRoutePath:node]){
                    Node * nextNode = [[RoutingService sharedService] getNextNodeInRoutePathForNode:node];
                    NSInteger currentFloor = [node.floorId  integerValue];
                    NSInteger nextFloor = [nextNode.floorId  integerValue];
                    
                    if (nextFloor > currentFloor){
                        objectType = Object3DTypeUpArrow;
                    }else{
                        objectType = Object3DTypeDownArrow;
                    }
                
                //Normal turn
                }else if (type == RoutePathNodeTypeMiddle){
                    objectType = [self getObjectTypeForHeading: turnHeading remainingHeading: &remainingHeading];
                }else if (type == RoutePathNodeTypeFirst){
                    objectType = Object3DTypeStraightArrow;
                }else if (type == RoutePathNodeTypeLast || type == RoutePathNodeTypeOnlyOne){
                    objectType = Object3DTypeFinnish;
                }
                
                int placementAngle;
                
                PlacementType placementType = PlacementTypeFloor;
                placementAngle= [self getPlacementAngleForNode:node placementType:&placementType];
                
                self.isLoading = NO;
                [self reloadControls];
                
                int degrees1 = placementAngle + remainingHeading;
                //int degrees2 = sumDegrees(placementAngle, remainingHeading);
               
                onSuccess(objectType, placementType, degrees1 );
            }else{
                [self rerouteFrom: node success:^{
                    self.isLoading = NO;
                } failure:^(NSError * error){
                    self.isLoading = NO;
                    if (error.code == APIErrorNotFound){
                        [DataService showErrorMessageWithTitle:NSLocalizedString(@"Cesta nebyla nalezena", nil) subtitle:nil onView:self.view];
                    }else{
                        [DataService showError:error messageOnView:self.view];
                    }
                    
                    
                    onSuccess(Object3DTypeWarning, ([node.marker.type isEqualToString:@"wall"]) ? PlacementTypeWallStraight : PlacementTypeFloor, 0);
                    [self reloadControls];
                    [self showErrorInTopControl:NSLocalizedString(@"Cesta nebyla nalezena, zkuste jiný marker", nil)];
                }];
                
                onSuccess(Object3DTypeLoading, ([node.marker.type isEqualToString:@"wall"]) ? PlacementTypeWallStraight : PlacementTypeFloor, 0);
            }
            
        }else{
            Object3DType type;
            
            if ([RoutingService sharedService].isRouting){
                type = Object3DTypeLoading;
            }else{
                type = Object3DTypePlaceholder;
                self.isLoading = NO;
            }
            [self reloadControls];
            onSuccess(type, ([node.marker.type isEqualToString:@"wall"]) ? PlacementTypeWallStraight : PlacementTypeFloor, 0);
        }
        
    } failure:^(NSError *error) {
        self.isLoading = NO;
        NSString * errorMessage;
        
        if (error.code == APIErrorNotFound){
            errorMessage = NSLocalizedString(@"Marker se nepodařilo stáhnout", nil);
        }else if ([DataService getNetworkErrorTypeFor:error] == NetworkErrorTypeConnection){
            errorMessage = NSLocalizedString(@"Nelze se připojit k Internetu", nil);
            
            [[DataService sharedService] startCheckingForReachability:^{
                //We are online again
                //Reload markers in eaglview
                [self sendRoutingStateChangedNotification];
                
                [self reloadControls];
                
                //Stop checking for reachability, we've found it
                [[DataService sharedService] stopCheckingForReachability];
            }];
        }else{
            errorMessage = NSLocalizedString(@"Nelze se připojit k Internetu", nil);
        }
        
        [self showErrorInTopControl:errorMessage];
        [DataService showErrorMessageWithTitle:nil subtitle:errorMessage onView:nil];
        
        onSuccess(Object3DTypeWarning, PlacementTypeFloor, 0);
        ;
    }];
    
    //[self performSelectorInBackground:@selector(backgroundMarkerLoad:) withObject:@{@"markerId":[NSNumber numberWithInt:markerId], @"onSuccess":onSuccess}];
}

- (Object3DType) getObjectTypeForHeading: (int) heading remainingHeading: (int *) remainingHeading{
    heading = normalize180(heading);
    
    if (heading >= - 45 && heading <= 45){
        *remainingHeading = heading;
        return Object3DTypeStraightArrow;
    }else if (heading > 45 && heading <= 180 ){
        *remainingHeading = heading-90;
        return Object3DTypeRightArrow;
    }else{
        *remainingHeading = heading+90;
        return Object3DTypeLeftArrow;
    }
}

- (int) getPlacementAngleForNode: (Node *) node placementType: (PlacementType *) placementType{
    //Angle in which marker is on the floor or wall in direction to the north
    int markerAngle = [node.marker.angle intValue];
    //markerAngle = 135;
    //Heading which we should came from, according to route. If it is first point of the route, it is the direction to next node.
    int currentHeading;
    
    int placementAngle;
    
    RoutePathNodeType type =[[RoutingService sharedService] getRoutePathTypeForNode:node];
    
    
    
    if (type == RoutePathNodeTypeFirst){
        currentHeading = [[RoutingService sharedService] getNextHeadingInRoutePathForNode:node];
    }else{
        currentHeading = [[RoutingService sharedService] getPreviousHeadingInRoutePathForNode:node];
    }
    
    //Difference between marker angle and our current heading. Tell us how should we position object on marker
    int difference = differenceBetweenDegrees(currentHeading, markerAngle);
    
    //If the marker is on the wall
    if ([node.marker.type isEqualToString:@"wall"]){
        
        //Marker is in the same direction as we are going (e.g. on the doors)
        if (difference <= 10 && difference >= -10){
            placementAngle = difference;
            *placementType = PlacementTypeWallStraight;
            //Marker is on the right side
        }else if (difference > 10 && difference < 170){
            *placementType = PlacementTypeWallSide;
            placementAngle = -difference;
            
            //Marker is on the left side
        }else if (difference < - 10 && difference > - 170){
            *placementType = PlacementTypeWallSide;
            placementAngle = - difference;
            
            //Reverse direction
        }else{
            placementAngle = -difference;
            //objectType = Object3DTypeReverseArrow;
            *placementType = PlacementTypeWallStraight;
        }
        
    }else{
        placementAngle = -difference;
        *placementType = PlacementTypeFloor;
    }
    
    return placementAngle;
}

#pragma mark RouteViewControllerDelegate
- (void) routingStartedStart: (RoutePoint *) start end: (RoutePoint *) end{
    [self sendRoutingStateChangedNotification];
    self.isLoading = YES;
}

#pragma mark ControlsOverlayDelegate

- (void)navigateButtonTapped{
    RouteViewController * controller = [self.storyboard instantiateViewControllerWithIdentifier:@"RouteViewController"];
    controller.senderController = self;
    controller.delegate = self;
    RoutePoint * startPoint = [[RoutePoint alloc] init];
    startPoint.item = CURRENT_LOCATION;
    
    
    [self presentModalViewController:controller animated:YES];
    
    controller.start = startPoint;
}

- (Node *)nodeForPlanView{
    return [LocationService sharedService].currentIndoorLocationNode;
}

- (void)mapButtonTapped{
    Node * currentNode = [LocationService sharedService].currentIndoorLocationNode;
    
    if (currentNode){
        
        PlanViewController * planViewController = [[PlanViewController alloc] initCenteringOnNode: currentNode animated: NO];
        planViewController.delegate = self;
        [self presentModalViewController:planViewController animated:YES];
        
    }
}

- (void)planViewControllerDismissed{
    [controlsOverlayVC showMinimizedTopbarAnimated:NO];
}

- (void)routingCancelled{
    [[RoutingService sharedService] routeCancel];
    controlsOverlayVC.mode = ControlsOverlayModePositionFound;
    [self sendRoutingStateChangedNotification];
}


- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (void)viewDidLoad
{
    NSLog(@"DomParentVC: loading");
    // it's important to do this from here as arViewController has the wrong idea of orientation
    [self handleARViewRotation:self.interfaceOrientation];
    // we also have to set the overlay view to the correct width/height for the orientation
    [overlayViewController handleViewRotation:self.interfaceOrientation];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    NSLog(@"DomParentVC: appearing");
    // make sure we're oriented/sized properly before reappearing/restarting
    [self handleARViewRotation:self.interfaceOrientation];
    [overlayViewController handleViewRotation:self.interfaceOrientation];
    [arViewController viewWillAppear:animated];
}

// This is called on iOS 4 devices (when built with SDK 5.1 or 6.0) and iOS 6
// devices (when built with SDK 5.1)
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation duration:(NSTimeInterval)duration
{
    // ensure overlay size and AR orientation is correct for screen orientation
    [overlayViewController handleViewRotation:self.interfaceOrientation];
    [self handleARViewRotation:interfaceOrientation];
    
    if (YES == [arViewController.view isHidden] && UIInterfaceOrientationIsLandscape([self interfaceOrientation])) {
        // iPad - the interface orientation is landscape, so we must switch to
        // the landscape splash image
        [super updateSplashScreenImageForLandscape];
    }
}

- (void) handleARViewRotation:(UIInterfaceOrientation)interfaceOrientation
{
    CGPoint centre, pos;
    NSInteger rot;
    
    // Set the EAGLView's position (its centre) to be the centre of the window, based on orientation
    centre.x = arViewController.arViewSize.width / 2;
    centre.y = arViewController.arViewSize.height / 2;
    
    BOOL largeScreen = NO ;
    
    if (self.view.frame.size.height > 480) {
        // iPad and iPhone5
        largeScreen = YES;
    }
    
    int yOffset = 200;
    if (YES == largeScreen)
        yOffset = 300;
    
    CGFloat buttonWidth = 70.0;
    CGFloat buttonHeight = 30.0;
    
    CGRect viewBounds;
    viewBounds.origin.x = 0;
    viewBounds.origin.y = 0;
    
    if (interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight)
    {
        if (interfaceOrientation == UIInterfaceOrientationLandscapeLeft)
        {
            NSLog(@"DomParentVC: Rotating to Landscape Left");
            rot = 180;
        }
        else
        {
            NSLog(@"DomParentVC: Rotating to Landscape Right");
            rot=0;
        }
        
        isInterfaceOrientationPortrait= false;
        pos.x = centre.y;
        pos.y = centre.x;
        /*controlsOverlayVC.menuButton.frame = CGRectMake(10,self.view.frame.size.height-yOffset, buttonWidth, buttonHeight);
         controlsOverlayVC.clearButton.frame = CGRectMake(self.view.frame.size.height - 180,self.view.frame.size.width-40, buttonWidth, buttonHeight);
         controlsOverlayVC.resetButton.frame = CGRectMake(self.view.frame.size.height - 100,self.view.frame.size.width-40, buttonWidth, buttonHeight);
         controlsOverlayVC.runButton.frame = CGRectMake(self.view.frame.size.height - 100,self.view.frame.size.width-40, buttonWidth, buttonHeight);
         controlsOverlayVC.deleteButton.frame = CGRectMake(self.view.frame.size.height - 100, 10, buttonWidth, buttonHeight); */
        
        viewBounds.size.width = arViewController.arViewSize.height;
        viewBounds.size.height = arViewController.arViewSize.width;
        
    }
    else
    {
        if (interfaceOrientation == UIInterfaceOrientationPortrait)
        {
            NSLog(@"DomParentVC: Rotating to Portrait");
            rot = 90;
        }
        else
        {
            NSLog(@"DomParentVC: Rotating to Upside Down");
            rot = 270;
        }
        isInterfaceOrientationPortrait= true;
        pos = centre;
        /*controlsOverlayVC.menuButton.frame = CGRectMake(10,self.view.frame.size.height-50, buttonWidth, buttonHeight);
         controlsOverlayVC.clearButton.frame = CGRectMake(self.view.frame.size.width - 160,self.view.frame.size.height-50, buttonWidth, buttonHeight);
         controlsOverlayVC.resetButton.frame = CGRectMake(self.view.frame.size.width - 80,self.view.frame.size.height-50, buttonWidth, buttonHeight);
         controlsOverlayVC.runButton.frame = CGRectMake(self.view.frame.size.width - 80,self.view.frame.size.height-50, buttonWidth, buttonHeight);
         controlsOverlayVC.deleteButton.frame = CGRectMake(self.view.frame.size.width - 80, 10, buttonWidth, buttonHeight);*/
        
        viewBounds.size.width = arViewController.arViewSize.width;
        viewBounds.size.height = arViewController.arViewSize.height;
        
    }
    [controlsOverlayVC.view setFrame:viewBounds];
    arViewController.arView.layer.position = pos;
    CGAffineTransform rotate = CGAffineTransformMakeRotation(rot * M_PI  / 180);
    arViewController.arView.transform = rotate;
}


// provoke the menu pop-up
- (void) showMenu
{
    [overlayViewController showOverlay];
}

#pragma mark -
#pragma mark Splash screen control
- (void)endSplash:(NSTimer*)theTimer
{
    // Poll to see if the camera video stream has started and if so remove the
    // splash screen
    [super endSplash:theTimer];
    
    if ([QCARutils getInstance].videoStreamStarted == YES)
    {
        // Display the button overlay view
        [controlsOverlayVC.view setHidden:NO];
        [controlsOverlayVC showMinimizedTopbarAnimated:YES];
    }
}

- (void) routePathLoaded{
    self.isLoading = NO;
    controlsOverlayVC.mode = ControlsOverlayModeRouting;
    //[self showDirections:[[RoutingService sharedService] getDirectionTextForIndoorNode:[LocationService sharedService].currentIndoorLocationNode]];
    [self sendRoutingStateChangedNotification];
}


- (void) sendRoutingStateChangedNotification{
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"VuforiaRoutingStateChanged"
     object:nil userInfo:nil];
}


#pragma mark Routing

- (void) rerouteFrom: (Node*) node success: (void(^)()) onSuccess failure: (void(^)(NSError*)) onFailure {
    
    RoutePoint * start = [[RoutePoint alloc] init];
    start.item = node;
    
    RoutePoint * end = [RoutingService sharedService].end;
    
    [[RoutingService sharedService] routeStart:start end:end success:^{
        [self routePathLoaded];
        onSuccess();
    } failure:^(NSError * error) {
        onFailure(error);
    }];
    
    self.isLoading = YES;
    [self sendRoutingStateChangedNotification];
}

- (void)route{
    RoutingService * routingService = [RoutingService sharedService];
    
    if ([routingService isRouting]){
        
        
        Node * currentLocationNode = [LocationService sharedService].currentIndoorLocationNode;
        
        if (currentLocationNode){
            
            NSArray * routePath = [routingService indoorRoutePathForBuilding:[currentLocationNode.building.id integerValue]];
            if (routePath){
                controlsOverlayVC.mode = ControlsOverlayModeRouting;
                [self showDirections:[[RoutingService sharedService] getDirectionTextForIndoorNode:currentLocationNode]];
                [self sendRoutingStateChangedNotification];
            }
        }
    }
}



@end

#endif
