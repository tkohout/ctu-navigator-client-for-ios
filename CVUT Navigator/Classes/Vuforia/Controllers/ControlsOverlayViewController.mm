/*==============================================================================
Copyright (c) 2010-2013 QUALCOMM Austria Research Center GmbH .
All Rights Reserved.
Qualcomm Confidential and Proprietary
==============================================================================*/
#ifndef VUFORIA_DISABLE

#import <QuartzCore/QuartzCore.h>

#import "ControlsOverlayViewController.h"
#import "OverlayViewController.h"
#import "PlanView.h"
#import "Node.h"
#import "Floor.h"
#import "Building.h"

bool isInterfaceOrientationPortrait=false;

#define panDirectionDown 0
#define panDirectionUp 1
#define panDirectionUnknown 2

#define topBarMinimized 0
#define topBarFull 1

#define positionFoundRouteButtonYMinimized 135
#define positionFoundRouteButtonYFull 261

#define positionFoundMapButtonYMinimized 75
#define positionFoundMapButtonYFull 181


@interface ControlsOverlayViewController() <PlanViewDelegate>

@property (nonatomic, retain) UILabel * topLabel;
@property (nonatomic, retain) UILabel * directionsLabel;
@property (nonatomic, retain) UILabel * errorLabel;

@property (nonatomic, retain) UIView * buttonsView;
@property (nonatomic, retain) UIView * textView;
@property (nonatomic, retain) UIView * directionsView;
@property (nonatomic, retain) UIView * loadingView;
@property (nonatomic, retain) UIView * errorView;
@property (nonatomic, retain) UIImageView * topLineView;

@property (nonatomic, retain) PlanView * planView;

@property (nonatomic, retain) UIButton * routeButton;
@property (nonatomic, retain) UIButton * mapButton;


@end


@implementation ControlsOverlayViewController

NSInteger topBarState;
NSString * defaultTopBarText = NSLocalizedString(@"Najděte marker v budově a namiřte na něj fotoaparát", nil);

// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    self.view = [[UIView alloc] initWithFrame: screenBounds];
}

- (int) getTopbarMinimizedHeight{
    if (self.mode == ControlsOverlayModePositionFound){
        return 145;
    }else{
        return 50;
    }
}

- (void)showMinimizedTopbarAnimated: (BOOL) animated{
    [self.view addSubview:self.topBar];
    int minimizedHeight = [self getTopbarMinimizedHeight];
    
    void (^animatedBlock)();
    
    animatedBlock = ^{
        self.topBar.frame = CGRectMake(0, -self.topBar.frame.size.height + minimizedHeight, self.topBar.frame.size.width, self.topBar.frame.size.height);
        self.topLabel.alpha = 1.0f;
        self.routeButton.frame = CGRectMake(self.routeButton.frame.origin.x, self.topBar.frame.size.height - positionFoundRouteButtonYMinimized, self.routeButton.frame.size.width, self.routeButton.frame.size.height);
        
        self.mapButton.frame = CGRectMake(self.routeButton.frame.origin.x, self.topBar.frame.size.height - positionFoundMapButtonYMinimized, self.routeButton.frame.size.width, self.routeButton.frame.size.height);
    };
    
    if (animated){
        [UIView animateWithDuration:0.5 animations:^{
            animatedBlock();
        } completion:^(BOOL finished) {
            topBarState = topBarMinimized;
        }];
        
    }else{
        animatedBlock();
        topBarState = topBarMinimized;
    }
}

- (void)showFullTopbarAnimated: (BOOL) animated{
    [self.view addSubview:self.topBar];
    
    void (^animatedBlock)();
    
    animatedBlock = ^{
        self.topBar.frame = CGRectMake(0, 0, self.topBar.frame.size.width, self.topBar.frame.size.height);
        self.topLabel.alpha = 0;
        self.routeButton.frame = CGRectMake(self.routeButton.frame.origin.x, self.topBar.frame.size.height - positionFoundRouteButtonYFull, self.routeButton.frame.size.width, self.routeButton.frame.size.height);
        
        self.mapButton.frame = CGRectMake(self.routeButton.frame.origin.x, self.topBar.frame.size.height - positionFoundMapButtonYFull, self.routeButton.frame.size.width, self.routeButton.frame.size.height);
    };
    
    if (animated){
        [UIView animateWithDuration:0.5 animations:^{
            animatedBlock();
        } completion:^(BOOL finished) {
            topBarState = topBarFull;
        }];
    }else{
        animatedBlock();
        topBarState = topBarFull;
    }
}



- (UIView *)topBar{
    if (!_topBar){
        UIImage *buttonImageNormal = [UIImage imageNamed:@"button-big.png"];
        UIImage *stretchableButtonImageNormal = [buttonImageNormal stretchableImageWithLeftCapWidth:12 topCapHeight:0];
        
        UIImage *buttonImageCancel = [UIImage imageNamed:@"button-big-cancel.png"];
        UIImage *stretchableButtonImageCancel = [buttonImageCancel stretchableImageWithLeftCapWidth:12 topCapHeight:0];
        
        
        _topBar = [[UIView alloc] initWithFrame:CGRectMake(0, -self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height)];
        _topBar.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        
        _topBar.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
        UIPanGestureRecognizer * panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(topBarMoved:)];
        UITapGestureRecognizer * tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(topBarTapped)];
        [tapGestureRecognizer requireGestureRecognizerToFail:panGestureRecognizer];
        
        [_topBar addGestureRecognizer:tapGestureRecognizer];
        [_topBar addGestureRecognizer:panGestureRecognizer];
        
        //_topBar.alpha = 0.5f;
        
        /* Directions view */
        self.directionsView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _topBar.frame.size.width, _topBar.frame.size.height)];
        [_topBar addSubview:self.directionsView];
        self.directionsView.hidden = YES;
        self.directionsView.autoresizingMask = _topBar.autoresizingMask;
        
        self.directionsLabel = [[UILabel alloc] initWithFrame:CGRectMake(15,_topBar.frame.size.height - 45, self.view.frame.size.width-20, 25)];
        self.directionsLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        self.directionsLabel.textColor = [UIColor whiteColor];
        self.directionsLabel.textAlignment = NSTextAlignmentCenter;
        self.directionsLabel.backgroundColor = [UIColor clearColor];
        self.directionsLabel.font = [UIFont boldSystemFontOfSize:14];
        self.directionsLabel.numberOfLines = 1;
        self.directionsLabel.minimumFontSize = 10;
        self.directionsLabel.adjustsFontSizeToFitWidth = YES;
        self.directionsLabel.text = defaultTopBarText;
        [self.directionsView addSubview:self.directionsLabel];
        
        
        UILabel * directionsDescriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 10, self.view.frame.size.width-20, 100)];
        directionsDescriptionLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        directionsDescriptionLabel.textColor = [UIColor whiteColor];
        directionsDescriptionLabel.shadowColor = [UIColor grayColor];
        directionsDescriptionLabel.shadowOffset = CGSizeMake(-1,0);
        directionsDescriptionLabel.textAlignment = NSTextAlignmentCenter;
        directionsDescriptionLabel.text = NSLocalizedString(@"Najděte marker a namiřte na něj fotoaparát. Marker vám ukáže cestu k vašemu cíli pomocí šipky. Pokračujte v zobrazeném směru a nacházejte další markery.", nil);
        directionsDescriptionLabel.font = [UIFont systemFontOfSize:15];
        directionsDescriptionLabel.backgroundColor = [UIColor clearColor];
        directionsDescriptionLabel.adjustsFontSizeToFitWidth = NO;
        directionsDescriptionLabel.numberOfLines = 0;
        
        [self.directionsView addSubview:directionsDescriptionLabel];
        
        UIButton * mapButton = [UIButton buttonWithType:UIButtonTypeCustom];
        
        mapButton.frame           = CGRectMake(10,_topBar.frame.size.height - 261, 300, 50);
        mapButton.backgroundColor = [UIColor clearColor];
        mapButton.titleLabel.font = [UIFont boldSystemFontOfSize:20.0f];
        [mapButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [mapButton setTitle:NSLocalizedString(@"Mapa", nil) forState:UIControlStateNormal];
        [mapButton setBackgroundImage:stretchableButtonImageNormal forState:UIControlStateNormal];
        [mapButton addTarget:self action:@selector(mapButtonTapped) forControlEvents:UIControlEventTouchUpInside];
        [self.directionsView addSubview:mapButton];
        
        UIButton * cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
        
        cancelButton.frame           = CGRectMake(10,_topBar.frame.size.height - 181, 300, 50);
        cancelButton.backgroundColor = [UIColor clearColor];
        cancelButton.titleLabel.font = [UIFont boldSystemFontOfSize:20.0f];
        [cancelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [cancelButton setTitle:NSLocalizedString(@"Zrušit navigaci", nil) forState:UIControlStateNormal];
        [cancelButton setBackgroundImage:stretchableButtonImageCancel forState:UIControlStateNormal];
        [cancelButton addTarget:self action:@selector(cancelNavigationButtonTapped) forControlEvents:UIControlEventTouchUpInside];
        [self.directionsView addSubview:cancelButton];

        
        
        /* Routing buttons*/
        
        self.buttonsView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _topBar.frame.size.width, _topBar.frame.size.height)];
        [_topBar addSubview:self.buttonsView];
        self.buttonsView.hidden = YES;
        self.buttonsView.autoresizingMask = _topBar.autoresizingMask;
        
        
        UILabel * routingDescriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 10, self.view.frame.size.width-20, 100)];
        routingDescriptionLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        routingDescriptionLabel.textColor = [UIColor whiteColor];
        routingDescriptionLabel.shadowColor = [UIColor grayColor];
        routingDescriptionLabel.shadowOffset = CGSizeMake(-1,0);
        routingDescriptionLabel.textAlignment = NSTextAlignmentCenter;
        routingDescriptionLabel.text = NSLocalizedString(@"Máme vaši pozici! Nyní se můžete navigovat do vašeho cíle v budově, nebo zobrazit svou pozici na mapě.", nil);
        routingDescriptionLabel.font = [UIFont systemFontOfSize:15];
        routingDescriptionLabel.backgroundColor = [UIColor clearColor];
        routingDescriptionLabel.adjustsFontSizeToFitWidth = NO;
        routingDescriptionLabel.numberOfLines = 0;
        
        [self.buttonsView addSubview:routingDescriptionLabel];
        
        self.routeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        
        self.routeButton.frame           = CGRectMake(10,_topBar.frame.size.height - positionFoundRouteButtonYMinimized, 300, 50);
        self.routeButton.backgroundColor = [UIColor clearColor];
        self.routeButton.titleLabel.font = [UIFont boldSystemFontOfSize:20.0f];
        [self.routeButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [self.routeButton setTitle:NSLocalizedString(@"Navigovat", nil) forState:UIControlStateNormal];
        [self.routeButton setBackgroundImage:stretchableButtonImageNormal forState:UIControlStateNormal];
        [self.routeButton addTarget:self action:@selector(navigationButtonTapped) forControlEvents:UIControlEventTouchUpInside];
        
        [self.buttonsView addSubview:self.routeButton];
        
        
        
        self.mapButton = [UIButton buttonWithType:UIButtonTypeCustom];
        
        self.mapButton.frame           = CGRectMake(10,_topBar.frame.size.height - positionFoundMapButtonYMinimized, 300, 50);
        self.mapButton.backgroundColor = [UIColor clearColor];
        self.mapButton.titleLabel.font = [UIFont boldSystemFontOfSize:20.0f];
        [self.mapButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [self.mapButton setTitle:NSLocalizedString(@"Mapa", nil) forState:UIControlStateNormal];
        [self.mapButton setBackgroundImage:stretchableButtonImageNormal forState:UIControlStateNormal];
        [self.mapButton addTarget:self action:@selector(mapButtonTapped) forControlEvents:UIControlEventTouchUpInside];
        [self.buttonsView addSubview:self.mapButton];
        
        
        /*Information text*/
        
        self.textView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _topBar.frame.size.width, _topBar.frame.size.height)];
        [_topBar addSubview:self.textView];
        self.textView.autoresizingMask = _topBar.autoresizingMask;
        
        
        self.topLabel = [[UILabel alloc] initWithFrame:CGRectMake(15,_topBar.frame.size.height - 50, self.view.frame.size.width-20, 30)];
        self.topLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        self.topLabel.textColor = [UIColor whiteColor];
        self.topLabel.shadowColor = [UIColor grayColor];
        self.topLabel.shadowOffset = CGSizeMake(-1,0);
        self.topLabel.textAlignment = NSTextAlignmentCenter;
        self.topLabel.text = defaultTopBarText;
        self.topLabel.font = [UIFont boldSystemFontOfSize:14];
        self.topLabel.numberOfLines = 1;
        self.topLabel.minimumFontSize = 10;
        self.topLabel.adjustsFontSizeToFitWidth = YES;
        self.topLabel.backgroundColor = [UIColor clearColor];
        
        [self.textView addSubview:self.topLabel];
        
        
        UIImage * image = [UIImage imageNamed:@"top_lines.png"];
        
        
        self.topLineView = [[UIImageView alloc] initWithImage:image];
        self.topLineView.frame = CGRectMake((self.view.frame.size.width - image.size.width)/2, _topBar.frame.size.height - 15, image.size.width, image.size.height);
        self.topLineView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
        
        [_topBar addSubview:self.topLineView];
        
        
        UILabel * descriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 10, self.view.frame.size.width-20, 100)];
        descriptionLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        descriptionLabel.textColor = [UIColor whiteColor];
        descriptionLabel.shadowColor = [UIColor grayColor];
        descriptionLabel.shadowOffset = CGSizeMake(-1,0);
        descriptionLabel.textAlignment = NSTextAlignmentCenter;
        descriptionLabel.text = NSLocalizedString(@"ČVUT Navigátor obsahuje navigaci pomocí rošířené reality. Pro zahájení navigace najděte libovolný marker podobný tomu na obrázku. Může být na podlaze, na stěně nebo na dveřích učebny.", nil);
        descriptionLabel.font = [UIFont systemFontOfSize:15];
        descriptionLabel.backgroundColor = [UIColor clearColor];
        descriptionLabel.adjustsFontSizeToFitWidth = NO;
        descriptionLabel.numberOfLines = 0;
        
        [self.textView addSubview:descriptionLabel];
        
        
        /*Hidden part*/
        
        UIImage * markerSample = [UIImage imageNamed:@"marker_sample.png"];
        UIImageView * markerSampleView = [[UIImageView alloc] initWithImage:markerSample];
        markerSampleView.frame = CGRectMake((self.view.frame.size.width - markerSample.size.width)/2, 120, markerSample.size.width, markerSample.size.height);
        markerSampleView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
        
        [self.textView addSubview:markerSampleView];
        
        /*Loading view*/
        
        self.loadingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _topBar.frame.size.width, _topBar.frame.size.height)];
        [_topBar addSubview:self.loadingView];
        self.loadingView.autoresizingMask = _topBar.autoresizingMask;
        self.loadingView.hidden = YES;
        
        UILabel * loadingLabel = [[UILabel alloc] initWithFrame:CGRectMake(15,_topBar.frame.size.height - 40, self.view.frame.size.width-20, 30)];
        loadingLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        loadingLabel.textColor = [UIColor whiteColor];
        loadingLabel.shadowColor = [UIColor grayColor];
        loadingLabel.shadowOffset = CGSizeMake(-1,0);
        loadingLabel.textAlignment = NSTextAlignmentCenter;
        loadingLabel.text = defaultTopBarText;
        loadingLabel.font = [UIFont boldSystemFontOfSize:14];
        loadingLabel.numberOfLines = 1;
        loadingLabel.backgroundColor = [UIColor clearColor];
        loadingLabel.text = NSLocalizedString(@"Načítám...", nil);
        
        [self.loadingView addSubview:loadingLabel];
        
        UIActivityIndicatorView * spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        [spinner startAnimating];
        
        [self.loadingView addSubview:spinner];
        spinner.center = CGPointMake(loadingLabel.center.x - 70, loadingLabel.center.y);
        
        /*Loading view*/
        
        self.errorView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _topBar.frame.size.width, _topBar.frame.size.height)];
        [_topBar addSubview:self.errorView];
        self.errorView.autoresizingMask = _topBar.autoresizingMask;
        self.errorView.hidden = YES;
        
        self.errorLabel = [[UILabel alloc] initWithFrame:CGRectMake(15,_topBar.frame.size.height - 40, self.view.frame.size.width-20, 30)];
        self.errorLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        self.errorLabel.textColor = [UIColor whiteColor];
        self.errorLabel.shadowColor = [UIColor grayColor];
        self.errorLabel.shadowOffset = CGSizeMake(-1,0);
        self.errorLabel.textAlignment = NSTextAlignmentCenter;
        self.errorLabel.text = defaultTopBarText;
        self.errorLabel.font = [UIFont boldSystemFontOfSize:14];
        self.errorLabel.numberOfLines = 1;
        self.errorLabel.backgroundColor = [UIColor clearColor];
        
        [self.errorView addSubview:self.errorLabel];
        
    }
    
    return _topBar;
}

- (void)setErrorModeWithMessage: (NSString *)message {
    [self setMode:ControlsOverlayModeError withMessage:message];
}

- (void)setMode: (ControlsOverlayMode)mode {
    [self setMode:mode withMessage:nil];
}

- (void)setMode: (ControlsOverlayMode)mode withMessage: (NSString*) message{
    if (mode == _mode) return;
    
    for (UIView * view in self.topBar.subviews){
        if (view.hidden == NO && (view == self.buttonsView || view == self.textView || view == self.directionsView || view == self.loadingView || view == self.errorView)){
            view.alpha = 0;
            view.hidden = YES;
        }
    }
    
    UIView * viewToShow;
    
    self.topLineView.hidden = NO;
    
    if (mode == ControlsOverlayModeNoPosition){
        viewToShow = self.textView;
    }else if (mode == ControlsOverlayModePositionFound){
        viewToShow = self.buttonsView;
    }else if (mode == ControlsOverlayModeRouting){
        viewToShow = self.directionsView;
        self.directionsLabel.text = defaultTopBarText;
    }else if (mode == ControlsOverlayModeLoading){
        viewToShow = self.loadingView;
        self.topLineView.hidden = YES;
    }else if (mode == ControlsOverlayModeError){
        viewToShow = self.errorView;
        self.errorLabel.text = message;
        self.topLineView.hidden = YES;
    }
    
    viewToShow.hidden = NO;
    viewToShow.alpha = 1;
    
    _mode = mode;
    
    [self showMinimizedTopbarAnimated:YES];
    
}

- (void)showDirections: (NSString *) directions{
    if ([directions isEqualToString:@""]){
        self.directionsLabel.text = defaultTopBarText;
    }else{
        self.directionsLabel.text = directions;
    }
    [self.directionsLabel setNeedsDisplay];
}

- (void)topBarTapped{
    if (self.mode == ControlsOverlayModeLoading || self.mode == ControlsOverlayModeError){
        return;
    }
    
    if (topBarState == topBarMinimized){
        [self showFullTopbarAnimated:YES];
    }else{
        [self showMinimizedTopbarAnimated:YES];
    }
}

- (void) navigationButtonTapped{
    if (self.delegate && [self.delegate respondsToSelector: @selector(navigateButtonTapped)]){
        [self.delegate navigateButtonTapped];
    }
}

- (void) mapButtonTapped{
    if (self.delegate && [self.delegate respondsToSelector: @selector(mapButtonTapped)]){
        [self.delegate mapButtonTapped];
    }
}

- (void) cancelNavigationButtonTapped{
    if (self.delegate && [self.delegate respondsToSelector:@selector(routingCancelled)]){
        [self.delegate routingCancelled];
    }
}

- (double) getPercentageOfMovement: (double) y{
    int topBarHeight = [self getTopbarMinimizedHeight];
    
    double translatedY = y + ((self.topBar.frame.size.height)/2);
    
    return (translatedY - topBarHeight)/(self.topBar.frame.size.height-topBarHeight);
}

- (void) topBarMoved: (UIPanGestureRecognizer*)recognizer{
    if (self.mode == ControlsOverlayModeLoading || self.mode == ControlsOverlayModeError){
        return;
    }
    
    static int direction = panDirectionUnknown;
    
    CGPoint translation = [recognizer translationInView:self.view];
    
    
    double newY = self.topBar.center.y + translation.y;
    
    if (self.topBar.frame.size.height/2 - newY < 0){
        newY = self.topBar.frame.size.height/2;
    }
    double percentage = [self getPercentageOfMovement:newY];
    self.topLabel.alpha = 1 - percentage;
    
    double routeButtonY = self.topBar.frame.size.height - positionFoundRouteButtonYMinimized  - ((positionFoundRouteButtonYFull - positionFoundRouteButtonYMinimized)*(percentage));
    
    
    self.routeButton.frame = CGRectMake(self.routeButton.frame.origin.x,  routeButtonY, self.routeButton.frame.size.width, self.routeButton.frame.size.height);
    
    double mapButtonY = self.topBar.frame.size.height - positionFoundMapButtonYMinimized - ((positionFoundMapButtonYFull - positionFoundMapButtonYMinimized)*(percentage));
    
    self.mapButton.frame = CGRectMake(self.routeButton.frame.origin.x,  mapButtonY, self.routeButton.frame.size.width, self.routeButton.frame.size.height);
    
    self.topBar.center = CGPointMake(self.topBar.center.x, newY);
    
    
    [recognizer setTranslation:CGPointMake(0, 0) inView:self.view];
    
    if (recognizer.state == UIGestureRecognizerStateEnded){
        if (direction == panDirectionDown){
            [self showFullTopbarAnimated:YES];
        }else if (direction == panDirectionUnknown){
            if (topBarState == topBarMinimized){
                [self showFullTopbarAnimated:YES];
            }else{
                [self showMinimizedTopbarAnimated:YES];
            }
        }else{
            [self showMinimizedTopbarAnimated:YES];
        }
    }
    
   if (translation.y > 0){
       direction = panDirectionDown;
   }else if (translation.y == 0){
       direction = panDirectionUnknown;
   }else{
       direction = panDirectionUp;
   }
}

-(UIButton*) createConfigureMenuButton:(NSString*)buttonTitle
{
    NSString* selectorName= @"press";
    selectorName=[selectorName stringByAppendingString:buttonTitle];
    selectorName = [selectorName stringByAppendingString:@"Button"];
    UIButton* customButton= [UIButton buttonWithType:UIButtonTypeRoundedRect] ;
    [customButton addTarget:self action:NSSelectorFromString(selectorName) forControlEvents:UIControlEventTouchUpInside];
    [customButton setTitle:buttonTitle forState:UIControlStateNormal];   
    customButton.titleLabel.font= [UIFont fontWithName:@"Helvetica-Bold" size:15.0];
    customButton.hidden = ![OverlayViewController doesOverlayHaveContent];
    [customButton.layer setBorderWidth:0];
    customButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    return customButton;
    
}


- (void)dealloc {
    [super dealloc];
    /*[menuButton release];
    [resetButton release];
    [runButton release];
    [clearButton release];
    [deleteButton release];
    [messageLabel release];
    messageLabel = nil;
    if (messageTimer != nil)
    {
        [messageTimer invalidate];
        messageTimer = nil;
    }
    */
    
}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesBegan:touches withEvent:event];
}

@end
#endif
