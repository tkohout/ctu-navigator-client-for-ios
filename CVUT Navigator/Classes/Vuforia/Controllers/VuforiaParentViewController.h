/*============================================================================
 Copyright (c) 2010-2013 QUALCOMM Austria Research Center GmbH .
 All Rights Reserved.
 Qualcomm Confidential and Proprietary
 ============================================================================*/
#ifndef VUFORIA_DISABLE

#import <UIKit/UIKit.h>
#import "ARParentViewController.h"
#import "ControlsOverlayViewController.h"
#import "RouteViewController.h"
@protocol EAGLViewDelegate;


@interface VuforiaParentViewController : ARParentViewController <EAGLViewDelegate, ControlsOverlayDelegate, RouteViewControllerDelegate> {
    ControlsOverlayViewController *controlsOverlayVC;
}

- (void)setAppWindow: (UIWindow * ) window;

- (void) route;

@end
#endif