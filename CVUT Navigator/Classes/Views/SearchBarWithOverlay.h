//
//  SearchBarWithOverlay.h
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 12.05.13.
//  Copyright (c) 2013 Tomáš Kohout. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SearchBarWithOverlay;

@protocol SearchBarWithOverlayDelegate <NSObject>
@optional
- (void) searchBar: (SearchBarWithOverlay *) searchBar textDidChange: (NSString *) text;

- (void) searchBarSearchingEnded: (SearchBarWithOverlay *) searchBar;
- (void) searchBarSearchingBegin: (SearchBarWithOverlay *) searchBar;
@end

@interface SearchBarWithOverlay : NSObject <UISearchBarDelegate>
    @property (nonatomic, weak) id<SearchBarWithOverlayDelegate> delegate;
@property (nonatomic, assign) BOOL enableOverlay;
@property (nonatomic, weak) UIView * overlayView;
@property (nonatomic, weak) UINavigationItem * navigationItem;
@property (nonatomic, assign, readonly) BOOL isSearching;
@property (nonatomic, retain) UISearchBar * field;
- (void)show;
- (void)hide;

@end
