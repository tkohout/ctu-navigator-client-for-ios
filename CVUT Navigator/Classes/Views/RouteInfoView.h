//
//  RouteInfoView.h
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 08.03.13.
//  Copyright (c) 2013 Tomáš Kohout. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RoutePoint.h"
#import "RouteInfoViewDelegate.h"

@interface RouteInfoView : UIView

@property (nonatomic, retain) id <RouteInfoViewDelegate> delegate;
@property (nonatomic, assign) NSInteger step;

-(void)reload;

@end
