//
//  SearchBarWithOverlay.m
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 12.05.13.
//  Copyright (c) 2013 Tomáš Kohout. All rights reserved.
//

#import "SearchBarWithOverlay.h"

@interface SearchBarWithOverlay()
@property (nonatomic, retain) UIView * overlay;
@end

@implementation SearchBarWithOverlay

- (id)init
{
    self = [super init];
    if (self) {
        self.field = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, 400, 42)];
        self.field.delegate = self;
        self.field.placeholder = NSLocalizedString(@"Hledat místa, předměty nebo učitele", nil);
        self.field.autocorrectionType = UITextAutocorrectionTypeNo;
        self.field.autocapitalizationType = UITextAutocapitalizationTypeNone;
        self.enableOverlay = YES;
    }
    return self;
}


- (BOOL)isSearching{
    return (self.field.text.length != 0);
}

#pragma mark UISearchBarDelegate
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    if ([searchText length] == 0){
        [self showOverlay];
    }else{
        [self hideOverlay];
    }
    
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(searchBar:textDidChange:)]){
        [self.delegate searchBar:self textDidChange:searchText];
    }
    
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    [self showWithOverlay:(searchBar.text.length == 0)];
}


#pragma mark Cancel button
- (void) hideCancelButton{
    DEFINE_BLOCK_SELF;
    [UIView animateWithDuration:0.3 animations:^{
        [blockSelf.navigationItem.titleView setFrame: CGRectMake(self.navigationItem.titleView.frame.origin.x, self.navigationItem.titleView.frame.origin.y, 312, self.navigationItem.titleView.frame.size.height)];
    }];
    
    [self.navigationItem setRightBarButtonItem: nil animated: YES];
}

- (void) showCancelButton{
    [self.navigationItem setRightBarButtonItem: [[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Zrušit", nil) style:UIBarButtonSystemItemAction target:self action:@selector(cancelButtonTapped)] animated: YES];
}

- (void)cancelButtonTapped{
    [self hide];
}


#pragma mark Show and hide search
- (void)show{
    [self showWithOverlay:YES];
}

- (void) showWithOverlay: (BOOL) withOverlay{
    if (withOverlay){
        [self showOverlayWithDuration:0.3];
    }
    [self showCancelButton];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(searchBarSearchingBegin:)]){
        [self.delegate searchBarSearchingBegin:self];
    }
}

- (void) hide{
    self.field.text = nil;
    [self hideOverlayWithDuration:0.3];
    [self hideCancelButton];
    [self.field resignFirstResponder];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(searchBarSearchingEnded:)]){
        [self.delegate searchBarSearchingEnded:self];
    }
}


#pragma mark Overlay
-(void) overlayTapped{
    [self hide];
}

- (void)showOverlay{
    [self showOverlayWithDuration:0];
}

- (UIView *)overlay{
    if (!_overlay){
        _overlay = [[UIView alloc]initWithFrame: CGRectMake(0.0, 0.0, self.overlayView.bounds.size.width, self.overlayView.bounds.size.height)];
        _overlay.backgroundColor = [UIColor blackColor];
        _overlay.alpha = 0.0;
        [self.overlayView addSubview:_overlay];
        [_overlay addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(overlayTapped)]];
        [_overlay addGestureRecognizer:[[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(overlayTapped)]];
    }
    
    return _overlay;
}

-(void) showOverlayWithDuration: (double) duration{
    if (self.enableOverlay){
        [self.overlayView bringSubviewToFront:self.overlay];
        
        [UIView animateWithDuration:duration animations:^{
            self.overlay.alpha = 0.8; // move to new location
            
        }];
    }
    
}

-(void) hideOverlay{
    [self hideOverlayWithDuration: 0];
}

-(void) hideOverlayWithDuration: (double) duration{
    if (self.enableOverlay){
        [UIView animateWithDuration:duration animations:^{
            self.overlay.alpha = 0; // move to new location
        }];
    }
}

@end
