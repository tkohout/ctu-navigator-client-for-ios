//
//  TextTableViewCell.m
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 23.07.13.
//  Copyright (c) 2013 Tomáš Kohout. All rights reserved.
//

#import "TextTableViewCell.h"

@implementation TextTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundView = [[UIView alloc] initWithFrame:CGRectZero] ;
        self.backgroundColor = [UIColor clearColor];
        self.textLabel.textColor = [UIColor colorWithRed:76.0f/255.0f green:86.0f/255.0f blue:108.0f/255.0f alpha:1.0f];
        
        self.textLabel.font = [UIFont systemFontOfSize:15.0f];
        self.textLabel.numberOfLines = 0;
        self.textLabel.shadowColor = [UIColor whiteColor];
        self.textLabel.shadowOffset = CGSizeMake(0, 1);
        self.textLabel.textAlignment = NSTextAlignmentCenter;
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
