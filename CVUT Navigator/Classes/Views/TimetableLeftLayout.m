//
//  TimetableTopLayout.m
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 10.03.13.
//  Copyright (c) 2013 Tomáš Kohout. All rights reserved.
//

#import "TimetableLeftLayout.h"

@implementation TimetableLeftLayout


-(CGSize)collectionViewContentSize {
    
    CGSize originalSize = [super collectionViewContentSize];
    
    if (!self.timetable){
        return originalSize;
    }
    
    NSInteger xSize = originalSize.width;
    NSInteger ySize = [self.timetable subRowsCount] * self.itemSize.height;
    return CGSizeMake(xSize, ySize);
}

- (UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath{
    UICollectionViewLayoutAttributes* attributes = [UICollectionViewLayoutAttributes layoutAttributesForCellWithIndexPath:indexPath];
    
    if (!self.timetable){
        return attributes;
    }
    
    
    NSInteger subRows = [self.timetable subRowsForDay:indexPath.row];
    NSInteger subRowsBefore = [self.timetable subRowsBeforeDay:indexPath.row];
    
    
    attributes.size = CGSizeMake(self.itemSize.width, self.itemSize.height * subRows);
    attributes.center = CGPointMake(attributes.center.x + self.itemSize.width/2, attributes.size.height/2 + subRowsBefore * self.itemSize.height);
    
    return attributes;
}

-(NSArray*)layoutAttributesForElementsInRect:(CGRect)rect {
    if (!self.timetable){
        return [super layoutAttributesForElementsInRect:rect];
    }
    
    NSMutableArray* attributes = [NSMutableArray array];
    
        for(NSInteger day=0 ; day < 5; day++) {
            
            NSIndexPath* indexPath = [NSIndexPath indexPathForItem:day inSection:0];
            [attributes addObject:[self layoutAttributesForItemAtIndexPath: indexPath]];
            
        }
    
    
    return attributes;
}



@end
