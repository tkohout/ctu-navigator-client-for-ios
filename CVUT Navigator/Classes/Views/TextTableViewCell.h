//
//  TextTableViewCell.h
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 23.07.13.
//  Copyright (c) 2013 Tomáš Kohout. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TextTableViewCell : UITableViewCell
@end
