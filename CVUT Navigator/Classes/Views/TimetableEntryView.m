//
//  TimetableEntryLabel.m
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 11.04.13.
//  Copyright (c) 2013 Tomáš Kohout. All rights reserved.
//

#import "TimetableEntryView.h"
#import <QuartzCore/QuartzCore.h>
#import "TimetableEntry.h"

#define PADDING 1

@interface TimetableEntryView()
@property (nonatomic, strong) UILabel * titleLabel;
@property (nonatomic, strong) UILabel * subtitleLabel;
@property (nonatomic, strong) UIView * cellBackgroundView;
@property (nonatomic, strong) UIView * grayView;
@property (nonatomic, strong) UILabel * lineLabel;

@end

@implementation TimetableEntryView

- (UILabel *)titleLabel{
    if (!_titleLabel){
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(PADDING, 15+PADDING, self.frame.size.width, 15)];
        _titleLabel.backgroundColor = [UIColor clearColor];
        _titleLabel.font = [UIFont boldSystemFontOfSize:14];
        _titleLabel.textColor = [UIColor whiteColor];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
    }
    
    return _titleLabel;
}

- (UILabel *)subtitleLabel{
    if (!_subtitleLabel){
        _subtitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(PADDING, 35+PADDING, self.frame.size.width, 15)];
        _subtitleLabel.textAlignment = NSTextAlignmentCenter;
        _subtitleLabel.textColor = [UIColor whiteColor];
        _subtitleLabel.backgroundColor = [UIColor clearColor];
        _subtitleLabel.font = [UIFont boldSystemFontOfSize:12];
       
    }
    
    return  _subtitleLabel;
}

- (UILabel *)lineLabel{
    if (!_lineLabel){
        _lineLabel = [[UILabel alloc] initWithFrame:CGRectMake(PADDING, PADDING, 2, self.frame.size.height)];
    }
    
    return _lineLabel;
}

- (UIView *)cellBackgroundView{
    if (!_cellBackgroundView){
        _cellBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(PADDING, PADDING, self.frame.size.width, self.frame.size.height)];
    }
    
    return _cellBackgroundView;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:CGRectMake(frame.origin.x, frame.origin.y, frame.size.width-PADDING*2, frame.size.height-PADDING*2)];
    if (self) {
        self.layer.masksToBounds = YES;
        //self.layer.cornerRadius = 5;
        //self.layer.borderWidth = 1;
        [self addSubview:self.cellBackgroundView];
        [self addSubview:self.titleLabel];
        [self addSubview:self.subtitleLabel];
        [self addSubview:self.lineLabel];
    }
    return self;
}

- (void) setColor: (UIColor *) color{
    self.backgroundColor = [UIColor clearColor];
    self.titleLabel.textColor = color;
    self.subtitleLabel.textColor = color;
    self.lineLabel.backgroundColor = color;
    self.cellBackgroundView.backgroundColor = [color colorWithAlphaComponent:0.4];
    //self.layer.borderColor = color.CGColor;
}



- (void)setSubtitle:(NSString *)subtitle{
    self.subtitleLabel.text = subtitle;
}

- (void)setTitle:(NSString *)title{
    self.titleLabel.text = title;
}

- (UIView *)grayView{
    if (!_grayView){
        _grayView = [[UIView alloc] initWithFrame:self.bounds];
        _grayView.alpha = 0.5f;
        _grayView.backgroundColor = [UIColor blackColor];
    }
    
    return _grayView;
}

- (void)setHighlighted:(BOOL)highlighted{
    if (highlighted){
        [self addSubview:self.grayView];
    }else{
        [self.grayView removeFromSuperview];
    }
}

/*-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.grayView removeFromSuperview];
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.grayView removeFromSuperview];
}*/


@end
