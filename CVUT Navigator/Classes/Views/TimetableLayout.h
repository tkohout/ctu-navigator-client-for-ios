//
//  MultipleLineLayout.h
//  CollectionView
//
//  Created by Tomáš Kohout on 10.03.13.
//
//

#import <UIKit/UIKit.h>

#import "Timetable.h"

@interface TimetableLayout: UICollectionViewFlowLayout
@property (nonatomic, strong) Timetable * timetable;
@end
