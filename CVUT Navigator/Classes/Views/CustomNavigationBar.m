//
//  CustomNavigationBar.m
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 10.03.13.
//  Copyright (c) 2013 Tomáš Kohout. All rights reserved.
//

#import "CustomNavigationBar.h"


@implementation CustomNavigationBar

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if ((self = [super initWithCoder:aDecoder]))
    {
        UISwipeGestureRecognizer *swipeRight;
        swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(didSwipeRight)];
        [swipeRight setDirection:UISwipeGestureRecognizerDirectionRight];
        [swipeRight setNumberOfTouchesRequired:1];
        [swipeRight setEnabled:YES];
        [self addGestureRecognizer:swipeRight];
        
        UISwipeGestureRecognizer *swipeLeft;
        swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(didSwipeLeft)];
        [swipeLeft setDirection:UISwipeGestureRecognizerDirectionLeft];
        [swipeLeft setNumberOfTouchesRequired:1];
        [swipeLeft setEnabled:YES];
        [self addGestureRecognizer:swipeLeft];
    }
    
    return self;
}

- (void) didSwipeRight{
    if (self.swipeDelegate && [self.swipeDelegate respondsToSelector:@selector(customNavigationBarDidFinishSwipeRight:)]){
        [self.swipeDelegate customNavigationBarDidFinishSwipeRight: self];
    }
}

- (void) didSwipeLeft{
    if (self.swipeDelegate && [self.swipeDelegate respondsToSelector:@selector(customNavigationBarDidFinishSwipeLeft:)]){
        [self.swipeDelegate customNavigationBarDidFinishSwipeLeft: self];
    }
}



@end
