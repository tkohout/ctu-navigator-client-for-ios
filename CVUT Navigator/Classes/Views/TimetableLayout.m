//
//  MultipleLineLayout.m
//  CollectionView
//
//  Created by Tomáš Kohout on 10.03.13.
//
//

#import "TimetableLayout.h"
#import "TimetableEntry.h"
//#import "TimetableEntry+IndexPath.h"



@implementation TimetableLayout {
    NSInteger itemWidth;
    NSInteger itemHeight;
    NSInteger itemSpacing;
}

-(id)init {
    if (self = [super init]) {
        itemWidth = 102;
        itemHeight = 60;
        itemSpacing = 0;
    }
    return self;
}

-(CGSize)collectionViewContentSize {
    //itemWidth is one lecture
    NSInteger xSize =  [Timetable viewWidthIfTime:90 isPx:itemWidth] ;
    NSInteger ySize;
    
    if (self.timetable){
        ySize = [self.timetable subRowsCount] * (itemHeight + itemSpacing);
    }else{
        ySize= [self.collectionView numberOfSections] * (itemHeight + itemSpacing) ;
    }
    return CGSizeMake(xSize, ySize);
}

- (UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewLayoutAttributes* attributes = [UICollectionViewLayoutAttributes layoutAttributesForCellWithIndexPath:indexPath];
    
    if (self.timetable){
        NSInteger subRow = 0;
        
        TimetableEntry * entry = [self.timetable getEntryForIndexPath:indexPath subRow: &subRow];
        
        NSInteger fromX = [self.timetable convertTimeToPx:[entry.time intValue] ];
        NSInteger toX = [self.timetable convertTimeToPx:[entry.time intValue]+[entry.length intValue]];
        
        
        
        NSInteger itemHeightBefore = (itemHeight + itemSpacing) * [self.timetable subRowsBeforeDay: indexPath.section];
        
        attributes.size = CGSizeMake(toX-fromX, itemHeight);
        
        attributes.center = CGPointMake(fromX + attributes.size.width/2, (subRow * attributes.size.height) + (attributes.size.height/2) + itemHeightBefore);
        
    }
    return attributes;
}


-(NSArray*)layoutAttributesForElementsInRect:(CGRect)rect {
    NSMutableArray* attributes = [NSMutableArray array];
    
    if (self.timetable){
        NSArray * entries = [self.timetable getEntriesInRect:rect];
        
        for (NSArray * dayArray in entries){
            for (NSArray * subRowArray in dayArray){
                
                for (NSDictionary * entryDict in subRowArray){
                    
                    NSIndexPath* indexPath = [entryDict objectForKey:@"indexPath"];
                    
                    [attributes addObject:[self layoutAttributesForItemAtIndexPath:indexPath]];
                }
            }
        }
    }
    
    return attributes;
}
@end
