//
//  TimetableEntryLabel.h
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 11.04.13.
//  Copyright (c) 2013 Tomáš Kohout. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TimetableEntry.h"

@interface TimetableEntryView : UICollectionViewCell
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * subtitle;

@property (nonatomic, assign) UIColor * color;

@end
