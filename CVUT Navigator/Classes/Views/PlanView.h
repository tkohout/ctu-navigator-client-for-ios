//
//  PlanView.h
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 07.03.13.
//  Copyright (c) 2013 Tomáš Kohout. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Plan.h"
#import "RMMapView.h"
#import "RoutePoint.h"
#import "Node.h"
#import "RouteInfoViewDelegate.h"

typedef NS_ENUM(NSInteger, PlanViewChangeFloorDirection){
    PlanViewChangeFloorDirectionUp,
    PlanViewChangeFloorDirectionDown
};


@class PlanView;

@protocol PlanViewDelegate <NSObject>
@optional
- (void) planView: (PlanView *) planView titleChanged: (NSString *) title;
- (void) planView:(PlanView *) planView tappedOnNode: (Node *) node;
@end


@interface PlanView : UIView <RMMapViewDelegate, RouteInfoViewDelegate>

@property (nonatomic, retain) Floor * floor;
@property (nonatomic, retain) NSString * title;

@property (assign, nonatomic) BOOL disableDragging;
@property (assign, nonatomic) BOOL isLoading;
@property (assign, nonatomic) double zoom;

@property (nonatomic, retain) id<PlanViewDelegate, RouteInfoViewDelegate> delegate;

- (void) resizingBegan;
- (void) resizingEnded;

- (id)initWithFrame:(CGRect)frame;
- (void) setFullscreen: (BOOL) fullscreen animated: (BOOL) animated;

- (void) setCornerRadius: (int) radius;
- (void)centerOnNode: (Node *) node;
- (void) centerOnNode:(Node *)node animated: (BOOL) animated;
- (void) centerOnNode:(Node *)node animated: (BOOL) animated withHighlight: (BOOL) highlight;
- (void) centerOnUserLocation;
- (void) centerOnCoordinate: (CLLocationCoordinate2D) coordinate animated:(BOOL)animated;
    
- (void) changeFloor: (PlanViewChangeFloorDirection) direction;

- (void) cancelRouting;
- (void)rotateMapInAngle: (double) angle animated:(BOOL)animated;
- (void) showHighlightOnNode: (Node *) node;

- (void) reloadRouteInfoView;



@end


