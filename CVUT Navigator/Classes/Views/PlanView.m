//
//  PlanView.m
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 07.03.13.
//  Copyright (c) 2013 Tomáš Kohout. All rights reserved.
//

#import "PlanView.h"
#import "Building.h"
#import "RMMapView.h"
#import "RMDBMapSource.h"
#import "RMMarker.h"
#import "Floor.h"
#import "Building.h"
#import "Node.h"
#import "RMMarker.h"
#import "Room.h"

#import "RMZoomConverter.h"
#import "RMAnnotation.h"
#import "RMShape.h"

#import "RouteInfoView.h"
#import "RoutePoint.h"
#import "ClassroomDetailViewController.h"

#import "RoutingService.h"
#import "DataService.h"
#import "NodeAnnotation.h"
#import "CTUNavigatorMapSource.h"

#import "IndoorLocationManager.h"
#import "LocationService.h"
#import "RMUserLocation.h"
#import "StaticVars.h"

@interface PlanView()
    @property (nonatomic, retain) RMZoomConverter * zoomConverter;
    @property (nonatomic, retain) RMMapView * mapView;
    @property (nonatomic, retain) IndoorLocationManager * indoorLocationManager;
    @property (nonatomic, retain) RouteInfoView * routeInfoView;
    @property (nonatomic, retain, readonly) Plan * plan;
    @property (nonatomic, retain) UIActivityIndicatorView * spinner;
@end


@implementation PlanView
@synthesize mapView;

BOOL _delegateHasPlanViewTappedOnNode;
BOOL _delegateHasPlanViewTitleChanged;


CLLocationCoordinate2D center;

/* For animations */
CGRect originalFrame;
UIView * originalSuperView;
UIView * controllerView;




#pragma mark Initialization
- (RMZoomConverter *)zoomConverter{
    if (!_zoomConverter){
        RMTile baseTile;
        baseTile.x = 566272;
        baseTile.y = 354304;
        baseTile.zoom = 20;
        
        _zoomConverter = [[RMZoomConverter alloc] initWithBaseTile:baseTile zoomSteps:5];
    }
    
    return _zoomConverter;
}

- (RouteInfoView *)routeInfoView{
    if (!_routeInfoView){
        _routeInfoView = [[RouteInfoView alloc] init];
        _routeInfoView.delegate = self.delegate;
    }
    
    return _routeInfoView;
}

- (IndoorLocationManager *)indoorLocationManager{
    if (!_indoorLocationManager){
        _indoorLocationManager = [[IndoorLocationManager alloc] initWithZoomConverter:self.zoomConverter];
    }
    return _indoorLocationManager;
}

- (void) setDelegate:(id<PlanViewDelegate, RouteInfoViewDelegate>)delegate{
    if (delegate){
        _delegate = delegate;
    }
    _delegateHasPlanViewTappedOnNode = [self.delegate respondsToSelector:@selector(planView:tappedOnNode:)];
    _delegateHasPlanViewTitleChanged = [self.delegate respondsToSelector:@selector(planView:titleChanged:)];
}

-(RMMapView *)mapView{
    if (!mapView){
        mapView = [[RMMapView alloc]initWithFrame:CGRectMake(0,0,self.frame.size.width, self.frame.size.height)];
        [mapView setDecelerationMode:RMMapDecelerationFast];
        mapView.customLocationManager = self.indoorLocationManager;
        [mapView setShowsUserLocation:YES];
        
        //Could be useful in future
        //self.mapView.userTrackingMode = RMUserTrackingModeFollowWithHeading;
        
        mapView.delegate = self;
    }
    
    return mapView;
}

- (UIActivityIndicatorView *)spinner{
    if (!_spinner){
        _spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        
        _spinner.center = self.center;
        _spinner.hidesWhenStopped = YES;
    }
    
    return _spinner;
}

- (void)setIsLoading:(BOOL)isLoading{
    _isLoading = isLoading;
    
    if (isLoading){
        [self.spinner startAnimating];
        [self addSubview:self.spinner];
    }else{
        [self.spinner stopAnimating];
    }
    
}


- (id)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"LoadingTile.png"]];
        
        
        self.isLoading = YES;
        
        //[self.zoomConverter convertTileSource:[[NSBundle mainBundle] pathForResource:@"fit2.db" ofType:nil]];
        
        
        //We will wait for notification from routing service
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(routePathLoaded:)
                                                     name:@"RouteServiceIndoorRoutePathLoadedNotification"
                                                   object:nil];
        
    }
    return self;
}

-(void)setFrame:(CGRect)frame{
    [super setFrame:frame];
    self.spinner.center = self.center;
    [mapView setFrame:CGRectMake(0,0, self.frame.size.width, self.frame.size.height)];
}

- (void)setFloor:(Floor *)floor{
    if (!floor){
        [DataService showErrorMessageWithTitle:NSLocalizedString(@"Podlaží nenalezeno", nil) subtitle:nil onView:nil];
        return;
    }
    
    _floor = floor;
    
    
    
    [self addSubview: self.mapView];
    
    [self setTitle:[floor floorName]];
    [self showAnnotations];
    
    self.indoorLocationManager.selectedFloor = floor;
    
    CTUNavigatorMapSource * mapSource = [self getTileSourceForFloor: self.floor];
    self.mapView.enableBouncing= NO;
    [self.mapView setTileSource: mapSource];
    
    //[self setMapConstrains];
    if ([RoutingService sharedService].isRouting){
        [self showRoutePath];
    }
    
    //[self.mapView setZoom:21.0f];
    
}

- (Plan *)plan{
    if (self.floor){
        return self.floor.plan;
    }
    return nil;
}

#pragma mark User interaction
- (void)setDisableDragging:(BOOL)disableDragging{
    self.mapView.enableDragging = !disableDragging;
}



#pragma mark Annotations
- (void) cleanAnnotations{
    for (RMAnnotation * annotation in self.mapView.annotations){
        if (! annotation.isUserLocationAnnotation)
        {
            [self.mapView removeAnnotation:annotation];
        }
    }
}

- (void) cleanRoutePath{
    for (RMAnnotation * annotation in self.mapView.annotations){
        if ([annotation.annotationType isEqualToString:@"path"])
        {
            [self.mapView removeAnnotation:annotation];
        }
    }
}


- (void)showAnnotations{
    
    [self cleanAnnotations];
    
    
    
    for (Node * node in [self.floor nodes]){
        if (([node isKindOfClass:[Room class]] && node.name) || [node.type isEqualToString:@"stairs"] || [node.type isEqualToString:@"elevator"] || [node.type isEqualToString:@"entrance"]){
            
            CLLocationCoordinate2D coordinate;
            
            coordinate.latitude = [node.latitude doubleValue];
            coordinate.longitude = [node.longitude doubleValue];
            
            NSString * title;
            
            UIImage *markerImage;
            
            
            if ([node isKindOfClass:[Room class]] && node.name){
                markerImage = [UIImage imageNamed:@"room.png"];
            }else if ([node.type isEqualToString:@"stairs"]){
                markerImage = [UIImage imageNamed:@"stairs.png"];
            }else if ([node.type isEqualToString:@"elevator"]){
                markerImage = [UIImage imageNamed:@"elevator.png"];
            }else if ([node.type isEqualToString:@"entrance"]){
                markerImage = [UIImage imageNamed:@"entrance.png"];
            }
            
            
            NodeAnnotation *annotation = [NodeAnnotation annotationWithMapView:mapView coordinate:[self.zoomConverter convertCoordinate:coordinate] andTitle:nil];
            
            annotation.annotationIcon = markerImage;
            annotation.anchorPoint = CGPointMake(0.5, 1.0);
            annotation.annotationType = @"marker";
            annotation.node = node;
            
            [mapView addAnnotation:annotation];
        }
    }
    
}

/*-(void)willMoveToSuperview:(UIView *)newSuperview{
    if (newSuperview && ![newSuperview isEqual:controllerView]){
        originalSuperView = newSuperview;
    }
}*/

#pragma mark Appearance
- (void) setCornerRadius: (int) radius{
    [[mapView layer] setCornerRadius:radius];
}

- (void)resizingBegan{
    mapView.mapZooming = YES;
}

- (void) resizingEnded{
    mapView.mapZooming = NO;
}

#pragma mark Routing
- (void) routePathLoaded:(NSNotification *) notification{
    if (self.plan){
        NSDictionary *userInfo = notification.userInfo;
        
        Building * building = [userInfo objectForKey:@"building"];
        Node * node = [[RoutingService sharedService] getNodeForStep:[RoutingService sharedService].currentStep];
        
        if (node){
            [self centerOnNode:node];
        }
        
        //We have notification that is for us
        if ([building.id isEqualToNumber:self.plan.building.id]){
            [self performSelectorOnMainThread:@selector(showRoutePath) withObject:nil waitUntilDone:NO];
        }
    }
}


- (void) cancelRouting{
    [[RoutingService sharedService] routeCancel];
    
    [self cleanRoutePath];
    [self cleanHighlights];
    
    [self.routeInfoView removeFromSuperview];
    self.routeInfoView = nil;
}

- (BOOL) isRoutePathDisplayed{
    for (RMAnnotation * annotation in mapView.annotations){
        if ([annotation.annotationType isEqualToString: @"path"]){
            return YES;
        }
    }
    
    return NO;
}

- (void) showRoutePath{
    NSArray * routePathNodes = [[RoutingService sharedService] indoorRoutePathForBuilding:[self.plan.building.id integerValue]];
    
    if (routePathNodes){
        Node * startNode = [routePathNodes objectAtIndex:0];
        
        CLLocationCoordinate2D startCoord;
        //We are on the same floor
        if ([startNode isFromFloor: self.floor]){
            startCoord = CLLocationCoordinate2DMake([[startNode latitude] doubleValue], [[startNode longitude] doubleValue]);
        //We have to find stairs or elevator
        }else{
            for (Node * node in routePathNodes){
                if ([node.type isEqualToString:@"stairs"] || [node.type isEqualToString:@"elevator"]){
                    startCoord = CLLocationCoordinate2DMake([[node latitude] doubleValue], [[node longitude] doubleValue]);
                }
            }
        }
        
        RMAnnotation *annotation = [RMAnnotation annotationWithMapView:mapView coordinate:[self.zoomConverter convertCoordinate: startCoord] andTitle:nil];
        
        annotation.annotationIcon = [UIImage imageNamed:@"marker-red.png"];
        annotation.anchorPoint = CGPointMake(0.5, 1.0);
        annotation.annotationType = @"path";
        
        [annotation setBoundingBoxCoordinatesSouthWest:[self.zoomConverter convertCoordinate: self.floor.southWestConstrain] northEast:[self.zoomConverter convertCoordinate: self.floor.northEastConstrain]];
        [mapView addAnnotation:annotation];
        
        [self showRouteInfo];
    }
}

- (void) reloadRouteInfoView{
    [self.routeInfoView reload];
}

- (void) showRouteInfo{
    [self addSubview:self.routeInfoView];
    [self bringSubviewToFront:self.routeInfoView];
}

- (void) hideRouteInfo{
    if (self.routeInfoView){
        [self.routeInfoView removeFromSuperview];
        self.routeInfoView = nil;
    }
}

#pragma mark Centering and rotating

- (void) setZoom: (double) zoom{
    _zoom = zoom;
    [self.mapView setZoom:[self.zoomConverter convertZoom:zoom]];
}

- (void)centerOnNode: (Node *) node{
    [self centerOnNode:node animated:NO];
}

- (void) cleanHighlights{
    for (RMAnnotation * anAnotation in self.mapView.annotations){
        if ([anAnotation.annotationType isEqualToString:@"highlight_marker"]){
            [self.mapView removeAnnotation:anAnotation];
        }
    }
}

- (void) showHighlightOnNode: (Node *) node{
    
    [self cleanHighlights];
    
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake([node.latitude doubleValue], [node.longitude doubleValue]);
    
    
    RMAnnotation *annotation = [RMAnnotation annotationWithMapView:mapView coordinate:[self.zoomConverter convertCoordinate: coordinate] andTitle:nil];
    
    annotation.annotationIcon = [UIImage imageNamed:@"selected_node.png"];
    annotation.anchorPoint = CGPointMake(0.5, 0.5);
    annotation.annotationType = @"highlight_marker";
    
    [self.mapView addAnnotation:annotation];
}


- (void) centerOnNode:(Node *)node animated: (BOOL) animated withHighlight: (BOOL) highlight{
    //Set coordinate
    [mapView setCenterCoordinate: [self.zoomConverter convertCoordinate: CLLocationCoordinate2DMake([node.latitude doubleValue], [node.longitude doubleValue])] animated:animated];
    
    //Rotate in direction if we are routing
    RoutingService * service = [RoutingService sharedService];
    
    if (service.isRouting){
        if ([service isNodeInRoutePath:node]){
            int angle = 0;
            
            //If it is not first, use previous heading
            if ([service getPreviousNodeInRoutePathForNode:node]){
                angle = [service getPreviousHeadingInRoutePathForNode:node];
                //For first use next heading
            }else if ([service getNextNodeInRoutePathForNode:node]){
                angle = [service getNextHeadingInRoutePathForNode:node];
            }
            
            [self rotateMapInAngle:angle animated:YES];
        }
    }
    
    
    //Selecting right annotation
    [self cleanHighlights];
    [self.mapView deselectAnnotation:self.mapView.selectedAnnotation animated:YES];
    
    for (RMAnnotation * annotation in mapView.annotations){
        if ([annotation isKindOfClass:[NodeAnnotation class]]){
            NodeAnnotation * nodeAnnotation = (NodeAnnotation *) annotation;
            if ([nodeAnnotation.node isKindOfClass:[Room class]] && [nodeAnnotation.node.id isEqualToNumber: node.id]){
                [mapView selectAnnotation:nodeAnnotation animated:YES];
                return;
            }
        }
        
    }
    
    
    
    //It's not on the same floor we are right now
    Floor * floor = [self floorForNode:node];
    
    //It's intersection, does not have annotation to select
    if ([self.floor isEqualToEntity: floor]){
        if (highlight){
            [self showHighlightOnNode: node];
        }
        return;
    }
    
    if (floor){
        self.floor = floor;
        [self centerOnNode:node animated:animated];
    }else{
        //This shouldn't happen
        [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Pozice nenalezena", nil) message:NSLocalizedString(@"Pozice vámi hledaného uzlu nebyla nalezena, pravděpodobně byl smazán", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles: nil] show];
    }
}

- (void) centerOnNode:(Node *)node animated: (BOOL) animated{
    [self centerOnNode:node animated:animated withHighlight:NO];
}

- (void)rotateMapInAngle: (double) angle animated:(BOOL)animated{
    [self.mapView rotateMapInAngle:angle animated:animated];
}

- (Floor *) floorForNode: (Node *) searchedNode{
    for (Floor * floor in self.plan.floors){
        for (Node * node in floor.nodes){
            if ([node.id isEqualToNumber:searchedNode.id]){
                return floor;
            }
        }
    }
    
    return nil;
}

- (void) centerOnUserLocation{
    if (mapView.userLocation){
        [mapView setCenterCoordinate: mapView.userLocation.location.coordinate animated:YES];
    }
}

- (void) centerOnCoordinate: (CLLocationCoordinate2D) coordinate animated:(BOOL)animated{
    [mapView setCenterCoordinate: [self.zoomConverter convertCoordinate:coordinate] animated:animated];
}


#pragma mark RMMapViewDelegate methods
- (RMMapLayer *)mapView:(RMMapView *)aMapView layerForAnnotation:(RMAnnotation *)annotation
{
    if ([annotation isKindOfClass:[RMUserLocation class]]){
        
        
        NSLog(@"Hej");
    }
    
    /*Draw a path*/
    if ([annotation.annotationType isEqualToString:@"path"]){
        
        RMShape * routepath = [[RMShape alloc] initWithView:mapView] ;
        [routepath setLineColor:[StaticVars sharedService].tintColor];
        
        
        [routepath setLineWidth:4];
        [routepath setLineCap:kCALineCapRound];
        [routepath moveToCoordinate: annotation.coordinate];
        
        NSArray * routePathNodes = [[RoutingService sharedService] indoorRoutePathForBuilding:[self.floor.plan.building.id integerValue]];
        
        if (routePathNodes){
            for (Node * node in routePathNodes){
                
                if ([node isFromFloor: self.floor]){
                    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake([node.latitude doubleValue], [node.longitude doubleValue]);
                    CLLocationCoordinate2D convertedCoordinate =[self.zoomConverter convertCoordinate: coordinate];
                    
                    [routepath addLineToCoordinate: convertedCoordinate];
                    [routepath moveToCoordinate: convertedCoordinate];
                }
            }
        }
        
        //routepath.zPosition = -1;
        
        return routepath;
        
    }
    /*Simple marker*/
    RMMapLayer *marker = [[RMMarker alloc] initWithUIImage:annotation.annotationIcon anchorPoint:annotation.anchorPoint] ;
    
    marker.canShowCallout = NO;
    
    
    //Disclosure only for rooms
    if ([annotation isKindOfClass:[NodeAnnotation class]]){
        Node * node = ((NodeAnnotation *)annotation).node;
        NSString * text;
        
        if ([node isKindOfClass: [Room class]]){
            annotation.title = NSLocalizedString(@"Detail", nil);
            marker.canShowCallout = YES;
            marker.calloutOffset = CGPointMake(0, 0);
            marker.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
            text = ((NodeAnnotation *)annotation).node.name;
        }else{
            marker.canShowCallout = YES;
            marker.calloutOffset = CGPointMake(0, 0);
            if([node.type isEqualToString:@"stairs"]){
                annotation.title= NSLocalizedString(@"Schody", nil);
            }else if([node.type isEqualToString:@"entrance"]){
                annotation.title= NSLocalizedString(@"Vchod", nil);
            }else if([node.type isEqualToString:@"elevator"]){
                annotation.title= NSLocalizedString(@"Výtah", nil);
            }
        }
        
        if (text){
            
            UIFont * font = [UIFont systemFontOfSize:15.0f];
            
            CGSize titleSize = [text sizeWithFont:font];
            
            UIColor * textColor = [StaticVars sharedService].tintColor;
            
            
            UILabel * label = [[UILabel alloc] initWithFrame:CGRectMake([marker bounds].size.width / 2 - (titleSize.width+20) / 2, 35, titleSize.width+20, titleSize.height)];
            label.text = text;
            label.textAlignment = NSTextAlignmentCenter;
            label.textColor = textColor;
            label.backgroundColor = [StaticVars sharedService].backgroundColor;
            label.layer.borderColor = textColor.CGColor;
            label.layer.borderWidth = 1.0f;
            label.layer.cornerRadius = 5.0f;
            label.font = font;
            
            ((RMMarker*)marker).label = label;
        }
        
    }
    
    
   
    return marker;
}

-(void)tapOnCalloutAccessoryControl:(UIControl *)control forAnnotation:(RMAnnotation *)annotation onMap:(RMMapView *)map{
    if (_delegateHasPlanViewTappedOnNode){
        if ([annotation isKindOfClass:[NodeAnnotation class]]){
            [self.delegate planView:self tappedOnNode:((NodeAnnotation *)annotation).node];
        }
    }
}


- (void)setTitle: (NSString *) title{
    if (_delegateHasPlanViewTitleChanged){
        [self.delegate planView:self titleChanged:title];
    }
    _title = title;
}

- (Floor *) floorInPlan: (NSInteger) floorNumber{
    
    for (Floor * floor in self.plan.floors){
        if ([floor.floorNumber integerValue] == floorNumber ){
            return floor;
        }
    }
    
    return nil;
}

- (void) changeFloor: (PlanViewChangeFloorDirection) direction{
    [self.mapView rotateMapInAngle:0 animated:YES];
    
    if (direction == PlanViewChangeFloorDirectionUp){
        if ([self.floor.floorNumber isEqualToNumber:self.plan.topFloor.floorNumber]) return;
        self.floor = [self floorInPlan:[self.floor.floorNumber integerValue] + 1];
    }else{
        if ([self.floor.floorNumber isEqualToNumber:self.plan.bottomFloor.floorNumber]) return;
        self.floor = [self floorInPlan:[self.floor.floorNumber integerValue] - 1];
    }
}

- (void) setMapConstrains{
    [mapView setConstraintsSouthWest:[self.zoomConverter convertCoordinate:self.floor.southWestConstrain] northEast:[self.zoomConverter convertCoordinate: self.floor.northEastConstrain]];
}

- (CTUNavigatorMapSource *) getTileSourceForFloor: (Floor *) floor{
    return [[CTUNavigatorMapSource alloc] initWithZoomConverter:self.zoomConverter floor:floor];
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"RouteServiceIndoorRoutePathLoadedNotification" object:nil];
}

@end
