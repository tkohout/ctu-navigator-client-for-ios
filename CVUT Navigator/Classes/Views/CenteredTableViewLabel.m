//
//  CenteredTableViewLabel.m
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 28.10.12.
//  Copyright (c) 2012 Tomáš Kohout. All rights reserved.
//

#import "CenteredTableViewLabel.h"

@implementation CenteredTableViewLabel

- (id)initWithFrame:(CGRect)frame 
{
    self = [super initWithFrame:frame];
    if (self) {
        self.textAlignment = NSTextAlignmentCenter;
        self.backgroundColor = [UIColor clearColor];
        self.textColor = [ UIColor colorWithRed:0.22 green:0.33 blue:0.53 alpha:1];
        self.highlightedTextColor = [UIColor whiteColor];
        self.font = [UIFont boldSystemFontOfSize: 17.0f];
        
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
