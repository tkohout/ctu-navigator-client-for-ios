//
//  CustomNavigationBar.h
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 10.03.13.
//  Copyright (c) 2013 Tomáš Kohout. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CustomNavigationBar;
@protocol CustomNavigationBarDelegate <NSObject>
@optional
- (void)customNavigationBarDidFinishSwipeRight: (CustomNavigationBar *) navigationBar;
- (void)customNavigationBarDidFinishSwipeLeft: (CustomNavigationBar *) navigationBar;
@end


@interface CustomNavigationBar : UINavigationBar
@property (nonatomic, weak) id<CustomNavigationBarDelegate> swipeDelegate;
@end
