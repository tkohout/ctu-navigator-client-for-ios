//
//  CenteredTableViewLabel.h
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 28.10.12.
//  Copyright (c) 2012 Tomáš Kohout. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CenteredTableViewLabel : UILabel
@end
