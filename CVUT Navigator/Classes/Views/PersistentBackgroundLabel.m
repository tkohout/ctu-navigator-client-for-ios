//
//  PersistentBackgroundLabel.m
//  MapView
//
//  Created by Tomáš Kohout on 23.11.13.
//
//

#import "PersistentBackgroundLabel.h"

@implementation PersistentBackgroundLabel

- (void)setPersistentBackgroundColor:(UIColor*)color {
    super.backgroundColor = color;
}

- (void)setBackgroundColor:(UIColor *)color {
    // do nothing - background color never changes
}

@end
