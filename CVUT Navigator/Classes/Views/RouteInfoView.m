//
//  RouteInfoView.m
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 08.03.13.
//  Copyright (c) 2013 Tomáš Kohout. All rights reserved.
//

#import "RouteInfoView.h"
#import  <QuartzCore/QuartzCore.h>
#import "RoutePoint.h"
#import "RouteViewController.h"
#import "PlanViewController.h"
#import "Building.h"
#import "Room.h"
#import "RoutingService.h"
#import "StaticVars.h"

@interface RouteInfoView()
@property (nonatomic, retain) UILabel * directionsLabel;
@property (nonatomic, retain) UIButton * leftButton;
@property (nonatomic, retain) UIButton * rightButton;
@end


@implementation RouteInfoView


- (void)setStep: (NSInteger) step{
    
    [RoutingService sharedService].currentStep = step;
    
    if (_step != step && [self.delegate respondsToSelector:@selector(routeInfoView:stepChanged:)]){
        [self.delegate routeInfoView:self stepChanged:step];
    }
    
    _step = step;
    [self loadDirectionsForStep: step];
    
    
}

- (void) loadDirectionsForStep: (NSInteger)step{
    if ([[RoutingService sharedService] isRoutePathCompletelyLoaded]){
        NSString * text = [[RoutingService sharedService] getDirectionForStep:step];
        [self setDirectionsLabelText: text];
        
        //Delete the spinner
        for(UIView *subview in [self.directionsLabel subviews]) {
            [subview removeFromSuperview];
        }
    }else{
        self.directionsLabel.text = @"";
        
        UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc]  initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        CGPoint center = self.directionsLabel.center;
        
        spinner.center = CGPointMake(center.x- self.directionsLabel.frame.origin.x, center.y) ;
        [self.directionsLabel addSubview:spinner];
        [spinner startAnimating];

    }
    
    NSInteger stepCount = [[RoutingService sharedService] stepCount];
    
    if (self.step == 0 && self.step == stepCount -1){
        [self.leftButton setHidden:YES];
        [self.rightButton setHidden:YES];
    }else{
        [self.leftButton setHidden:NO];
        [self.rightButton setHidden:NO];
    }

    
    [self.leftButton setEnabled:(self.step == 0) ? NO : YES];
    [self.rightButton setEnabled:(self.step == stepCount -1) ? NO : YES];
}

- (void)setDirectionsLabelText: (NSString *) text{
   UIFont *font = [UIFont boldSystemFontOfSize:14];
    
    int i;
    for(i = 14; i > 10; i=i-2)
    {
        font = [font fontWithSize:i];
        CGSize constraintSize = CGSizeMake(self.directionsLabel.frame.size.width, MAXFLOAT);
        CGSize labelSize = [text sizeWithFont:font constrainedToSize:constraintSize lineBreakMode:UILineBreakModeWordWrap];
        
        if(labelSize.height <= self.directionsLabel.frame.size.height)
            break;
    }
    
    self.directionsLabel.font = font;
    self.directionsLabel.text = text;
}

- (void)reload{
    self.step = [RoutingService sharedService].currentStep;
}

- (id)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, 70)];
    if (self) {
        
        UISwipeGestureRecognizer *swipeRight;
        swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(didSwipeRight)];
        [swipeRight setDirection:UISwipeGestureRecognizerDirectionRight];
        [swipeRight setNumberOfTouchesRequired:1];
        [swipeRight setEnabled:YES];
        [self addGestureRecognizer:swipeRight];
        
        UISwipeGestureRecognizer *swipeLeft;
        swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(didSwipeLeft)];
        [swipeLeft setDirection:UISwipeGestureRecognizerDirectionLeft];
        [swipeLeft setNumberOfTouchesRequired:1];
        [swipeLeft setEnabled:YES];
        [self addGestureRecognizer:swipeLeft];
        
        UIColor * color = [StaticVars sharedService].backgroundColor;
        
        
        self.backgroundColor = [color colorWithAlphaComponent:0.9];
        
        self.leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.leftButton setImage:[UIImage imageNamed:@"left_dark.png"] forState:UIControlStateNormal];
        [self.leftButton setFrame:CGRectMake(10, 19, 32, 32)];
        [self.leftButton addTarget:self action:@selector(leftButtonTapped) forControlEvents:UIControlEventTouchUpInside];
        
        self.rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
        
        [self.rightButton setImage:[UIImage imageNamed:@"right_dark.png"] forState:UIControlStateNormal];
        [self.rightButton setFrame:CGRectMake(278, 19, 32, 32)];
        [self.rightButton addTarget:self action:@selector(rightButtonTapped) forControlEvents:UIControlEventTouchUpInside];
        
        
        
        self.directionsLabel = [[UILabel alloc] initWithFrame:CGRectMake(52,0, 216, 70)];
        self.directionsLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        self.directionsLabel.textColor = [UIColor blackColor];
        self.directionsLabel.textAlignment = NSTextAlignmentCenter;
        self.directionsLabel.backgroundColor = [UIColor clearColor];
        //self.directionsLabel.font = [UIFont boldSystemFontOfSize:16];
        self.directionsLabel.numberOfLines = 0;
        self.directionsLabel.minimumFontSize = 11;
        self.directionsLabel.adjustsFontSizeToFitWidth = YES;
        
        //[self.directionsLabel sizeToFit];
        
        [self addSubview:self.leftButton];
        [self addSubview:self.directionsLabel];
        [self addSubview:self.rightButton];
        
        self.step = [RoutingService sharedService].currentStep;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(routePathLoaded) name:@"RouteServiceCompleteRoutePathLoadedNotification" object:nil];
        
    }
    return self;
}



- (id)init
{
    CGRect baseFrame = CGRectMake(0,0, [[UIScreen mainScreen] applicationFrame].size.width, 70);
    return [self initWithFrame:baseFrame];
}

- (void)routePathLoaded{
    [self loadDirectionsForStep: self.step];
}

- (void)leftButtonTapped{
    if (self.step > 0){
        self.step = self.step-1;
    }
}

- (void)rightButtonTapped{
    if (self.step < [[RoutingService sharedService] stepCount]-1){
        self.step = self.step+1;
    }
}

- (void) didSwipeRight{
    [self leftButtonTapped];
}

- (void) didSwipeLeft{
    [self rightButtonTapped];
}


- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"RouteServiceCompleteRoutePathLoadedNotification" object:nil];
}



@end
