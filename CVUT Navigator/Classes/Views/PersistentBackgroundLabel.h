//
//  PersistentBackgroundLabel.h
//  MapView
//
//  Created by Tomáš Kohout on 23.11.13.
//
//

#import <UIKit/UIKit.h>

@interface PersistentBackgroundLabel : UILabel

- (void)setPersistentBackgroundColor:(UIColor*)color;

@end
