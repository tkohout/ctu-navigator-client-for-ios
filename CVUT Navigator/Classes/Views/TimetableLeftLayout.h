//
//  TimetableTopLayout.h
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 10.03.13.
//  Copyright (c) 2013 Tomáš Kohout. All rights reserved.
//

#import "Timetable.h"

@interface TimetableLeftLayout : UICollectionViewFlowLayout

@property (nonatomic, retain) Timetable * timetable;

@end
