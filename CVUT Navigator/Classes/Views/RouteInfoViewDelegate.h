//
//  RouteInfoViewDelegate.h
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 08.03.13.
//  Copyright (c) 2013 Tomáš Kohout. All rights reserved.
//

#ifndef CVUT_Navigator_RouteInfoViewDelegate_h
#define CVUT_Navigator_RouteInfoViewDelegate_h

@class RouteInfoView;

@protocol RouteInfoViewDelegate <NSObject>
@optional

- (void) routeInfoView: (RouteInfoView *) routeInfoView stepChanged: (NSInteger) step;

@end

#endif
