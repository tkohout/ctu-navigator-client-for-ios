//
//  DetailViewController.m
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 28.10.12.
//  Copyright (c) 2012 Tomáš Kohout. All rights reserved.
//

#import "DetailViewController.h"
#import "Room.h"
#import "CenteredTableViewLabel.h"
#import "MapAllViewController.h"
#import "StaticVars.h"

@interface DetailViewController ()

@end

@implementation DetailViewController



- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:UITableViewStyleGrouped];
    return self;
}


#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0){
        return [self tableViewNumberOfRowsInContents:tableView];
    }else if(section == [self numberOfSectionsInTableView:tableView]-1){
        return [self tableViewNumberOfRowsInFooter:tableView];
    }else{
        return [self tableViewNumberOfRowsInOthers:tableView];
    }
    
}

- (NSInteger) tableViewNumberOfRowsInContents:(UITableView *)tableView {
    return 0;
}

- (NSInteger) tableViewNumberOfRowsInFooter:(UITableView *)tableView {
    return 2;
}

- (NSInteger) tableViewNumberOfRowsInOthers:(UITableView *)tableView {
    return 0;
}

//For main contents
- (UITableViewCell *) tableView:(UITableView *)tableView contentsAtIndexPath: (NSIndexPath *) indexPath cell: (UITableViewCell *)  cell{
    return cell;
}

//For additional rows in middle of the table
- (UITableViewCell *) tableView:(UITableView *)tableView othersAtIndexPath: (NSIndexPath *) indexPath cell: (UITableViewCell *)  cell{
    return cell;
}

//Footer on bottom of the table
- (UITableViewCell *) tableView:(UITableView *)tableView footerAtIndexPath: (NSIndexPath *) indexPath cell: (UITableViewCell *)  cell{
    UILabel *label = [[CenteredTableViewLabel alloc] initWithFrame:CGRectMake(0, 0, 320, 45)];
    
    label.textColor = [StaticVars sharedService].tintColor;
    
    switch (indexPath.row){
        case 0:
            label.text = NSLocalizedString(@"Navigovat", nil);
            break;
        case 1:
            label.text =  NSLocalizedString(@"Zobrazit na mapě", nil);
            break;
        case 2:
            label.text = NSLocalizedString(@"Zobrazit na plánku", nil);
            break;
    }
    
    [cell addSubview:label];
    
    return cell;
}

- (UITableViewCellStyle) tableView: (UITableView *) tableView styleForIndexPath: (NSIndexPath *)indexPath{
    return UITableViewCellStyleValue1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:[self tableView:tableView styleForIndexPath:indexPath] reuseIdentifier:nil];
    }
    
    if (indexPath.section == 0){
        cell = [self tableView:tableView contentsAtIndexPath:indexPath cell: cell];
    }else if (indexPath.section == [self numberOfSectionsInTableView:tableView]-1){
        cell = [self tableView:tableView footerAtIndexPath:indexPath cell: cell];
        
    }else{
        cell = [self tableView:tableView othersAtIndexPath:indexPath cell: cell];
    }
    return cell;
}



@end
