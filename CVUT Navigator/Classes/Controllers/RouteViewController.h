//
//  RouteViewController.h
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 24.02.13.
//  Copyright (c) 2013 Tomáš Kohout. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RoutePoint.h"


@protocol RouteViewControllerDelegate <NSObject>

@optional
- (void) routingStartedStart: (RoutePoint *) start end: (RoutePoint *) end;
@end

@interface RouteViewController : UIViewController <UITableViewDelegate, UITextFieldDelegate>
//Outlets and actions
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UITextField *startField;
@property (weak, nonatomic) IBOutlet UITextField *endField;
@property (weak, nonatomic) IBOutlet UIToolbar *toolbar;

@property (weak, nonatomic) UIViewController * senderController;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *routeButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *cancelButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *travelMode;
@property (weak, nonatomic) IBOutlet UILabel *fieldsBackground;

- (IBAction)routeTapped:(id)sender;
- (IBAction)cancelTapped:(id)sender;

- (IBAction)switchButtonTapped:(id)sender;

- (IBAction)startValueChanged:(id)sender;
- (IBAction)endValueChanged:(id)sender;


//Start and end of the navigation
@property (nonatomic, retain) RoutePoint * start;
@property (nonatomic, retain) RoutePoint * end;
@property (retain, nonatomic) id<RouteViewControllerDelegate> delegate;

@end
