//
//  DetailViewController.h
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 28.10.12.
//  Copyright (c) 2012 Tomáš Kohout. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Building.h"

@interface DetailViewController : UITableViewController
@property (weak) id  delegate;

@end
