//
//  AppDelegate.h
//


#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "Building.h"
#import "RoutePoint.h"
#import "RouteInfoViewDelegate.h"
#import "SearchBarWithOverlay.h"

#define METERS_PER_MILE 1609.344



@interface MapAllViewController: UIViewController<UITableViewDelegate, MKMapViewDelegate, SearchBarWithOverlayDelegate, UIGestureRecognizerDelegate, RouteInfoViewDelegate, UINavigationControllerDelegate>

@property (weak, nonatomic) IBOutlet MKMapView *_mapView;
@property (strong, nonatomic) IBOutlet UITableView *tableView;

//Center on the building if it is on the map
- (void) centerOnBuilding: (Building *) building;

//Starts download and drawing of path on the outside map
-(void) route;

//Starts routing, but pushes the PlanViewController immediately
//it is used when the routing is in the same building or from building outside
- (void) routeInBuildingNode: (Node *) node;

@end
