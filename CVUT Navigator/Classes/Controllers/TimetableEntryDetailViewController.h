//
//  TimetableEntryDetailViewController.h
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 07.12.12.
//  Copyright (c) 2012 Tomáš Kohout. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DetailViewController.h"
#import "TimetableEntry.h"

@interface TimetableEntryDetailViewController : UITableViewController
@property (nonatomic, strong) TimetableEntry * entry;
@end
