//
//  AppDelegate.h
//  ArrestsPlotter
//
//  Created by Malek Trabelsi on 18/03/12.
//  Copyright (c) 2012 __MediGarage.com__. All rights reserved.
//


#import "MapAllViewController.h"
//#import "Model.h"
#import "BuildingAnnotation.h"
#import "Building.h"
#import "BuildingDetailViewController.h"
#import "Room.h"
#import "Support.h"
#import "PlanViewController.h"
#import <RestKit/RestKit.h>
#import <RestKit/CoreData.h>
#import <CoreLocation/CoreLocation.h>
#import "RoutePoint.h"
#import "CourseDetailViewController.h"
#import "TimetableEntry.h"
#import "DataService.h"
#import  <QuartzCore/QuartzCore.h>
#import "RouteViewController.h"

#import "RouteInfoView.h"
#import "RoutingService.h"
#import "LocationService.h"
#import "GoogleDirectionsAPI.h"
#import "GoogleDirectionsRouteAnnotation.h"

#import "UserLocationAnnotation.h"

#import "ToastService.h"
#import "MBProgressHUD.h"
#import "TeacherDetailViewController.h"
#import "SearchBarWithOverlay.h"
#import "StaticVars.h"

#define BASE_USER_LOCATION_ZOOM_RADIUS 500

@interface MapAllViewController ()

@property (nonatomic, retain) RouteInfoView * routeInfoView;
@property (nonatomic, retain) UIButton * locationButton;
@property (nonatomic, retain) UserLocationAnnotation * marbleAnnotation;
@property (nonatomic, retain) UserLocationAnnotation * haloAnnotation;

@property (nonatomic, retain) RoutePoint * displayedRouteStart;
@property (nonatomic, retain) RoutePoint * displayedRouteEnd;
@property (nonatomic, retain) UIView * titleView;
@property (nonatomic, retain) UILabel * titleText;
@property (nonatomic, retain) UILabel * stepText;

@property (strong, nonatomic) SearchBarWithOverlay *searchBar;

@property (nonatomic, retain) NSMutableArray *searchedResults;

@end


@implementation MapAllViewController
@synthesize _mapView;

-(SearchBarWithOverlay *)searchBar{
    if (!_searchBar){
        _searchBar = [[SearchBarWithOverlay alloc] init];
        _searchBar.delegate = self;
        _searchBar.overlayView = self.view;
        _searchBar.navigationItem = self.navigationItem;
    }
    
    return _searchBar;
}

-(NSMutableArray *)searchedResults{
    if (!_searchedResults){
        _searchedResults = [[NSMutableArray alloc] init];
    }
    
    return _searchedResults;
}


UIBarButtonItem * currentLocationButton;


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}




#pragma mark View methods
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.navigationController.navigationBar.translucent = YES;
    
    [[LocationService sharedService] startUpdatingLocation];
    
    [self.view bringSubviewToFront:_mapView];
    
    //self.searchBar.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    
    [self loadTopControls];
    
    
    
    CLLocationCoordinate2D zoomLocation;
    
    zoomLocation.latitude = 50.07499;
    zoomLocation.longitude= 14.43976;
    
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation, 10000, 10000);
    MKCoordinateRegion adjustedRegion = [_mapView regionThatFits:viewRegion];
    [_mapView setRegion:adjustedRegion animated:YES];
    
    
    

    
    
    MBProgressHUD * activityIndicator = [[ToastService sharedService] toastActivityWithTitle:NSLocalizedString(@"Načítám budovy", nil) inView:nil];
    
    [[DataService sharedService] getBuildingsSuccess:^(NSArray * buildings) {
        [activityIndicator hide:YES];
        [self showBuildingAnnotations:buildings];
    }
    failure:^(NSError *error) {
        [activityIndicator hide:YES];
        if (error.code == APIErrorNotFound){
            [DataService showErrorMessageWithTitle:NSLocalizedString(@"Žádná budova nenalezena", nil) subtitle:nil onView:self.view];
        }else{
            [DataService showError:error messageOnView:self.view];
        }
    }];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateUserLocation:)
                                                 name:@"CurrentLocationNotification"
                                               object:nil];
    
    
    
    [_mapView addSubview:self.locationButton];
    
    
    
    if ([LocationService sharedService].currentLocation){
        [self showUserLocation: [LocationService sharedService].currentLocation type:[LocationService sharedService].currentLocationType];
    }
}

-(void)viewWillAppear:(BOOL)animated{
    
    if (![[RoutingService sharedService] isRouting]){
        [self setRoutingModeOn:NO];
    }else{
        [self setRoutingModeOn:YES];
        [self loadViewForCurrentStep:[RoutingService sharedService].currentStep];
    }
    
}


-(void)viewDidAppear:(BOOL)animated{
    if (self.searchBar.isSearching){
        [self.searchBar.field becomeFirstResponder];
    }
}

-(void)viewWillDisappear:(BOOL)animated{
    if (self.searchBar.isSearching){
        [self.searchBar.field resignFirstResponder];
    }
}

- (void)viewDidUnload
{
    [self set_mapView:nil];
    [self setSearchBar:nil];
    [self setTableView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}



#pragma mark Annotations

//Returns any annotation on the given coordinate
- (id) getAnnotationOnCoordinate: (CLLocationCoordinate2D) coordinate{
    for (id <MKAnnotation> annotation in _mapView.annotations){
        if (![annotation isKindOfClass:[UserLocationAnnotation class]]){
            
            if (annotation.coordinate.latitude == coordinate.latitude && annotation.coordinate.longitude == coordinate.longitude){
                return annotation;
            }
        }
    }
    
    return nil;
}

//Merges building annotations with route annotation if the have the same coordinate
- (BuildingAnnotation *) mergeBuildingAnnotation: (BuildingAnnotation *) buildingAnnotation routeAnnotation: (GoogleDirectionsRouteAnnotation *) routeAnnotation{
    
    if ([[RoutingService sharedService] isRouting]){
        if (routeAnnotation){
            //
            if (routeAnnotation.type == GoogleDirectionsRouteAnnotationEnd && [[RoutingService sharedService].end.item isKindOfClass: [Room class]]){
                buildingAnnotation.name = [RoutingService sharedService].end.name;
                buildingAnnotation.detail = buildingAnnotation.building.name;
                
            }else if (routeAnnotation.type == GoogleDirectionsRouteAnnotationStart && ([[RoutingService sharedService].start.item isKindOfClass: [Room class]])){
                buildingAnnotation.name = [RoutingService sharedService].start.name;
                buildingAnnotation.detail = buildingAnnotation.building.name;
            }
        }
        
    }
    
    return buildingAnnotation;
}

//Show building annotations on the map
- (void) showBuildingAnnotations: (NSArray *) buildings{
    
    //Clear all annotations
    for (id annotation in [_mapView annotations]){
        if ([annotation isKindOfClass:[BuildingAnnotation class]]){
            [_mapView removeAnnotation:annotation];
        }
    }
    
    for (Building * building in buildings){
                
        CLLocationCoordinate2D coordinate;
        coordinate.latitude = [building.latitude doubleValue];
        coordinate.longitude = [building.longitude doubleValue];
        
        //Create new building annotation
        BuildingAnnotation *annotation = [[BuildingAnnotation alloc] initWithName:building.name detail:building.address coordinate:coordinate building:building] ;
        
        //Get the route annotation from the coordinate
        GoogleDirectionsRouteAnnotation * routeAnnotation = [self getAnnotationOnCoordinate: coordinate];
        
        //If exists, merge them together
        if (routeAnnotation){
            annotation = [self mergeBuildingAnnotation:annotation routeAnnotation: routeAnnotation];
            //And remove route annotation
            [_mapView removeAnnotation:routeAnnotation];
        }
        
        [_mapView addAnnotation:annotation];
        
        if (routeAnnotation){
            [self._mapView  performSelector:@selector(selectAnnotation:animated:) withObject:(annotation) afterDelay: 0.5];
        }
    }
    
    [self zoomToFitMapAnnotations:self._mapView];
    
}





#pragma Centering
- (void) centerOnBuilding: (Building *) building{
    //Hide search bar when centering
    [self.searchBar hide];
    
    //Go through all annotation if there is one with the same id, select it
    for (BuildingAnnotation * annotation in [self._mapView annotations]){
        if ([annotation isKindOfClass:[BuildingAnnotation class]] && [annotation.building.id isEqualToNumber: building.id]){
            
            [self._mapView  performSelector:@selector(selectAnnotation:animated:) withObject:(annotation) afterDelay: 0.5];
            
            break;
        }
    }
    
    //Center on the coordinate
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(building.coordinate, 1000, 1000);
    [self._mapView setRegion:region animated:YES];
}

#pragma mark Map view - annotations

-(MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation{
    
    //Simple pin annotation for building
    if ([annotation isKindOfClass:[BuildingAnnotation class]]){
        
        return [self mapView:mapView viewForBuildingAnnotation:annotation];
    
    //Red and green pin for route end and start
    }else if ([annotation isKindOfClass:[GoogleDirectionsRouteAnnotation class]]) {
	
        return [self mapView:mapView viewForRouteAnnotation:annotation];
	
    //For the fake user location annotation
    }else if ([annotation isKindOfClass:[UserLocationAnnotation class]]){
        UserLocationAnnotation * userLocationAnnotation = (UserLocationAnnotation *) annotation;
        
        if (userLocationAnnotation.type == UserLocationAnnotationTypeMarble){
            return [self mapView:mapView viewForUserLocationMarble:userLocationAnnotation];
        }else{
            return [self mapView:mapView viewForUserLocationHalo:userLocationAnnotation];
        }

        
    }
    
    return nil;
}

- (MKPinAnnotationView *) mapView:(MKMapView *)mapView viewForBuildingAnnotation: (BuildingAnnotation *) annotation{
    
    MKPinAnnotationView *pinAnnotation = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:@"building"];
    
    if(!pinAnnotation) {
        pinAnnotation = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"building"] ;
    }
    
    pinAnnotation.pinColor =  MKPinAnnotationColorPurple;
    
    pinAnnotation.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    
    pinAnnotation.animatesDrop=([RoutingService sharedService].isRouting) ? NO : YES;
    pinAnnotation.canShowCallout = YES;
    pinAnnotation.calloutOffset = CGPointMake(-5, 5);
    
    return pinAnnotation;
}

- (MKPinAnnotationView *) mapView:(MKMapView *)mapView viewForRouteAnnotation: (GoogleDirectionsRouteAnnotation *) annotation{
    MKPinAnnotationView *pinAnnotation = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:@"route"];
    
    if(!pinAnnotation) {
        pinAnnotation = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"route"] ;
    }
    
    if ([(GoogleDirectionsRouteAnnotation *)annotation type] == GoogleDirectionsRouteAnnotationStart) {
        pinAnnotation.pinColor = MKPinAnnotationColorGreen;
    } else if ([(GoogleDirectionsRouteAnnotation *)annotation type] == GoogleDirectionsRouteAnnotationEnd) {
        pinAnnotation.pinColor = MKPinAnnotationColorRed;
    } else {
        pinAnnotation.pinColor = MKPinAnnotationColorPurple;
    }
    
    pinAnnotation.animatesDrop = YES;
    pinAnnotation.enabled = YES;
    pinAnnotation.canShowCallout = YES;
    return pinAnnotation;
}

- (MKAnnotationView *) mapView:(MKMapView *)mapView viewForUserLocationMarble: (UserLocationAnnotation *) annotation{
    MKAnnotationView * annotationView = [mapView dequeueReusableAnnotationViewWithIdentifier:@"userLocation"];
    
    //Simple annotation with image
    if (!annotationView){
        annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"userLocation"];
        annotationView.image = annotation.image;
        annotationView.canShowCallout = YES;
    }
    
    return  annotationView;
}

//Circle which will scale in and out
- (MKAnnotationView *) mapView:(MKMapView *)mapView viewForUserLocationHalo: (UserLocationAnnotation *) annotation{
    MKAnnotationView * annotationView = [mapView dequeueReusableAnnotationViewWithIdentifier:@"userLocationHalo"];
    if (!annotationView){
        annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"userLocationHalo"];
        annotationView.image = [UIImage imageNamed:@"TrackingDotHalo.png"];
        
        [CATransaction begin];
        [CATransaction setAnimationDuration:2.5];
        [CATransaction setAnimationTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
        
        // scale out radially
        //
        CABasicAnimation *boundsAnimation = [CABasicAnimation animationWithKeyPath:@"transform"];
        boundsAnimation.repeatCount = MAXFLOAT;
        boundsAnimation.fromValue = [NSValue valueWithCATransform3D:CATransform3DMakeScale(0.1, 0.1, 1.0)];
        boundsAnimation.toValue   = [NSValue valueWithCATransform3D:CATransform3DMakeScale(2.0, 2.0, 1.0)];
        boundsAnimation.removedOnCompletion = NO;
        boundsAnimation.fillMode = kCAFillModeForwards;
        
        [annotationView.layer addAnimation:boundsAnimation forKey:@"animateScale"];
        
        // go transparent as scaled out
        //
        CABasicAnimation *opacityAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
        opacityAnimation.repeatCount = MAXFLOAT;
        opacityAnimation.fromValue = [NSNumber numberWithFloat:1.0];
        opacityAnimation.toValue   = [NSNumber numberWithFloat:-1.0];
        opacityAnimation.removedOnCompletion = NO;
        opacityAnimation.fillMode = kCAFillModeForwards;
        
        [annotationView.layer addAnimation:opacityAnimation forKey:@"animateOpacity"];
        
        [CATransaction commit];
        
        
    }
    
    return  annotationView;
    
}

//When tapped on the pin
-(void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control{
    
    
    BuildingAnnotation * annotation = view.annotation;
    Building * building = annotation.building;
    
    RoutePoint * routeStart = [RoutingService sharedService].start;
    RoutePoint * routeEnd = [RoutingService sharedService].end;
    
    //If we are routing we will go directly to the plan, but only if it is the right building
    if ([[RoutingService sharedService] isRouting] ){
        BOOL isTheBuildingRouteStart = ([routeStart.item isKindOfClass: [Node class]] && [((Node *)routeStart.item).building.id isEqualToNumber: building.id] );
        BOOL isTheBuildingRouteEnd = ([routeEnd.item isKindOfClass: [Node class]] && [((Node *)routeEnd.item).building.id isEqualToNumber: building.id] );
        
        
        if (isTheBuildingRouteEnd || isTheBuildingRouteStart){
            
            NSInteger nextStep = [RoutingService sharedService].currentStep;
            
            if (isTheBuildingRouteEnd){
                nextStep ++;
            }else{
                nextStep--;
            }
            
            [RoutingService sharedService].currentStep = nextStep;
            
            [self loadViewForCurrentStep:nextStep];
            
            return;
        }
    }
    
    //If not, we are going to building detail
    BuildingDetailViewController * detail = [self.storyboard instantiateViewControllerWithIdentifier:@"BuildingDetailViewController"];
    detail.building = building;
    
    [self.navigationController pushViewController: detail animated:YES];
    
}

- (void)zoomToFitMapAnnotations:(MKMapView *)mapView {
    if ([mapView.annotations count] == 0) return;
    
    CLLocationCoordinate2D topLeftCoord;
    topLeftCoord.latitude = -90;
    topLeftCoord.longitude = 180;
    
    CLLocationCoordinate2D bottomRightCoord;
    bottomRightCoord.latitude = 90;
    bottomRightCoord.longitude = -180;
    
    for(id<MKAnnotation> annotation in mapView.annotations) {
        topLeftCoord.longitude = fmin(topLeftCoord.longitude, annotation.coordinate.longitude);
        topLeftCoord.latitude = fmax(topLeftCoord.latitude, annotation.coordinate.latitude);
        bottomRightCoord.longitude = fmax(bottomRightCoord.longitude, annotation.coordinate.longitude);
        bottomRightCoord.latitude = fmin(bottomRightCoord.latitude, annotation.coordinate.latitude);
    }
    
    MKCoordinateRegion region;
    region.center.latitude = topLeftCoord.latitude - (topLeftCoord.latitude - bottomRightCoord.latitude) * 0.5;
    region.center.longitude = topLeftCoord.longitude + (bottomRightCoord.longitude - topLeftCoord.longitude) * 0.5;
    region.span.latitudeDelta = fabs(topLeftCoord.latitude - bottomRightCoord.latitude) * 1.1;
    
    // Add a little extra space on the sides
    region.span.longitudeDelta = fabs(bottomRightCoord.longitude - topLeftCoord.longitude) * 1.1;
    
    region = [mapView regionThatFits:region];
    [mapView setRegion:region animated:YES];
}

#pragma mark Route path

- (BOOL) isRoutePathDisplayed{
    for (id<MKOverlay> overlay in  self._mapView.overlays){
        if ([overlay isKindOfClass:[MKPolyline class]]){
            return YES;
        }
    }
    
    return NO;
}

- (void) cleanRoutePath{
    //Remove all paths
    NSArray *selectedAnnotations = _mapView.selectedAnnotations;
    [self._mapView removeOverlays:_mapView.overlays];
    
    //Deselect
    for(id annotationView in selectedAnnotations) {
        [_mapView deselectAnnotation:annotationView  animated:NO];
    }
    
    NSMutableArray * annotations = [[NSMutableArray alloc] init];
    
    //Route clean
    for (id annotationView in _mapView.annotations){
        //Add to deleted annotations
        if ([annotationView isKindOfClass:[GoogleDirectionsRouteAnnotation class]]){
            [annotations addObject: annotationView];
        
        //Unmerge annotations
        }else if ([annotationView isKindOfClass:[BuildingAnnotation class]]){
            BuildingAnnotation * buildingAnnotation = annotationView;
            buildingAnnotation.name = buildingAnnotation.building.name;
            buildingAnnotation.detail = buildingAnnotation.building.detail;
        }
    }
    
    //Remove all route path annotations
    [_mapView removeAnnotations:annotations];
}



#pragma mark Geolocation

- (UIButton *)locationButton{
    if (!_locationButton){
        _locationButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        _locationButton.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
        [_locationButton setFrame:CGRectMake(self._mapView.bounds.size.width - 50, self._mapView.bounds.size.height - 50, 40, 40)];
        [_locationButton addTarget:self action:@selector(centerOnCurrentLocation) forControlEvents:UIControlEventTouchUpInside];
        [_locationButton setImage:[UIImage imageNamed:@"location_current_black.png"] forState:UIControlStateNormal];
        [_locationButton setEnabled:NO];
    }
    return _locationButton;
}


- (UserLocationAnnotation *)marbleAnnotation{
    if (!_marbleAnnotation){
        _marbleAnnotation = [[UserLocationAnnotation alloc] init];
        _marbleAnnotation.type = UserLocationAnnotationTypeMarble;
        [_mapView addAnnotation:_marbleAnnotation];
    }
    
    return _marbleAnnotation;
}

- (UserLocationAnnotation *)haloAnnotation{
    if (!_haloAnnotation){
        _haloAnnotation = [[UserLocationAnnotation alloc] init];
        _haloAnnotation.type = UserLocationAnnotationTypeHalo;
        [_mapView addAnnotation:_haloAnnotation];
    }
    
    return _haloAnnotation;
}

//Notification from location service
- (void)updateUserLocation:(NSNotification *) notification{
    NSDictionary *userInfo = notification.userInfo;
    CLLocation * currentLocation = [userInfo objectForKey:@"currentLocation"];
    LocationServiceCurrentLocationType locationType = [(NSNumber *)[userInfo objectForKey:@"type"] integerValue];
    [self showUserLocation: currentLocation type:locationType];
}

//Decides if there should be fake or real user location displayed
- (void) showUserLocation: (CLLocation *) currentLocation type: (LocationServiceCurrentLocationType) locationType{
    if (currentLocation){
        [self.locationButton setEnabled:YES];
        //Type is indoor, we have to use fake
        if (locationType == LocationServiceCurrentLocationTypeIndoor){
            [self showIndoorUserLocationAnnotation: currentLocation];
            _mapView.showsUserLocation = NO;
        //Type is outdoor, switch to classic mapview user location
        }else{
            [self hideIndoorUserLocationAnnotation];
            _mapView.showsUserLocation = YES;
        }
    }else{
        [self.locationButton setEnabled:NO];
    }
}

//Move indoor annotation on the current position
- (void) showIndoorUserLocationAnnotation: (CLLocation *) currentLocation{
    [self.marbleAnnotation setCoordinate:currentLocation.coordinate];
    [self.haloAnnotation setCoordinate:currentLocation.coordinate];
}

- (void) hideIndoorUserLocationAnnotation{
    [_mapView removeAnnotations:@[self.marbleAnnotation, self.haloAnnotation]];
    self.marbleAnnotation = nil;
    self.haloAnnotation = nil;
}

//Centering on the current location. If the zoom is smaller than current one, the current one is used, and the map is just moved
- (void) centerOnCurrentLocation{
    [self.searchBar hide];
    
    if ([LocationService sharedService].currentLocation){
        MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance([LocationService sharedService].currentLocation.coordinate, BASE_USER_LOCATION_ZOOM_RADIUS, BASE_USER_LOCATION_ZOOM_RADIUS);
        
        //If the zoom would be smaller than is now we will just move to the coordinate
        if (viewRegion.span.latitudeDelta > _mapView.region.span.latitudeDelta || viewRegion.span.longitudeDelta > _mapView.region.span.longitudeDelta){
            [_mapView setCenterCoordinate:[LocationService sharedService].currentLocation.coordinate animated:YES];
            //if the zoom is bigger we will zoom to region
        }else{
            MKCoordinateRegion adjustedRegion = [_mapView regionThatFits:viewRegion];
            [_mapView setRegion:adjustedRegion animated:YES];
        }
        
        
    }
}


#pragma mark Routing and directions

//Push the routing to the PlanViewController
- (void) routeInBuildingNode: (Node *) node{
    [self.searchBar hide];
    
    PlanViewController * controller = [[PlanViewController alloc] initCenteringOnNode:node animated:YES];
    
    [[self navigationController] pushViewController:controller animated:NO];
    
    if (node.building){
        [self centerOnBuilding:node.building];
    }
}

//Draws outdoor route path from the Google Directions
//Loads route info view
-(void) route{
    [self.searchBar hide];
    
    if ([RoutingService sharedService].isRouting){
        
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
        
        __block RoutePoint * routeStart = [RoutingService sharedService].start;
        __block RoutePoint * routeEnd = [RoutingService sharedService].end;
        
        
        //If the route path is not already displayed
        if (![self isRoutePathDisplayed] || ![self.displayedRouteStart isEqualToRoutePoint: routeStart] || ![self.displayedRouteEnd isEqualToRoutePoint: routeEnd]){
            
            //Loading polyline from the server
            [self loadPolylineFromGoogleDirections:routeStart end:routeEnd];
            
            //Load an info strip on top
            [self setRoutingModeOn:YES];
            
        }
    }
}


- (void) setRoutingModeOn: (BOOL) routingModeOn{
    [self loadTopControls];
    
    if (routingModeOn){
        [self.view addSubview:self.routeInfoView];
        [self.routeInfoView reload];
    }else{
        [self cleanRoutePath];
        [self.routeInfoView removeFromSuperview];
        self.routeInfoView = nil;
        
    }
}


#pragma mark Google directions
- (NSString *)getGoogleDirectionsString: (RoutePoint *) point{
    if ([point isAddress]){
        return [point name];
    }else{
        return [NSString stringWithFormat:@"%f,%f", point.coordinate.latitude, point.coordinate.longitude];
    }

    
}

- (void) loadPolylineFromGoogleDirections: (RoutePoint *) routeStart end: (RoutePoint *) routeEnd{
    MBProgressHUD * activityIndicator = [[ToastService sharedService] toastActivityWithTitle:NSLocalizedString(@"Vykresluji trasu", nil) inView:nil];
    
    [[GoogleDirectionsAPI sharedService] polylineFromStart:[self getGoogleDirectionsString:routeStart] end:[self getGoogleDirectionsString:routeEnd] mode: GoogleDirectionsModeDriving success:^(MKPolyline *polyline, MKCoordinateRegion region, CLLocationCoordinate2D start, CLLocationCoordinate2D end) {
        [self cleanRoutePath];
        [self._mapView addOverlay:polyline];
        [self._mapView setRegion:region animated:YES];
        
        //Set displayed, so it does not get downloaded next time
        self.displayedRouteStart = routeStart;
        self.displayedRouteEnd = routeEnd;
        
        
        id startAnnotation, endAnnotation;
        
        id startBuildingAnnotation = [self getAnnotationOnCoordinate:routeStart.coordinate];
        
        if (![startBuildingAnnotation isKindOfClass:[BuildingAnnotation class]]){
            startBuildingAnnotation = nil;
        }
        
        //Start route annotation
        GoogleDirectionsRouteAnnotation * startRouteAnnotation = [[GoogleDirectionsRouteAnnotation alloc] init];
        startRouteAnnotation.coordinate = ([routeStart isAddress]) ? start : [routeStart coordinate];
        startRouteAnnotation.title = [routeStart name];
        startRouteAnnotation.type = GoogleDirectionsRouteAnnotationStart;
        
        
        id endBuildingAnnotation = [self getAnnotationOnCoordinate:routeEnd.coordinate];
        
        if (![endBuildingAnnotation isKindOfClass:[BuildingAnnotation class]]){
            endBuildingAnnotation = nil;
        }
        
        //End route annotaion
        GoogleDirectionsRouteAnnotation * endRouteAnnotation = [[GoogleDirectionsRouteAnnotation alloc] init];
        endRouteAnnotation.coordinate = ([routeEnd isAddress]) ? end : [routeEnd coordinate];
        endRouteAnnotation.title = [routeEnd name];
        endRouteAnnotation.type = GoogleDirectionsRouteAnnotationEnd;
        
        //If there is building on the same place as the start of the route, merge them
        if (startBuildingAnnotation){
            startAnnotation = [self mergeBuildingAnnotation:startBuildingAnnotation routeAnnotation:startRouteAnnotation];
        }else{
            startAnnotation = startRouteAnnotation;
        }
        
        //If there is building on the same place as the end of the route, merge them
        if (endBuildingAnnotation){
            endAnnotation = [self mergeBuildingAnnotation:endBuildingAnnotation routeAnnotation:endRouteAnnotation];
        }else{
            endAnnotation = endRouteAnnotation;
        }
        
        //Select the destination
        [self._mapView  performSelector:@selector(selectAnnotation:animated:) withObject:(endAnnotation) afterDelay: 0.5];
        [self._mapView addAnnotations:[NSArray arrayWithObjects:startAnnotation, endAnnotation, nil]];
        
        [activityIndicator hide:YES];
        
    } failure:^(NSError *error) {
        [activityIndicator hide:YES];
        
        if (error.code == APIErrorNotFound){
            [DataService showErrorMessageWithTitle:NSLocalizedString(@"Venkovní trasa nebyla nalezena", nil) subtitle:nil onView:self.view];
        }else{
            [DataService showError:error messageOnView:self.view];
        }
    }];
}

- (MKOverlayView *)mapView:(MKMapView *)mapView viewForOverlay:(id <MKOverlay>)overlay {
    MKPolylineView *polylineView = [[MKPolylineView alloc] initWithPolyline:overlay];
    polylineView.strokeColor = [StaticVars sharedService].tintColor;
    polylineView.lineWidth = 4.0;
    
    return polylineView;
}

#pragma mark Top controls for routing mode

- (UIView *)titleView{
    if (!_titleView){
        _titleView = [[UIView alloc] initWithFrame:CGRectMake(0,0,216,32)];
        [_titleView addSubview:self.titleText];
        [_titleView addSubview:self.stepText];
    }
    
    return _titleView;
}

- (UILabel *)titleText{
    if (!_titleText){
        _titleText = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 216, 18)];
        _titleText.textAlignment = NSTextAlignmentCenter;
        [_titleText setFont:[UIFont boldSystemFontOfSize:16]];
        [_titleText setBackgroundColor:[UIColor clearColor]];
        _titleText.textColor = [UIColor blackColor];
        
        
    }
    
    return _titleText;
}

- (UILabel *)stepText{
    if (!_stepText){
        _stepText = [[UILabel alloc] initWithFrame:CGRectMake(0, 20, 216, 14)];
        _stepText.textAlignment = NSTextAlignmentCenter;
        [_stepText setFont:[UIFont systemFontOfSize:14]];
        [_stepText setBackgroundColor:[UIColor clearColor]];
        _stepText.textColor = [UIColor blackColor];
        
    }
    
    return _stepText;
}


- (void)loadTopControls{
    if ([RoutingService sharedService].isRouting){
        //If it is routing change title view with double lined title view
        self.navigationItem.titleView = self.titleView;
        self.titleText.text = [RoutingService sharedService].end.name;
        
        //Show step text on the second line
        if ([[RoutingService sharedService] isRoutePathCompletelyLoaded]){
            self.stepText.text = [NSString stringWithFormat:NSLocalizedString(@"%i z %i", nil), [RoutingService sharedService].currentStep+1, [[RoutingService sharedService] stepCount]];
        }else{
            self.stepText.text = NSLocalizedString(@"Načítám ...", nil);
        }
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Konec", nil) style:UIBarButtonItemStyleBordered target:self action:@selector(cancelRouting)];
    }else{
        self.navigationItem.leftBarButtonItem = nil;
        self.navigationItem.titleView = self.searchBar.field;
    }
}

//Tapped on route end button
- (void)cancelRouting{
    [[RoutingService sharedService] routeCancel];
    [self setRoutingModeOn:NO];
}

#pragma mark Route info view

-(RouteInfoView *)routeInfoView{
    if (!_routeInfoView){
        //_routeInfoView = [[RouteInfoView alloc] initWithFrame:CGRectMake(0, self.navigationController.navigationBar.frame.size.height, self.view.frame.size.width, 70)];
        _routeInfoView = [[RouteInfoView alloc] init];
        _routeInfoView.delegate = self;
    }
    
    return _routeInfoView;
}

//Step changed in route info view
- (void) routeInfoView:(RouteInfoView *)routeInfoView stepChanged:(NSInteger)step{
    [self loadViewForCurrentStep:step];
}

//Push plan view controller
- (void) loadViewForCurrentStep: (NSInteger) step{
    if ([[RoutingService sharedService] isRoutePathCompletelyLoaded] && ![[RoutingService sharedService] isStepOutdoor:step]){
        Node * node = [[RoutingService sharedService] getNodeForStep:step];
    
        PlanViewController * controller = [[PlanViewController alloc] initCenteringOnNode:node animated:NO];
        [[self navigationController] pushViewController:controller animated:NO];
    }
}



#pragma mark Search Bar

- (void)searchBar:(SearchBarWithOverlay *)searchBar textDidChange:(NSString *)text{
    
    BOOL found = [self searchTableView: text];
    if (!found){
        [self.view sendSubviewToBack:self.tableView];
        
    }else{
        [self.view sendSubviewToBack:_mapView];
    }
}

- (void)searchBarSearchingEnded:(SearchBarWithOverlay *)searchBar{
     [self.view sendSubviewToBack:self.tableView];
}


#pragma mark Search table

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    cell = [tableView dequeueReusableCellWithIdentifier:@"Searchable"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:nil];
    }
    
    
    Searchable *  object = [self.searchedResults objectAtIndex:indexPath.row];
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.textLabel.text = object.name;
    cell.detailTextLabel.text = object.detail;
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.searchedResults count];
}

//Push to right detail when tapped on search results
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    id object;
    
    object = [self.searchedResults objectAtIndex:indexPath.row];
    
    if ([object isKindOfClass:[Room class]]){
        PlanViewController * controller = [[PlanViewController alloc] initCenteringOnNode:((Room *) object) animated:NO];
        [[self navigationController] pushViewController:controller animated:YES];
        
    }else if ([object isKindOfClass:[Building class]]){
        
        [self.searchBar hide];
        
        [self centerOnBuilding:(Building *) object];
    }else if ([object isKindOfClass:[Course class]]){
        CourseDetailViewController * detail = [[CourseDetailViewController alloc] init];
        detail.course = object;
        
        [ self.navigationController pushViewController: detail animated:YES];
    }else if ([object isKindOfClass:[Person class]]){
        TeacherDetailViewController * detail = [self.storyboard instantiateViewControllerWithIdentifier:@"TeacherDetailViewController"];
        detail.person = object;
        [ self.navigationController pushViewController: detail animated:YES];
    }
    
    
    
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"CurrentLocationNotification" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"RouteServiceCompleteRoutePathLoadedNotification" object:nil];
}

- (BOOL) searchTableView: (NSString *) searchtext {
    
    [self.searchedResults removeAllObjects];
    
    if (![searchtext isEqualToString:@""]){
        
        
        [[DataService sharedService] searchAllQuery:searchtext success:^(NSArray * results) {
            for (id object in results)
            {
                [self.searchedResults addObject:object];
            }
            
            [self.tableView reloadData];
        }];
        
    }
    
    return (BOOL) [self.searchedResults count];
    
}

#pragma mark Tab bar delegate

-(void)tabBarItemTapped{
    [self.searchBar hide];
}



@end
