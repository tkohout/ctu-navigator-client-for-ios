//
//  PlanViewController.m
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 03.02.13.
//  Copyright (c) 2013 Tomáš Kohout. All rights reserved.
//

#import "PlanViewController.h"
#import "MapAllViewController.h"
#import "RMMapView.h"
//#import "RMOpenStreetMapSource.h"
#import "RMDBMapSource.h"
#import "RMMarker.h"
#import "DataService.h"
#import "Plan.h"
#import "Floor.h"
#import "Node.h"
#import <RestKit/RestKit.h>
#import "Room.h"
#import "RoutingService.h"
#import "LocationService.h"
#import "ClassroomDetailViewController.h"
#import "VuforiaParentViewController.h"
#import "PlanView.h"
#import "RoutePoint.h"

@interface PlanViewController ()
    @property (nonatomic, retain) Plan * plan;

    @property (nonatomic, retain) UISegmentedControl * segmentedControl;
    @property (nonatomic, retain) UIToolbar * toolbar;
    @property (nonatomic, retain) UILabel * titleLabel;
    @property (nonatomic, retain) UILabel * stepLabel;
    @property (nonatomic, retain) UIView * titleView;
    @property (nonatomic, retain) PlanView * planView;
    @property (nonatomic, assign) BOOL isLoading;
    @property (nonatomic, retain) UIButton * locationButton;
@end

@implementation PlanViewController

- (PlanView *)planView{
    
    if (!_planView){
        CGRect frame = [[UIScreen mainScreen] applicationFrame];
        
        _planView = [[PlanView alloc] initWithFrame: frame];
        
        _planView.disableDragging = NO;
        
        _planView.delegate = self;
        
    }
    
    return _planView;
}

- (PlanViewController *)initCenteringOnNode:(Node *)node animated: (BOOL) animated{
    self = [super init];
    if (self){
        self.isLoading = YES;
        
    
        
        //Load building first
        [[DataService sharedService] getBuildingForNode:node success:^(Building *building) {
            
            //Load plan for building
            [[DataService sharedService] getPlanForBuilding:building success:^(Plan *plan) {
                self.isLoading = NO;
                
                self.plan = plan;
                self.planView.floor = [node floor];
                self.planView.zoom = 21.0f;
                self.planView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
                [self.planView centerOnNode: node animated:animated];
                    
            } failure:^(NSError *error) {
                self.isLoading = NO;
                
                if (error.code == APIErrorNotFound){
                    self.titleLabel.text = NSLocalizedString(@"Plán nenalezen", nil);
                    [DataService showErrorMessageWithTitle:NSLocalizedString(@"Plán budovy nebyl nalezen", nil) subtitle:nil onView:self.view];
                }else{
                    [DataService showError:error messageOnView:self.view];
                }
                
                
            }];
            
            
        } failure:^(NSError *error) {
            if (error.code == APIErrorNotFound){
                self.titleLabel.text = NSLocalizedString(@"Budova nenalezena", nil);
                [DataService showErrorMessageWithTitle:NSLocalizedString(@"Budova nebyla nalezena", nil) subtitle:nil onView:self.view];
            }else{
                [DataService showError:error messageOnView:self.view];
            }
            
            self.isLoading = NO;
        }];
    }
    return self;
}


-(PlanViewController *)initWithBuilding:(Building *)building{
    self = [super init];
    if (self){
        self.isLoading = YES;
        [[DataService sharedService] getPlanForBuilding:building success:^(Plan *plan) {
            self.plan = plan;
            self.planView.floor =[plan groundFloor];
            self.isLoading = NO;
            [self.planView centerOnCoordinate:CLLocationCoordinate2DMake([building.latitude doubleValue], [building.longitude doubleValue]) animated:NO];
            
            
            self.planView.zoom = 19.0f;
        } failure:^(NSError *error) {
            self.isLoading = NO;
            
            if (error.code == APIErrorNotFound){
                self.titleLabel.text = NSLocalizedString(@"Plán nenalezen", nil);
                [DataService showErrorMessageWithTitle:NSLocalizedString(@"Plán budovy nebyl nalezen", nil) subtitle:nil onView:self.view];
            }else{
                [DataService showError:error messageOnView:self.view];
            }
            
           
        }];
    }
    return self;
}

#pragma mark View methods


- (void)viewDidLoad{
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateUserLocation:)
                                                 name:@"CurrentIndoorLocationNotification"
                                               object:nil];
    
}


- (BOOL)prefersStatusBarHidden {
    
#ifndef VUFORIA_DISABLE
    
    if (self.delegate && [self.delegate isKindOfClass:[VuforiaParentViewController class]]){
        return YES;
    }
    
#endif
    
    
    return NO;
}

- (void) viewWillAppear:(BOOL)animated{
    [self showTopControlls];
    [self.view addSubview:self.planView];
    
    if ([LocationService sharedService].currentIndoorLocationNode){
        self.locationButton.enabled = YES;
    }else{
        self.locationButton.enabled = NO;
    }
    
    [self.view addSubview:self.locationButton];
    
    
}

- (void)viewWillDisappear:(BOOL)animated{
    if (self.delegate && [self.delegate respondsToSelector:@selector(planViewControllerDismissed)]){
        [self.delegate planViewControllerDismissed];
    }
}


- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"CurrentIndoorLocationNotification" object:nil];
}

- (void)viewDidUnload {
    [super viewDidUnload];
}

#pragma mark Loading

- (void)setIsLoading:(BOOL)isLoading{
    _isLoading = isLoading;
    
    if (isLoading){
        self.titleLabel.text = @"Načítám...";
    }else{
        self.titleLabel.text = self.planView.title;
    }
    
    self.planView.isLoading = isLoading;
}


#pragma mark Top controlls
- (UISegmentedControl *)segmentedControl{
    if (!_segmentedControl){
        _segmentedControl = [[UISegmentedControl alloc] initWithItems:
                             [NSArray arrayWithObjects:
                              [UIImage imageNamed:@"up.png"],
                              [UIImage imageNamed:@"down.png"],
                              nil]];
        
        [_segmentedControl addTarget:self action:@selector(changeFloor) forControlEvents:UIControlEventValueChanged];
        _segmentedControl.frame = CGRectMake(0, 0, 90, 30);
        _segmentedControl.segmentedControlStyle = UISegmentedControlStyleBar;
        _segmentedControl.momentary = YES;
    }
    
    return _segmentedControl;
}


- (UILabel *)titleLabel{
    if (!_titleLabel){
        
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] applicationFrame].size.width - 100 - self.segmentedControl.frame.size.width, 32)];
        _titleLabel.backgroundColor = [UIColor redColor];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        [_titleLabel setFont:[UIFont boldSystemFontOfSize:19]];
        [_titleLabel setBackgroundColor:[UIColor clearColor]];
        _titleLabel.textColor = [UIColor blackColor];
        
    }
    
    return _titleLabel;
}

- (void)resizeTitleLabel{
    NSInteger height, fontSize;
    if ([RoutingService sharedService].isRouting){
        height = 18;
        fontSize = 16;
    }else{
        height = 32;
        fontSize = 19;
    }
    
    [self.titleLabel setFrame:CGRectMake(0, 0, self.titleLabel.frame.size.width, height)];
    [self.titleLabel setFont:[UIFont boldSystemFontOfSize:fontSize]];
    
}


- (UIView *)titleView{
    if (!_titleView){
        _titleView = [[UIView alloc] initWithFrame:CGRectMake(0,0,[[UIScreen mainScreen] applicationFrame].size.width - 100 - self.segmentedControl.frame.size.width,32)];
        [_titleView addSubview:self.titleLabel];
    }
    
    return _titleView;
}


- (UILabel *)stepLabel{
    if (!_stepLabel){
        _stepLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 20, [[UIScreen mainScreen] applicationFrame].size.width - 100 - self.segmentedControl.frame.size.width, 14)];
        _stepLabel.textAlignment = NSTextAlignmentCenter;
        [_stepLabel setFont:[UIFont systemFontOfSize:14]];
        [_stepLabel setBackgroundColor:[UIColor clearColor]];
        _stepLabel.textColor = [UIColor blackColor];
    }
    
    return _stepLabel;
}

- (UIToolbar *)toolbar{
    if (!_toolbar){
        _toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 49)];
        
        UIBarButtonItem * cancelButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelPlanViewController)];
        UIBarButtonItem *segmentBarItem = [[UIBarButtonItem alloc] initWithCustomView:self.segmentedControl];
        
        UIBarButtonItem * titleItem = [[UIBarButtonItem alloc] initWithCustomView:self.titleView];
        
        
        [_toolbar setItems:@[cancelButtonItem, titleItem, segmentBarItem]];
        
    }
    
    return _toolbar;
}

- (void)cancelPlanViewController{
    [self dismissModalViewControllerAnimated:YES];
}

//If it is routing, show End button, 2 lined title view and segmented control
//If it is not, show one lined title view and segmented control
- (void) showTopControlls{
    if (self.navigationController){
        UIBarButtonItem *segmentBarItem = [[UIBarButtonItem alloc] initWithCustomView:self.segmentedControl];
        self.navigationItem.rightBarButtonItem = segmentBarItem;
        
       
        self.navigationItem.titleView = self.titleView;
        if ([RoutingService sharedService].isRouting){
            self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Konec", nil) style:UIBarButtonItemStyleBordered target:self action:@selector(cancelRouting)];
            [_titleView addSubview:self.stepLabel];
        }else{
            self.navigationItem.leftBarButtonItem = self.navigationItem.backBarButtonItem;
            if (_stepLabel){
                [self.stepLabel removeFromSuperview];
            }
        }
    }else{
        
        [self.view addSubview:self.toolbar];
        
        if ([RoutingService sharedService].isRouting){
            [_titleView addSubview:self.stepLabel];
        }else{
            if (_stepLabel){
                [self.stepLabel removeFromSuperview];
            }
        }
    }
    
    [self resizeTitleLabel];
}

-(void)cancelRouting{
    [[RoutingService sharedService] routeCancel];
    [self.planView cancelRouting];
    [self showTopControlls];
}

//Tapped on segmented control
- (void)changeFloor{
    if (self.segmentedControl.selectedSegmentIndex == 0){
        [self.planView changeFloor:PlanViewChangeFloorDirectionUp];
    }else{
        [self.planView changeFloor:PlanViewChangeFloorDirectionDown];
    }
}


- (void) reloadRouteSteps{
    if ([RoutingService sharedService].isRouting){
        if ([[RoutingService sharedService] isRoutePathCompletelyLoaded]){
            self.stepLabel.text = [NSString stringWithFormat:NSLocalizedString(@"%i z %i", nil), [RoutingService sharedService].currentStep+1, [RoutingService sharedService].stepCount];
        }else{
            self.stepLabel.text = NSLocalizedString(@"Načítám...", nil);
        }
    }
}

#pragma mark PlanView delegate
- (void)planView:(PlanView *)planView titleChanged:(NSString *)title{
    self.titleLabel.text = title;
    [self reloadRouteSteps];
}

-(void)planView:(PlanView *)planView tappedOnNode:(Node *)node{
    if ([node isKindOfClass:[Room class]]){
        ClassroomDetailViewController * controller = [[ClassroomDetailViewController alloc] init];
        controller.room = (Room *)node;
        [self.navigationController pushViewController:controller animated:YES];
    }
}


#pragma mark User location
- (void)updateUserLocation:(NSNotification *) notification{
    [self.locationButton setEnabled:YES];
}

- (UIButton *)locationButton{
    if (!_locationButton){
        _locationButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        _locationButton.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
        [_locationButton setFrame:CGRectMake(self.view.bounds.size.width - 50, self.view.bounds.size.height - 50, 40, 40)];
        [_locationButton addTarget:self action:@selector(centerOnCurrentLocation) forControlEvents:UIControlEventTouchUpInside];
        [_locationButton setImage:[UIImage imageNamed:@"location_current_black.png"] forState:UIControlStateNormal];
        [_locationButton setEnabled:NO];
    }
    return _locationButton;
}

- (void) centerOnCurrentLocation{
    Node * node = [LocationService sharedService].currentIndoorLocationNode;
    
    if ([RoutingService sharedService].isRouting){
        NSInteger step  = [[RoutingService sharedService] getStepForNode:node addToStepsWhenMissing:NO];
        if (step != NSNotFound){
            [RoutingService sharedService].currentStep = step;
            [self.planView reloadRouteInfoView];
            
        }
    }
    
    if (self.plan && node.buildingId == self.plan.buildingId){
        [self.planView centerOnNode:node animated:YES];
    }else{
        //What happen when the current location is in a different building?
    }
}



- (void)viewDidLayoutSubviews{
    
    if (self.navigationController){
        self.planView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    }else{
        self.planView.frame = CGRectMake(0, self.toolbar.frame.size.height, self.view.frame.size.width, self.view.frame.size.height-self.toolbar.frame.size.height);
    }
    
}

- (void)routeInfoView:(RouteInfoView *)routeInfoView stepChanged:(NSInteger)step{
    [self reloadRouteSteps];
    
    if ([[RoutingService sharedService] isStepOutdoor:step]){
        
        NSArray *viewControllers = self.navigationController.viewControllers;
        //parent view controller
        MapAllViewController *mapController = [viewControllers objectAtIndex:viewControllers.count - 2];
        
        [[self navigationController] popToRootViewControllerAnimated:NO];
        
        [mapController route];
    }else{
        Node * node = [[RoutingService sharedService] getNodeForStep:step];
        [self.planView centerOnNode:node animated:YES withHighlight:YES];
        
    }
}

@end
