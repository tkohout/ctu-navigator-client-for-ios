//
//  TeacherDetailViewController.m
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 12.05.13.
//  Copyright (c) 2013 Tomáš Kohout. All rights reserved.
//

#import "TeacherDetailViewController.h"
#import "DataService.h"
#import "Room.h"
#import "ClassroomDetailViewController.h"
#import "RouteViewController.h"
#import "PlanViewController.h"
#import "StaticVars.h"

@interface TeacherDetailViewController ()

@end

@implementation TeacherDetailViewController

#pragma mark View methods
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (self.person){
        
        self.navigationItem.title = self.person.name;
        self.nameCell.detailTextLabel.text = self.person.name;
        self.nameCell.detailTextLabel.textColor = [UIColor blackColor];
        
        
        self.usernameCell.detailTextLabel.text = self.person.username;
        self.usernameCell.detailTextLabel.textColor = [UIColor blackColor];
        
        
        self.emailCell.detailTextLabel.text = self.person.email;
        self.emailCell.detailTextLabel.textColor = [StaticVars sharedService].tintColor;
        self.emailCell.selectionStyle = UITableViewCellSelectionStyleBlue;
        
        
        
        self.officeCell.detailTextLabel.text = @"";
        self.officeCell.detailTextLabel.textColor = [StaticVars sharedService].tintColor;
        
        [self setDirectionsEnabled: NO];
        
        if (self.person.room){
            
            UIActivityIndicatorView * spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
            spinner.hidesWhenStopped = YES;
            spinner.center = self.officeCell.detailTextLabel.center;
            
            [self.officeCell addSubview:spinner];
            [spinner startAnimating];
            [[DataService sharedService] getRoomForPerson: self.person success:^(Room *room) {
                //self.building = building;
                self.officeCell.detailTextLabel.text = room.name;
                [spinner stopAnimating];
                [self setDirectionsEnabled: YES];
                [self.tableView reloadData];
            } failure:^(NSError *error) {
                [spinner stopAnimating];
                
                if (error.code == APIErrorNotFound){
                    [DataService showErrorMessageWithTitle:NSLocalizedString(@"Kancelář nebyla nalezena", nil)  subtitle:nil onView:self.view];
                }else{
                    [DataService showError:error messageOnView:self.view];
                }
                
            }];
            
        }else{
            if (self.person.roomName){
                self.officeCell.detailTextLabel.text = self.person.roomName;
            }
        }
        
    }
}

#pragma mark Direction and show map buttons
- (void) setDirectionsEnabled: (BOOL) enabled{
    if (enabled){
        self.navigateCell.textLabel.textColor = [UIColor blackColor];
        self.navigateCell.selectionStyle = UITableViewCellSelectionStyleBlue;
        self.showOnTheMapCell.textLabel.textColor = [UIColor blackColor];
        self.showOnTheMapCell.selectionStyle = UITableViewCellSelectionStyleBlue;
    }else{
        self.navigateCell.textLabel.textColor = [UIColor grayColor];
        self.navigateCell.selectionStyle = UITableViewCellSelectionStyleNone;
        self.showOnTheMapCell.textLabel.textColor = [UIColor grayColor];
        self.showOnTheMapCell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0){
        return (![self.person.hasRoom boolValue] && self.person.roomName.length == 0) ? 3 : 4;
    }
    
    return 2;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
        if (indexPath.section == 0 && indexPath.row == 3){
            
            ClassroomDetailViewController * controller = [[ClassroomDetailViewController alloc] init];
            if (self.person.room){
                controller.room = self.person.room;
            }else if (self.person.roomName){
                controller.roomName = self.person.roomName;
            }else{
                return;
            }
            [self.navigationController pushViewController:controller animated:YES];
        }else if (indexPath.section == 0 && indexPath.row == 2){
            
            NSString * mailto = [NSString stringWithFormat:@"mailto:%@", self.person.email];
            
            NSString *url = [mailto stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding ];
            [[UIApplication sharedApplication]  openURL: [NSURL URLWithString: url]];
            
        }else if (indexPath.section == 1 && indexPath.row == 0){
            if (self.person.room){
            RouteViewController * controller = [self.navigationController.storyboard instantiateViewControllerWithIdentifier:@"RouteViewController"];
            
            RoutePoint * startPoint = [[RoutePoint alloc] init];
            startPoint.item = CURRENT_LOCATION;
            
            
            RoutePoint * endPoint = [[RoutePoint alloc] init];
            endPoint.item = self.person.room;
            
            controller.senderController = self;
            [self presentModalViewController:controller animated:YES];
            
            [controller setStart: startPoint];
            [controller setEnd: endPoint];
            }
            //Zobrazit na mapě
        }else if (indexPath.section == 1 && (indexPath.row == 1)){
            if (self.person.room){
            PlanViewController * controller = [[PlanViewController alloc] initCenteringOnNode:self.person.room animated:NO];
            [self.navigationController pushViewController: controller animated:YES];
            }
        }
        
    
    
}

- (void)viewDidUnload {
    [self setNameCell:nil];
    [self setUsernameCell:nil];
    [self setEmailCell:nil];
    [self setOfficeCell:nil];
    [self setOfficeCell:nil];
    [self setNavigateCell:nil];
    [self setShowOnTheMapCell:nil];
    [super viewDidUnload];
}
@end
