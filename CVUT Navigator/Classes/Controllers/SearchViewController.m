//
//  SearchViewController.m
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 23.11.13.
//  Copyright (c) 2013 Tomáš Kohout. All rights reserved.
//

#import "SearchViewController.h"
#import "SearchBarWithOverlay.h"

#import "Room.h"
#import "ClassroomDetailViewController.h"
#import "PlanViewController.h"
#import  "Building.h"
#import "RoutingService.h"
#import "BuildingDetailViewController.h"
#import "Course.h"
#import "CourseDetailViewController.h"
#import "TimetableEntry.h"
#import "TimetableEntryDetailViewController.h"
#import "Person.h"
#import "TeacherDetailViewController.h"
#import "DataService.h"


@interface SearchViewController ()  <SearchBarWithOverlayDelegate>

@property (strong, nonatomic) SearchBarWithOverlay *searchBar;
@property (nonatomic, retain) NSMutableArray *searchedResults;

@end

@implementation SearchViewController

-(SearchBarWithOverlay *)searchBar{
    if (!_searchBar){
        _searchBar = [[SearchBarWithOverlay alloc] init];
        _searchBar.delegate = self;
        _searchBar.overlayView = self.view;
        _searchBar.enableOverlay = NO;
        _searchBar.navigationItem = self.navigationItem;
    }
    
    return _searchBar;
}

- (NSMutableArray *)searchedResults{
    if (!_searchedResults){
        _searchedResults = [[NSMutableArray alloc] init];
    }
    
    return _searchedResults;
}


#pragma mark Initialization
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.titleView = self.searchBar.field;
    self.edgesForExtendedLayout = UIRectEdgeNone;
}

-(void)viewDidAppear:(BOOL)animated{
    [self.tableView reloadData];
    //[self.searchBar.field becomeFirstResponder];
    
}

-(void)viewWillDisappear:(BOOL)animated{
    [self.searchBar.field resignFirstResponder];
}

#pragma mark Table view delegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    id object;
    
    object = [self.searchedResults objectAtIndex:indexPath.row];
    
    if ([object isKindOfClass:[Room class]]){
        
        ClassroomDetailViewController * detail = [[ClassroomDetailViewController alloc] init];
        detail.room = object;
        [ self.navigationController pushViewController: detail animated:YES];
        
    }else if ([object isKindOfClass:[Building class]]){
        if ([RoutingService sharedService].isRouting){
            PlanViewController * controller = [[PlanViewController alloc] initWithBuilding:object];
            [self.navigationController pushViewController: controller animated:YES];
        }else{
            BuildingDetailViewController * detail = [self.storyboard instantiateViewControllerWithIdentifier:@"BuildingDetailViewController"];
            detail.building = object;
            [ self.navigationController pushViewController: detail animated:YES];
        }
    }else if ([object isKindOfClass:[Course class]]){
        CourseDetailViewController * detail = [[CourseDetailViewController alloc] init];
        detail.course = object;
        [ self.navigationController pushViewController: detail animated:YES];
    }else if ([object isKindOfClass:[TimetableEntry class]]){
        TimetableEntryDetailViewController * detail = [[TimetableEntryDetailViewController alloc] init];
        detail.entry = object;
        [ self.navigationController pushViewController: detail animated:YES];
    }else if ([object isKindOfClass:[Person class]]){
        TeacherDetailViewController * detail = [self.storyboard instantiateViewControllerWithIdentifier:@"TeacherDetailViewController"];
        detail.person = object;
        [ self.navigationController pushViewController: detail animated:YES];
    }
    
    
}



#pragma mark Table view data source

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    
    cell = [tableView dequeueReusableCellWithIdentifier:@"Searchable"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:nil];
    }
    
    
    Searchable *  object = [self.searchedResults objectAtIndex:indexPath.row];
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.textLabel.text = object.name;
    cell.detailTextLabel.text = object.detail;
    
    return cell;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return nil;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.searchedResults count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

#pragma mark Search bar
- (void)searchBar:(SearchBarWithOverlay *)searchBar textDidChange:(NSString *)searchText{
    [self searchAllQuery: searchText];
}

- (void) searchAllQuery: (NSString *) searchText{
    [[DataService sharedService] searchAllQuery:searchText success:^(NSArray * results) {
        
        [self.searchedResults removeAllObjects];
        
        if (![searchText isEqual:@""]){
            for (id object in results)
            {
                [self.searchedResults addObject:object];
            }
        }
        
        [self.tableView reloadData];
    }];
}

- (void)searchBarSearchingBegin:(SearchBarWithOverlay *)searchBar{
    [self searchAllQuery: searchBar.field.text];
}

-(void)searchBarSearchingEnded:(SearchBarWithOverlay *)searchBar{
    [self searchAllQuery: searchBar.field.text];
    [self.tableView reloadData];
}




#pragma mark Tab bar item

- (void) tabBarItemTapped{
    [self.searchBar hide];
    [self.tableView reloadData];
}


- (void)viewDidUnload {
    [self setSearchBar:nil];
    [self setSearchBar:nil];
    [super viewDidUnload];
}
@end
