//
//  BuildingDetailViewController.h
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 28.10.12.
//  Copyright (c) 2012 Tomáš Kohout. All rights reserved.
//

#import "DetailViewController.h"

#import "Building.h"
#import "RMMapView.h"

@interface BuildingDetailViewController : UITableViewController 
    
@property (nonatomic, strong) Building * building;
@property (weak, nonatomic) IBOutlet UITableViewCell *planCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *addressCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *navigateCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *showOnMapCell;


@end
