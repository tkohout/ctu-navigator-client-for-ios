//
//  NavigationController.h
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 08.05.13.
//  Copyright (c) 2013 Tomáš Kohout. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NavigationController : UINavigationController <UINavigationControllerDelegate>

@end
