//
//  PlanViewController.h
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 03.02.13.
//  Copyright (c) 2013 Tomáš Kohout. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "RMMapView.h"
#import "PlanView.h"
#import "Plan.h"
#import "RoutePoint.h"
#import "Node.h"

@protocol PlanViewControllerDelegate <NSObject>
@optional
- (void) planViewControllerDismissed;
@end

@interface PlanViewController : UIViewController <PlanViewDelegate, RouteInfoViewDelegate>{
IBOutlet RMMapView * mapView;
}

@property (nonatomic, retain) id<PlanViewControllerDelegate> delegate;

//Init plan view and center on node when the plan is loaded
- (PlanViewController *)initCenteringOnNode:(Node *)node animated: (BOOL) animated;

//Init plan view and center on building when p
- (PlanViewController *)initWithBuilding: (Building*) building;



@end
