//
//  LoginViewController.m
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 25.04.13.
//  Copyright (c) 2013 Tomáš Kohout. All rights reserved.
//

#import "LoginViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "DataService.h"
#import "User.h"
#import "AppDelegate.h"
#import "ToastService.h"
#import "MBProgressHUD.h"
#import "TextTableViewCell.h"
#import "AMBlurView.h"
#import "TPKeyboardAvoidingScrollView.h"

@interface LoginViewController() <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImage;
@property (nonatomic, retain) AMBlurView * blurView;
@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIImageView *logoView;
@end

@implementation LoginViewController

-(void)showWarning{
    MBProgressHUD * hud = [[ToastService sharedService] toastTitle:NSLocalizedString(@"Byl jste odhlášen", nil) subtitle:NSLocalizedString(@"Došlo k tomu pravděpodobně kvůli změně hesla, nebo smazání účtu", nil) image:nil inView:self.view autohide:NO];
    [hud hide:YES afterDelay:5];
    
}

- (AMBlurView *)blurView{
    if (!_blurView){
        _blurView = [AMBlurView new];
        [_blurView setFrame:self.view.frame];
        //[_blurView setAlpha:0.9f];
    }
    return _blurView;
}


#pragma mark Textfield delegate
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    [self textFieldUpdateReturnKey:textField];
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    [self textFieldUpdateReturnKey:textField];
    return YES;
}

/* Handle of the return key - if the other field is empty, next return key should be displayed*/
- (void) textFieldUpdateReturnKey: (UITextField *) textField{
    UIReturnKeyType keyType;
    
    if (textField == self.usernameTextField){
        if ([self.passwordTextField.text isEqualToString:@""] || self.passwordTextField.text == nil){
            keyType = UIReturnKeyNext;
        }else{
            keyType = UIReturnKeySend;
        }
        
        if ( self.usernameTextField.returnKeyType != keyType){
            self.usernameTextField.returnKeyType = keyType;
            [self.usernameTextField reloadInputViews];
        }
        
    }else{
        if ([self.usernameTextField.text isEqualToString:@""] || self.usernameTextField.text == nil){
            keyType = UIReturnKeyNext;
        }else{
            keyType = UIReturnKeySend;
        }
        
        if ( self.passwordTextField.returnKeyType != keyType){
            self.passwordTextField.returnKeyType = keyType;
            [self.passwordTextField reloadInputViews];
        }
        
    }

}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if ([self.passwordTextField.text isEqualToString:@""] || self.passwordTextField.text == nil){

        [self.passwordTextField becomeFirstResponder];
    }else if ([self.usernameTextField.text isEqualToString:@""] || self.usernameTextField.text == nil){
        
        [self.usernameTextField becomeFirstResponder];
    }else{
        [self login];
    }
    
    
    return YES;
}

- (void) login{
    
    MBProgressHUD * hud = [[ToastService sharedService] toastActivityWithTitle:NSLocalizedString(@"Přihlašuji", nil) inView:nil];
    
    [[DataService sharedService] loginWithUsername:self.usernameTextField.text password:self.passwordTextField.text onSuccess:^(User * user) {
        AppDelegate* appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        appDelegate.window.rootViewController = [self.storyboard instantiateInitialViewController];
        [hud hide:YES];
        
    } failure:^(NSError *error) {
        [hud hide:YES];
        
        if (error.code == APIErrorUnauthorized){
            [DataService showErrorMessageWithTitle:NSLocalizedString(@"Špatné jméno nebo heslo", nil) subtitle:nil onView:self.view];
        }else{
            [DataService showError:error messageOnView:self.view];
        }
    }];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    [self.scrollView addSubview:self.blurView];
    [self.scrollView bringSubviewToFront: self.loginButton];
    [self.scrollView bringSubviewToFront: self.logoView];
    [self.scrollView bringSubviewToFront: self.usernameTextField];
    [self.scrollView bringSubviewToFront: self.passwordTextField];
    
    //[self.usernameTextField becomeFirstResponder];
}

#pragma mark Table view data source
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section  == 0){
        return 100;
    }else if (indexPath.section  == 1 || indexPath.section  == 2){
        return 40;
    }
    
    return 50;
    
    //return [super tableView:tableView heightForRowAtIndexPath:indexPath];
}


- (IBAction)loginPressed:(id)sender {
    [self login];
}

@end
