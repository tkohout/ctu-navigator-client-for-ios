//
//  NavigationController.m
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 08.05.13.
//  Copyright (c) 2013 Tomáš Kohout. All rights reserved.
//

#import "NavigationController.h"
#import "MapAllViewController.h"
#import "PlanViewController.h"
#import "BuildingDetailViewController.h"

@interface NavigationController ()

@end

@implementation NavigationController

//Prepared for future advanced push handling
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.delegate = self;
	// Do any additional setup after loading the view.
}





@end
