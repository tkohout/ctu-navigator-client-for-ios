//
//  TeacherDetailViewController.h
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 12.05.13.
//  Copyright (c) 2013 Tomáš Kohout. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Person.h"

@interface TeacherDetailViewController : UITableViewController
@property (nonatomic, retain) Person * person;
@property (weak, nonatomic) IBOutlet UITableViewCell *nameCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *usernameCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *emailCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *officeCell;

@property (weak, nonatomic) IBOutlet UITableViewCell *navigateCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *showOnTheMapCell;

@end
