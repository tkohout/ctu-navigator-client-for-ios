//
//  ARTabBarController.m
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 18.03.13.
//  Copyright (c) 2013 Tomáš Kohout. All rights reserved.
//

#import "ARTabBarController.h"
#import "ARParentViewController.h"
#import "VuforiaParentViewController.h"
#import "QCARutils.h"
#import "NavigationController.h"

@interface ARTabBarController ()

@end

@implementation ARTabBarController 

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.delegate = self;
}

- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController
{
    if ([viewController isKindOfClass:[UINavigationController class]]){
        //Send navigation controllers to their root view controllers
        [(UINavigationController *)viewController popToRootViewControllerAnimated:NO ];

        UIViewController * topController = [(UINavigationController *)viewController topViewController];
        if ([topController respondsToSelector:@selector(tabBarItemTapped)]){
            [topController performSelector: @selector(tabBarItemTapped)];
        }
        
    }
    
    #ifndef VUFORIA_DISABLE
    
    if ([viewController isKindOfClass:[ARParentViewController class]])
    {
        //Connect the application with Vuforia
        QCARutils *qUtils = [QCARutils getInstance];
        
        qUtils.targetType = TYPE_FRAMEMARKERS;
        qUtils.orientation = UIInterfaceOrientationPortrait;
        
        [qUtils cameraSetContinuousAFMode:YES];
        
        VuforiaParentViewController * cameraViewController = (VuforiaParentViewController*)viewController;
        [cameraViewController setAppWindow:[[UIApplication sharedApplication] keyWindow]];
        cameraViewController.arViewSize = self.view.bounds.size;
       
        
    }
    
    #endif
    
    return YES;
}

@end
