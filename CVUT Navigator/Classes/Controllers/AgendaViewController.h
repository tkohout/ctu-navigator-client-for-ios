//
//  AgendaViewController.h
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 23.11.13.
//  Copyright (c) 2013 Tomáš Kohout. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AgendaViewControllerChildProtocol <NSObject>

- (void)moveToToday;

@end

@interface AgendaViewController : UIViewController

@end
