//
//  AgendaViewController.m
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 23.11.13.
//  Copyright (c) 2013 Tomáš Kohout. All rights reserved.
//

#import "AgendaViewController.h"

#import "ListViewController.h"
#import "TimetableViewController.h"

@interface AgendaViewController ()
@property (nonatomic, retain) UISegmentedControl * toolbar;

@property (nonatomic, retain) ListViewController * listController;
@property (nonatomic, retain) TimetableViewController * weekController;


@end



@implementation AgendaViewController

- (UISegmentedControl *) toolbar{
    if (!_toolbar){
        
        NSArray * items = @[NSLocalizedString(@"Seznam", nil), NSLocalizedString(@"Týden", nil)];
        
        
        _toolbar = [[UISegmentedControl alloc] initWithItems:items];
        
        [_toolbar setSelectedSegmentIndex:0];
        [_toolbar addTarget:self action:@selector(toolbarTapped) forControlEvents:UIControlEventValueChanged];
    }
    
    return _toolbar;
}



- (void)toolbarTapped{
    if (self.toolbar.selectedSegmentIndex == 0){
        
        [self transitionFromViewController:self.weekController toViewController:self.listController duration:0.5 options:UIViewAnimationTransitionNone animations:^{
            ;
        } completion:^(BOOL finished) {
            ;
        }];
        
    }else{
        
        [self transitionFromViewController:self.listController toViewController:self.weekController duration:0.5 options:UIViewAnimationTransitionNone animations:^{
            ;
        } completion:^(BOOL finished) {
            ;
        }];
        
    }
}

-(void) todayTapped{
    if (self.toolbar.selectedSegmentIndex == 0){
        [self.listController moveToToday];
    }else{
        [self.weekController moveToToday];
    }
}

- (ListViewController *)listController{
    if (!_listController){
        _listController = [self.storyboard instantiateViewControllerWithIdentifier:@"ListViewController"];
    }
    
    return _listController;
}

- (TimetableViewController *)weekController{
    if (!_weekController){
        _weekController = [self.storyboard instantiateViewControllerWithIdentifier:@"TimetableViewController"];
    }
    
    return _weekController;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.navigationItem.titleView = self.toolbar;
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Dnes", nil) style:UIBarButtonItemStylePlain target:self action:@selector(todayTapped)];
    [self addChildViewController:self.weekController];
    [self addChildViewController:self.listController];
    
    [self.view addSubview:self.listController.view];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
