//
//  BuildingDetailViewController.m
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 28.10.12.
//  Copyright (c) 2012 Tomáš Kohout. All rights reserved.
//

#import "BuildingDetailViewController.h"
#import "RouteViewController.h"
#import "PlanViewController.h"
#import "Building.h"
#import "Floor.h"
#import "Node.h"
#import "Room.h"
#import "RoutePoint.h"
#import "PlanView.h"
#import "DataService.h"
#import "MapAllViewController.h"
#import "ClassroomDetailViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "ToastService.h"
#import "TextTableViewCell.h"
#import "AMBlurView.h"
#import "StaticVars.h"

@interface BuildingDetailViewController () <PlanViewDelegate, RouteInfoViewDelegate>
@property (nonatomic, retain) UISegmentedControl * segmentedControl;
@property (nonatomic, retain) PlanView * planView;
@property (nonatomic, retain) AMBlurView * blurView;

@property (nonatomic, retain) UIButton * showPlanButton;
@property (nonatomic, assign) BOOL planViewFullscreen;
@property (nonatomic, assign) BOOL hasPlan;

@end

@implementation BuildingDetailViewController

#pragma mark initializations

UIActivityIndicatorView * spinner;

-(void)setHasPlan:(BOOL)hasPlan{
    _hasPlan = hasPlan;
    [self.tableView reloadData];
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0)
        return 1.0f;
    
    return [super tableView: tableView heightForHeaderInSection:section];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0 && indexPath.row == 0 && self.hasPlan == NO){
        return 50;
    }else{
        return [super tableView:tableView heightForRowAtIndexPath:indexPath];
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0 && indexPath.row == 0 && self.hasPlan == NO){
            UITableViewCell * cell = [[TextTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
            cell.textLabel.text = NSLocalizedString(@"Bohužel, k této budově zatím nemáme mapové podklady", nil);
            
            return cell;
    }else{
        return [super tableView:tableView cellForRowAtIndexPath:indexPath];
    }
}

-(PlanView *)planView{
    if (!_planView){
        _planView = [[PlanView alloc] initWithFrame: CGRectMake(0,0, self.planCell.contentView.bounds.size.width, self.planCell.contentView.bounds.size.height)];
        _planView.delegate = self;
        _planView.disableDragging = YES;
        [_planView setCornerRadius: 10];
    }
    
    return _planView;
}

- (UIButton *)showPlanButton{
    if (!_showPlanButton){
        _showPlanButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        
        int width = self.planCell.contentView.frame.size.width, height = self.planCell.contentView.frame.size.height;
        
        _showPlanButton.frame = CGRectMake(0, 0, width, height);
        _showPlanButton.titleLabel.font = [UIFont systemFontOfSize:25.0f];
        
        [_showPlanButton setTitle:NSLocalizedString(@"Zobrazit plánek", nil) forState:UIControlStateNormal];
        
        [_showPlanButton addTarget:self action:@selector(showPlanButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return _showPlanButton;
}

- (UISegmentedControl *)segmentedControl{
    if (!_segmentedControl){
        _segmentedControl = [[UISegmentedControl alloc] initWithItems:
                             [NSArray arrayWithObjects:
                              [UIImage imageNamed:@"up.png"],
                              [UIImage imageNamed:@"down.png"],
                              nil]];
        
        [_segmentedControl addTarget:self action:@selector(changeFloor) forControlEvents:UIControlEventValueChanged];
        _segmentedControl.frame = CGRectMake(0, 0, 90, 30);
        _segmentedControl.segmentedControlStyle = UISegmentedControlStyleBar;
        _segmentedControl.momentary = YES;
    }
    
    return _segmentedControl;
}

- (AMBlurView *)blurView{
    if (!_blurView){
        _blurView = [AMBlurView new];
        [_blurView setFrame:CGRectMake(0.0f,0.0f,self.planCell.contentView.frame.size.width,self.planCell.contentView.frame.size.height)];
        [_blurView setAlpha:0.9f];
    }
    return _blurView;
}


#pragma mark View methods

- (void)viewDidLayoutSubviews{
    if (!self.planViewFullscreen){
        [self.planView setFrame:CGRectMake(0,0, self.planCell.contentView.bounds.size.width, self.planCell.contentView.bounds.size.height)];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tableView.contentInset = UIEdgeInsetsMake(-1.0f, 0.0f, 0.0f, 0.0);
    
    self.navigationItem.title = self.building.name;
    self.addressCell.textLabel.text = self.building.address;
    self.hasPlan = NO;
    
    self.navigateCell.textLabel.textColor = [StaticVars sharedService].tintColor;
    self.showOnMapCell.textLabel.textColor = [StaticVars sharedService].tintColor;
    
    if ([self.building.planId integerValue] != 0){
        self.hasPlan = YES;
        
        spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        spinner.center = CGPointMake(self.planCell.contentView.center.x - spinner.frame.size.width/2, self.planCell.contentView.center.y );
        spinner.hidesWhenStopped = YES;
        
        
        [self.planCell.contentView addSubview: spinner];
        
        [spinner startAnimating];
        
        //Loading plan view in one of the cells
        
        [[DataService sharedService] getPlanForBuilding:self.building success:^(Plan *plan) {
            [self loadPlanView:plan];
            [spinner stopAnimating];
        } failure:^(NSError *error) {
            if (error.code == APIErrorNotFound){
                [DataService showErrorMessageWithTitle:NSLocalizedString(@"Chyba", nil) subtitle:NSLocalizedString(@"Plán budovy nebyl nalezen", nil) onView:self.view];
                self.hasPlan = NO;
            }else{
                [DataService showError:error messageOnView:self.view];
            }
            
            [spinner stopAnimating];
        }];
    }
}

- (void)loadPlanView: (Plan *)plan{
    
    
    Floor * floor;
    
    //Setting showed floor as ground floor, if it exists
    if (plan.groundFloor && plan.groundFloor.canBeShowedOnPlan){
        floor = plan.groundFloor;
    }else{
        //Otherwise we will find some other floor
        for (Floor * floorInPlan in plan.floors){
            if (floorInPlan.canBeShowedOnPlan){
                floor = floorInPlan;
            }
        }
    }
    
    if (!floor){
        self.hasPlan = NO;
        [self.tableView reloadData];
        return;
    }
    
    self.planView.floor = floor;
    
    
    
    self.planView.zoom = 21.0f;
    
    if (!self.planViewFullscreen){
        
        
        [self.planCell.contentView addSubview:self.planView];
        
       
        
        [self.planCell.contentView addSubview:self.blurView];
        
        [self.planCell.contentView addSubview:self.showPlanButton];
        
    }
    
    if (floor == plan.groundFloor && [plan.entrances count] > 0){
        [self.planView centerOnCoordinate: ((Node *)[plan.entrances objectAtIndex:0]).coordinate animated:NO];
    }else{
        [self.planView centerOnCoordinate:CLLocationCoordinate2DMake([[self.building latitude] doubleValue], [[self.building longitude] doubleValue]) animated:NO];
    }
}

#pragma mark PlanViewDelegate

-(void)planView:(PlanView *)planView titleChanged:(NSString *)title{
    if (self.planViewFullscreen){
        self.navigationItem.title = title;
    }
}

-(void)planView:(PlanView *)planView tappedOnNode:(Node *)node{
    if ([node isKindOfClass:[Room class]]){
        ClassroomDetailViewController * controller = [[ClassroomDetailViewController alloc] init];
        controller.room = (Room *)node;
        [self.navigationController pushViewController:controller animated:YES];
    }
}

#pragma mark Top controlls

- (void)changeFloor{
    if (self.segmentedControl.selectedSegmentIndex == 0){
        [self.planView changeFloor:PlanViewChangeFloorDirectionUp];
    }else{
        [self.planView changeFloor:PlanViewChangeFloorDirectionDown];
    }
}


- (void) hideTopControlls{
    self.navigationItem.leftBarButtonItem = self.navigationItem.backBarButtonItem;
    self.navigationItem.rightBarButtonItem = nil;
    self.navigationItem.title = self.building.name;
}
 
- (void) showTopControlls{
    UIBarButtonItem *segmentBarItem = [[UIBarButtonItem alloc] initWithCustomView:self.segmentedControl];
    self.navigationItem.rightBarButtonItem = segmentBarItem;
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelPlanViewFullscreen)];
    self.navigationItem.title = self.planView.title;
}


#pragma mark Fullscreen animations

- (void)showPlanButtonTapped{
    if (!self.planViewFullscreen){
        [self setPlanViewFullscreen];
    }
}

//Enlarges plan view on whole screen
- (void) setPlanViewFullscreen{
    [self.tableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
    
    [self showTopControlls];
    
    [self.planView removeFromSuperview];
    [self.view addSubview:self.planView];
    
    [self.blurView removeFromSuperview];
    [self.view addSubview:self.blurView];
    
    [self.planView setFrame:CGRectMake(self.planCell.bounds.origin.x + 10, self.planCell.frame.origin.y, self.planView.frame.size.width, self.planView.frame.size.height)];
    [self.blurView setFrame:CGRectMake(self.planCell.bounds.origin.x + 10, self.planCell.frame.origin.y, self.blurView.frame.size.width, self.blurView.frame.size.height)];
    
    
    
    [self.planView resizingBegan];
    self.planView.disableDragging = NO;
    
    self.planViewFullscreen = YES;
    
    [UIView animateWithDuration:0.3 animations:^{
        [self.planView setFrame:self.view.frame];
        [self.blurView setFrame:self.view.frame];
        [self.blurView setAlpha:0.0f];
    } completion:^(BOOL finished) {
        [self.planView resizingEnded];
        [self.tableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
    }];
}

//Minimizes plan view to its cell
- (void) cancelPlanViewFullscreen{
    
    [self hideTopControlls];
    
    [self.planView resizingBegan];
    
    [UIView animateWithDuration:0.3 animations:^{
        [self.planView setFrame:CGRectMake(self.planCell.bounds.origin.x + 10, self.planCell.frame.origin.y, self.planCell.frame.size.width-20, self.planCell.frame.size.height-3)];
        [self.blurView setFrame:CGRectMake(self.planCell.bounds.origin.x + 10, self.planCell.frame.origin.y, self.planCell.frame.size.width-20, self.planCell.frame.size.height-3)];
        [self.blurView setAlpha:0.90f];
    } completion:^(BOOL finished) {
        [self.planView removeFromSuperview];
        [self.planCell.contentView addSubview:self.planView];
        
        [self.planView setFrame:CGRectMake(0,0, self.planCell.contentView.bounds.size.width, self.planCell.contentView.bounds.size.height)];
        
        [self.blurView removeFromSuperview];
        [self.planCell.contentView addSubview:self.blurView];
        [self.planCell.contentView sendSubviewToBack:self.blurView];
        [self.blurView setFrame:CGRectMake(0,0, self.planCell.contentView.bounds.size.width, self.planCell.contentView.bounds.size.height)];
        
        [self.planCell.contentView sendSubviewToBack:self.planView];
        
        self.planView.disableDragging = YES;
        
        self.planViewFullscreen = NO;
        [self.planView resizingEnded];
    }];
}




#pragma mark TableView data source
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    if (section == 0){
        return @"";
    }else if (section == 2){
        return  NSLocalizedString(@"Adresa", nil);
    }else{
        return @"";
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0){
        return 1;
    }else if (section == 1){
        return 2;
    }else{
        return 1;
    }
}

- (void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (cell == self.addressCell){
        cell.textLabel.font = [UIFont systemFontOfSize:15];
    }
}


#pragma mark TableView delegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell * cell = [tableView cellForRowAtIndexPath:indexPath];
    
    //Navigating to the building
    if (cell == self.navigateCell){
        RouteViewController * controller = [self.storyboard instantiateViewControllerWithIdentifier:@"RouteViewController"];
        
        RoutePoint * startPoint = [[RoutePoint alloc] initAsCurrentLocation];
        
        RoutePoint * endPoint = [[RoutePoint alloc] init];
        endPoint.item = self.building;
        
        controller.senderController = self;
        [self presentModalViewController:controller animated:YES];
        
        if (cell == self.navigateCell){
            [controller setStart: startPoint];
            [controller setEnd: endPoint];
        }        
        
    
    //Showing the building on the outside map
    }else if (cell == self.showOnMapCell){
        
        MapAllViewController * controller = [self.storyboard instantiateViewControllerWithIdentifier:@"MapAllViewController"];
        [self.navigationController pushViewController:controller animated:YES];
        [controller centerOnBuilding: self.building];
        
    }
}

#pragma mark Copy paste handling
-(void)tableView:(UITableView*)tableView performAction:(SEL)action forRowAtIndexPath:(NSIndexPath*)indexPath withSender:(id)sender{
    
    UITableViewCell * cell = [self tableView:tableView cellForRowAtIndexPath:indexPath];
    
    if (cell == self.addressCell){
        UIPasteboard *pb = [UIPasteboard generalPasteboard];
        [pb setString:cell.textLabel.text];
    }
}
-(BOOL)tableView:(UITableView*)tableView canPerformAction:(SEL)action forRowAtIndexPath:(NSIndexPath*)indexPath withSender:(id)sender{
    
    UITableViewCell * cell = [self tableView:tableView cellForRowAtIndexPath:indexPath];
    
    if (cell == self.addressCell){
        if (action == @selector(copy:)) {
            return YES;
        }
    }
    
    return NO;

}
-(BOOL)tableView:(UITableView*)tableView shouldShowMenuForRowAtIndexPath:(NSIndexPath*)indexPath{
    
    UITableViewCell * cell = [self tableView:tableView cellForRowAtIndexPath:indexPath];
    
    if (cell == self.addressCell){
        return YES;
    }
    
    return NO;
    
}


- (void)viewDidUnload {
    [self setPlanCell:nil];
    [self setAddressCell:nil];
    [self setNavigateCell:nil];
    [self setShowOnMapCell:nil];
    [self setShowOnMapCell:nil];
    [self setShowOnMapCell:nil];
    [self setShowOnMapCell:nil];
    [super viewDidUnload];
}
@end
