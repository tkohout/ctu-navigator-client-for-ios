//
//  ARTabBarController.h
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 18.03.13.
//  Copyright (c) 2013 Tomáš Kohout. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ARTabBarController : UITabBarController <UITabBarControllerDelegate>

@end
