//
//  CourseDetailViewController.m
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 11.12.12.
//  Copyright (c) 2012 Tomáš Kohout. All rights reserved.
//

#import "CourseDetailViewController.h"
#import "TimetableEntry.h"
#import "Room.h"
#import "TimetableEntryDetailViewController.h"
#import "StaticVars.h"

@interface CourseDetailViewController ()
    @property (nonatomic, retain) NSMutableArray * entries;
@end

@implementation CourseDetailViewController

#pragma mark Initializations
- (NSMutableArray *)entries{
    if (!_entries){
        _entries = [[NSMutableArray alloc] init];
    }
    return _entries;
}

#pragma mark View methods
-(void)viewDidLoad{
    [super viewDidLoad];
    
    if (self.course) {
        
        self.navigationItem.title = self.course.name;
        if (self.course.timetableEntries){
            for (TimetableEntry * entry in self.course.timetableEntries){
                [self.entries addObject:entry];
            }
        }
    }
}


#pragma mark Table View data source
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    if (section == 1){
        return NSLocalizedString(@"Hodiny", nil);
    }
    
    return nil;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 3;
}

- (NSInteger) tableViewNumberOfRowsInContents:(UITableView *)tableView {
    return 2;
}

- (NSInteger) tableViewNumberOfRowsInOthers:(UITableView *)tableView {
    return [self.entries count];
}


- (UITableViewCell *) tableView:(UITableView *)tableView contentsAtIndexPath: (NSIndexPath *) indexPath cell: (UITableViewCell *)  cell{
    cell.textLabel.textColor = cell.detailTextLabel.textColor;
    cell.detailTextLabel.textColor = [UIColor blackColor];
    
    switch (indexPath.row){
        case 0:
            
            cell.textLabel.text = NSLocalizedString(@"Název", nil);
            cell.detailTextLabel.text = self.course.longName;
            [cell setSelectionStyle:(UITableViewCellSelectionStyleNone)];
            break;
        case 1:
            cell.textLabel.text = NSLocalizedString(@"Web", nil);
            cell.detailTextLabel.text =  self.course.web;
            cell.detailTextLabel.textColor = [StaticVars sharedService].tintColor;
            break;
    }
    
    return cell;
}

- (UITableViewCell *) tableView:(UITableView *)tableView othersAtIndexPath: (NSIndexPath *) indexPath cell: (UITableViewCell *)  cell{
    TimetableEntry * entry = [self.entries objectAtIndex:indexPath.row];
    
    cell.textLabel.textColor = cell.detailTextLabel.textColor;
    cell.detailTextLabel.textColor = [StaticVars sharedService].tintColor;
    
    cell.textLabel.text = [entry timeFormatted];
    
    cell.detailTextLabel.text = [NSString stringWithFormat:NSLocalizedString(@"%@ v %@", nil), [entry typeString], entry.roomName];
    return cell;
}

#pragma mark TableView delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    //Web
    if (indexPath.section == 0 && indexPath.row == 1){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString: self.course.web]];
    }else if (indexPath.section == 1){
        TimetableEntry * entry = [self.entries objectAtIndex:indexPath.row];
        
        TimetableEntryDetailViewController * bController = [[TimetableEntryDetailViewController alloc] init];
        bController.entry = entry;
        [self.navigationController pushViewController:bController animated:YES];
    }
}

@end
