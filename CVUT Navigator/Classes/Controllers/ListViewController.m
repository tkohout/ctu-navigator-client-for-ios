//
//  FirstViewController.m
//  CVUT Navigator
///Users/kvoky/XCode/CVUT Navigator/CVUT Navigator/MapViewController.h
//  Created by Tomáš Kohout on 23.09.12.
//  Copyright (c) 2012 Tomáš Kohout. All rights reserved.
//

#import "ListViewController.h"
#import "Room.h"
#import "Building.h"

#import "ClassroomDetailViewController.h"
#import "BuildingDetailViewController.h"
#import "CourseDetailViewController.h"
#import "TimetableEntryDetailViewController.h"
#import "TeacherDetailViewController.h"

//#import "Model.h"
#import "Searchable.h"
#import "Course.h"
#import "TimetableEntry.h"
#import "DataService.h"
#import "Event.h"
#import "WeekInfo.h"
#import "Calendar.h"

#import "SVPullToRefresh.h"

#import "ToastService.h"
#import "SearchBarWithOverlay.h"
#import "RoutingService.h"
#import "PlanViewController.h"


#import "PersistentBackgroundLabel.h"

@interface ListViewController()<SearchBarWithOverlayDelegate>

@property (nonatomic, retain) NSDate *dateFrom;
@property (nonatomic, retain) NSDate *dateTo;
@property (nonatomic, retain) WeekInfo *weekInfo;
@property (nonatomic, retain) NSMutableDictionary *entries;



@end


@implementation ListViewController

- (NSMutableDictionary *)entries{
    if (!_entries){
        _entries = [[NSMutableDictionary alloc] init];
    }
    
    return _entries;
}




#pragma mark Initialization
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    
    MBProgressHUD * hud = [[ToastService sharedService] toastActivityWithTitle:NSLocalizedString(@"Načítám rozvrh ...", nil) inView:nil];
    
    [[DataService sharedService] getTimetableSuccess:^(NSArray * timetable) {
        
        [self loadAgendaForCurrentWeekSuccess:^{
            [hud hide:YES];
            [self.tableView reloadData];
        }];
        
    } failure:^(NSError * error) {
        if (error.code == APIErrorNotFound){
            [DataService showErrorMessageWithTitle:NSLocalizedString(@"Rozvrh nenalezen", nil) subtitle:NSLocalizedString(@"Váš rozvrh je prázdný", nil) onView:nil];
        }else{
            [DataService showError:error messageOnView:nil];
            [hud hide:YES];
        }
    }];
    
    [[DataService sharedService] createDummyMarkers];
    
    //[self loadAgendaForCurrentWeekSuccess:^{
#warning Download all nodes in the buildings, so the user can search them. Just for testing!!
    /*MBProgressHUD * hud = [[ToastService sharedService] toastActivityWithTitle:NSLocalizedString(@"Načítám budovy ...", nil) inView:nil];
     
     [[DataService sharedService] getBuildingsSuccess:^(NSArray * buildings) {
     for (Building * building in buildings){
     if ([building.hasPlan boolValue]){
     [[DataService sharedService] getPlanForBuilding:building success:^(Plan *plan) {
     [[DataService sharedService] createDummyMarkers];
     
     [hud hide:YES];
     } failure:^(NSError *error) {
     [hud hide:YES];
     [DataService showError:error messageOnView:nil];
     }];
     }
     }
     } failure:^(NSError *error) {
     [hud hide:YES];
     [DataService showError:error messageOnView:nil];
     }];*/
    
    
    //}];
    
    
    
}

- (void) initializePullToRefresh{
    DEFINE_BLOCK_SELF;
    
    /*[self.tableView addPullToRefreshWithActionHandler:^{
     [blockSelf performSelector:@selector(loadPrevious) withObject:nil afterDelay:0.5];
     }];*/
    
    [self.tableView.pullToRefreshView setTitle:NSLocalizedString(@"Předchozí týden uvolněním", nil) forState:SVPullToRefreshStateTriggered];
    [self.tableView.pullToRefreshView setTitle:NSLocalizedString(@"Předchozí týden popotažením", nil) forState:SVPullToRefreshStateStopped];
    [self.tableView.pullToRefreshView setTitle:NSLocalizedString(@"Načítám...", nil) forState:SVPullToRefreshStateLoading];
    
    [self.tableView addInfiniteScrollingWithActionHandler:^{
        [blockSelf performSelector:@selector(loadNext) withObject:nil afterDelay:0.2];
    }];
    
    [self.tableView.infiniteScrollingView setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];
    
    [self.tableView triggerInfiniteScrolling];
    
    
}

#pragma mark Data loading
- (void) loadPrevious{
    if (self.weekInfo){
        NSDate * dateFrom =  [Calendar date:self.dateFrom byAddingWeeks:-1];
        NSInteger week = [[self.weekInfo getWeekForDate:self.dateFrom] integerValue];
        
        if (week < 1){
            week = 1;
            dateFrom = [[DataService sharedService] getWeekStart:week];
        }
        
        self.dateFrom = dateFrom;
        
        NSInteger parity = [[self.weekInfo getParityForDate:self.dateFrom] integerValue];
        
        [self loadAgendaForWeek:week parity:parity success:^{
            [self.tableView.pullToRefreshView stopAnimating];
            if (week <= 1){
                self.tableView.showsPullToRefresh = NO;
            }
        } failure:nil];
    }
}

- (void) loadNext{
    if (self.weekInfo){
        self.dateTo =  [Calendar date:self.dateTo byAddingHours:23 minutes:59 seconds:59 days:6 weeks:0 months:0 years:0];
        NSInteger week = [[self.weekInfo getWeekForDate:self.dateTo ] integerValue];
        NSInteger parity = [[self.weekInfo getParityForDate:self.dateTo ] integerValue];
        
        [self loadAgendaForWeek:week parity:parity success:^{
            [self.tableView.infiniteScrollingView stopAnimating];
        } failure:nil];
    }
}

- (void) loadAgendaForWeek: (NSInteger) week parity: (NSInteger) parity success: (void(^)())onSuccess failure: (void(^)(NSError * error))onFailure{
    
    [[DataService sharedService] getEntriesForWeekGroupedByDate:week parity:parity success:^(NSDictionary * entries) {
        if ([entries count]==0){
            [DataService showErrorMessageWithTitle:NSLocalizedString(@"Váš rozvrh je prázdný", nil) subtitle:nil onView:self.view];
        }
        
        for (NSDate * date in entries){
            [self.entries setObject:[entries objectForKey:date] forKey:date];
        }
        
        onSuccess();
        
        [self.tableView reloadData];
        
    } failure:^(NSError * error) {
        if (error.code == APIErrorNotFound){
            [DataService showErrorMessageWithTitle:NSLocalizedString(@"Váš rozvrh se nepodařilo najít", nil) subtitle:nil onView:self.view];
        }else{
            [DataService showError:error messageOnView:self.view];
        }
        if (onFailure){
            onFailure(error);
        }
    }];
}

- (void) loadAgendaForCurrentWeekSuccess: (void(^)())onSuccess{
    
    
    [[DataService sharedService] getWeekInfoSuccess:^(WeekInfo * weekInfo) {
        self.weekInfo = weekInfo;
        
        
        
        self.dateFrom = [NSDate date];
        self.dateTo = [[DataService sharedService] getWeekEnd:[weekInfo.currentWeek integerValue]];
        
        [self loadAgendaForWeek:[weekInfo.currentWeek integerValue] parity:[weekInfo.currentParity integerValue] success:^{
            
            [self loadNext];
            [self initializePullToRefresh];
            onSuccess();
            
        } failure:nil];
        
    } failure:^(NSError *error) {
        [DataService showError:error messageOnView:self.view];
    }];
}



#pragma mark Table view delegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    id object;
    object = [self getEntryForIndexPath:indexPath];
    
    TimetableEntryDetailViewController * detail = [[TimetableEntryDetailViewController alloc] init];
    detail.entry = object;
    [ self.navigationController pushViewController: detail animated:YES];
}



#pragma mark Table view data source

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    
    cell = [tableView dequeueReusableCellWithIdentifier:@"Entry"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"Entry"];
    }
    
    TimetableEntry *  entry = [self getEntryForIndexPath:indexPath];
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    for (UIView * subView in cell.contentView.subviews){
        [subView removeFromSuperview];
    }
    
    UIFont * smallFont = [UIFont systemFontOfSize:14.0];
    
    UILabel * timeStartLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 15, 50, 15)];
    timeStartLabel.text = [entry startTimeFormatted];
    timeStartLabel.textAlignment = NSTextAlignmentRight;
    
    [timeStartLabel setFont:smallFont];
    timeStartLabel.textColor = [UIColor blackColor];
    timeStartLabel.highlightedTextColor = [UIColor whiteColor];
    
    UILabel * timeEndLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 35, 50, 15)];
    timeEndLabel.text = [entry endTimeFormatted];
    timeEndLabel.textColor = [UIColor grayColor];
    timeEndLabel.textAlignment = NSTextAlignmentRight;
    [timeEndLabel setFont:smallFont];
    timeEndLabel.highlightedTextColor = [UIColor whiteColor];
    
    
    UILabel * subtitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(97, 35, cell.bounds.size.width - 100, 15)];
    subtitleLabel.text = [entry roomName];
    subtitleLabel.textColor = [UIColor grayColor];
    [subtitleLabel setFont:smallFont];
    subtitleLabel.highlightedTextColor = [UIColor whiteColor];
    
    
    PersistentBackgroundLabel * lineLabel = [[PersistentBackgroundLabel alloc] initWithFrame:CGRectMake(80, 2, 2, cell.bounds.size.height-4)];
    
    [lineLabel setPersistentBackgroundColor:entry.color];
    
    
    
    UILabel * courseLabel = [[UILabel alloc] initWithFrame:CGRectMake(97, 10, cell.bounds.size.width - 100, 25)];
    courseLabel.text = [NSString stringWithFormat:@"%@ - %@", entry.course.name, [entry typeString]];
    [courseLabel setFont:[UIFont systemFontOfSize:17]];
    courseLabel.backgroundColor = [UIColor clearColor];
    courseLabel.highlightedTextColor = [UIColor whiteColor];
    
    
    if ([entry isKindOfClass:[Event class]]){
        Event * event = (Event *) entry;
        courseLabel.text = event.eventName;
        subtitleLabel.text = event.eventDetail;
    }
    
    [cell.contentView addSubview:subtitleLabel];
    [cell.contentView addSubview:timeStartLabel];
    [cell.contentView addSubview:timeEndLabel];
    [cell.contentView addSubview:courseLabel];
    [cell.contentView addSubview:lineLabel];
    
    return cell;
}

- (TimetableEntry *) getEntryForIndexPath: (NSIndexPath *) indexPath{
    NSDate * date = [self getDateForSection:indexPath.section];
    NSArray * entries = [self getEntriesForDate:date];
    
    return [entries objectAtIndex:indexPath.row];
}

- (NSArray *) getEntriesForDate: (NSDate *) date{
    
    NSArray * entries = [self.entries objectForKey:date];
    
    //If the day is the first one (today) and we want to show only classes from current time
    if ([Calendar isDate:date sameDayAs:self.dateFrom]){
        NSMutableArray * updatedEntries = [[NSMutableArray alloc] initWithCapacity:[entries count]];
        
        for (TimetableEntry * entry in entries){
            if ([entry.endTime integerValue] >= [Calendar minutesFromDate:self.dateFrom]){
                [updatedEntries addObject:entry];
            }
        }
        
        entries = updatedEntries;
    }
    
    return entries;
}


-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    
        NSDate * date = [self getDateForSection:section];
        
        NSInteger day = [Calendar weekDayOfDate:date];
        
        NSArray * entries = [self getEntriesForDate:date];
        
        if ([entries count] == 0){
            return nil;
        }
        
        NSString * dayString;
        
        switch (day) {
            case 1:
                dayString = NSLocalizedString(@"Po", nil);
                break;
            case 2:
                dayString = NSLocalizedString(@"Út", nil);
                break;
            case 3:
                dayString = NSLocalizedString(@"St", nil);
                break;
            case 4:
                dayString = NSLocalizedString(@"Čt", nil);
                break;
            case 5:
                dayString = NSLocalizedString(@"Pá", nil);
                break;
            case 6:
                dayString = NSLocalizedString(@"So", nil);
                break;
            case 7:
                dayString = NSLocalizedString(@"Ne", nil);
                break;
        }
        
        NSDateFormatter * formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"dd. MM.";
        
        if ([Calendar isDate:date sameDayAs:[NSDate date]]){
            return [NSString stringWithFormat:@"%@, %@", NSLocalizedString(@"Dnes", nil), [formatter stringFromDate:date]];
        }else{
            return [NSString stringWithFormat:@"%@, %@", dayString, [formatter stringFromDate:date]];
        }
        
    
    
    return nil;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
        NSDate * date = [self getDateForSection:section];
        NSArray * entries = [self getEntriesForDate:date];
        
        return [entries count];
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
            NSInteger number = [Calendar daysBetween:self.dateFrom and:self.dateTo];
        return number;
    
}



- (NSDate *) getDateForSection: (NSInteger) section{
    if (section == 0){
        return [Calendar resetSecondsMinutesAndHoursOfDate:self.dateFrom];
    }
    return [Calendar resetSecondsMinutesAndHoursOfDate: [Calendar date:self.dateFrom byAddingDays:section]];
}

#pragma mark Agenda Child View Protocol
- (void)moveToToday{
    
    if (self.entries){
        NSDate * now = [NSDate date];
        int section = [self getClosestSectionTo:now];
        [self.tableView setContentOffset:CGPointZero animated:YES];
        //[self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:section] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
}

- (int) getClosestSectionTo: (NSDate * ) toDate {
    
    return 0;
    
    NSDate * closestDate;
    
    NSDate * previousDate;
    
    toDate = [Calendar resetSecondsMinutesAndHoursOfDate:toDate];
    
    int section = 0;
    
    for (NSDate * date in self.entries){
        if (previousDate){
            //Prev date <= now && now < date
            if (([previousDate compare:toDate] == NSOrderedDescending || [previousDate compare:toDate] == NSOrderedSame) && [toDate compare:date] == NSOrderedDescending) {
                closestDate = date;
                break;
            }
        }else{
            if ([toDate compare:date] == NSOrderedDescending || [toDate compare:date] == NSOrderedSame) {
                closestDate = date;
                break;
            }
        };
        previousDate = date;
        section ++;
    }
    
    if (!closestDate){
        section = section -1;
    }
    
    return section;
}


#pragma mark Tab bar item

- (void) tabBarItemTapped{
    [self.tableView reloadData];
}


- (void)viewDidUnload {
    [super viewDidUnload];
}
@end
