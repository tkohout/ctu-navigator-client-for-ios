//
//  FirstViewController.h
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 23.09.12.
//  Copyright (c) 2012 Tomáš Kohout. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchBarWithOverlay.h"
#import "AgendaViewController.h"

@interface ListViewController  : UITableViewController <AgendaViewControllerChildProtocol>

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;


- (void) tabBarItemTapped;
- (void) moveToToday;

@end
