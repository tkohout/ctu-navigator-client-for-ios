//
//  ClassroomDetailViewController.m
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 28.10.12.
//  Copyright (c) 2012 Tomáš Kohout. All rights reserved.
//

#import "ClassroomDetailViewController.h"
#import "BuildingDetailViewController.h"
#import "PlanViewController.h"
#import "Room.h"
#import "Building.h"
#import "Floor.h"
#import "Plan.h"
#import "MapAllViewController.h"
#import "RouteViewController.h"
#import "DataService.h"
#import "RoutingService.h"
#import "CenteredTableViewLabel.h"
#import "TextTableViewCell.h"
#import "StaticVars.h"

@interface ClassroomDetailViewController()

@property (nonatomic, retain) Building * building;
@property (nonatomic, retain) UIActivityIndicatorView * buildingSpinner;

@end

@implementation ClassroomDetailViewController

#pragma mark Initializations
-(UIActivityIndicatorView *)buildingSpinner{
    if (!_buildingSpinner){
        _buildingSpinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        _buildingSpinner.hidesWhenStopped = YES;
        [_buildingSpinner startAnimating];
    }
    
    return _buildingSpinner;
}

#pragma mark View methods
-(void)viewDidLoad{
    [super viewDidLoad];
    
    if (self.room){
        
        self.navigationItem.title = self.room.name;
        
        //Loading building for current classroom
        [[DataService sharedService] getBuildingForNode:self.room success:^(Building *building) {
            self.building = building;
            
            [self.buildingSpinner stopAnimating];
            
            [self.tableView reloadData];
        } failure:^(NSError *error) {
            [self.buildingSpinner stopAnimating];
            
            if (error.code == APIErrorNotFound){
                [DataService showErrorMessageWithTitle:NSLocalizedString(@"Chyba", nil) subtitle:NSLocalizedString(@"Budova učebny nebyla nalezena", nil) onView:self.view];
            }else{
                [DataService showError:error messageOnView:self.view];
            }
            
        }];
        
    }else if (self.roomName){
        self.navigationItem.title = self.roomName;
    }
    
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:UITableViewStyleGrouped];
    return self;
}

#pragma mark Table View data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0){
        if (!self.room){
            return 1;
        }else{
            return 2;
        }
    }else{
        return 2;
    }
}

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell;
    
    
    
    if (indexPath.section == 0){
        
        
        
        if (self.room){
             cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
            if (cell == nil) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"Cell"];
            }
            
            cell.textLabel.textColor = cell.detailTextLabel.textColor;
            cell.detailTextLabel.textColor = [UIColor blackColor];
            
            
            switch (indexPath.row){
                case 0:
                    cell.textLabel.text = NSLocalizedString(@"Budova", nil);
                    if (self.building){
                        
                        cell.detailTextLabel.text = self.building.name;
                        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                        cell.detailTextLabel.textColor = [StaticVars sharedService].tintColor;
                    }else{
                        
                        self.buildingSpinner.center = cell.center;
                        [cell addSubview:self.buildingSpinner];
                    }
                    break;
                case 1:
                    cell.textLabel.text = NSLocalizedString(@"Patro", nil);
                    cell.detailTextLabel.text = self.room.floorName;
                    break;
            }
        }else{
            cell = [tableView dequeueReusableCellWithIdentifier:@"TextCell"];
            if (cell == nil) {
                cell = [[TextTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TextCell"];
            }
            
            cell.textLabel.text = [NSString stringWithFormat: NSLocalizedString(@"Bohužel, místnost %@ prozatím není v našich mapových podkladech", nil), self.roomName];
        }
    }else{
        cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"Cell"];
        }
        
        UILabel *label = [[CenteredTableViewLabel alloc] initWithFrame:CGRectMake(0, 0, 320, 45)];
        
        
        
        switch (indexPath.row){
            case 0:
                label.text = NSLocalizedString(@"Navigovat", nil);
                break;
            case 1:
                label.text =  NSLocalizedString(@"Zobrazit na mapě", nil);
                break;
        }
        
        if (!self.room){
            label.textColor = [UIColor grayColor];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }else{
            label.textColor = [StaticVars sharedService].tintColor;
            cell.selectionStyle = UITableViewCellSelectionStyleBlue;
        }
        
        [cell addSubview:label];
    }
    
    return cell;
}

#pragma mark Table View delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.room){
        //Budova
        if (indexPath.section == 0 && indexPath.row == 0){
            if (self.building){
                if ([RoutingService sharedService].isRouting){
                    PlanViewController * controller = [[PlanViewController alloc] initWithBuilding:self.building];
                    [self.navigationController pushViewController: controller animated:YES];
                }else{
                    BuildingDetailViewController * detail = [self.navigationController.storyboard instantiateViewControllerWithIdentifier:@"BuildingDetailViewController"];
                    detail.building = self.building;
                    [self.navigationController pushViewController: detail animated:YES];
                }
            }
            
        //Navigate
        }else if (indexPath.section == 1 && (indexPath.row == 0)){
            
            
            RouteViewController * controller = [self.navigationController.storyboard instantiateViewControllerWithIdentifier:@"RouteViewController"];
            
            RoutePoint * startPoint = [[RoutePoint alloc] initAsCurrentLocation];
            
            RoutePoint * endPoint = [[RoutePoint alloc] init];
            endPoint.item = self.room;
            
            controller.senderController = self;
            [self presentModalViewController:controller animated:YES];
            
            [controller setStart: startPoint];
            [controller setEnd: endPoint];
            
        //Show on the map
        }else if (indexPath.section == 1 && (indexPath.row == 1)){
            PlanViewController * controller = [[PlanViewController alloc] initCenteringOnNode:self.room animated:NO];
            [self.navigationController pushViewController: controller animated:YES];
        }
        
    }
    
    
}

@end
