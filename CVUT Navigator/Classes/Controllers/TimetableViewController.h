//
//  TimetableViewController.h
//  CVUT Navigator
//  Created by Tomáš Kohout on 23.09.12.
//  Copyright (c) 2012 Tomáš Kohout. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AgendaViewController.h"

@interface TimetableViewController : UIViewController <AgendaViewControllerChildProtocol>
@property (nonatomic, retain) UICollectionView * collectionView;

- (void) moveToToday;
@end


