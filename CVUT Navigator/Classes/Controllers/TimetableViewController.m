//
//  TimetableViewController.m
//  CVUT Navigator
//  Created by Tomáš Kohout on 23.09.12.
//  Copyright (c) 2012 Tomáš Kohout. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "TimetableViewController.h"
#import "TimetableEntry.h"
#import "Course.h"
#import "Room.h"
#import <RestKit/RestKit.h>

#import "TimetableEntryDetailViewController.h"

#import "TimetableLayout.h"
#import "TimetableLeftLayout.h"
#import "CustomNavigationBar.h"

#import "DataService.h"
#import "WeekInfo.h"
#import "Event.h"
#import "Calendar.h"

#import "TimetableEntryView.h"
#import "TimetableBackgroundView.h"
#import "StaticVars.h"


#define LESSON_LENGTH_IN_MINUTES 90
#define BREAK_LENGTH_IN_MINUTES 15
#define STARTING_TIME_IN_MINUTES 7*60 + 30
#define LESSONS_PER_DAY 8
#define END_TIME_IN_MINUTES STARTING_TIME_IN_MINUTES + LESSONS_PER_DAY * (LESSON_LENGTH_IN_MINUTES + BREAK_LENGTH_IN_MINUTES)
#define DAYS 5




//////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark ViewController (privates methods)
//////////////////////////////////////////////////////////////

@interface TimetableViewController () <UIScrollViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, CustomNavigationBarDelegate>
{
    
}
@property (nonatomic, retain) UICollectionView * leftCollectionView;
@property (nonatomic, retain) UICollectionView * topCollectionView;
@property (nonatomic, retain) Timetable * timetable;
@property (nonatomic, retain) UILabel * cornerLabel;
@property (nonatomic, retain) UILabel * weekTextLabel;
@property (nonatomic, retain) UILabel * dateTextLabel;

@property (nonatomic, retain) UIColor * headerColor;
@property (nonatomic, retain) UIColor * lineColor;
@property (nonatomic, retain) UIColor * currentColor;
@property (nonatomic, retain) UIColor * headerTextColor;

@property (nonatomic, retain) WeekInfo * weekInfo;
@property (nonatomic, assign) NSInteger displayedWeek;
@property (nonatomic, assign) NSInteger displayedWeekParity;

@property (nonatomic, retain) UIView * topBar;

@property (nonatomic, assign) BOOL firstWeekLoaded;
@end





@implementation TimetableViewController
- (UICollectionView *) leftCollectionView{
    if (!_leftCollectionView){
        
        TimetableLeftLayout *flowLayout = [[TimetableLeftLayout alloc] init];
        [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
        [flowLayout setItemSize:CGSizeMake(50, 60)];
        
        [flowLayout setMinimumInteritemSpacing:0.f];
        [flowLayout setMinimumLineSpacing:0.0f];
        
        
        _leftCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 50 + self.topBar.frame.size.height, 50, self.view.bounds.size.height - (50 + self.topBar.frame.size.height)) collectionViewLayout:flowLayout];
        
        _leftCollectionView.dataSource = self;
        _leftCollectionView.delegate = self;
        
        _leftCollectionView.showsVerticalScrollIndicator = NO;
        
        
        [_leftCollectionView setBackgroundColor:[UIColor whiteColor]];
        
        [_leftCollectionView setAutoresizingMask: UIViewAutoresizingFlexibleHeight];
        [_leftCollectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"Cell2"];
    }
    
    return _leftCollectionView;
}

- (UICollectionView *) topCollectionView{
    if (!_topCollectionView){
        
        UICollectionViewFlowLayout * flowLayout = [[UICollectionViewFlowLayout alloc] init];
        [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
        [flowLayout setItemSize:CGSizeMake(119, 50)];
        [flowLayout setMinimumInteritemSpacing:1.f];
        [flowLayout setMinimumLineSpacing:0.0f];
        
        
        _topCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(50, self.topBar.frame.size.height, self.view.frame.size.width - 50, 50) collectionViewLayout:flowLayout];
        _topCollectionView.dataSource = self;
        _topCollectionView.delegate = self;
        _topCollectionView.showsHorizontalScrollIndicator = NO;
        
        [_topCollectionView setBackgroundColor:[UIColor whiteColor]];
        [_topCollectionView setAutoresizingMask: UIViewAutoresizingFlexibleWidth];
        [_topCollectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"Cell2"];
    }
    
    return _topCollectionView;
}


- (UICollectionView *) collectionView{
    
    if (!_collectionView){
        TimetableLayout *layout = [[TimetableLayout alloc] init];
        layout.scrollDirection = UICollectionViewScrollDirectionVertical;
        
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(50, 50 + self.topBar.frame.size.height, self.view.bounds.size.width - 50, self.view.bounds.size.height-(50 + self.topBar.frame.size.height))  collectionViewLayout:layout];
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        
        [_collectionView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"grid_bg_solid.png"]]];
        
        [_collectionView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
        
        [_collectionView registerClass:[TimetableEntryView class] forCellWithReuseIdentifier:@"Cell"];
        [_collectionView setBounces:YES];
        
    }
    return _collectionView;
}

- (UIColor *) headerColor{
    if (!_headerColor){
        _headerColor = [UIColor colorWithRed:221.0/255.0 green:221.0/255.0 blue:221.0/255.0 alpha:1.0];
    }
    return _headerColor;
}

- (UIColor *) headerTextColor{
    if (!_headerTextColor){
        _headerTextColor = [UIColor grayColor];
    }
    return _headerTextColor;
}

- (UIColor *) lineColor{
    if (!_lineColor){
        _lineColor = [StaticVars sharedService].lineColor;
    }
    return _lineColor;
}

- (UIColor *) currentColor{
    if (!_currentColor){
        _currentColor = [StaticVars sharedService].tintColor;
    }
    return _currentColor;
}

- (UILabel *) cornerLabel{
    if (!_cornerLabel){
        _cornerLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, self.topBar.frame.size.height,50,50)];
        [_cornerLabel setBackgroundColor:[UIColor whiteColor]];
        
        UIView * bottomBorder = [[UIView alloc] initWithFrame:CGRectMake(0, _cornerLabel.bounds.size.height-1, _cornerLabel.bounds.size.width, 1)];
        bottomBorder.backgroundColor = self.lineColor;
        [_cornerLabel addSubview:bottomBorder];
        
        UIView * rightBorder = [[UIView alloc] initWithFrame:CGRectMake(_cornerLabel.bounds.size.width-1, 0, 1, _cornerLabel.bounds.size.height)];
        rightBorder.backgroundColor = self.lineColor;
        [_cornerLabel addSubview:rightBorder];
    }
    
    return _cornerLabel;
}



- (UIView *)topBar{
    if (!_topBar){
        _topBar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
        [_topBar setBackgroundColor:[UIColor whiteColor]];

        
        UIButton * leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [leftButton setImage:[UIImage imageNamed:@"left_dark.png"] forState:UIControlStateNormal];
        [leftButton setFrame:CGRectMake(10, 9, 32, 32)];
        [leftButton addTarget:self action:@selector(leftButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    
        
        UIButton * rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
        
        [rightButton setImage:[UIImage imageNamed:@"right_dark.png"] forState:UIControlStateNormal];
        [rightButton setFrame:CGRectMake(278, 9, 32, 32)];
        [rightButton addTarget:self action:@selector(rightButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    
        self.weekTextLabel = [[UILabel alloc] initWithFrame:CGRectMake(52, 9, 216, 18)];
        self.weekTextLabel.textAlignment = NSTextAlignmentCenter;
        [self.weekTextLabel setFont:[UIFont boldSystemFontOfSize:16]];
        [self.weekTextLabel setBackgroundColor:[UIColor clearColor]];
        
        self.weekTextLabel.textColor = self.headerTextColor;
        
        
        self.dateTextLabel = [[UILabel alloc] initWithFrame:CGRectMake(52, 30, 216, 14)];
        self.dateTextLabel.textAlignment = NSTextAlignmentCenter;
        [self.dateTextLabel setFont:[UIFont boldSystemFontOfSize:14]];
        [self.dateTextLabel setBackgroundColor:[UIColor clearColor]];
        self.dateTextLabel.textColor = [UIColor grayColor];
        
        
        [_topBar addSubview:leftButton];
        [_topBar addSubview:self.weekTextLabel];
        [_topBar addSubview:self.dateTextLabel];
        [_topBar addSubview:rightButton];
        
        
        UISwipeGestureRecognizer *swipeRight;
        swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(didSwipeRight)];
        [swipeRight setDirection:UISwipeGestureRecognizerDirectionRight];
        [swipeRight setNumberOfTouchesRequired:1];
        [swipeRight setEnabled:YES];
        [_topBar addGestureRecognizer:swipeRight];
        
        UISwipeGestureRecognizer *swipeLeft;
        swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(didSwipeLeft)];
        [swipeLeft setDirection:UISwipeGestureRecognizerDirectionLeft];
        [swipeLeft setNumberOfTouchesRequired:1];
        [swipeLeft setEnabled:YES];
        [_topBar addGestureRecognizer:swipeLeft];
        
        UIView * bottomBorder = [[UIView alloc] initWithFrame:CGRectMake(0, _topBar.bounds.size.height-1, _topBar.bounds.size.width, 1)];
        bottomBorder.backgroundColor = self.lineColor;
        [_topBar addSubview:bottomBorder];
    
    }
    
    return _topBar;
}


- (void) showWeek: (NSInteger ) aWeek parity: (NSInteger) parity animated: (BOOL) animated{
    [self showWeek:aWeek parity:parity animated:animated completion:nil];
}

- (BOOL) isDisplayedCurrentWeek{
    return ([self.weekInfo.currentWeek integerValue] == self.displayedWeek);
}

- (void) showTodayWeek{
    if ([self isDisplayedCurrentWeek]){
         [self scrollToCurrentTime];
    }else{
        [self showWeek:[self.weekInfo.week integerValue] parity:[self.weekInfo.parity integerValue] animated:NO completion:^{
            [self loadEntriesForWeek:self.displayedWeek parity:self.displayedWeekParity scrollToContent:NO];
            [self scrollToCurrentTime];
        }];
    }
    
}

- (void) showWeek: (NSInteger ) aWeek parity: (NSInteger) parity animated: (BOOL) animated completion: (void (^)())onComplete {
    NSString * parityText;
    
    self.displayedWeekParity = parity;
    self.displayedWeek = aWeek;
    
    
    if (self.displayedWeekParity == WeekParityEven){
        parityText = NSLocalizedString(@"Sudý", nil);
    }else{
        parityText = NSLocalizedString(@"Lichý", nil);
    }
    
    
    self.weekTextLabel.text = [NSString stringWithFormat: NSLocalizedString(@"%i. Týden - %@", nil), self.displayedWeek, parityText];
    
    if ([self isDisplayedCurrentWeek]){
        self.weekTextLabel.textColor = self.currentColor;
    }else{
        self.weekTextLabel.textColor = self.headerTextColor;
    }
    
    
    NSDate * weekStart = [[DataService sharedService] getWeekStart:aWeek];
    NSDate * weekEnd = [[DataService sharedService] getWeekEnd:aWeek];
    
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd. MM."];
    
    self.dateTextLabel.text = [NSString stringWithFormat:@"%@ - %@", [dateFormat stringFromDate:weekStart], [dateFormat stringFromDate:weekEnd]];
    
    if (animated){
        [UIView animateWithDuration:0.5 animations:^{
            [self.collectionView setAlpha:0.0f];
        } completion:^(BOOL finished) {
            if (onComplete){
                onComplete();
            }
            
            [UIView animateWithDuration:0.5 animations:^{
                [self.collectionView setAlpha:1.0f];
            }];
        }];
        
    }else{
        if (onComplete){
            onComplete();
        }
    }
}



- (void) didSwipeRight{
    [self decreaseWeek];
}

- (void) didSwipeLeft{
    [self increaseWeek];
}


-(void) leftButtonTapped{
    [self decreaseWeek];
}

-(void) rightButtonTapped{
    [self increaseWeek];
}

- (void) increaseWeek{
    NSInteger parity = (self.displayedWeekParity + 1) % 2;
    NSInteger week = self.displayedWeek + 1;
    
    [self showWeek: week parity:parity animated:YES completion:^{
        if (week == self.displayedWeek){
            [self loadEntriesForWeek:self.displayedWeek parity:self.displayedWeekParity];
        }
    }];
    
    
}

- (void) decreaseWeek{
    if (self.displayedWeek > 1){
        NSInteger parity = (self.displayedWeekParity - 1) % 2;
        NSInteger week = self.displayedWeek - 1;
        
        [self showWeek: week parity:parity animated:YES completion:^{
            if (week == self.displayedWeek){
                NSLog(@"%i - %i", week, self.displayedWeekParity);
                [self loadEntriesForWeek:self.displayedWeek parity:self.displayedWeekParity];
            }
        }];
        
        
        
    }
}


- (void)viewDidLoad {
    self.edgesForExtendedLayout = UIRectEdgeNone;
    [self.view addSubview:self.topBar];
    
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.navigationItem.title = NSLocalizedString(@"Rozvrh", nil);
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Dnes", nil) style:UIBarButtonItemStyleBordered target:self action:@selector(showTodayWeek)];
    
    
    
    if (self.weekInfo.currentWeek){
        [self showWeek:[self.weekInfo.currentWeek integerValue] parity:[self.weekInfo.currentParity integerValue] animated:NO];
    }
    
    self.firstWeekLoaded = NO;
    
    [[DataService sharedService] getWeekInfoSuccess:^(WeekInfo * info) {
        self.weekInfo = info;
        
        [self showWeek:[info.currentWeek integerValue] parity:[info.currentParity integerValue] animated:NO];
        [self loadEntriesForWeek:[info.currentWeek integerValue] parity:[info.currentParity integerValue]];
    } failure:^(NSError *error) {
        [DataService showError:error messageOnView:nil];
    }];
    
    [self.view addSubview:self.collectionView];
    [self.collectionView reloadData];
    
    
    [self.view addSubview:self.topCollectionView];
    [self.topCollectionView reloadData];
    
    [self.view addSubview:self.leftCollectionView];
    [self.view bringSubviewToFront:self.leftCollectionView];
    [self.leftCollectionView reloadData];
    
    [self.view addSubview:self.cornerLabel];
    
    [self.view bringSubviewToFront:self.topBar];
    
}

- (void) loadEntriesForWeek: (NSInteger) week parity: (NSInteger) parity{
    [self loadEntriesForWeek:week parity:parity scrollToContent:YES];
}

- (void) loadEntriesForWeek: (NSInteger) week parity: (NSInteger) parity scrollToContent: (BOOL) scrollToContent{
    [[DataService sharedService] getEntriesForWeek:week parity: parity success:^(NSArray * entries) {
        
        
        Timetable * timetable = [[Timetable alloc] initWithEntries:entries collectionView:self.collectionView minuteToPixelRatio:90/102.0f];
        self.timetable = timetable;
        
        TimetableLayout * timetableLayout = (TimetableLayout *)self.collectionView.collectionViewLayout;
        timetableLayout.timetable = timetable;
        
        TimetableLeftLayout * timetableLeftLayout = (TimetableLeftLayout *)self.leftCollectionView.collectionViewLayout;
        timetableLeftLayout.timetable = timetable;
        
        [self.collectionView reloadData];
        [self.leftCollectionView reloadData];
        [self.topCollectionView reloadData];
        ;
        
        //Some late initialization prevents to move properly, have to wait a little bit
        if (!self.firstWeekLoaded){
            self.firstWeekLoaded = YES;
            [self performSelector:@selector(scrollToContent) withObject:nil afterDelay:0.5];
        }else{
            if (scrollToContent){
                [self scrollToContent];
            }
        }
        
    } failure:^(NSError *error) {
        [DataService showError:error messageOnView:self.view];
    }];
}

- (void) scrollToContent{
    
    [self scrollToLeftMostEntry];
    
}

- (void) scrollToCurrentTime{
    NSInteger currentTime = [self getCurrentTime];
    
    NSInteger xPosition;
    
    if (currentTime < STARTING_TIME_IN_MINUTES || currentTime > END_TIME_IN_MINUTES){
        xPosition = self.collectionView.contentOffset.x;
    }else{
        xPosition = [self.timetable convertTimeToPx: currentTime];
    }
    
    NSInteger weekDay = [self getCurrentWeekDay];
    
    CGPoint point = CGPointMake(xPosition, [self.timetable getPositionForDay: weekDay-1]);
    
    [self scrollToPoint:point];
}

- (void) scrollToLeftMostEntry{
    
    CGPoint point = [self.timetable getLeftMostEntryPosition];
    [self scrollToPoint:point];
}

- (void) scrollToPoint: (CGPoint) point{
    point.x-=10;
    point.y-=10;
    
    if (point.x < 0) point.x = 0;
    if (point.y < 0) point.y = 0;
    
    double maxWidth = self.collectionView.collectionViewLayout.collectionViewContentSize.width - self.collectionView.bounds.size.width;
    double maxHeight = self.collectionView.collectionViewLayout.collectionViewContentSize.height - self.collectionView.bounds.size.height;
    
    
    if (point.x > maxWidth) point.x = maxWidth;
    if (point.y > maxHeight) point.y = maxHeight;
    
    [UIView animateWithDuration:0.5 animations:^{
        [self.collectionView setContentOffset:point];
    } completion:^(BOOL finished) {
        
    }];

}

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    if (view == self.collectionView){
        return LESSONS_PER_DAY;
    }else if (view == self.topCollectionView){
        return LESSONS_PER_DAY;
    }else{
        return LESSONS_PER_DAY;
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (self.firstWeekLoaded){
        if (scrollView == self.collectionView){
            
            CGPoint leftOffset, topOffset;
            
            leftOffset.x = [self.leftCollectionView contentOffset].x;
            leftOffset.y = [scrollView contentOffset].y;
            
            topOffset.x = [scrollView contentOffset].x;
            topOffset.y = [self.topCollectionView contentOffset].y;
            
            
            [self.leftCollectionView setContentOffset:leftOffset];
            
            [self.topCollectionView setContentOffset:topOffset];
            
            
        }else if (scrollView == self.topCollectionView){
            [self.collectionView setContentOffset:CGPointMake(self.topCollectionView.contentOffset.x, self.collectionView.contentOffset.y)];
        }else if (scrollView == self.leftCollectionView){
            [self.collectionView setContentOffset:CGPointMake(self.collectionView.contentOffset.x, self.leftCollectionView.contentOffset.y)];
        }
    }
}

- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    if (collectionView == self.collectionView){
        return 7;
    }else{
        return 1;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView  cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView == self.topCollectionView){
        UICollectionViewCell *cell = [collectionView  dequeueReusableCellWithReuseIdentifier:@"Cell2" forIndexPath:indexPath];
        [cell.contentView.subviews.lastObject removeFromSuperview];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, cell.contentView.bounds.size.width, cell.contentView.bounds.size.height)];
        
        label.textAlignment = NSTextAlignmentCenter;
        label.textColor = self.headerTextColor;
        label.font = [UIFont systemFontOfSize:16];
        
        [label setBackgroundColor:[UIColor whiteColor]];
        
        NSInteger time = STARTING_TIME_IN_MINUTES + (indexPath.row)* (LESSON_LENGTH_IN_MINUTES + BREAK_LENGTH_IN_MINUTES);
        
        NSInteger hour = (time) / 60;
        NSInteger minute = (time) % 60;
        
        NSInteger endHour = (time + LESSON_LENGTH_IN_MINUTES) / 60;
        NSInteger endMinute = (time + LESSON_LENGTH_IN_MINUTES) % 60;
        
        
        NSString *format = [NSString stringWithFormat:@"%%0%dd", 2];
        
        label.text = [NSString stringWithFormat:@"%d:%@ - %d:%@", hour,  [NSString stringWithFormat:format,minute ], endHour, [NSString stringWithFormat:format,endMinute]];
        
        if ([self isDisplayedCurrentWeek]){
            NSInteger currentTime = [self getCurrentTime];
            if (currentTime >= time && currentTime < time+LESSON_LENGTH_IN_MINUTES + BREAK_LENGTH_IN_MINUTES){
                label.textColor = self.currentColor;
            }
        }
        
        [cell.contentView addSubview:label];
        
        UIView * bottomBorder = [[UIView alloc] initWithFrame:CGRectMake(0, cell.bounds.size.height-1, cell.bounds.size.width, 1)];
        bottomBorder.backgroundColor = self.lineColor;
        [cell addSubview:bottomBorder];
        
        UIView * rightBorder = [[UIView alloc] initWithFrame:CGRectMake(cell.bounds.size.width-1, 0, 1, cell.bounds.size.height)];
        rightBorder.backgroundColor = self.lineColor;
        [cell addSubview:rightBorder];
        
        
        return cell;
        
    }else if (collectionView == self.collectionView){
        
        TimetableEntryView *cell = [collectionView  dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
        
        
        
        TimetableEntry * entry = [self.timetable getEntryForIndexPath:indexPath];
        
        cell.title = entry.course.name;
        cell.subtitle = entry.roomName;
        cell.color = entry.color;
        
        if  ([entry isKindOfClass:[Event class]]){
            cell.title = ((Event *)entry).eventName;
            cell.subtitle = ((Event *)entry).eventDetail;
        }
        
        return cell;
        
    }else{
        UICollectionViewCell *cell = [collectionView  dequeueReusableCellWithReuseIdentifier:@"Cell2" forIndexPath:indexPath];
        [cell.contentView.subviews.lastObject removeFromSuperview];
        
        
        UILabel *label = [[UILabel alloc] initWithFrame:cell.contentView.bounds];
        
        label.textAlignment = NSTextAlignmentCenter;
        label.textColor = self.headerTextColor;
        label.font = [UIFont systemFontOfSize:16];
        
        
        NSString * text;
        switch (indexPath.row) {
            case 0:
                text = NSLocalizedString(@"Po", nil);
                break;
            case 1:
                text = NSLocalizedString(@"Út", nil);
                break;
            case 2:
                text = NSLocalizedString(@"St", nil);
                break;
            case 3:
                text = NSLocalizedString(@"Čt", nil);
                break;
            case 4:
                text = NSLocalizedString(@"Pá", nil);
                break;
            case 5:
                text = NSLocalizedString(@"So", nil);
                break;
            case 6:
                text = NSLocalizedString(@"Ne", nil);
                break;
        }
        
        if ([self isDisplayedCurrentWeek]){
            NSInteger weekDay = [self getCurrentWeekDay]-1;
            if (weekDay == indexPath.row){
                label.textColor = self.currentColor;
            }
        }
        
        [label setBackgroundColor:[UIColor whiteColor]];
        label.text = text;
        
        [cell.contentView addSubview:label];
        
        UIView * bottomBorder = [[UIView alloc] initWithFrame:CGRectMake(0, cell.bounds.size.height-1, cell.bounds.size.width, 1)];
        bottomBorder.backgroundColor = self.lineColor;
        [cell addSubview:bottomBorder];
        
        UIView * rightBorder = [[UIView alloc] initWithFrame:CGRectMake(cell.bounds.size.width-1, 0, 1, cell.bounds.size.height)];
        rightBorder.backgroundColor = self.lineColor;
        [cell addSubview:rightBorder];
        return cell;
        
    }
}

- (NSInteger) getCurrentWeekDay{
    return [Calendar weekDayOfDate:[NSDate date]];
}

- (NSInteger) getCurrentTime{
    return [Calendar minutesFromDate:[NSDate date]];
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (collectionView == self.collectionView){
        TimetableEntry  * entry = [self.timetable getEntryForIndexPath:indexPath];
        TimetableEntryDetailViewController * controller = [[TimetableEntryDetailViewController alloc] init];
        
        controller.entry = entry;
        [self.navigationController pushViewController:controller animated:YES];
    }
}


#pragma mark Agend View Controller Child Protocol

-(void)moveToToday{
    [self showTodayWeek];
}




@end
