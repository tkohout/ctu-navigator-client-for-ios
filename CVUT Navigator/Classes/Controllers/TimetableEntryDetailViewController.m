//
//  TimetableEntryDetailViewController.m
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 07.12.12.
//  Copyright (c) 2012 Tomáš Kohout. All rights reserved.
//

#import "TimetableEntryDetailViewController.h"
#import "TimetableEntry.h"
#import "Course.h"
#import "Room.h"
#import "Building.h"
#import "Person.h"
#import "ClassroomDetailViewController.h"
#import "CourseDetailViewController.h"
#import "Event.h"
#import "RouteViewController.h"
#import "PlanViewController.h"
#import "TeacherDetailViewController.h"
#import "CenteredTableViewLabel.h"
#import "StaticVars.h"

@interface TimetableEntryDetailViewController ()

@end

@implementation TimetableEntryDetailViewController



-(void)viewDidLoad{
    [super viewDidLoad];
    
    if (self.entry) {
        
        self.navigationItem.title = ([self.entry.type integerValue] == TimetableEntryTypeEvent) ? ((Event *)self.entry).eventName : self.entry.course.name;
    }else{
        //TODO: push back
    }
    
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:UITableViewStyleGrouped];
    return self;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0){
        if ([self.entry.type integerValue] == TimetableEntryTypeEvent){
            if (self.entry.teacher){
                return 6;
            }else{
                return 5;
            }
        }
        
        if (self.entry.teacher){
            return 5;
        }else{
            return 4;
        }
        
    }else{
        return 2;
    }
}


- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
    }
    
    if (indexPath.section == 0){
        
        cell.textLabel.textColor = cell.detailTextLabel.textColor;
        cell.detailTextLabel.textColor = [UIColor blackColor];
        
        
        switch (indexPath.row){
            case 0:
                if ([self.entry.type integerValue] == TimetableEntryTypeEvent ){
                    cell.detailTextLabel.text = ((Event*)self.entry).eventName;
                }else{
                    cell.detailTextLabel.textColor = [StaticVars sharedService].tintColor;
                    cell.detailTextLabel.text = self.entry.course.longName;
                    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                }
                cell.textLabel.text = NSLocalizedString(@"Název", nil);
                
                break;
                
            case 1:
                
                cell.textLabel.text = NSLocalizedString(@"Čas", nil);
                cell.detailTextLabel.text = [NSString stringWithFormat:NSLocalizedString(@"%@ od %@", nil), self.entry.dayString, self.entry.timeFormatted];
                break;
                
            case 2:
                
                cell.textLabel.text = NSLocalizedString(@"Typ", nil);
                cell.detailTextLabel.text = self.entry.typeString;
                break;
                
            case 3:
                
                cell.textLabel.text = NSLocalizedString(@"Místnost", nil);
                cell.detailTextLabel.text = self.entry.roomName;
                cell.detailTextLabel.textColor = [StaticVars sharedService].tintColor;
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                break;
                
            case 4:
            case 5:
                
                if (self.entry.teacher && indexPath.row == 4){
                
                cell.textLabel.text = NSLocalizedString(@"Vyučující", nil);
                cell.detailTextLabel.text = self.entry.teacher.name;
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                cell.detailTextLabel.textColor = [StaticVars sharedService].tintColor;
                    
                }else{
                
                
            
                
                if ([self.entry.type integerValue] == TimetableEntryTypeEvent ){
                    cell.textLabel.text = NSLocalizedString(@"Popis", nil);
                    
                    cell.detailTextLabel.numberOfLines = 0;
                    cell.detailTextLabel.text  = ((Event*)self.entry).eventDetail;
                    
                }
                }
                break;
        }
        
    }else{
        UILabel *label = [[CenteredTableViewLabel alloc] initWithFrame:CGRectMake(0, 0, 320, 45)];
        
        
        
        switch (indexPath.row){
            case 0:
                label.text = NSLocalizedString(@"Navigovat", nil);
                break;
            case 1:
                label.text =  NSLocalizedString(@"Zobrazit na mapě", nil);
                break;
        }
        
        if (!self.entry.room){
            label.textColor = [UIColor grayColor];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }else{
            label.textColor = [StaticVars sharedService].tintColor;
            cell.selectionStyle = UITableViewCellSelectionStyleBlue;
        }
        
        [cell addSubview:label];
    }
    
    
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0 && indexPath.row == 0){
        CourseDetailViewController * bController = [[CourseDetailViewController alloc] init];
        
        bController.course = self.entry.course;
        [self.navigationController pushViewController:bController animated:YES];
        
    }else if (indexPath.section == 0 && indexPath.row == 3){
        ClassroomDetailViewController * bController = [[ClassroomDetailViewController alloc] init];
        if (self.entry.room){
            bController.room = self.entry.room;
        }else if (self.entry.roomName){
            bController.roomName = self.entry.roomName;
        }else{
            return;
        }
        
        [self.navigationController pushViewController:bController animated:YES];
    }else if (indexPath.section == 0 && indexPath.row == 4){
        TeacherDetailViewController * controller = [self.navigationController.storyboard instantiateViewControllerWithIdentifier:@"TeacherDetailViewController"];
        controller.person = self.entry.teacher;
        [self.navigationController pushViewController:controller animated:YES];
    }else if (indexPath.section == 1 && indexPath.row == 0){
        
        if (self.entry.room){
            RouteViewController * controller = [self.navigationController.storyboard instantiateViewControllerWithIdentifier:@"RouteViewController"];
            
            RoutePoint * startPoint = [[RoutePoint alloc] init];
            startPoint.item = CURRENT_LOCATION;
            
            
            RoutePoint * endPoint = [[RoutePoint alloc] init];
            endPoint.item = self.entry.room;
            
            controller.senderController = self;
            [self presentModalViewController:controller animated:YES];
            
            [controller setStart: startPoint];
            [controller setEnd: endPoint];
            
        }
        
    //Zobrazit na mapě
    }else if (indexPath.section == 1 && (indexPath.row == 1)){
        if (self.entry.room){
            
            PlanViewController * controller = [[PlanViewController alloc] initCenteringOnNode:self.entry.room animated:NO];
            [self.navigationController pushViewController: controller animated:YES];
            
        }
    }
    
}

@end
