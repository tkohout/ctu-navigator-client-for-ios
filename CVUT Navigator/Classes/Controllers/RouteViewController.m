//
//  RouteViewController.m
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 24.02.13.
//  Copyright (c) 2013 Tomáš Kohout. All rights reserved.
//

#import "RouteViewController.h"
#import "DataService.h"
#import <CoreLocation/CoreLocation.h>
#import "Searchable.h"
#import "Building.h"
#import "Room.h"
#import "MapAllViewController.h"
#import "RoutePoint.h"

#import "RoutingService.h"
#import "LocationService.h"
#import "VuforiaParentViewController.h"
#import "ToastService.h"
#import "MBProgressHUD.h"
#import "StaticVars.h"

@interface RouteViewController ()

@end

@implementation RouteViewController
CLGeocoder * geocoder;
CLRegion * region;
NSMutableArray * geocodeSuggestions;
NSMutableArray * placesSuggestions;

RoutePoint * currentLocation;


- (BOOL)prefersStatusBarHidden {
    return YES;
}
   
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    [[LocationService sharedService] startUpdatingLocation];
    
    //Top frame was messing up with the top toolbar
    CGRect statusFrame = [[UIApplication sharedApplication] statusBarFrame];
    [self.view setFrame:CGRectMake(0, statusFrame.size.height - 20, self.view.frame.size.width, self.view.frame.size.height - (statusFrame.size.height - 20))];
    [self.toolbar setFrame:CGRectMake(0, self.toolbar.frame.origin.y + statusFrame.size.height - 20, self.toolbar.frame.size.width, self.toolbar.frame.size.height)];
    
    self.fieldsBackground.backgroundColor = [UIColor colorWithRed:247.0/255.0 green:247.0/255.0 blue:247.0/255.0 alpha:1.0];
    
    UIView * bottomBorder = [[UIView alloc] initWithFrame:CGRectMake(0, self.fieldsBackground.frame.size.height - 2, self.fieldsBackground.frame.size.width, 1)];
    bottomBorder.backgroundColor = [UIColor colorWithRed:228.0/255.0 green:228.0/255.0 blue:228.0/255.0 alpha:1.0f];
    [self.fieldsBackground addSubview:bottomBorder];
    
    //Prepare current location for search suggestions
    currentLocation = [[RoutePoint alloc ] initAsCurrentLocation];
    
    //Enable or disable route button
    [self routeButtonHandle];
    [self.routeButton setStyle:UIBarButtonSystemItemSave];
    
    //Create region in which will the geocoder search
    CLLocationCoordinate2D center = CLLocationCoordinate2DMake(PRAGUE_LATITUDE, PRAGUE_LONGITUDE);
    
    CLLocationDistance distance;
    distance = 10000;
    
    
    self.startField.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.startField.returnKeyType = UIReturnKeyRoute;
    self.startField.enablesReturnKeyAutomatically = YES;
    
    
    self.endField.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.endField.returnKeyType = UIReturnKeyRoute;
    self.endField.enablesReturnKeyAutomatically = YES;
    
    geocoder = [[CLGeocoder alloc] init];
    region = [[CLRegion alloc] initCircularRegionWithCenter:center radius:distance identifier:@"Prague"];
}

#pragma mark Outlets and actions
- (IBAction)routeTapped:(id)sender {
    [self route];
}

- (IBAction)cancelTapped:(id)sender {
    [self dismissModalViewControllerAnimated:YES];
}

- (IBAction)switchButtonTapped:(id)sender {
    NSString * tempStartString = [self.startField.text copy];
    RoutePoint * tempStart = [self.start copy];
    
    self.startField.text = self.endField.text;
    self.endField.text = tempStartString;
    
    self.start = [self.end copy];
    self.end = tempStart;
    
    
    
    [self.tableView reloadData];
    
}

- (IBAction)startValueChanged:(id)sender{
    [self suggestForQuery:[self.startField text]];
    [self routeButtonHandle];
}

- (IBAction)endValueChanged:(id)sender {
    [self suggestForQuery:[self.endField text]];
    [self routeButtonHandle];
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self route];
    return YES;
}


- (void) resetCurrentLocationTextField: (UITextField *) textField{
    textField.textColor = [UIColor blackColor];
}

- (void) routeButtonHandle{
    if (![self.startField.text isEqualToString: @""] && ![self.endField.text isEqualToString: @""]){
        [self.routeButton setEnabled:YES];
    }else{
        [self.routeButton setEnabled:NO];
    }
}

- (void) clearButtonHandle: (UITextField *) textField{
    if (( [self.start isCurrentLocation] || [self.end isCurrentLocation])){
        [self resetCurrentLocationTextField:textField];
        
        if (textField == self.startField){
            self.start = nil;
        }else{
            self.end = nil;
        }
    }
}

-(BOOL)textFieldShouldClear:(UITextField *)textField{
    [self clearButtonHandle:textField];
    return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (textField == self.startField){
       
        //Rewriting field with current location in it
        if ([self.start isCurrentLocation]){
            [self resetCurrentLocationTextField: textField];
            textField.text = string;
            
            [[self tableView] reloadData];
             self.start = nil;
            return NO;
        }
         self.start = nil;
    }else{
        
        //Rewriting field with current location in it
        if ([self.end isCurrentLocation]){
            [self resetCurrentLocationTextField: textField];
            textField.text = string;
            
            [[self tableView] reloadData];
            self.end = nil;
            return NO;
        }
        self.end = nil;
    }
    
    return YES;
}

#pragma mark Suggestions
- (void) suggestForQuery: (NSString *) query{
    [geocoder geocodeAddressString:query inRegion:region completionHandler:^(NSArray *placemarks, NSError *error) {
        geocodeSuggestions = [[NSMutableArray alloc] init];
        
for (CLPlacemark * placemark in placemarks){
            if ([[placemark ISOcountryCode] isEqualToString:@"CZ"]){
                RoutePoint * point = [[RoutePoint alloc] init];
                point.item = [placemark copy];
                
                [geocodeSuggestions addObject: point];
                
            }
        }
        
        [self.tableView reloadData];
        
    }];
    
    
    [[DataService sharedService] searchPlacesQuery:query success:^(NSArray * results) {
        
        placesSuggestions = [[NSMutableArray alloc] init];
        
        for (id item in results){
            
            RoutePoint * point = [[RoutePoint alloc] init];
            point.item = item;
            
            [placesSuggestions addObject: point];
        }
        
        
        [self.tableView reloadData];
    }];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (RoutePoint *) resultAtIndex: (NSIndexPath *) indexPath{
    
    //1 line for current location
    NSInteger currentLocationCount = 0;
    
    if ([self showCurrentLocationRow]){
        currentLocationCount = 1;
        
        if (indexPath.row == 0){
            return currentLocation;
        }
    }
    
    //Then rooms or buildings
    if (indexPath.row < [placesSuggestions count] + currentLocationCount){
        return [placesSuggestions objectAtIndex:indexPath.row - currentLocationCount];
        //Then geocode suggestions
    }else{
        return [geocodeSuggestions objectAtIndex:indexPath.row - [placesSuggestions count] - currentLocationCount];
    }
}

- (BOOL) currentLocationAvailable{
    if (([LocationService sharedService].currentLocationType == LocationServiceCurrentLocationTypeOutdoor  && [LocationService sharedService].authorizationStatus != kCLAuthorizationStatusAuthorized) || ![LocationService sharedService].currentLocation){
        return NO;
    }
    
    return YES;
}

- (BOOL) showCurrentLocationRow{
    
    if ([self.startField isFirstResponder] && ![self.start isCurrentLocation]){
        return YES;
    }
    
    return NO;
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //1 line for current location
    NSInteger currentLocationCount = 0;
    
    if ([self showCurrentLocationRow]){
        currentLocationCount = 1;
    }
    
    return [placesSuggestions count] + [geocodeSuggestions count] + currentLocationCount;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    
    RoutePoint * item = [self resultAtIndex: indexPath];
    [cell.textLabel setTextColor:[UIColor blackColor]];
    
    
    cell.textLabel.text = item.name;
    cell.detailTextLabel.text = item.detail;
    cell.imageView.image = item.image;
    
    if ([item isCurrentLocation]){
        [cell.textLabel setTextColor: [StaticVars sharedService].tintColor];
    }
    
    
    return cell;
}


#pragma mark - Table view delegate

- (BOOL) areFieldsComplete{
    if (self.start && self.end){
        return YES;
    }
    
    return NO;
}


- (void) setStart:(RoutePoint *)start{
    _start = start;
    
    if (start != nil){
        self.startField.text = [start name];
        
        if ([start isCurrentLocation]){
            self.startField.textColor = [StaticVars sharedService].tintColor;
        }else{
            self.startField.textColor = [UIColor blackColor];
        }
        
        [self.endField becomeFirstResponder];
        
        
    }else{
        self.startField.textColor = [UIColor blackColor];
    }
    
    
    [self routeButtonHandle];
    [self.tableView reloadData];
    
}

- (void) setEnd:(RoutePoint *)end{
    _end = end;
    
    if (_end != nil){
        self.endField.text = [end name];
        
        if ([end isCurrentLocation]){
            self.endField.textColor = [UIColor blueColor];
        }else{
            self.endField.textColor = [UIColor blackColor];
        }
        
        if (![self areFieldsComplete]){
            [self.startField becomeFirstResponder];
        }

    }else{
        self.endField.textColor = [UIColor blackColor];
    }
    
    
    [self routeButtonHandle];
    [self.tableView reloadData];
    
}

- (void) textFieldDidBeginEditing:(UITextField *)textField{

    [[self tableView] reloadData];
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    RoutePoint * item = [self resultAtIndex:indexPath];
    
    if ([self.startField isFirstResponder]){
        self.start = item;
    }else{
        self.end = item;
        
        if ([self areFieldsComplete]){
            [self route];
        }
    }
    
}

#pragma mark Routing
- (void)route{
    
    if (![self.startField.text isEqualToString:@""] && ![self.endField.text isEqualToString:@""]){
        
        RoutePoint * start;
        RoutePoint *  end;
        
        start = self.start;
        end = self.end;
        
        if (!self.start){
            
            
            for (RoutePoint * point in placesSuggestions){
                if ([[point name] isEqualToString:self.startField.text]){
                    start = point;
                }
            }
            
            if (!start){
                start = [[RoutePoint alloc] init];
                start.item = self.startField.text;
            }
        }
        
        if (!self.end){
            for (RoutePoint * point in placesSuggestions){
                if ([[point name] isEqualToString:self.endField.text]){
                    end = point;
                }
            }
            
            if (!end){
                end = [[RoutePoint alloc] init];
                end.item = self.endField.text;
            }
        }
        
        if ([start isCurrentLocation] || [end isCurrentLocation]){
            if (![self currentLocationAvailable]){
                
                
                
                if (([LocationService sharedService].authorizationStatus == kCLAuthorizationStatusDenied  || [LocationService sharedService].authorizationStatus == kCLAuthorizationStatusNotDetermined )){
                    UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Současná pozice", nil) message:NSLocalizedString(@"Vaše současná pozice není známá, protože není povolena. Povolíte ji v Nastavení/Soukromí/Polohové služby/CVUT Navigator", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles: nil];
                    [alertView show];
                    
                }else{
                    [DataService showErrorMessageWithTitle:NSLocalizedString(@"Současná pozice", nil) subtitle:NSLocalizedString(@"Vaše současná pozice není známá, prosím zadejte odkud se chcete navigovat", nil) onView:nil];
                    if ([start isCurrentLocation]){
                        self.start = nil;
                        self.startField.text = @"";
                        self.startField.textColor = [UIColor blackColor];
                        [self.startField becomeFirstResponder];
                    }else{
                        self.end = nil;
                        self.endField.text = @"";
                        self.endField.textColor = [UIColor blackColor];
                        [self.endField becomeFirstResponder];
                    }
                    
                }
                return;
            }
        }
        
        
        if ([start isCurrentLocation] && [LocationService sharedService].currentLocationType == LocationServiceCurrentLocationTypeIndoor){
            start = [[RoutePoint alloc] initWithRouteItem:[LocationService sharedService].currentIndoorLocationNode];
        }
        
        if ([end isCurrentLocation] && [LocationService sharedService].currentLocationType == LocationServiceCurrentLocationTypeIndoor){
            end = [[RoutePoint alloc] initWithRouteItem:[LocationService sharedService].currentIndoorLocationNode];
        }
        
        
        MBProgressHUD * activityIndicator = [[ToastService sharedService] toastActivityWithTitle:NSLocalizedString(@"Hledám cestu", nil) inView:nil];
        
        [[RoutingService sharedService] routeStart:start end:end success:^{
            
            [activityIndicator hide:YES];
            
            if (self.delegate && [self.delegate respondsToSelector:@selector(routingStartedStart:end:)]){
                [self.delegate routingStartedStart:start end:end];
            }
            
            
            if ([end isEqualToRoutePoint:start]){
                [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Navigace", nil) message:NSLocalizedString(@"Nelze se navigovat do stejného místa z kterého vycházíte", nil) delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
                return;
            }
            
            #ifndef VUFORIA_DISABLE
            if ([self.senderController isKindOfClass:[VuforiaParentViewController class]]){
                [self dismissModalViewControllerAnimated:YES];
                [(VuforiaParentViewController *)self.senderController route];
                return;
            }
            
            #endif
            
            
            [self dismissModalViewControllerAnimated:YES];
            
            [self.senderController.tabBarController setSelectedIndex:2];
            
            UINavigationController * navController = (UINavigationController *)self.senderController.tabBarController.selectedViewController;
            [navController popToRootViewControllerAnimated:NO];
            
            MapAllViewController * controller;
            
            
            controller =  (MapAllViewController *) navController.topViewController;
            
            
            
            if ([start isInSameBuildingAsRoutePoint:end] || [start.item isKindOfClass:[Node class]]){
                [controller routeInBuildingNode: start.item];
            }else{
                [controller route];
            }
        } failure:^(NSError * error) {
            [activityIndicator hide:YES];
            [[RoutingService sharedService] routeCancel];
            
            if (error.code == APIErrorNotFound){
                [DataService showErrorMessageWithTitle:NSLocalizedString(@"Cesta nebyla nalezena", nil) subtitle: nil onView:self.view];
            }else{
                [DataService showError:error messageOnView:self.view];
            }
        }];
        
        
        
    }else{
        UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Trasa", nil) message:NSLocalizedString(@"Vyplňte prosím všechna pole", nil) delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alertView show];
    }
    
}

- (void)viewDidUnload {
    [self setTableView:nil];
    [self setStartField:nil];
    [self setEndField:nil];
    [self setRouteButton:nil];
    [self setCancelButton:nil];
    [self setTravelMode:nil];
    [self setToolbar:nil];
    [self setFieldsBackground:nil];
    [super viewDidUnload];
}


@end
