//
//  LoginViewController.h
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 25.04.13.
//  Copyright (c) 2013 Tomáš Kohout. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *usernameTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;

//Shows warning message when is the user logged out
-(void)showWarning;
@end
