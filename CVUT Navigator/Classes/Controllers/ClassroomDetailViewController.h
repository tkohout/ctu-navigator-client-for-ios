//
//  ClassroomDetailViewController.h
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 28.10.12.
//  Copyright (c) 2012 Tomáš Kohout. All rights reserved.
//

#import "DetailViewController.h"
#import "Room.h"


@interface ClassroomDetailViewController : UITableViewController

@property (nonatomic, strong) Room * room;
@property (nonatomic, strong) NSString * roomName;

@end
