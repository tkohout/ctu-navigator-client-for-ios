//
//  CourseDetailViewController.h
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 11.12.12.
//  Copyright (c) 2012 Tomáš Kohout. All rights reserved.
//

#import "DetailViewController.h"
#import "Course.h"

@interface CourseDetailViewController : DetailViewController
@property (nonatomic, strong) Course * course;
@end
