//
//  UserLocationAnnotation.m
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 04.04.13.
//  Copyright (c) 2013 Tomáš Kohout. All rights reserved.
//

#import "UserLocationAnnotation.h"
#import "LocationService.h"

@implementation UserLocationAnnotation

//It's necessary to call setCoordinate outside of the class when the value is changed due to KVO
- (CLLocationCoordinate2D)coordinate{
    if ([LocationService sharedService].currentLocation){
        return [LocationService sharedService].currentLocation.coordinate;
    }
    
    return CLLocationCoordinate2DMake(0, 0);
}

- (NSString *)title{
    return NSLocalizedString(@"Současná poloha", nil);
}

- (UIImage *) image{
    return [UIImage imageNamed:@"TrackingDot.png"];
}

@end
