#import "BuildingAnnotation.h"

@implementation BuildingAnnotation
@synthesize name = _name;
@synthesize detail = _detail;
@synthesize coordinate = _coordinate;
@synthesize building   = _building;

- (id)initWithName:(NSString*)name detail:(NSString*)detail coordinate:(CLLocationCoordinate2D)coordinate building:(Building *)building{
    if ((self = [super init])) {
        _name = [name copy];
        _detail = [detail copy];
        _coordinate = coordinate;
        _building = building;
    }
    return self;
}

- (NSString *)title {
        return _name;
}

- (NSString *)subtitle {
    return _detail;
}



@end