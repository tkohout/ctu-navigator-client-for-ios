//
//  NodeAnnotation.h
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 08.03.13.
//  Copyright (c) 2013 Tomáš Kohout. All rights reserved.
//

#import "RMAnnotation.h"

@class Node;

@interface NodeAnnotation : RMAnnotation

@property (nonatomic, retain) Node * node;

@end
