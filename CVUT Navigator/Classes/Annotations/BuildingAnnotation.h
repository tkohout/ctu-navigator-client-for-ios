#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@class Building;

@interface BuildingAnnotation : NSObject <MKAnnotation> {
    NSString *_name;
    NSString *_detail;
    CLLocationCoordinate2D _coordinate;
    Building * _building;
}

@property (copy) NSString *name;
@property (copy) NSString *detail;
@property (nonatomic, retain) Building * building;
@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;

- (id)initWithName:(NSString*)name detail:(NSString*)detail coordinate:(CLLocationCoordinate2D)coordinate building: (Building *) building;

@end
