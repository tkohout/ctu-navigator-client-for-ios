//
//  UserLocationAnnotation.h
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 04.04.13.
//  Copyright (c) 2013 Tomáš Kohout. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

typedef NS_ENUM(NSInteger, UserLocationAnnotationType){
    UserLocationAnnotationTypeMarble,
    UserLocationAnnotationTypeHalo
};

@interface UserLocationAnnotation : NSObject <MKAnnotation>

@property (nonatomic, assign) CLLocationCoordinate2D coordinate;
@property (nonatomic, readonly, copy) NSString *title;
@property (nonatomic, readonly) UIImage * image;
@property (nonatomic, assign) UserLocationAnnotationType type;
@end
