//
//  TestMapSource.h
//  MapTestbedFlipMaps
//
//  Created by Tomáš Kohout on 23.01.13.
//
//

#import <Foundation/Foundation.h>
#import "RMAbstractWebMapSource.h"
#import "RMZoomConverter.h"
@class Floor;

@interface CTUNavigatorMapSource: RMAbstractWebMapSource
@property (nonatomic, retain) Floor * floor;

- initWithZoomConverter: (RMZoomConverter *) aConverter floor: (Floor *) floor;
@end
