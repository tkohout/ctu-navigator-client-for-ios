//
//  Calendar.h
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 20.04.13.
//  Copyright (c) 2013 Tomáš Kohout. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Calendar : NSObject
+ (NSCalendar *)defaultCalendar;
+ (NSInteger) daysBetween: (NSDate *) start and: (NSDate *) end;
+ (NSInteger) weekDayOfDate: (NSDate *) date;
+ (NSDate *) firstDayOfTheWeekForDate: (NSDate *) date;
+ (NSDate *) resetSecondsMinutesAndHoursOfDate: (NSDate *) date;
+ (BOOL) isDate: (NSDate *) date1 sameDayAs: (NSDate *) date2;


+ (NSDate *) date: (NSDate *) date byAddingDays: (NSInteger) days;
+ (NSDate *) date: (NSDate *) date byAddingWeeks: (NSInteger) weeks;
+ (NSDate *) date: (NSDate *) date byAddingHours: (NSInteger) hours minutes: (NSInteger) minutes seconds: (NSInteger) seconds days: (NSInteger) days weeks: (NSInteger) weeks months: (NSInteger) months years: (NSInteger) years;
+ (NSInteger) minutesFromDate: (NSDate *) date;

@end
