//
//  Calendar.m
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 20.04.13.
//  Copyright (c) 2013 Tomáš Kohout. All rights reserved.
//

#import "Calendar.h"

@implementation Calendar

static NSCalendar * defaultCalendar;

+ (NSCalendar *) defaultCalendar{
    if (!defaultCalendar){
        defaultCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        
        [defaultCalendar setTimeZone:[NSTimeZone timeZoneWithName:@"Europe/Prague"]];
        [defaultCalendar setFirstWeekday:2];
    }
    return defaultCalendar;
    
}

+ (NSInteger) daysBetween: (NSDate *) start and: (NSDate *) end{
    NSDateComponents *components = [[Calendar defaultCalendar] components:NSDayCalendarUnit
                                                                                  fromDate:start
                                                                                    toDate:end
                                                                                   options:0];
    
    return [components day];
}

+ (NSInteger) weekDayOfDate: (NSDate *) date{
    return [[Calendar defaultCalendar] ordinalityOfUnit:NSWeekdayCalendarUnit inUnit:NSWeekCalendarUnit forDate:date];
}


+ (NSDate *) date: (NSDate *) date byAddingDays: (NSInteger) days{
    return [Calendar date:date byAddingHours:0 minutes:0 seconds:0 days:days weeks:0 months:0 years:0];
}

+ (NSDate *) date: (NSDate *) date byAddingWeeks: (NSInteger) weeks{
    return [Calendar date:date byAddingHours:0 minutes:0 seconds:0 days:0 weeks:weeks months:0 years:0];
}

+ (NSDate *) date: (NSDate *) date byAddingHours: (NSInteger) hours minutes: (NSInteger) minutes seconds: (NSInteger) seconds days: (NSInteger) days weeks: (NSInteger) weeks months: (NSInteger) months years: (NSInteger) years{
    NSDateComponents *diff = [[NSDateComponents alloc] init];
    [diff setHour:hours];
    [diff setMinute:minutes];
    [diff setSecond:seconds];
    [diff setDay:days];
    [diff setWeek:weeks];
    [diff setMonth:months];
    [diff setYear:years];
    
    return [[Calendar defaultCalendar] dateByAddingComponents:diff toDate:date options:0];
}

+ (NSDate *) firstDayOfTheWeekForDate: (NSDate *) date{
    //Find weekday number
    NSInteger weekdayOfDate = [Calendar weekDayOfDate: date];
    NSInteger numberOfDaysToStartOfCurrentWeek = weekdayOfDate - 1;
    
    //Substract difference of days to start of the week
    NSDate *startOfWeek = [Calendar date:date byAddingDays:-numberOfDaysToStartOfCurrentWeek];
    
    //Reseting seconds and minutes
    startOfWeek = [Calendar resetSecondsMinutesAndHoursOfDate:startOfWeek];
    
    return startOfWeek;
}

+ (BOOL) isDate: (NSDate *) date1 sameDayAs: (NSDate *) date2{
    
    NSDateComponents *dateComponents1 = [[Calendar defaultCalendar] components:NSEraCalendarUnit|NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:date1];
    NSDateComponents *dateComponents2 = [[Calendar defaultCalendar] components:NSEraCalendarUnit|NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:date2];
    if([dateComponents1 day] == [dateComponents2 day] &&
       [dateComponents1 month] == [dateComponents2 month] &&
       [dateComponents1 year] == [dateComponents2 year] &&
       [dateComponents1 era] == [dateComponents2 era]) {
        return YES;
    }
    
    return NO;
}

+ (NSDate *) resetSecondsMinutesAndHoursOfDate: (NSDate *) date{
    NSDateComponents *dateComponents = [[Calendar defaultCalendar] components:(NSDayCalendarUnit | NSYearCalendarUnit | NSMonthCalendarUnit) fromDate:date];
    
    NSDateComponents *components = [[NSDateComponents alloc] init] ;
    [components setYear:[dateComponents year]];
    [components setMonth:[dateComponents month]];
    [components setDay:[dateComponents day]];
    
    return [[Calendar defaultCalendar] dateFromComponents:components];
}

+ (NSInteger) minutesFromDate: (NSDate *) date{
    NSDateComponents *components = [[Calendar defaultCalendar] components:NSHourCalendarUnit|NSMinuteCalendarUnit fromDate:date];
    return [components hour] * 60 + [components minute];
}
@end
