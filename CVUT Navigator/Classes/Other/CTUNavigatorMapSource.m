//
//  Created by Tomáš Kohout on 23.01.13.
//
//

#import "CTUNavigatorMapSource.h"
#import "RMTile.h"
#import "Floor.h"
#import "RMZoomConverter.h"
#import "Plan.h"

@implementation CTUNavigatorMapSource

RMZoomConverter * converter;


- initWithZoomConverter: (RMZoomConverter *) aConverter floor: (Floor *) floor{
    self = [super init];
    if (self){
        
        converter = aConverter;
        
        self.maxZoom = converter.maxZoom;
        self.minZoom = converter.minZoom;
        self.floor = floor;
    }
    
    return self;
}


- (NSURL *)URLForTile:(RMTile)tile{
    
    //NSLog(@"Bef: x: %i y: %i z: %i", tile.x, tile.y,tile.zoom);
    tile = [converter convertTile: tile];
    //NSLog(@"Aft: x: %i y: %i z: %i", tile.x, tile.y,tile.zoom);
    
    if (self.floor && self.floor.plan){
        NSURL* url = [NSURL URLWithString: [NSString stringWithFormat:@"https://navigator.fit.cvut.cz/api/1/map/%@/%@/%i/%i/%i?raw=1", self.floor.plan.buildingId, self.floor.id, tile.zoom, tile.x, tile.y]];
        
        //NSLog(@"Aft: x: %i y: %i z: %i", tile.x, ((int)pow(2, tile.zoom) -1 - tile.y),tile.zoom);
        NSLog(@"URL: %@", url);
        
        return url;
    }
    
    
    return nil;
}

-(NSString*) uniqueTilecacheKey
{
    if (self.floor && self.floor.plan){
        return [NSString stringWithFormat: @"CTU Navigator B%@ F%@", self.floor.plan.buildingId, self.floor.id];
    }else{
        return @"CTU Navigator";
    }
}

-(NSString *)shortName
{
    return @"CTU Navigator maps";
}
-(NSString *)longDescription
{
    return @"CTU Navigator maps";
}
-(NSString *)shortAttribution
{
    return @"CTU Navigator maps";
}
-(NSString *)longAttribution
{
    return @"CTU Navigator maps";
}


@end
