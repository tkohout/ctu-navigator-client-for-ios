//
//  Constants.h
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 25.02.13.
//  Copyright (c) 2013 Tomáš Kohout. All rights reserved.
//

#ifndef CVUT_Navigator_Constants_h
#define CVUT_Navigator_Constants_h

#define CURRENT_LOCATION @"currentLocation"
#define PRAGUE_LATITUDE 50.085785
#define PRAGUE_LONGITUDE 14.442902

typedef NS_ENUM(NSInteger, WeekParity) {
    WeekParityEven,
    WeekParityOdd,
    WeekParityBoth,
};

#endif
