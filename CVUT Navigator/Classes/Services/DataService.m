//
//  DataService.m
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 18.02.13.
//  Copyright (c) 2013 Tomáš Kohout. All rights reserved.
//

#import "DataService.h"
#import <RestKit/RestKit.h>
#import <RestKit/Search.h>
#import <RestKit/RKRoute.h>
#import <RestKit/RKRouteSet.h>
#import <RestKit/RKRouter.h>
#import "TimetableEntry.h"
#import "WeekInfo.h"
#import "User.h"
#import "Node.h"
#import "Room.h"
#import "Building.h"
#import "Calendar.h"
#import "Entity.h"
#import "Person.h"
#import <SystemConfiguration/SystemConfiguration.h>
#import "ToastService.h"
#import <RestKit/RKMappingErrors.h>
#import "Reachability.h"
#import "Marker.h"

//5 minutes
//#define MINUMUM_TIME_TO_RELOAD 5*60

//Infinity
#define MINUMUM_TIME_TO_RELOAD INT_MAX

@interface DataService()
@property (nonatomic, retain, readonly) NSString * errorDomain;
@property (nonatomic, retain) Reachability * reachability;
@end


@implementation DataService


+ (DataService  *) sharedService{
    DEFINE_SHARED_INSTANCE_USING_BLOCK(^{
        return [[self alloc] init];
    });
}
- (NSString *)errorDomain{
    return @"CTUNavigator";
}

- (void)initializeMapping{
    //RKLogConfigureByName("RestKit/ObjectMapping", RKLogLevelTrace);
    // Initialize RestKit
    NSURL *baseURL = [NSURL URLWithString:@"https://navigator.fit.cvut.cz/api/1/"/*@"http://cvutnavigator.apiary.io/"*/];
    
    
    RKObjectManager *objectManager = [RKObjectManager managerWithBaseURL:baseURL];
    
    // Initialize managed object store
    NSManagedObjectModel *managedObjectModel = [[NSManagedObjectModel mergedModelFromBundles:nil] mutableCopy];
    RKManagedObjectStore *managedObjectStore = [[RKManagedObjectStore alloc] initWithManagedObjectModel:managedObjectModel];
    objectManager.managedObjectStore = managedObjectStore;
    
    /*Set the right timezone*/
    NSTimeZone *timezone = [NSTimeZone timeZoneWithName:@"Europe/Prague"];
    [RKObjectMapping addDefaultDateFormatterForString:@"yyyy-MM-dd" inTimeZone:timezone];
    
    
    
    
    /* POI/Buildings */
    
    
    RKEntityMapping *poiMapping = [RKEntityMapping mappingForEntityForName:@"Building" inManagedObjectStore:managedObjectStore];
    
    poiMapping.identificationAttributes = @[ @"id" ];
    [poiMapping addAttributeMappingsFromDictionary:@{
     @"id": @"id",
     @"name": @"name",
     @"address": @"address",
     @"coordinates.latitude": @"latitude",
     @"coordinates.longitude": @"longitude",
     @"plan": @"planId"
     }];
    [poiMapping addConnectionForRelationship:@"plan" connectedBy:@{@"planId":@"id"} ];
    
    
    /* Marker*/
    RKEntityMapping *markerMapping = [RKEntityMapping mappingForEntityForName:@"Marker" inManagedObjectStore:managedObjectStore];
    
    
    markerMapping.identificationAttributes = @[ @"id" ];
    [markerMapping addAttributeMappingsFromDictionary:@{
     @"id": @"id",
     @"angle":@"angle",
     @"type": @"type"
     }];
    
    
    /* Room*/
    RKEntityMapping *roomMapping = [RKEntityMapping mappingForEntityForName:@"Room" inManagedObjectStore:managedObjectStore];
    
    
    roomMapping.identificationAttributes = @[ @"id" ];
    [roomMapping addAttributeMappingsFromDictionary:@{
     @"id": @"id",
     @"name":@"name",
     @"building":@"buildingId",
     @"floor":@"floorId",
     @"floorName":@"floorName",
     @"coordinates.latitude": @"latitude",
     @"coordinates.longitude": @"longitude"
     }];
    
    [roomMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"marker" toKeyPath:@"marker" withMapping:markerMapping]];
    
    [roomMapping addConnectionForRelationship:@"building" connectedBy:@{@"buildingId":@"id"} ];
    [roomMapping addConnectionForRelationship:@"floor" connectedBy:@{@"floorId":@"id"} ];
    
    
    /* Node*/
    
    RKEntityMapping *nodeMapping = [RKEntityMapping mappingForEntityForName:@"Node" inManagedObjectStore:managedObjectStore];
    
    nodeMapping.identificationAttributes = @[ @"id" ];
    [nodeMapping addAttributeMappingsFromDictionary:@{
     @"id": @"id",
     @"type": @"type",
     @"building":@"buildingId",
     @"floor":@"floorId",
     @"floorName":@"floorName",
     @"coordinates.latitude": @"latitude",
     @"coordinates.longitude": @"longitude"
     }];
    
    [nodeMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"marker" toKeyPath:@"marker" withMapping:markerMapping]];
    [nodeMapping addConnectionForRelationship:@"building" connectedBy:@{@"buildingId":@"id"} ];
    [nodeMapping addConnectionForRelationship:@"floor" connectedBy:@{@"floorId":@"id"} ];
    
    
    RKDynamicMapping *nodeRoomMapping = [RKDynamicMapping new];
    
    [nodeRoomMapping setObjectMappingForRepresentationBlock:^RKObjectMapping *(id representation) {
        if ([[representation valueForKey:@"type"] isEqualToString:@"room"] || [[representation valueForKey:@"type"] isEqualToString:@"lecture"] || [[representation valueForKey:@"type"] isEqualToString:@"auditorium"]) {
            return roomMapping;
        } else {
            return nodeMapping;
        }
    }];
    
    /* Floor*/
    RKEntityMapping *floorMapping = [RKEntityMapping mappingForEntityForName:@"Floor" inManagedObjectStore:managedObjectStore];
    
    
    floorMapping.identificationAttributes = @[ @"id" ];
    [floorMapping addAttributeMappingsFromDictionary:@{
     @"floor": @"id",
     @"boundingSW.latitude": @"southWestLatitude",
     @"boundingSW.longitude": @"southWestLongitude",
     @"boundingNE.latitude": @"northEastLatitude",
     @"boundingNE.longitude": @"northEastLongitude",
     @"maxZoom":@"maxZoom",
     @"minZoom":@"minZoom",
     @"tiles":@"tiles",
     @"floorName":@"floorName",
     @"floorNumber": @"floorNumber"
     }];
    
    [floorMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"nodes" toKeyPath:@"nodes" withMapping:nodeRoomMapping]];
    
    
    
    /*Plan*/
    
    RKEntityMapping *planMapping = [RKEntityMapping mappingForEntityForName:@"Plan" inManagedObjectStore:managedObjectStore];
    
    
    planMapping.identificationAttributes = @[ @"id" ];
    [planMapping addAttributeMappingsFromDictionary:@{
     @"id": @"id",
     @"building": @"buildingId"}];
    
    [planMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"floors" toKeyPath:@"floors" withMapping:floorMapping]];
    [planMapping addConnectionForRelationship:@"building" connectedBy:@{@"buildingId":@"id"} ];
    
    
    /*Course*/
    
    RKEntityMapping *courseMapping = [RKEntityMapping mappingForEntityForName:@"Course" inManagedObjectStore:managedObjectStore];
    
    courseMapping.identificationAttributes = @[ @"id" ];
    [courseMapping addAttributeMappingsFromDictionary:@{
     @"id": @"id",
     @"code": @"name",
     @"name": @"longName",
     @"web": @"web",
     }];
    
    
    
    /*Person*/
    
    RKEntityMapping *personMapping = [RKEntityMapping mappingForEntityForName:@"Person" inManagedObjectStore:managedObjectStore];
    
    personMapping.identificationAttributes = @[ @"id" ];
    [personMapping addAttributeMappingsFromDictionary:@{
     @"id": @"id",
     @"firstName": @"firstName",
     @"lastName": @"lastName",
     @"username": @"username",
     @"email": @"email",
     @"nodeID": @"roomId",
     @"roomName": @"roomName"
     }];
    
    [personMapping addConnectionForRelationship:@"room" connectedBy:@{@"roomId":@"id"} ];
    
    /*Timetable entry*/
    
    RKEntityMapping *timetableEntryMapping = [RKEntityMapping mappingForEntityForName:@"TimetableEntry" inManagedObjectStore:managedObjectStore];
    
    timetableEntryMapping.identificationAttributes = @[ @"id" ];
    [timetableEntryMapping addAttributeMappingsFromDictionary:@{
     @"id": @"id",
     @"day": @"day",
     @"length": @"length",
     @"type": @"typeString",
     @"time": @"timeString",
     @"parity": @"parityString",
     @"roomName": @"roomName"
     }];
    
    
    [timetableEntryMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"course" toKeyPath:@"course" withMapping:courseMapping]];
    [timetableEntryMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"room" toKeyPath:@"room" withMapping:roomMapping]];
    [timetableEntryMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"teacher" toKeyPath:@"teacher" withMapping:personMapping]];
    
    RKEntityMapping *eventMapping = [RKEntityMapping mappingForEntityForName:@"Event" inManagedObjectStore:managedObjectStore];
    
    eventMapping.identificationAttributes = @[ @"id" ];
    [eventMapping addAttributeMappingsFromDictionary:@{
     @"id": @"id",
     @"length": @"length",
     @"type": @"typeString",
     @"time": @"timeString",
     @"date": @"date",
     @"eventDetail": @"eventDetail",
     @"eventName": @"eventName"
     }];
    
    
    [eventMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"course" toKeyPath:@"course" withMapping:courseMapping]];
    [eventMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"room" toKeyPath:@"room" withMapping:roomMapping]];
    [eventMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"teacher" toKeyPath:@"teacher" withMapping:personMapping]];
    
    
    
    
    RKDynamicMapping *timetableMapping = [RKDynamicMapping new];
    
    
    [timetableMapping setObjectMappingForRepresentationBlock:^RKObjectMapping *(id representation) {
        if ([[representation valueForKey:@"type"] caseInsensitiveCompare:@"event"] == NSOrderedSame) {
            return eventMapping;
        } else {
            return timetableEntryMapping;
        }
    }];
    
    
    
    /* Response descriptors for services */
    RKResponseDescriptor *poisDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:poiMapping
                                                                                   pathPattern:@"map/pois/"
                                                                                       keyPath:@"pois"
                                                                                   statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    
    RKResponseDescriptor *poiDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:poiMapping
                                                                                  pathPattern:@"map/poi/:poiId"
                                                                                      keyPath:nil
                                                                                  statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    
    RKResponseDescriptor *planDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:planMapping
                                                                                   pathPattern:@"map/plan/:planId"
                                                                                       keyPath:nil
                                                                                   statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    
    
    RKResponseDescriptor *timetableDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:timetableMapping
                                                                                        pathPattern:@"kos/person/:username/timetable"
                                                                                            keyPath:@"timetableEntries"
                                                                                        statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    
    RKResponseDescriptor *nodeDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:nodeRoomMapping
                                                                                   pathPattern:@"map/node/marker/:markerId"
                                                                                       keyPath:nil
                                                                                   statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    
    RKResponseDescriptor *nodeDescriptor2 = [RKResponseDescriptor responseDescriptorWithMapping:nodeRoomMapping
                                                                                   pathPattern:@"map/node/:nodeId"
                                                                                       keyPath:nil
                                                                                   statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    /*Error handling*/
    
    RKObjectMapping *errorMapping = [RKObjectMapping mappingForClass:[RKErrorMessage class]];
    [errorMapping addPropertyMapping:[RKAttributeMapping attributeMappingFromKeyPath:nil toKeyPath:@"errorMessage"]];
    
    NSIndexSet *statusCodes = RKStatusCodeIndexSetForClass(RKStatusCodeClassClientError);
    RKResponseDescriptor *errorDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:errorMapping pathPattern:nil keyPath:@"exception.message" statusCodes:statusCodes];
    
    
    [objectManager addResponseDescriptorsFromArray:@[poisDescriptor, poiDescriptor, planDescriptor, timetableDescriptor, nodeDescriptor, nodeDescriptor2, errorDescriptor]];
    
    
    /*Support for timetable orphaned objects*/
    
    [objectManager addFetchRequestBlock:^NSFetchRequest *(NSURL *URL) {
        RKPathMatcher *pathMatcher = [RKPathMatcher pathMatcherWithPattern:@"kos/person/:username/timetable"];
        
        NSDictionary *argsDict = nil;
        BOOL match = [pathMatcher matchesPath:[URL relativePath] tokenizeQueryStrings:NO parsedArguments:&argsDict];
       
        if (match) {
            NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"TimetableEntry"];
            return fetchRequest;
        }
        
        return nil;
    }];
    
    
    
    /*Initialize indexing*/
    [managedObjectStore addSearchIndexingToEntityForName:@"Room" onAttributes:@[@"name"]];
    
    [managedObjectStore addSearchIndexingToEntityForName:@"Building" onAttributes:@[@"name", @"address"]];
    
    [managedObjectStore addSearchIndexingToEntityForName:@"Person" onAttributes:@[@"name", @"username", @"email"]];
    
    [managedObjectStore addSearchIndexingToEntityForName:@"Course" onAttributes:@[@"name", @"longName"]];
    
    
    
    
    
    // Uncomment this to use XML, comment it to use JSON
    //  objectManager.acceptMIMEType = RKMIMETypeXML;
    //  [objectManager.mappingProvider setMapping:statusMapping forKeyPath:@"statuses.status"];
    
    // Database seeding is configured as a copied target of the main application. There are only two differences
    // between the main application target and the 'Generate Seed Database' target:
    //  1) RESTKIT_GENERATE_SEED_DB is defined in the 'Preprocessor Macros' section of the build setting for the target
    //      This is what triggers the conditional compilation to cause the seed database to be built
    //  2) Source JSON files are added to the 'Generate Seed Database' target to be copied into the bundle. This is required
    //      so that the object seeder can find the files when run in the simulator.
    
    
    /**
     Complete Core Data stack initialization
     */
    
    [managedObjectStore createPersistentStoreCoordinator];
    
    NSString *storePath = [RKApplicationDataDirectory() stringByAppendingPathComponent:@"CVUTNavigator.sqlite"];
    NSString *seedPath = [[NSBundle mainBundle] pathForResource:@"RKSeedDatabase" ofType:@"sqlite"];
    NSError *error;
    
    
    //Temporary removal
    
    //[[NSFileManager defaultManager] removeItemAtPath:storePath error:&error];
    
    
    
    [managedObjectStore addSQLitePersistentStoreAtPath:storePath fromSeedDatabaseAtPath:seedPath withConfiguration:nil options:nil error:&error];
    
    
    //NSAssert(persistentStore, @"Failed to add persistent store with error: %@", error);
    
    // Create the managed object contexts
    [managedObjectStore createManagedObjectContexts];
    
    [managedObjectStore startIndexingPersistentStoreManagedObjectContext];
    
    // Configure a managed object cache to ensure we do not create duplicate objects
    managedObjectStore.managedObjectCache = [[RKInMemoryManagedObjectCache alloc] initWithManagedObjectContext:managedObjectStore.persistentStoreManagedObjectContext];
    
    }

- (NSManagedObjectContext *) context{
    return [RKManagedObjectStore defaultStore].mainQueueManagedObjectContext;
}



- (WeekInfo *) getWeekInfoFromDatabase{
    NSFetchRequest * request = [[NSFetchRequest alloc] initWithEntityName:@"WeekInfo"];
    
    NSError * error = nil;
    
    NSArray *results = [[self context] executeFetchRequest:request error:&error];
    
    if ([results count] != 0){
        return [results objectAtIndex:0];
    }
    
    return nil;
}


- (void) getWeekInfoSuccess: (void (^)(WeekInfo *)) onSuccess failure:(void (^)(NSError *error))onFailure{
    //If we already downloaded it
    WeekInfo * weekInfo = [self getWeekInfoFromDatabase];
    
    
    if (weekInfo){
        onSuccess(weekInfo);
        
        //If the week info is from the same day it won't be probably changed on the server, so we don't have to update
        if ([Calendar isDate:weekInfo.updatedDate sameDayAs:[NSDate date]]){
            return;
        }
    }
    
        //Downloading or updating week info
        [self getObjectWithoutMappingAndWithAuthorizationAtPath:@"info" params:nil success:^(NSDictionary * dictionary) {
            NSDictionary * week = [dictionary objectForKey:@"week"];
            
            if (week){
                
                NSString * parityString = [week objectForKey:@"parity"];
                WeekParity parity;
                
                if ([parityString isEqualToString:@"even"]){
                    parity = WeekParityEven;
                }else{
                    parity = WeekParityOdd;
                }
                
                NSInteger weekNumber = [((NSString *)[week objectForKey:@"number"]) intValue];
                
                
                
                WeekInfo * info = [self getWeekInfoFromDatabase];
                
                if (!info){
                    info = [NSEntityDescription
                            insertNewObjectForEntityForName:@"WeekInfo"
                            inManagedObjectContext:[self context]];
                }else{
                    //If nothing changed there is no reason to update
                    if (weekNumber == [info.week integerValue] && parity == [info.parity integerValue]){
                        return;
                    }
                }
                
                info.week = [NSNumber numberWithInt: weekNumber];
                info.parity = [NSNumber numberWithInt: parity];
                
                [[self context] saveToPersistentStore:nil];
                
                onSuccess(info);
                return;
            }else{
                onFailure([self errorWithCode:APIErrorBadRequest message:@"Bad request"]);
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            onFailure(error);
        }  ];
        
       
    
}

#pragma mark Timetable
- (NSArray *) getEntriesForDate: (NSDate *) date{
    NSLog(@"Loading date: %@", date);
    WeekInfo * weekInfo = [self getWeekInfoFromDatabase];
    if (weekInfo){
        NSInteger parity = [[weekInfo getParityForDate:date] integerValue];
        
        
        NSError * error;
        
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSEntityDescription *entity = [NSEntityDescription
                                       entityForName:@"TimetableEntry" inManagedObjectContext:[self context]];
        [fetchRequest setEntity:entity];
        
        
        NSInteger weekdayOfDate = [Calendar weekDayOfDate:date];
        
        NSPredicate * predicate = [NSPredicate predicateWithFormat:@"((type != %i AND (parity = %i OR parity = %i) AND day = %i) OR (type == %i AND date = %@)) AND (time + length) > %i",
                                   TimetableEntryTypeEvent, parity, WeekParityBoth, weekdayOfDate, TimetableEntryTypeEvent, date ,[Calendar minutesFromDate:date]];
        
        [fetchRequest setPredicate:predicate];
        
        NSSortDescriptor *sortByTime = [[NSSortDescriptor alloc] initWithKey:@"time" ascending:YES];
        
        [fetchRequest setSortDescriptors:[NSArray arrayWithObjects:sortByTime, nil]];
        
        
        NSArray *fetchedObjects = [[self context] executeFetchRequest:fetchRequest error:&error];
        
        return fetchedObjects;
        
    }
    
    return nil;
}


//Counts week relatively to the current week
- (NSDate *) getWeekStart: (NSInteger) week{
    NSDate * now = [NSDate date];
    
    WeekInfo * weekInfo = [self getWeekInfoFromDatabase];
    
    //Get difference between current week and week we want
    NSInteger diff = week - [weekInfo.currentWeek integerValue];
    
    //Get date from that week
    NSDate * startOfTheWeek = [Calendar date:now byAddingWeeks:diff];
    
    //Get first day of the week
    startOfTheWeek = [Calendar firstDayOfTheWeekForDate:startOfTheWeek];
    
    return startOfTheWeek;
}

- (NSDate *) getWeekEnd: (NSInteger) week{
    NSDate * weekStart = [self getWeekStart: week];
    NSDate * weekEnd = [Calendar date:weekStart byAddingWeeks:1];
    
    return weekEnd;
}

- (void)getEntriesForWeekGroupedByDate:(NSInteger) week parity: (NSInteger) parity success:(void (^)(NSDictionary *))onSuccess failure:(void (^)(NSError *))onFailure{
    
        NSDate * weekStart = [self getWeekStart:week];
        [self getEntriesForWeek:week parity:parity  success:^(NSArray * entries) {
            NSMutableDictionary * entriesByDates = [[NSMutableDictionary alloc] init];
            
            
            for (TimetableEntry * entry in entries){
                NSDate * date;
                //When event, just take the date without hours, seconds or minutes
                if ([entry.type integerValue] == TimetableEntryTypeEvent){
                    date = [Calendar resetSecondsMinutesAndHoursOfDate:entry.date];
                    //When timetable entry, the date have to be computed from start of the week
                }else{
                    date = [Calendar date:weekStart byAddingDays:[entry.day integerValue]-1];
                }
                
                //Adding result to array by date
                NSMutableArray * dateArray = [entriesByDates objectForKey:date];
                if (!dateArray){
                    dateArray = [[NSMutableArray alloc] init];
                }
                [dateArray addObject:entry];
                
                [entriesByDates setObject:dateArray forKey:date];
            }
            
            onSuccess(entriesByDates);
        } failure:onFailure];
}

- (void)getEntriesForWeek:(NSInteger)week parity:(NSInteger)parity success:(void (^)(NSArray *))onSuccess failure:(void (^)(NSError *))onFailure{
   
        NSError * error;
        
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSEntityDescription *entity = [NSEntityDescription
                                       entityForName:@"TimetableEntry" inManagedObjectContext:[self context]];
        [fetchRequest setEntity:entity];
        
        NSDate * weekStart = [self getWeekStart: week];
        NSDate * weekEnd = [self getWeekEnd: week];
        
        NSLog (@"Week %i %@ - %@",week, weekStart, weekEnd);
        
        
        NSPredicate * predicate = [NSPredicate predicateWithFormat:@"(type != %i AND (parity = %i OR parity = %i)) OR (type == %i AND date >= %@ AND date < %@)",
                                   TimetableEntryTypeEvent, parity, WeekParityBoth, TimetableEntryTypeEvent, weekStart, weekEnd];
        
        [fetchRequest setPredicate:predicate];
        
        
        NSSortDescriptor *sortByDay = [[NSSortDescriptor alloc] initWithKey:@"day" ascending:YES];
        NSSortDescriptor *sortByTime = [[NSSortDescriptor alloc] initWithKey:@"time" ascending:YES];
        [fetchRequest setSortDescriptors:[NSArray arrayWithObjects:sortByDay, sortByTime, nil]];
        
        
        NSArray *fetchedObjects = [[self context] executeFetchRequest:fetchRequest error:&error];
        
        onSuccess(fetchedObjects);
        
}

- (BOOL) objectNeedsToBeUpdated: (NSDate *) updatedDate{
    NSInteger timeInterval  = [[NSDate date] timeIntervalSinceDate:updatedDate];
    
    if (timeInterval > MINUMUM_TIME_TO_RELOAD){
        return YES;
    }
    
    return NO;
}

- (void) getTimetableSuccess: (void (^)(NSArray *)) onSuccess failure:(void (^)(NSError *))onFailure{
    if (self.user){
        
        
        NSDate * updatedDate = nil;
        
        //Check if it is in database
        NSFetchRequest * fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"TimetableEntry"];
        NSSortDescriptor * sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"updatedDate" ascending:NO];
        
        fetchRequest.sortDescriptors = @[sortDescriptor];
        
        NSArray * fetchedObjects = [self.context executeFetchRequest:fetchRequest error:nil];
       
        
        if ([fetchedObjects count] > 0){
            onSuccess(fetchedObjects);
            //It is loaded all in once, so we have to check only first one
            TimetableEntry * firstEntry = [fetchedObjects objectAtIndex:0];
            
            updatedDate = firstEntry.updatedDate;
            
            //It is so fresh it does not have to be updated
            if (![self objectNeedsToBeUpdated:updatedDate]){
                return;
            }
        }
        
        if (updatedDate){
            onFailure = nil;
        }
        
        //First load or update
        [self getObjectsWithAuthorizationAtPath:[NSString stringWithFormat:@"kos/person/%@/timetable", self.user.username] ifModifiedSince:updatedDate success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
            
            onSuccess([mappingResult array]);
            
        } failure:^(RKObjectRequestOperation *operation, NSError *error) {
            if (onFailure){
                onFailure(error);
            }
        }];
        
    }
    
}

#pragma mark POIs

- (void) getBuildingsSuccess: (void (^)(NSArray *)) onSuccess failure:(void (^)(NSError *error))onFailure{
    
    NSDate * updatedDate = nil;
    NSFetchRequest * request = [[NSFetchRequest alloc] initWithEntityName:@"Building"];
    NSError * error;
    NSArray * buildings = [[self context] executeFetchRequest:request error:&error];
    
    
    if ([buildings count] > 0){
        onSuccess(buildings);
        
        //Because building can be updated individually we are looking for the least updated building
        for (Building * building in buildings){
            if (!updatedDate || [building.updatedDate compare:updatedDate] == NSOrderedAscending){
                updatedDate = building.updatedDate;
            }
        }
        
        //No need for update
        if (![self objectNeedsToBeUpdated:updatedDate]){
            return;
        }
        
    }
    
    if (updatedDate){
        onFailure = nil;
    }
    
    //First load or update
    [self getObjectsWithAuthorizationAtPath:@"map/pois/" ifModifiedSince:updatedDate success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        if ([[mappingResult array] count] > 0){
            
            if (onSuccess){
                onSuccess([mappingResult array]);
            }
        }else{
            if (onFailure){
                onFailure([self errorWithCode:APIErrorNotFound message:@"Not found"]);
            }
        }
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        if (onFailure){
            onFailure(error);
        }
    }];
    
}

- (void) getBuildingForNode: (Node *) node success: (void(^)(Building * building)) onSuccess failure:(void (^)(NSError *error))onFailure{
    NSDate * updatedDate = nil;
    
    //If node is already connected with building
    if (node.building){
        onSuccess(node.building);
        updatedDate = node.building.updatedDate;
        
        if (![self objectNeedsToBeUpdated:updatedDate]){
            return;
        }
    }
    
    //No errors are raised when updating
    if (updatedDate){
        onFailure = nil;
    }
    
    //First load or update
    [self getObjectsWithAuthorizationAtPath:[NSString stringWithFormat:@"map/poi/%@", node.buildingId] ifModifiedSince:updatedDate success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        
        if ([[mappingResult array] count] >0){
            
            Building * building = [[mappingResult array] objectAtIndex:0];
            
            
            /* We have to update the relationship on the other side manualy, because restkit doesn't know he should do it*/
            NSFetchRequest * request = [[NSFetchRequest alloc] initWithEntityName:@"Node"];
            request.predicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"buildingId = %@", node.buildingId]];
            
            //Find all nodes with connection to the building
            NSArray * nodes = [self.context executeFetchRequest:request error:nil];
            for (Node * buildingNode in nodes){
                buildingNode.building = building;
                [self.context refreshObject:buildingNode mergeChanges:YES];
            }
            
            [self.context saveToPersistentStore:nil];
            
            onSuccess(building);
        }else{
            if (onFailure){
                onFailure([self errorWithCode:APIErrorNotFound message:@"Not found"]);
            }
        }
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        if (onFailure){
            onFailure(error);
        }
    }];
    
}
#pragma mark Person

- (void) getRoomForPerson:(Person *)person success:(void (^)(Room *))onSuccess failure:(void (^)(NSError *))onFailure{
    NSDate * updatedDate = nil;
    
    if (person.room){
        onSuccess(person.room);
        updatedDate = person.room.updatedDate;
        
        if (![self objectNeedsToBeUpdated:updatedDate]){
            return;
        }
    }
    
    if (updatedDate){
        onFailure = nil;
    }
    
    //First load or update
    [self getObjectsWithAuthorizationAtPath:[NSString stringWithFormat:@"map/node/%@", person.roomId] ifModifiedSince:updatedDate success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        if (onSuccess){
            
            if ([[mappingResult array] count] >0){
                onSuccess([[mappingResult array] objectAtIndex:0]);
                return;
            }
            
            if (onFailure){
                onFailure([self errorWithCode:APIErrorNotFound message:@"Not found"]);
            }
            
        }
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        if (onFailure){
            onFailure(error);
        }
    }];
}


#pragma mark Plan

- (void) getPlanForBuilding: (Building * ) building success: (void (^)(Plan * plan)) onSuccess failure:(void (^)(NSError *error))onFailure{
    NSDate * updatedDate = nil;
    
    if (building.plan){
        onSuccess(building.plan);
        updatedDate = building.plan.updatedDate;
        
        if (![self objectNeedsToBeUpdated:updatedDate]){
            return;
        }
    }
    
    if (updatedDate){
        onFailure = nil;
    }
    
    //First load or update
    [self getObjectsWithAuthorizationAtPath:[NSString stringWithFormat:@"map/plan/%@", /*[NSNumber numberWithInt:3]*/building.planId] ifModifiedSince:updatedDate success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        if (onSuccess){
            
            if ([[mappingResult array] count] >0){
                onSuccess([[mappingResult array] objectAtIndex:0]);
                return;
            }
            
            if (onFailure){
                onFailure([self errorWithCode:APIErrorNotFound message:@"Not found"]);
            }
            
        }
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        if (onFailure){
            onFailure(error);
        }
    }];
    
}


#pragma Routing
- (void) findRouteStart: (Node *) start end: (Node *) end success: (void (^)(NSArray *)) onSuccess failure:(void (^)(NSError *error))onFailure{
    
    [self getObjectWithoutMappingAtPath:[NSString stringWithFormat: @"map/path?from=%i&to=%i", [start.id intValue], [end.id intValue]] params:nil success:^(NSDictionary * dictionary) {
        NSArray * nodeIds = [dictionary objectForKey:@"nodeIds"];
        if ([nodeIds count] > 0){
            
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.id IN %@", nodeIds];
            
            NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
            NSEntityDescription *entity = [NSEntityDescription
                                           entityForName:@"Node" inManagedObjectContext:[self context]];
            
            [fetchRequest setEntity:entity];
            [fetchRequest setPredicate:predicate];
            
            
            NSError * error;
            
            //Retrieve objects
            NSArray *fetchedObjects = [[self context] executeFetchRequest:fetchRequest error:&error];
            
            
            NSMutableArray * nodes = [[NSMutableArray alloc] init];
            
            for (int i=0; i<[fetchedObjects count]; i++){
                [nodes addObject:[[NSNull alloc] init]];
            }
            
            //Sort it back in right order
            for (Node * node in fetchedObjects){
                
                NSInteger index = 0;
                for (NSNumber * nodeId in nodeIds){
                    if ([nodeId integerValue] == [node.id integerValue]){
                        break;
                    }
                    index++;
                }
                
                [nodes replaceObjectAtIndex:index withObject:node];
            }
            
            
            NSMutableArray * nodesToRemove = [[NSMutableArray alloc] init];
            
            for (Node * node in nodes){
                if ([node isKindOfClass:[NSNull class]]){
                    [nodesToRemove addObject:node];
                }
            }
            
            [nodes removeObjectsInArray:nodesToRemove];
            
            
            onSuccess(nodes);
            return;
            
        }else{
            if (onFailure){
                onFailure([self errorWithCode:APIErrorNotFound message:@"Not found"]);
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (onFailure){
            if (operation.response.statusCode == 404){
                onFailure([self errorWithCode:APIErrorNotFound message:@"Not found"]);
            }else{
                onFailure(error);
            }
        }
    }];
    
}



#pragma mark Search

- (void) searchAllQuery: (NSString *) query success: (void (^)(NSArray *)) onSuccess {
    [self searchPlacesQuery:query success:^(NSArray * placeMatches) {
        
        NSPredicate *predicate = [RKSearchPredicate searchPredicateWithText:query type:NSAndPredicateType];
        
        NSFetchRequest *personFetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"Person"];
        personFetchRequest.predicate = predicate;
        
        NSFetchRequest *courseFetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"Course"];
        courseFetchRequest.predicate = predicate;
        
        NSArray *personMatches = [[self context] executeFetchRequest:personFetchRequest error:nil];
        
        NSArray *courseMatches = [[self context] executeFetchRequest:courseFetchRequest error:nil];
        
        NSArray * matches = [[placeMatches arrayByAddingObjectsFromArray:personMatches] arrayByAddingObjectsFromArray:courseMatches];
        
        onSuccess(matches);
        
    }];
}

- (void) searchPlacesQuery: (NSString *) query success: (void (^)(NSArray *)) onSuccess {
    
    NSPredicate *predicate = [RKSearchPredicate searchPredicateWithText:query type:NSAndPredicateType];
    
    
    NSFetchRequest *roomFetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"Room"];
    roomFetchRequest.predicate = predicate;
    
    
    NSFetchRequest *buildingFetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"Building"];
    buildingFetchRequest.predicate = predicate;
    
    
    
    
    NSArray *roomMatches = [[self context] executeFetchRequest:roomFetchRequest error:nil];
    NSArray *buildingMatches = [[self context] executeFetchRequest:buildingFetchRequest error:nil];
    
    
    
    NSArray *matches=[roomMatches arrayByAddingObjectsFromArray:buildingMatches ];
    
    //TRC_OBJ(matches);
    
    onSuccess(matches);
    
    
}

#pragma mark Augmented Reality

- (Node *) getNodeForMarkerIdFromDatabase: (NSInteger) markerId{
   
    //Node for marker loaded from database
    NSFetchRequest * fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Node"];
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"marker.id = %@", [NSNumber numberWithInt:markerId]];
    fetchRequest.predicate = predicate;
    NSError * error;
    
    NSArray * nodes = [self.context executeFetchRequest:fetchRequest error: &error];
    
    if ([nodes count] > 0){
        Node * node = [nodes objectAtIndex:0];
        return node;
    }
    
    return nil;
}

- (void) getNodeForMarkerId: (NSInteger) markerId success: (void(^)(Node *)) onSuccess failure:(void (^)(NSError *error))onFailure{
    
    NSDate * updatedDate;
    Node * node = [self getNodeForMarkerIdFromDatabase:markerId];
    
    if (node){
        onSuccess(node);
        updatedDate = node.updatedDate;
        
        if (![self objectNeedsToBeUpdated:updatedDate]){
            return;
        }
    }
    
    if (updatedDate){
        onFailure = nil;
    }
    
    //First loaded or update
    [self getObjectsWithAuthorizationAtPath:[NSString stringWithFormat: @"map/node/marker/%i", markerId] ifModifiedSince:updatedDate success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        if ([mappingResult count] > 0){
            if (onSuccess){
                onSuccess([[mappingResult array] objectAtIndex:0]);
            }
        }else{
            if (onFailure){
                onFailure([self errorWithCode:APIErrorNotFound message:@"Not found"]);
            }
        }
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        if (onFailure){
            onFailure(error);
        }
    }];
    
}

#pragma mark User authentification

- (User *) user{
    NSFetchRequest * request = [[NSFetchRequest alloc] initWithEntityName:@"User"];
    
    NSError * error = nil;
    
    NSArray *results = [[self context] executeFetchRequest:request error:&error];
    
    if ([results count] != 0){
        return [results objectAtIndex:0];
    }
    
    return nil;
    
}


- (void) getAccessTokenSuccess: (void(^)(NSString *accessToken))onSuccess failure: (void (^)(NSError *error)) onFailure{
    User * user = self.user;
    
    if (user){
        NSDate * now = [NSDate date];
        if (NSOrderedAscending == [now compare:user.expiresIn]){
            onSuccess(user.accessToken);
            
        }else{
            [self refreshToken:user.refreshToken onSuccess:^(NSString * accessToken) {
                onSuccess(accessToken);
            } failure:^(NSError *error) {
                if (onFailure)
                    onFailure(error);
            }];
        }
        
    }else{
        if (onFailure){
            onFailure([self errorWithCode:APIErrorUnauthorized message:@"Uživatel není přihlášen"]);
        }
    }
}

- (NSError *) errorWithCode: (NSInteger) code message: (NSString *) message{
    NSError * error = [[NSError alloc] initWithDomain:self.errorDomain code:code userInfo:@{ NSLocalizedDescriptionKey : message}];
    return error;
}

- (void) setAuthorizationHeaderForRequestsWithAccessToken: (NSString *) accessToken{
    [[[RKObjectManager sharedManager] HTTPClient] setDefaultHeader:@"Authorization" value:[NSString stringWithFormat:@"Bearer %@", accessToken]];
}

- (void) refreshToken: (NSString *) refreshToken onSuccess: (void(^)(NSString * accessToken)) onSuccess failure:(void (^)(NSError *error))onFailure{
    
    NSDictionary *params = @{@"refresh_token": refreshToken};
    
    [self postObjectWithoutMappingAtPath:@"refreshToken" params:params success:^(NSDictionary * dictionary) {
        NSString * accessToken = [dictionary objectForKey:@"access_token"];
        NSString * tokenType = [dictionary objectForKey:@"token_type"];
        NSString * expiresIn = [dictionary objectForKey:@"expires_in"];
        NSString * refreshToken = [dictionary objectForKey:@"refresh_token"];
        
        
        User * user = self.user;
        NSInteger expiresInSeconds = [expiresIn integerValue];
        
        user.accessToken = accessToken;
        user.refreshToken = refreshToken;
        user.expiresIn = [Calendar date:[NSDate date] byAddingHours:0 minutes:0 seconds:expiresInSeconds days:0 weeks:0 months:0 years:0];
        user.tokenType = tokenType;
        
        [self.context saveToPersistentStore:nil];
        
        onSuccess(accessToken);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (onFailure){
            if (operation.response.statusCode == 400){
                //I am changing 400 -> 401 because I found it more logical
                onFailure([self errorWithCode:APIErrorUnauthorized message:@"Invalid grant"]);
            }else{
                onFailure(error);
            }
            
        }
    }];
    
    
    
}

- (void) loginWithUsername: (NSString *) username password: (NSString *) password onSuccess: (void(^)(User *)) onSuccess failure:(void (^)(NSError *error))onFailure{
    
    NSDictionary *params = @{@"user": username, @"password": password};
    
    [self postObjectWithoutMappingAtPath:@"authorize" params:params success:^(NSDictionary * dictionary) {
        
        NSString * accessToken = [dictionary objectForKey:@"access_token"];
        NSString * tokenType = [dictionary objectForKey:@"token_type"];
        NSString * expiresIn = [dictionary objectForKey:@"expires_in"];
        NSString * refreshToken = [dictionary objectForKey:@"refresh_token"];
        
        
        User * user = self.user;
        
        if (!user){
            user = [NSEntityDescription
                    insertNewObjectForEntityForName:@"User"
                    inManagedObjectContext:[self context]];
        }
        
        NSInteger expiresInSeconds = [expiresIn integerValue];
        
        
        user.accessToken = accessToken;
        user.refreshToken = refreshToken;
        user.expiresIn = [Calendar date:[NSDate date] byAddingHours:0 minutes:0 seconds:expiresInSeconds days:0 weeks:0 months:0 years:0];
        user.tokenType = tokenType;
        user.username = username;
        
        [[NSUserDefaults standardUserDefaults] setValue:username forKey:@"username"];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"logged_in"];
        
        [self.context saveToPersistentStore:nil];
        
        onSuccess(user);
        
        return;
    } failure:^(AFHTTPRequestOperation * operation,  NSError *error) {
        if (operation.response.statusCode == 401){
            onFailure([self errorWithCode:APIErrorUnauthorized message:@"Bad login"]);
        }else{
            onFailure(error);
        }
    }];
        
}

- (void) userUnauthorized{
    [self logout];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(userUnauthorized)]){
        [self.delegate userUnauthorized];
    }
}

- (void) logout{
    if (self.user){
        [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"username"];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"logged_in"];
        
        [self.context deleteObject:self.user];
        [self.context saveToPersistentStore:nil];
    }
}

#pragma mark Network methods

- (void) getObjectsWithAuthorizationAtPath: (NSString *) path ifModifiedSince: (NSDate *) modifiedSince success: (void(^)(RKObjectRequestOperation *operation, RKMappingResult *mappingResult))onSuccess failure: (void (^)(RKObjectRequestOperation *operation, NSError *error)) onFailure{
    
    [self getObjectsWithAuthorizationAtPath:path ifModifiedSince:modifiedSince depth:0 success:onSuccess failure:onFailure];
}

- (void) getObjectsWithAuthorizationAtPath: (NSString *) path ifModifiedSince: (NSDate *) modifiedSince depth: (NSInteger) depth success: (void(^)(RKObjectRequestOperation *operation, RKMappingResult *mappingResult))onSuccess failure: (void (^)(RKObjectRequestOperation *operation, NSError *error)) onFailure{
    
    [self getAccessTokenSuccess:^(NSString *accessToken) {
        [self getObjectsAtPath:path ifModifiedSince:modifiedSince accessToken: accessToken success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
            onSuccess(operation, mappingResult);
        } failure:^(RKObjectRequestOperation *operation, NSError *error) {
            
            //We are not authorized, it is possible that accessToken expired even though we checked it in getAccessToken just now
            if (error.code == APIErrorUnauthorized || error.code == APIErrorBadRequest){
                if (self.user){
                    
                    //We will first try refresh access token
                    [self refreshToken:self.user.refreshToken onSuccess:^(NSString * accessToken) {
                        
                        //Depth - so we don't get to the infinite loop
                        if (depth == 0){
                            [self getObjectsWithAuthorizationAtPath:path ifModifiedSince:modifiedSince depth: depth+1 success:onSuccess failure:onFailure];
                        }else{
                            [self userUnauthorized];
                        }
                    
                    //Something is wrong with refreshToken
                    } failure:^(NSError *error) {
                        if (error.code == APIErrorUnauthorized){
                            [self userUnauthorized];
                        //It is some other error
                        }else if (onFailure){
                            onFailure(operation, error);
                        }
                    }];
                }else{
                    [self userUnauthorized];
                }
                
            //There is some other problem
            }else{
                if (onFailure){
                    onFailure(operation, error);
                }
            }
            
            
            
        }];
        
    } failure:^(NSError *error) {
        onFailure(nil, error);
    }];
    
}


- (void) getObjectsAtPath: (NSString *) path ifModifiedSince: (NSDate *) modifiedSince accessToken: (NSString *) accessToken success: (void(^)(RKObjectRequestOperation *operation, RKMappingResult *mappingResult))onSuccess failure: (void (^)(RKObjectRequestOperation *operation, NSError *error)) onFailure{
    NSMutableURLRequest * request = [[RKObjectManager sharedManager] requestWithObject:nil method:RKRequestMethodGET path:path parameters:nil];
    
    
    if (modifiedSince){
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init]; // maybe there exist a new-method now
        NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
        [dateFormatter setLocale:usLocale];
        
        dateFormatter.dateFormat = @"EEE, dd MMM yyyy HH:mm:ss Z"; //RFC2822-Format
        
        NSString *dateString = [dateFormatter stringFromDate:modifiedSince];
        [request setValue:dateString forHTTPHeaderField:@"If-Modified-Since"];
    }
    
    if (accessToken){
        [request setValue:[NSString stringWithFormat:@"Bearer %@", accessToken] forHTTPHeaderField:@"Authorization"];
    }
    
    RKManagedObjectRequestOperation * operation = [[RKObjectManager sharedManager] managedObjectRequestOperationWithRequest:request managedObjectContext:[self context] success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        
        onSuccess(operation, mappingResult);
        
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
       
        
        if (onFailure){
            //If it is error code from the server, mapped on error descriptor
            if (error.code == RKMappingErrorFromMappingResult){
                NSArray * errors = [error.userInfo objectForKey:RKObjectMapperErrorObjectsKey];
                RKErrorMessage * errorMessage;
                NSString * textError = @"";
                if ([errors count] > 0){
                    
                    if ([[errors objectAtIndex:0] isKindOfClass:[RKErrorMessage class]]){
                        errorMessage = [errors objectAtIndex:0];
                        textError = errorMessage.errorMessage;
                    }
                }
                
                switch (operation.HTTPRequestOperation.response.statusCode){
                    case 400:
                        error = [self errorWithCode:APIErrorBadRequest message:textError];
                        break;
                    case 401:
                        error = [self errorWithCode:APIErrorUnauthorized message:textError];
                        break;
                    case 404:
                        error = [self errorWithCode:APIErrorNotFound message:textError];
                        break;
                }
            }
            
            onFailure(operation, error);
        }
    }];
    
    [[RKObjectManager sharedManager] enqueueObjectRequestOperation:operation];
    
}


- (void) getObjectWithoutMappingAndWithAuthorizationAtPath: (NSString *) path params: (NSDictionary *) params success: (void (^)(NSDictionary*)) onSuccess failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))onFailure{
    [self getObjectWithoutMappingAndWithAuthorizationAtPath:path params:params depth:0 success:onSuccess failure:onFailure];
}

- (void) getObjectWithoutMappingAndWithAuthorizationAtPath: (NSString *) path params: (NSDictionary *) params depth: (NSInteger) depth success: (void (^)(NSDictionary*)) onSuccess failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))onFailure{
    
    [self getAccessTokenSuccess:^(NSString *accessToken) {
        [self getObjectWithoutMappingAtPath:path accessToken:accessToken params:params success:^(NSDictionary * object) {
            onSuccess(object);
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            //We are not authorized, it is possible that accessToken expired even though we checked it in getAccessToken just now
            if (error.code == APIErrorUnauthorized){
                if (self.user){
                    
                    //We will first try refresh access token
                    [self refreshToken:self.user.refreshToken onSuccess:^(NSString * accessToken) {
                        
                        //Depth - so we don't get to the infinite loop
                        if (depth == 0){
                            [self getObjectWithoutMappingAndWithAuthorizationAtPath:path params: params depth: depth+1 success:onSuccess failure:onFailure];
                        }else{
                            [self userUnauthorized];
                        }
                        
                        //Something is wrong with refreshToken
                    } failure:^(NSError *error) {
                        if (error.code == APIErrorUnauthorized){
                            [self userUnauthorized];
                            //It is some other error
                        }else if (onFailure){
                            onFailure(operation, error);
                        }
                    }];
                }else{
                    [self userUnauthorized];
                }
                
                //There is some other problem
            }else{
                if (onFailure){
                    onFailure(operation, error);
                }
            }
            
        }];
        
    } failure:^(NSError *error) {
        onFailure(nil, error);
    }];
}

- (void) getObjectWithoutMappingAtPath: (NSString *) path params: (NSDictionary *) params success: (void (^)(NSDictionary*)) onSuccess failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))onFailure{
    [self getObjectWithoutMappingAtPath:path accessToken:nil params:params success:onSuccess failure:onFailure];
}

- (void) getObjectWithoutMappingAtPath: (NSString *) path accessToken: (NSString *) accessToken params: (NSDictionary *) params success: (void (^)(NSDictionary*)) onSuccess failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))onFailure{
    
    AFHTTPClient * client = [[AFHTTPClient alloc] initWithBaseURL:[RKObjectManager sharedManager].baseURL];
    
    [client registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [client setDefaultHeader:@"Accept" value:@"application/json"];
    
    if (accessToken){
        [client setDefaultHeader:@"Authorization" value:[NSString stringWithFormat:@"Bearer %@", accessToken]];
    }
    
    [client getPath:path
         parameters:params
            success:^(AFHTTPRequestOperation *operation, id JSON) {
                
                if (JSON && [JSON isKindOfClass:[NSDictionary class]]){
                    NSDictionary * dictionary = (NSDictionary *) JSON;
                    
                    onSuccess(dictionary);
                    return;
                }else{
                    if (onFailure)
                        onFailure(operation, [self errorWithCode:APIErrorBadRequest message:@"Bad request"]);
                }
            }
            failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                if (onFailure)
                    onFailure(operation, error);
            }];
}

- (void) postObjectWithoutMappingAtPath: (NSString *) path params: (NSDictionary *) params success: (void (^)(NSDictionary*)) onSuccess failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))onFailure{

    AFHTTPClient * client = [[RKObjectManager sharedManager] HTTPClient];
    [client setDefaultHeader:@"Accept" value:@"application/json"];
    [client setParameterEncoding:AFJSONParameterEncoding];
    
    [client postPath:path parameters:params success:^(AFHTTPRequestOperation *operation, id JSON) {
        if (JSON && [JSON isKindOfClass:[NSDictionary class]]){
            NSDictionary * dictionary = (NSDictionary *) JSON;
            onSuccess(dictionary);
        }else{
            if (onFailure){
                onFailure(operation, [self errorWithCode:APIErrorBadRequest message:@"Bad request"]);
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if(onFailure){
            onFailure(operation, error);
        }
    }];
}

+ (void) showNetworkUnavailableMessageOnView: (UIView *) view{
    [DataService showErrorMessageWithTitle:NSLocalizedString(@"Chyba", nil) subtitle:NSLocalizedString(@"Nelze se připojit k Internetu", nil) onView:view];
}

+ (void) showServerErrorMessageOnView: (UIView *) view{
    [DataService showErrorMessageWithTitle:NSLocalizedString(@"Chyba", nil) subtitle:NSLocalizedString(@"Nepodařilo se připojit k serveru", nil) onView:view];
}


+ (void) showError: (NSError *) error messageOnView: (UIView *) view{
    if ([DataService getNetworkErrorTypeFor:error] == NetworkErrorTypeConnection){
        [DataService showNetworkUnavailableMessageOnView:view];
    }else{
        [DataService showServerErrorMessageOnView:view];
    }
}

+(NetworkErrorType)getNetworkErrorTypeFor:(NSError *)error{
    switch (error.code){
        case NSURLErrorInternationalRoamingOff:
        case NSURLErrorTimedOut:
        case NSURLErrorNetworkConnectionLost:
        case NSURLErrorCannotFindHost:
        case NSURLErrorCannotConnectToHost:
        case NSURLErrorDNSLookupFailed:
            
        case NSURLErrorDataNotAllowed:
        case NSURLErrorNotConnectedToInternet:
            
            return NetworkErrorTypeConnection; 
            break;
            
        default:
            return NetworkErrorTypeServer;
            
    }
}


+ (void) showErrorMessageWithTitle: (NSString *) title subtitle: (NSString *) subtitle onView: (UIView *) view{
    [[ToastService sharedService] toastTitle:title subtitle:subtitle image:nil inView:nil autohide:YES];
}

- (Reachability *) reachability{
    if (!_reachability){
        _reachability = [Reachability reachabilityWithHostname:[RKObjectManager sharedManager].baseURL.host];
    }
    
    return _reachability;
}

- (void) startCheckingForReachability: (void(^)())onReachable{
    self.reachability.reachableBlock = ^(Reachability*reach)
    {
        onReachable();
    };

    [self.reachability startNotifier];
}

- (void) stopCheckingForReachability{
    [self.reachability stopNotifier];
}

#warning Testing markers - delete later

- (void) createDummyMarkers{
    [self createDummyMarkerWithId:[NSNumber numberWithInt:0] nodeId:[NSNumber numberWithInt:655] angle:[NSNumber numberWithInt:-45] type:@"floor"];
    [self createDummyMarkerWithId:[NSNumber numberWithInt:1] nodeId:[NSNumber numberWithInt:678] angle:[NSNumber numberWithInt:0] type:@"wall"];
    [self createDummyMarkerWithId:[NSNumber numberWithInt:2] nodeId:[NSNumber numberWithInt:37] angle:[NSNumber numberWithInt:-45] type:@"floor"];
    [self createDummyMarkerWithId:[NSNumber numberWithInt:3] nodeId:[NSNumber numberWithInt:75] angle:[NSNumber numberWithInt:45] type:@"floor"];
    [self createDummyMarkerWithId:[NSNumber numberWithInt:4] nodeId:[NSNumber numberWithInt:132] angle:[NSNumber numberWithInt:45] type:@"wall"];
    [self.context saveToPersistentStore:nil];
}

- (void) createDummyMarkerWithId: (NSNumber *) markerId nodeId: (NSNumber *) nodeId angle: (NSNumber *) angle type: (NSString *) type{
    Marker * marker = [NSEntityDescription
                        insertNewObjectForEntityForName:@"Marker"
                        inManagedObjectContext:self.context];
    //655
    marker.angle = angle;
    marker.type = type;
    marker.id = markerId;
    
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"id=%@", nodeId];
    NSFetchRequest * request = [[NSFetchRequest alloc] initWithEntityName:@"Node"];
    request.predicate = predicate;
    NSArray * nodes = [self.context executeFetchRequest:request error:nil];
    if ([nodes count]){
        Node * node = [nodes objectAtIndex:0];
        node.marker = marker;
    }
    
    
    
}


@end
