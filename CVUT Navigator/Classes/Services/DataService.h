//
//  DataService.h
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 18.02.13.
//  Copyright (c) 2013 Tomáš Kohout. All rights reserved.
//

#import <Foundation/Foundation.h>
@class RKObjectRequestOperation;
@class WeekInfo;
@class Node, Building, Plan;
@class User, Person, Room;


typedef NS_ENUM(NSInteger, APIError){
    APIErrorBadRequest = 2000,
    APIErrorUnauthorized = 2001,
    APIErrorNotFound = 2004,
};

typedef NS_ENUM(NSInteger, NetworkErrorType){
    NetworkErrorTypeServer = 3000,
    NetworkErrorTypeConnection = 30001
};


@protocol DataServiceUserDelegate <NSObject>

- (void)userUnauthorized;

@end


@interface DataService : NSObject
+ (DataService  *) sharedService;
- (void)initializeMapping;
- (void) initializeUser;

@property (nonatomic,retain) User * user;
@property (nonatomic, retain) id<DataServiceUserDelegate> delegate;

- (NSManagedObjectContext *) context;

/*Week Info*/
- (void) getWeekInfoSuccess: (void (^)(WeekInfo *)) onSuccess failure:(void (^)(NSError *error))onFailure;
- (NSDate *) getWeekStart: (NSInteger) week;
- (NSDate *) getWeekEnd: (NSInteger) week;

/*Timetable*/
- (void) getTimetableSuccess: (void (^)(NSArray *)) onSuccess failure:(void (^)(NSError *))onFailure;
- (void) getEntriesForWeek: (NSInteger) week parity: (NSInteger) parity success: (void (^)( NSArray *)) onSuccess failure:(void (^)(NSError *error))onFailure;
- (void)getEntriesForWeekGroupedByDate:(NSInteger) week parity: (NSInteger) parity success:(void (^)(NSDictionary *))onSuccess failure:(void (^)(NSError *))onFailure;
- (NSArray *) getEntriesForDate: (NSDate *) date;


/*Search*/
- (void) searchPlacesQuery: (NSString *) query success: (void (^)(NSArray *)) onSuccess;
- (void) searchAllQuery: (NSString *) query success: (void (^)(NSArray *)) onSuccess;

/* POIs*/
- (void) getBuildingsSuccess: (void (^)(NSArray *)) onSuccess failure:(void (^)(NSError *error))onFailure;
- (void) getBuildingForNode: (Node *) node success: (void(^)(Building * building)) onSuccess failure:(void (^)(NSError *error))onFailure;

/*Plan*/
- (void) getPlanForBuilding: (Building * ) building success: (void (^)(Plan * plan)) onSuccess failure:(void (^)(NSError *error))onFailure;

/*Person*/
- (void) getRoomForPerson: (Person *) person success: (void (^)(Room * room)) onSuccess failure:(void (^)(NSError *error))onFailure;

/*Routing*/
- (void) findRouteStart: (Node *) start end: (Node *) end success: (void (^)(NSArray *)) onSuccess failure:(void (^)(NSError *error))onFailure;

/*Augmented Reality*/
- (void) getNodeForMarkerId: (NSInteger) markerId success: (void(^)(Node *)) onSuccess failure:(void (^)(NSError *error))onFailure;
- (Node *) getNodeForMarkerIdFromDatabase: (NSInteger) markerId;

/*User*/
- (void) loginWithUsername: (NSString *) username password: (NSString *) password onSuccess: (void(^)(User *)) onSuccess failure:(void (^)(NSError *error))onFailure;
- (void) logout;

/*Network*/
+ (void) showErrorMessageWithTitle: (NSString *) title subtitle: (NSString *) subtitle onView: (UIView *) view;
+ (void) showError: (NSError *) error messageOnView: (UIView *) view;

- (NSError *) errorWithCode: (NSInteger) code message: (NSString *) message;

+ (NetworkErrorType) getNetworkErrorTypeFor: (NSError *) error;

- (void) startCheckingForReachability: (void(^)())onReachable;
- (void) stopCheckingForReachability;


#warning Just testing
- (void) createDummyMarkers;

@end
