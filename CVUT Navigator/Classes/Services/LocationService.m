//
//  LocationService.m
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 13.03.13.
//  Copyright (c) 2013 Tomáš Kohout. All rights reserved.
//

#import "LocationService.h"
#import <CoreLocation/CoreLocation.h>
#import "Node.h"


#define INDOOR_LOCATION_LASTS_WITH_EXTRA_HIGH_ACCURACY 30
#define INDOOR_LOCATION_LASTS_WITH_HIGH_ACCURACY 2*60
#define INDOOR_LOCATION_LASTS_WITH_LOW_ACCURACY 5*60

@interface LocationService()<CLLocationManagerDelegate>
@property (nonatomic, retain) CLLocationManager * locationManager;
@property (nonatomic, assign) NSTimeInterval indoorLocationLastUpdated;
@end



@implementation LocationService

+ (LocationService *) sharedService{
    DEFINE_SHARED_INSTANCE_USING_BLOCK(^{
        return [[self alloc] init];
    });
}

- (id)init{
    self = [super init];
    
    if (self){
        self.locationManager = [[CLLocationManager alloc] init];
        self.locationManager.delegate = self;
        self.locationManager.distanceFilter = kCLDistanceFilterNone; // whenever we move
        self.locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters; // 100 m
    }
    
    return self;
}

- (void)startUpdatingLocation{
    [self.locationManager startUpdatingLocation];
}

- (void)stopUpdatingLocation{
    [self.locationManager stopUpdatingLocation];
}


- (CLAuthorizationStatus) authorizationStatus{
    return [CLLocationManager authorizationStatus];
}

- (void)setCurrentLocation:(CLLocation *)currentLocation{
    _currentLocation = currentLocation;
    
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"CurrentLocationNotification"
     object:self userInfo:@{@"currentLocation": self.currentLocation, @"type": [NSNumber numberWithInteger:self.currentLocationType]}];
}

-(void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation{
    self.currentOutdoorLocation = newLocation;
    
    if (!self.indoorLocationLastUpdated || (newLocation.horizontalAccuracy <= 5.0 && self.indoorLocationLastUpdated < [[NSDate date] timeIntervalSince1970] - INDOOR_LOCATION_LASTS_WITH_EXTRA_HIGH_ACCURACY) ||
        (newLocation.horizontalAccuracy <= 50.0 && self.indoorLocationLastUpdated < [[NSDate date] timeIntervalSince1970] - INDOOR_LOCATION_LASTS_WITH_HIGH_ACCURACY) ||
        (self.indoorLocationLastUpdated <= [[NSDate date] timeIntervalSince1970] - INDOOR_LOCATION_LASTS_WITH_LOW_ACCURACY)
        ){
        self.indoorLocationLastUpdated = 0;
        self.currentLocation = newLocation;
        self.currentLocationType = LocationServiceCurrentLocationTypeOutdoor;
    }
    
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"CurrentOutdoorLocationNotification"
     object:self userInfo:@{@"currentOutdoorLocation": self.currentOutdoorLocation}];
}

- (void)setCurrentIndoorLocationNode:(Node *)currentIndoorLocationNode{
    
    if (currentIndoorLocationNode){
        self.currentLocation = [[CLLocation alloc] initWithLatitude:[currentIndoorLocationNode.latitude doubleValue] longitude:[currentIndoorLocationNode.longitude doubleValue]];
        self.indoorLocationLastUpdated = [[NSDate date] timeIntervalSince1970];
        self.currentLocationType = LocationServiceCurrentLocationTypeIndoor;
    }
    
    if (_currentIndoorLocationNode && [currentIndoorLocationNode.id isEqualToNumber: _currentIndoorLocationNode.id]){
        return;
    }
    
    
    Node * oldNode = _currentIndoorLocationNode;
    _currentIndoorLocationNode = currentIndoorLocationNode;
    
    
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"CurrentIndoorLocationNotification"
     object:self userInfo:@{@"currentIndoorLocationNode": currentIndoorLocationNode, @"oldIndoorLocationNode": oldNode ? oldNode : [NSNull null]}];
    
}

@end
