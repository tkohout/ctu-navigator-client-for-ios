//
//  StaticVars.h
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 25.11.13.
//  Copyright (c) 2013 Tomáš Kohout. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StaticVars : NSObject
+ (StaticVars  *) sharedService;

@property (nonatomic, readonly, retain) UIColor * tintColor;
@property (nonatomic, readonly, retain) UIColor * lineColor;
@property (nonatomic, readonly, retain) UIColor * backgroundColor;


@end
