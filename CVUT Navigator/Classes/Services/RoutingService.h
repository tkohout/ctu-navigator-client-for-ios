//
//  RoutingService.h
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 08.03.13.
//  Copyright (c) 2013 Tomáš Kohout. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RoutePoint.h"

typedef NS_ENUM(NSInteger, RoutePathNodeType){
    RoutePathNodeTypeFirst,
    RoutePathNodeTypeMiddle,
    RoutePathNodeTypeLast,
    RoutePathNodeTypeOnlyOne
};

typedef NS_ENUM(NSInteger, RouteStepType){
    RouteStepTypeIndoor,
    RouteStepTypeOutdoor
};

@class Node, Building;

@interface RoutingService : NSObject

@property (nonatomic,retain) RoutePoint * start;
@property (nonatomic,retain) RoutePoint * end;
@property (nonatomic, assign) NSInteger currentStep;

+ (RoutingService *) sharedService;
- (void) routeStart: (RoutePoint *) start end: (RoutePoint *) end success:(void (^)())onSuccess failure:(void (^)(NSError *))onFailure;
- (BOOL) isRouting;
- (void) routeCancel;

- (BOOL) isLoadedRoutePathForBuilding: (Building *) building;
- (BOOL) isRoutePathCompletelyLoaded;

- (NSArray *) indoorRoutePathForBuilding: (NSInteger) buildingId;
- (BOOL) getDirectionInfoForIndoorNode: (Node *) currentLocationNode info: (void(^)(RoutePathNodeType type, int turnAngle, int nodeAngle, Node * nextNode, double distance)) info;
- (NSString *) getDirectionTextForIndoorNode: (Node *) node;

/*Steps*/
- (NSString *) getDirectionForStep: (NSInteger) step;
- (NSInteger) stepCount;
- (Node *) getNodeForStep: (NSInteger) step;
- (NSInteger) getStepForNode: (Node *) node addToStepsWhenMissing: (BOOL) addWhenMissing;
- (BOOL) isStepOutdoor: (NSInteger) step;


- (BOOL) isNodeStairsOrElevatorInRoutePath: (Node *) node;
- (RoutePathNodeType) getRoutePathTypeForNode: (Node *) node;
- (Node *) getNextNodeInRoutePathForNode: (Node *) node;
- (Node *) getPreviousNodeInRoutePathForNode: (Node *) node;
- (BOOL) isNodeInRoutePath: (Node *) node;
- (int) getPreviousHeadingInRoutePathForNode: (Node *) node;
- (int) getNextHeadingInRoutePathForNode: (Node *) node;
- (int) getTurnHeadingInRoutePathForNode: (Node *) node;
- (int) getDistanceInRoutePathFrom: (Node *) nodeFrom to: (Node * ) nodeTo;


@end
