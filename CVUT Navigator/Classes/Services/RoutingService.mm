//
//  RoutingService.m
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 08.03.13.
//  Copyright (c) 2013 Tomáš Kohout. All rights reserved.
//

#import "RoutingService.h"
#import "RoutePoint.h"
#import "DataService.h"
#import "Node.h"
#import "Building.h"
#import "Plan.h"
#import "Floor.h"
#import "Object3DWrapper.h"
#import "UtilitiesGeo.h"
@interface RoutingService()
@property (nonatomic, retain) NSMutableDictionary * indoorRoutePathNodes;

@property (nonatomic, retain) NSArray * routeSteps;

@property (nonatomic, assign) NSInteger cachedRoutePathIndex;
@property (nonatomic, assign) NSInteger cachedRoutePathNodeId;


@end

@implementation RoutingService

+ (RoutingService *) sharedService{
    DEFINE_SHARED_INSTANCE_USING_BLOCK(^{
        RoutingService * instance = [[self alloc] init];
        [instance clearCache];
        return instance;
    });
}

- (void) clearCache{
    _cachedRoutePathNodeId = -1;
    _cachedRoutePathIndex = -1;
}

- (BOOL) isRouting{
    return self.start && self.end;
}

- (NSMutableDictionary *)indoorRoutePathNodes{
    if (!_indoorRoutePathNodes){
        _indoorRoutePathNodes = [[NSMutableDictionary alloc] init];
    }
    
    return _indoorRoutePathNodes;
}


- (void) routeStart: (RoutePoint *) start end: (RoutePoint *) end success:(void (^)())onSuccess failure:(void (^)(NSError *))onFailure{
    
    void (^onComplete)() = ^() {
        self.start = start;
        self.end = end;
        self.currentStep = 0;
        [self routePathCompletelyLoaded];
    };
    
    if ([start isNode] && [end isNode]){
        
        //Routing just inside the building
        if ([start isInSameBuildingAsRoutePoint:end]){
            [self loadSingleSegmentStart: start.item end:end.item success:^{
                onComplete();
                onSuccess();
                
            } failure:onFailure];
            
        //Routing in two different buildings
        }else{
            [self loadFirstSegmentStart:start.item success:^{
                [self loadLastSegmentEnd:end.item clear:NO success:^{
                    onComplete();
                    onSuccess();
                    
                } failure:onFailure];
            } failure:onFailure];
        }
    
    //Routing only in the first building
    }else if ([start isNode]){
        [self loadFirstSegmentStart:start.item success:^{
            onComplete();
            onSuccess();
        } failure:onFailure];
    
    //Routing only in the second building
    }else if ([end isNode]){
        [self loadLastSegmentEnd:end.item clear:YES success:^{
            onComplete();
            onSuccess();
        } failure:onFailure];
    //Routing is completely outside of the building, we don't have to load anything
    }else{
        onComplete();
        onSuccess();
    }
    
}
#pragma mark Directions

- (NSInteger) stepCount{
    return [self.routeSteps count];
}

- (Node *) getNodeForStep: (NSInteger) step{
    if (step >= [self stepCount]){
        return nil;
    }
    
    NSDictionary * stepDictionary = [self.routeSteps objectAtIndex:step];
    Node * node = [stepDictionary objectForKey:@"node"];
    
    if (node == (id)[NSNull null]){
        return nil;
    }
    
    return node;
}

- (NSInteger) getStepForNode: (Node *) node addToStepsWhenMissing: (BOOL) addWhenMissing{
    
    Node * stepNode;
    
    if ([self stepCount] == 0) return NSNotFound;
    
    if ([self isNodeInRoutePath:node]){
        for (int step =0; step<self.stepCount; step++){
            stepNode = [self getNodeForStep:step];
            if ([stepNode.id isEqualToNumber:node.id]){
                return step;
            }
        }
        
        Node * prevNode = [self getPreviousNodeInRoutePathForNode:node];
        
        //It was skipped, we will select the one before
        NSInteger prevStep = [self getStepForNode: prevNode addToStepsWhenMissing:NO];
        
        if (addWhenMissing){
            NSMutableArray * routeSteps = [[NSMutableArray alloc] init];
            
            NSInteger step = 0;
            
            for (NSDictionary * stepDict in self.routeSteps){
                [routeSteps addObject:stepDict];
                if (step == prevStep){
                    [routeSteps addObject:@{@"node":node, @"directions": [self getDirectionTextForIndoorNode:node], @"type": [NSNumber numberWithInt: RouteStepTypeIndoor]}];
                }
                step++;
            }
            
            self.routeSteps = routeSteps;
            
            return prevStep+1;
        }else{
            return prevStep;
        }
    }
    
    return NSNotFound;
}

- (void) initializeRouteSteps{
    NSMutableArray * routeSteps = [[NSMutableArray alloc] init];
    
    
    if ([self.start isNode] || (self.start.isCurrentLocation && self.start.building)){
        NSArray * startBuildingNodes = [self indoorRoutePathForBuilding:[self.start.building.id integerValue]];
        Node * node = [startBuildingNodes objectAtIndex:0];
        if ([startBuildingNodes count] > 0){
            while (node){
                [routeSteps addObject:@{@"node":node, @"directions": [self getDirectionTextForIndoorNode:node], @"type": [NSNumber numberWithInt: RouteStepTypeIndoor]}];
                node = [self getNextNodeInRoutePathForNode:node];
            }
            
        }
        
    }
    
    if (![self.start isInSameBuildingAsRoutePoint:self.end]){
        NSString * directions = [NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"Pokračujte po zvýrazněné trase do ", nil), (self.end.building) ? self.end.building.name : self.end.name];
        [routeSteps addObject: @{@"node": [NSNull null],  @"directions": directions, @"type": [NSNumber numberWithInt: RouteStepTypeOutdoor]}];
    }
    
    if ([self.end isNode] && ![self.start isInSameBuildingAsRoutePoint:self.end]){
        NSArray * endBuildingNodes = [self indoorRoutePathForBuilding:[self.end.building.id integerValue]];
        
        if ([endBuildingNodes count] > 0){
            Node * node = [endBuildingNodes objectAtIndex:0];
            
            while (node){
                [routeSteps addObject:@{@"node":node, @"directions": [self getDirectionTextForIndoorNode:node], @"type": [NSNumber numberWithInt: RouteStepTypeIndoor]}];
                node = [self getNextNodeInRoutePathForNode:node];
            }
        }
        
    }
    
    self.routeSteps = routeSteps;
}

- (BOOL) isStepOutdoor: (NSInteger) step{
    if (step >= [self stepCount]){
        return NO;
    }
    
    NSDictionary * stepDictionary = [self.routeSteps objectAtIndex:step];
    NSNumber * type = [stepDictionary objectForKey:@"type"];
    
    return ([type integerValue] == RouteStepTypeOutdoor);
}


- (NSString *) getDirectionForStep: (NSInteger) step{
    if (step >= [self stepCount]){
        return @"";
    }
    
    NSDictionary * stepDictionary = [self.routeSteps objectAtIndex:step];
    return [stepDictionary objectForKey:@"directions"];
}

- (BOOL) isRoutePathCompletelyLoaded{
    if ([self.start isNode]){
        if ([self indoorRoutePathForBuilding:[self.start.building.id integerValue]] == nil){
            return NO;
        }
    }
    
    if ([self.end isNode] && ![self.start isInSameBuildingAsRoutePoint:self.end]){
        if ([self indoorRoutePathForBuilding:[self.end.building.id integerValue]] == nil){
            return NO;
        }
    }
    
    
    return YES;
}

- (NSString *) getDirectionTextForIndoorNode: (Node *) node{
    if (self.isRouting){
        
        
        
        NSString * direction = @"";
        if ([self isLoadedRoutePathForBuilding:node.building]){
            Node * nextNode = [self getNextNodeInRoutePathForNode:node];
            RoutePathNodeType type = [self getRoutePathTypeForNode:node];
            int turnHeading = [self getTurnHeadingInRoutePathForNode:node];
            
            int distance = 0;
            if (nextNode){
                distance = round([self getDistanceInRoutePathFrom:node to:nextNode]);
            }
            NSString * distanceString = [NSString stringWithFormat:@"%i\u00A0m", distance];
            
            if ([self isNodeStairsOrElevatorInRoutePath:node]){
                if ([node.type isEqualToString: @"elevator"]){
                    direction = [NSString stringWithFormat: NSLocalizedString(@"Vyjeďte výtahem do %@", nil), nextNode.floorName];
                }else{
                    direction = [NSString stringWithFormat: NSLocalizedString(@"Jděte po schodech do %@", nil), nextNode.floorName];
                }
                
            }else if (type == RoutePathNodeTypeFirst){
                direction = [NSString stringWithFormat: NSLocalizedString(@"Pokračujte rovně %@", nil), distanceString];
            }else if (type == RoutePathNodeTypeMiddle){
                
                if (turnHeading >= - 45.0f && turnHeading <= 45.0f){
                    direction = [NSString stringWithFormat: NSLocalizedString(@"Pokračujte rovně %@", nil), distanceString];
                }else if (turnHeading > 45.0f && turnHeading < 170.0f ){
                    direction = [NSString stringWithFormat: NSLocalizedString(@"Zahněte doprava a pokračujte %@", nil), distanceString];
                }else if (turnHeading < -45.0f && turnHeading > -170.0f ){
                    direction = [NSString stringWithFormat: NSLocalizedString(@"Zahněte doleva a pokračujte %@", nil), distanceString];
                }else{
                    direction = [NSString stringWithFormat: NSLocalizedString(@"Pokračujte opačným směrem %@", nil), distanceString];
                }
            }else{
                direction = NSLocalizedString(@"Dorazili jste do cíle", nil);
            }
        }
        return direction;
    }
    
    return @"";
}

#pragma mark Route path info
- (BOOL) isLoadedRoutePathForBuilding: (Building *) building{
    if (([self indoorRoutePathForBuilding:[building.id integerValue]])){
        return YES;
    }
    
    return NO;
}


- (BOOL) isNodeStairsOrElevatorInRoutePath: (Node *) node{
    Node * nextNode = [self getNextNodeInRoutePathForNode:node];
    if (nextNode){
        return (![node isFromSameFloorAsNode: nextNode]);
    }
    
    return NO;
}


- (RoutePathNodeType) getRoutePathTypeForNode: (Node *) node{
    NSInteger index = [self getIndexInRoutePathForNode:node];
    NSArray * routePath = [self indoorRoutePathForBuilding:[node.building.id integerValue]];
    
    if ([routePath count] == 1){
        return RoutePathNodeTypeOnlyOne;
    }else if (index == [routePath count]-1 ){
        return RoutePathNodeTypeLast;
    }else if (index == 0 ){
        return RoutePathNodeTypeFirst;
    }else{
        return RoutePathNodeTypeMiddle;
    }
}

/*- (Node *) getPreviousNodeInRoutePathForNode: (Node *) node{
 NSInteger index = [self getIndexInRoutePathForNode:node];
 
 NSArray * routePath = [self indoorRoutePathForBuilding:[node.building.id integerValue]];
 if (index != -1 && index > 0){
 return [routePath objectAtIndex:index-1];
 }
 return nil;
 }*/

- (Node *) getNextNodeInRoutePathForNode: (Node *) node{
    NSInteger index = [self getIndexInRoutePathForNode:node];
    if (index == -1) return nil;
    
    NSArray * routePath = [self indoorRoutePathForBuilding:[node.building.id integerValue]];
    
    Node * nextNode = nil;
    
    //Find next node, if there is no turn, skip it until there is some turn. If there is elevator or stairs, skip the step between floors
    for (int i = index+1; i < [routePath count]; i++){
        nextNode = [routePath objectAtIndex:i];
        
        //Last node
        if (i == [routePath count]-1){
            break;
        }
        
        //If next node is not intersection, we don't want to skip it
        if (![nextNode.type isEqualToString: @"intersection"]){
            //Unless it is the elevator or stairs and have other stairs on the floor we are going, then we skip the next one
            if ((![node isFromSameFloorAsNode: nextNode]) && ([nextNode.type isEqualToString:@"elevator"] || [nextNode.type isEqualToString:@"stairs"])){
                continue;
            }
            
            break;
        }
        
        int prevHeading = [self getHeadingBetweenFirstNode:node secondNode:nextNode];
        int nextHeading = [self getHeadingBetweenFirstNode:nextNode secondNode:[routePath objectAtIndex:i+1]];
        
        int turnHeading = differenceBetweenDegrees(prevHeading, nextHeading);
        //If we don't continue straight, and it is intersection we don't want to skip it
        if (turnHeading > 20 || turnHeading < -20){
            break;
        }
        
        //If we continue straight and it is intersection, we will skip it
    }
    
    return nextNode;
}

- (Node *) getPreviousNodeInRoutePathForNode: (Node *) node{
    NSInteger index = [self getIndexInRoutePathForNode:node];
    if (index == -1) return nil;
    
    NSArray * routePath = [self indoorRoutePathForBuilding:[node.building.id integerValue]];
    
    Node * prevNode = nil;
    
    //Find prev node, if there is no turn, skip it until there is some turn. If there is elevator or stairs, skip the step between floors
    for (int i = index-1; i >= 0; i--){
        prevNode = [routePath objectAtIndex:i];
        
        //One before first node
        if (i == 0){
            break;
        }
        
        //If next node is not intersection, we don't want to skip it
        if (![prevNode.type isEqualToString: @"intersection"]){
            //Unless it is the elevator or stairs and have other stairs on the floor we are going, then we skip the next one
            if ((![node isFromSameFloorAsNode: prevNode]) && ([node.type isEqualToString:@"elevator"] || [node.type isEqualToString:@"stairs"])){
                continue;
            }
            
            break;
        }
        
        int prevHeading = [self getHeadingBetweenFirstNode:[routePath objectAtIndex:i-1] secondNode:prevNode];
        int nextHeading = [self getHeadingBetweenFirstNode:prevNode secondNode:node];
        
        int turnHeading = differenceBetweenDegrees(prevHeading, nextHeading);
        //If we don't continue straight, and it is intersection we don't want to skip it
        if (turnHeading > 20 || turnHeading < -20){
            break;
        }
        
        //If we continue straight and it is intersection, we will skip it
    }
    
    return prevNode;
}

- (BOOL) isNodeInRoutePath: (Node *) node{
    return ([self getIndexInRoutePathForNode:node] != -1);
}

- (int) getPreviousHeadingInRoutePathForNode: (Node *) node{
    Node * prevNode = [self getPreviousNodeInRoutePathForNode:node];
    return [self getHeadingBetweenFirstNode: prevNode secondNode:node];
}

- (int) getNextHeadingInRoutePathForNode: (Node *) node{
    Node * nextNode = [self getNextNodeInRoutePathForNode:node];
    return [self getHeadingBetweenFirstNode: node secondNode:nextNode];
}

- (int) getHeadingBetweenFirstNode: (Node *) firstNode secondNode: (Node *) secondNode{
    if (!firstNode || !secondNode){
        return 0;
    }
    return (int)headingInDegrees([firstNode.latitude doubleValue], [firstNode.longitude doubleValue], [secondNode.latitude doubleValue], [secondNode.longitude doubleValue]);
}

- (int) getTurnHeadingInRoutePathForNode: (Node *) node{
    int prevHeading = [self getPreviousHeadingInRoutePathForNode:node];
    int nextHeading = [self getNextHeadingInRoutePathForNode:node];
    
    return differenceBetweenDegrees(prevHeading, nextHeading);
}

- (int) getDistanceInRoutePathFrom: (Node *) nodeFrom to: (Node * ) nodeTo{
    return distanceInMetersFromDegrees([nodeFrom.latitude doubleValue], [nodeTo.latitude doubleValue], [nodeFrom.longitude doubleValue], [nodeTo.longitude doubleValue]);
}

- (NSInteger) getIndexInRoutePathForNode: (Node *) node{
    //Little cache so we don't have to go through array all the time

    
    if ([node.id integerValue] == self.cachedRoutePathNodeId){
        return self.cachedRoutePathIndex;
    }
    
    NSArray * routePath = [self indoorRoutePathForBuilding:[node.building.id integerValue]];
    NSInteger i = 0;
    for (Node * searchedNode in routePath){
        if ([node.id isEqualToNumber:searchedNode.id]){
            self.cachedRoutePathIndex = i;
            self.cachedRoutePathNodeId = [node.id integerValue];
            return i;
        }
        i++;
    }
    
    return -1;
}



#pragma mark Loading route paths

- (void) loadSegmentFrom:(Node *) from to: (Node *) to clear: (BOOL) clear success: (void (^)(NSArray * nodes))onSuccess failure:(void (^)(NSError *))onFailure{
    
    [[DataService sharedService] findRouteStart:from end:to success:^(NSArray * nodes) {
        if (clear){
            [self clearIndoorRoutePath];
        }
        [self.indoorRoutePathNodes setObject:nodes forKey:from.buildingId];
        
        onSuccess(nodes);
        
        
        
    } failure:onFailure];
}

- (void) loadSingleSegmentStart: (Node *) start end: (Node *) end success:(void (^)())onSuccess failure:(void (^)(NSError *))onFailure
{
    [[DataService sharedService] getBuildingForNode: start success:^(Building *building) {
        [[DataService sharedService] getPlanForBuilding:building success:^(Plan *plan) {
            [self loadSegmentFrom:start to:end clear:YES success:^(NSArray *nodes) {
                onSuccess();
            } failure:onFailure];
        }failure:onFailure];
    }failure:onFailure];
    
}

- (void) loadFirstSegmentStart: (Node *) start success:(void (^)())onSuccess failure:(void (^)(NSError *))onFailure{
    
    [[DataService sharedService] getBuildingForNode:start success:^(Building *building) {
        [[DataService sharedService] getPlanForBuilding:building success:^(Plan *plan) {
            //First have to find exit from the building
            NSArray * exits = plan.entrances;
            Node * end;
            
            if (exits){
                end = [exits objectAtIndex:0];
            }else{
                onFailure([[DataService sharedService] errorWithCode:APIErrorNotFound message:@"Not found"]);
            }
            
            [self loadSegmentFrom:start to:end clear:YES success:^(NSArray *nodes) {
                onSuccess();
            } failure:onFailure];
        } failure:onFailure];
    } failure:onFailure];
    
}

- (void) loadLastSegmentEnd: (Node *) end clear: (BOOL) clear success:(void (^)())onSuccess failure:(void (^)(NSError *))onFailure{
    
    [[DataService sharedService] getBuildingForNode:end success:^(Building *building) {
        [[DataService sharedService] getPlanForBuilding:building success:^(Plan *plan) {
            //Have to find entrance to the building
            NSArray * entrances = plan.entrances;
            Node * start;
            
            if (entrances){
                start = [entrances objectAtIndex:0];
            }else{
                onFailure([[DataService sharedService] errorWithCode:APIErrorNotFound message:@"Not found"]);
            }
            
            [self loadSegmentFrom:start to:end clear: clear success:^(NSArray *nodes) {
                onSuccess();
            } failure:onFailure];
        } failure:onFailure];
    } failure:onFailure];
    
}

- (void) routePathCompletelyLoaded{
    [self initializeRouteSteps];
}

- (void) clearIndoorRoutePath{
    [self.indoorRoutePathNodes removeAllObjects];
    [self clearCache];
}

- (NSArray *) indoorRoutePathForBuilding: (NSInteger) buildingId{
    if (self.isRouting){
        return [self.indoorRoutePathNodes objectForKey:[NSNumber numberWithInteger:buildingId]];
    }
    
    return nil;
}


- (void) routeCancel{
    self.start = nil;
    self.end = nil;
}
@end
