//
//  LocationService.h
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 13.03.13.
//  Copyright (c) 2013 Tomáš Kohout. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
@class Node;

typedef NS_ENUM(NSInteger, LocationServiceCurrentLocationType){
    LocationServiceCurrentLocationTypeOutdoor,
    LocationServiceCurrentLocationTypeIndoor
};

@interface LocationService : NSObject
@property (nonatomic, retain) CLLocation * currentOutdoorLocation;
@property (nonatomic, retain) CLLocation * currentLocation;
@property (nonatomic, assign) LocationServiceCurrentLocationType currentLocationType;

@property (nonatomic, retain) Node * currentIndoorLocationNode;
@property (nonatomic, assign, readonly) CLAuthorizationStatus authorizationStatus;

+ (LocationService *) sharedService;
- (void) startUpdatingLocation;
- (void) stopUpdatingLocation;

@end
