//
//  IndoorLocationManager.m
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 03.04.13.
//  Copyright (c) 2013 Tomáš Kohout. All rights reserved.
//

#import "IndoorLocationManager.h"
#import "LocationService.h"
#import "Node.h"
#import "Building.h"
#import "Floor.h"
#import "RMZoomConverter.h"
#import <CoreLocation/CoreLocation.h>
#import <CoreLocation/CLLocationManager.h>

@interface IndoorLocationManager()
@property (nonatomic, retain) RMZoomConverter * zoomConverter;
@end

@implementation IndoorLocationManager

- initWithZoomConverter: (RMZoomConverter *) zoomConverter{
    self = [super init];
    if (self){
        self.zoomConverter = zoomConverter;
    }
    
    return self;
}

- (void)startUpdatingLocation{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                           selector:@selector(indoorLocationChanged:)
                                               name:@"CurrentIndoorLocationNotification"
                                             object:nil];
    if ([LocationService sharedService].currentIndoorLocationNode){
        [self didUpdateToIndoorLocation:[LocationService sharedService].currentIndoorLocationNode fromIndoorLocation:nil];
    }
}

- (void) indoorLocationChanged: (NSNotification *) notification{
    NSDictionary *userInfo = notification.userInfo;
    
    Node * oldLocationNode = [userInfo objectForKey:@"oldIndoorLocationNode"];
    Node * currentLocationNode = [userInfo objectForKey:@"currentIndoorLocationNode"];
    
    [self didUpdateToIndoorLocation:currentLocationNode fromIndoorLocation:oldLocationNode];
    
}



- (void) didUpdateToIndoorLocation: (Node * ) newLocationNode fromIndoorLocation: (Node *) oldLocationNode{
    CLLocation * oldLocation;
    
    if (oldLocationNode && ![oldLocationNode isKindOfClass:[NSNull class]]){
        CLLocationCoordinate2D oldNormalCoordinate = CLLocationCoordinate2DMake([oldLocationNode.latitude doubleValue], [oldLocationNode.longitude doubleValue]);
        CLLocationCoordinate2D oldCoordinate = (self.zoomConverter) ? [self.zoomConverter convertCoordinate:oldNormalCoordinate] : oldNormalCoordinate;
        
        oldLocation =[[CLLocation alloc] initWithLatitude:oldCoordinate.latitude longitude:oldCoordinate.longitude];
    }else{
        oldLocation = nil;
    }
    
    CLLocation * newLocation;
    
    if (newLocationNode){
        CLLocationCoordinate2D newNormalCoordinate = CLLocationCoordinate2DMake([newLocationNode.latitude doubleValue], [newLocationNode.longitude doubleValue]);
        CLLocationCoordinate2D newCoordinate = (self.zoomConverter) ? [self.zoomConverter convertCoordinate:newNormalCoordinate] : newNormalCoordinate;
        newLocation =[[CLLocation alloc] initWithLatitude:newCoordinate.latitude longitude:newCoordinate.longitude];
    }else{
        newLocation = nil;
    }
    
        
    if ([self.delegate respondsToSelector:@selector(locationManager:didUpdateLocations:)]){
        [self.delegate locationManager:self didUpdateLocations:@[newLocation]];
    }else if ([self.delegate respondsToSelector:@selector(locationManager:didUpdateToLocation:fromLocation:)]){
        [self.delegate locationManager:self didUpdateToLocation:newLocation fromLocation:oldLocation];
    }
}

- (void)stopUpdatingLocation{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"CurrentIndoorLocationNotification" object:nil];
}

- (void)setSelectedFloor:(Floor *)selectedFloor{
    _selectedFloor = selectedFloor;
    
    Node * currentNode = [LocationService sharedService].currentIndoorLocationNode;
    
    if (currentNode){
        if ([currentNode.building.id isEqualToNumber: selectedFloor.plan.building.id] && [currentNode isFromFloor: selectedFloor] ){
            [self didUpdateToIndoorLocation:currentNode fromIndoorLocation:nil];
        }else{
            [self didUpdateToIndoorLocation:nil fromIndoorLocation:currentNode];
        }
    }
}


/*Mock*/
+ (BOOL)locationServicesEnabled __OSX_AVAILABLE_STARTING(__MAC_10_7,__IPHONE_4_0){
    return YES;
}
+ (BOOL)headingAvailable __OSX_AVAILABLE_STARTING(__MAC_10_7,__IPHONE_4_0){
    return NO;
}

+ (BOOL)significantLocationChangeMonitoringAvailable __OSX_AVAILABLE_STARTING(__MAC_10_7,__IPHONE_4_0){
    return NO;
}
+ (BOOL)regionMonitoringAvailable __OSX_AVAILABLE_STARTING(__MAC_10_7,__IPHONE_4_0){
    return NO;
}
+ (BOOL)regionMonitoringEnabled __OSX_AVAILABLE_BUT_DEPRECATED(__MAC_NA, __MAC_NA,__IPHONE_4_0, __IPHONE_6_0){
    return NO;
}
+ (CLAuthorizationStatus)authorizationStatus __OSX_AVAILABLE_STARTING(__MAC_10_7,__IPHONE_4_2){
    return kCLAuthorizationStatusAuthorized;
}

- (void)startUpdatingHeading __OSX_AVAILABLE_STARTING(__MAC_NA,__IPHONE_3_0){}
- (void)stopUpdatingHeading __OSX_AVAILABLE_STARTING(__MAC_NA,__IPHONE_3_0){}


- (void)dismissHeadingCalibrationDisplay __OSX_AVAILABLE_STARTING(__MAC_NA,__IPHONE_3_0){}
- (void)startMonitoringSignificantLocationChanges __OSX_AVAILABLE_STARTING(__MAC_10_7,__IPHONE_4_0){}


- (void)stopMonitoringSignificantLocationChanges __OSX_AVAILABLE_STARTING(__MAC_10_7,__IPHONE_4_0){}
- (void)startMonitoringForRegion:(CLRegion *)region
desiredAccuracy:(CLLocationAccuracy)accuracy __OSX_AVAILABLE_BUT_DEPRECATED(__MAC_NA, __MAC_NA,__IPHONE_4_0, __IPHONE_6_0){}

- (void)stopMonitoringForRegion:(CLRegion *)region __OSX_AVAILABLE_STARTING(__MAC_10_7,__IPHONE_4_0){}
- (void)startMonitoringForRegion:(CLRegion *)region __OSX_AVAILABLE_STARTING(__MAC_TBD,__IPHONE_5_0){}

- (void)allowDeferredLocationUpdatesUntilTraveled:(CLLocationDistance)distance
timeout:(NSTimeInterval)timeout __OSX_AVAILABLE_STARTING(__MAC_NA,__IPHONE_6_0){}

- (void)disallowDeferredLocationUpdates __OSX_AVAILABLE_STARTING(__MAC_NA,__IPHONE_6_0){}
+ (BOOL)deferredLocationUpdatesAvailable __OSX_AVAILABLE_STARTING(__MAC_NA,__IPHONE_6_0){return NO;}



@end
