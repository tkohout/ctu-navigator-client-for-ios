//
//  StaticVars.m
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 25.11.13.
//  Copyright (c) 2013 Tomáš Kohout. All rights reserved.
//

#import "StaticVars.h"

@implementation StaticVars

+ (StaticVars  *) sharedService{
    DEFINE_SHARED_INSTANCE_USING_BLOCK(^{
        return [[self alloc] init];
    });
}

-(UIColor *)tintColor{
    return [UIColor colorWithRed:0.0 green:121.0/255.0 blue:193.0/255.0 alpha:1.0];
}

- (UIColor *)lineColor{
    return [UIColor colorWithRed:227.0/255.0 green:227.0/255.0 blue:227.0/255.0 alpha:1.0];
}

-(UIColor *)backgroundColor{
    return [UIColor colorWithRed:237.0/255.0 green:237.0/255.0 blue:237.0/255.0 alpha:1];
}

@end
