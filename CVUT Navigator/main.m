//
//  main.m
//  CVUT Navigator
//
//  Created by Tomáš Kohout on 23.09.12.
//  Copyright (c) 2012 Tomáš Kohout. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
